<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('organization_name')->nullable();
            $table->string('email')->nullable();
            $table->string('logo')->nullable();
            $table->string('support_time')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->text('about')->nullable();
            $table->string('facebook_uri')->nullable();
            $table->string('instagram_uri')->nullable();
            $table->string('linkedin_uri')->nullable();
            $table->string('youtube_uri')->nullable();
            $table->string('twitter_uri')->nullable();
            $table->string('viber_uri')->nullable();
            $table->string('whatsapp_uri')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}

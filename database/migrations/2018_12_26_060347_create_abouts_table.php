<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner_image')->nullable();
            $table->string('banner_image_title')->nullable();
            $table->text('banner_image_text')->nullable();
            $table->string('about_first_image')->nullable();
            $table->string('about_first_title')->nullable();
            $table->text('about_first_text')->nullable();
            $table->string('about_second_image')->nullable();
            $table->string('about_second_title')->nullable();
            $table->text('about_second_text')->nullable();
            $table->string('about_third_image')->nullable();
            $table->string('about_third_title')->nullable();
            $table->text('about_third_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent_id')->default(0);
            $table->string('name');
            $table->text('description');
            $table->string('featured_image')->nullable();
            $table->tinyInteger('featured')->default(0);
            $table->string('slug');
            $table->string('meta_title', 100)->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_content')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

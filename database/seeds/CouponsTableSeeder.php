<?php

use Illuminate\Database\Seeder;
use App\Coupon;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Coupon::create([
            'coupon_code' => 'DEF123',
            'amount_type' => 'Percentage',
            'amount' => 20,
            'expiry_date' => '2019-01-30',
            'status' => 1

        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\SiteSetting;

class SiteSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sites = new SiteSetting();
        $sites->organization_name = 'Zeal Sale';
        $sites->email = 'contact@onlinezeal.com';
        $sites->logo = 'logo.jpg';
        $sites->support_time = '9:00am - 7:00pm';
        $sites->phone = '(+977) 01 5199625';
        $sites->address = 'Near Prabhu Bank, Tinkune, Kathmandu';
        $sites->about = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';
        $sites->facebook_uri = 'https://www.facebook.com/';
        $sites->instagram_uri = 'https://www.instagram.com/';
        $sites->linkedin_uri = 'https://www.linkedin.com/feed/';
        $sites->twitter_uri = 'https://twitter.com/';
        $sites->youtube_uri = 'https://www.youtube.com/';
        $sites->viber_uri = 'https://www.viber.com/';
        $sites->whatsapp_uri = 'https://www.whatsapp.com/';
        $sites->save();
        
    }
}

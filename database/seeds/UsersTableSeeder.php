<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@onlinezeal.com';
        $user->password = Hash::make('password');
        $user->admin = 1;
        $user->save();
        
        App\UserProfile::create([
        'user_id' => $user->id,
        'avatar' => 'public/backend/images/avatar.png',
        'address' => 'Tinkune, Kathmandu',
        'phone' => '9803961736',
        'about' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, blanditiis.',
        ]);

    }
}

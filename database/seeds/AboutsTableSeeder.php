<?php

use Illuminate\Database\Seeder;
use App\About;

class AboutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $about = new About();
        $about->banner_image = 'banner.jpg';
        $about->banner_image_title = 'About Us';
        $about->banner_image_text = 'Passion may be a friendly or eager interest in or admiration for a proposal, cause, discovery, or activity or love to a feeling of unusual excitement.';
        $about->about_first_image = 'what_we_do.jpg';
        $about->about_first_title = 'What we really do?';
        $about->about_first_text = 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.';
        $about->about_second_image = 'our_vision.jpg';
        $about->about_second_title = 'Our Vision';
        $about->about_second_text = 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.';
        $about->about_third_image = 'history.jpg';
        $about->about_third_title = 'History of Beginning';
        $about->about_third_text = 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.';
        $about->save();
    }
}

<?php

use Illuminate\Support\Facades\Input;
use App\Product;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::get('/product/single', 'ProductController@productsingle')->name('product.single');
Route::get('/product/single/{slug}', 'ProductController@productsingle')->name('product.single.details');

Route::get('/wishlist', 'WishlistController@index')->name('wishlist');

Route::get('/featured-brands', 'FeaturedBrandController@index')->name('featured.brands');

Route::get('/super-deals', 'SuperDealController@index')->name('super.deals');

Route::get('/trending-styles', 'TrendingStyleController@index')->name('trending.styles');

Route::get('/product-category', 'ProductCategoryController@index')->name('product.category');
Route::get('/product-category/{slug}', 'ProductCategoryController@index')->name('product.category.details');

Route::get('/product-subcategory/{slug}', 'ProductCategoryController@subcategory')->name('product.subcategory');

Route::get('/my-account', 'MyAccountController@myaccount')->name('my.account')->middleware('guest:customer');

Route::get('/about-us', 'AboutUsController@index')->name('about.us');

Route::get('/contact-us', 'ContactUsController@index')->name('contact.us');
Route::post('/contact-us/send-mail', 'ContactUsController@sendContactUsMail')->name('contact.us.mail');

Route::get('/faq', 'FAQController@index')->name('faq');

Route::get('/terms-and-conditions', 'TACController@index')->name('tac');

Route::post( '/search', function () {
    $q = Input::get ( 'q' );
    $cat = Input::get('category_id');
    // echo $q."-".$cat; die; 
    if($q != "" && $cat != ""){
        // echo "Hello"; die;
        $products = App\Product::where ( 'product_name', 'LIKE', '%' . $q . '%' )->orWhere ( 'description', 'LIKE', '%' . $q . '%' )->get ();
        $categories = App\Product::where ( 'category_id', 'LIKE', '%' . $cat . '%' )->get ();  
        if (count ( $products ) > 0){
            return view ( 'frontend.search' )->withDetails ( $products )->withQuery ( $q);
        }
        else
            return view ( 'frontend.search' )->withMessage ( 'Can’t find the product you’re looking for? Let us help you!' );
    }
    return view ( 'frontend.search' )->withMessage ( 'Can’t find the product you’re looking for? Let us help you!' );
 } )->name('search');
 
 Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
 });

Route::group(['middleware' => ['auth']], function (){
    Route::get('/admin/dashboard', 'UsersController@dashboard')->name('admin.dashboard');

    //change admin password/email
    Route::match(['get', 'post'],'admin/account-setting', 'UsersController@settings')->name('admin.settings');
    Route::post('/admin/check-pwd', 'UsersController@chkUserPassword');
    Route::post('/admin/check-email', 'UsersController@chkUserEmail');

    //category
    Route::get('admin/category/index', 'backend\CategoryController@index')->name('category.index');
    Route::match(['get', 'post'],'admin/category/create', 'backend\CategoryController@create')->name('category.create');
    Route::match(['get', 'post'],'admin/category/edit/{id}', 'backend\CategoryController@edit')->name('category.edit');
    Route::get('admin/category/delete/{id}', 'backend\CategoryController@delete')->name('category.delete');
    Route::post('/admin/category/featured-category/{id}', 'backend\CategoryController@featuredCategory')->name('category.featured');

    //couponcategory
    Route::get('admin/couponcategory/index', 'CouponCategoryController@index')->name('couponcategory.index');
    Route::match(['get', 'post'],'admin/couponcategory/create', 'CouponCategoryController@create')->name('couponcategory.create');
    Route::match(['get', 'post'],'admin/couponcategory/edit/{id}', 'CouponCategoryController@edit')->name('couponcategory.edit');
    Route::get('admin/couponcategory/delete/{id}', 'CouponCategoryController@delete')->name('couponcategory.delete');



    //Products Route
    Route::get('/admin/product/create', 'backend\ProductController@create')->name('product.create');
    Route::post('/admin/product/store', 'backend\ProductController@store')->name('product.store');
    Route::get('/admin/product/index', 'backend\ProductController@index')->name('product.index');
    Route::get('/admin/product/soft-delete', 'backend\ProductController@softdelete')->name('product.softdelete');
    Route::get('/admin/product/restore/{id}', 'backend\ProductController@restore')->name('product.restore');
    Route::get('/admin/product/force-delete/{id}', 'backend\ProductController@forcedelete')->name('product.forcedelete');
    Route::get('/admin/product/show/{slug}', 'backend\ProductController@show')->name('product.show');
    Route::get('/admin/product/edit/{slug}', 'backend\ProductController@edit')->name('product.edit');
    Route::post('/admin/product/update/{id}', 'backend\ProductController@update')->name('product.update');
    Route::get('/admin/product/destroy/{id}', 'backend\ProductController@destroy')->name('product.destroy');
    Route::post('/admin/product/onsale/{id}','backend\ProductController@onsale')->name('product.onsale');
    Route::get('/admin/product/onsale/remove/{id}','backend\ProductController@removeOnsale')->name('product.onsale.remove');
    Route::post('/admin/product/featured-product/{id}', 'backend\ProductController@featuredProduct')->name('product.featured');


    //product attribute
    Route::match(['post','get'],'admin/product/attribute/{id}', 'backend\ProductController@attribute')->name('product.attribute');
    Route::post('admin/product/attribute/edit/{id}','backend\ProductController@editAttributes')->name('product.attribute.edit');
    Route::get('/admin/delete-attribute/{id}', 'backend\ProductController@deleteAttribute')->name('product.attribute.delete');

    //product specifications
    Route::match(['post','get'],'admin/product/specification/{id}', 'backend\ProductController@specification')->name('product.specification');
    Route::post('admin/product/specification/edit/{id}','backend\ProductController@editSpecifications')->name('product.specification.edit');
    Route::get('/admin/delete-specification/{id}', 'backend\ProductController@deleteSpecification')->name('product.specification.delete');

    //product images
    Route::match(['post','get'],'admin/product/image/{id}', 'backend\ProductController@image')->name('product.image');
    Route::post('admin/product/image/edit/{id}','backend\ProductController@editImage')->name('product.image.edit');
    Route::get('/admin/delete-image/{id}', 'backend\ProductController@deleteImage')->name('product.image.delete');

    // Site Settings Route    
    Route::get('/admin/sitesetting/edit/{slug}', 'backend\SiteSettingController@edit')->name('sitesetting.edit');
    Route::post('/admin/sitesetting/update/{id}', 'backend\SiteSettingController@update')->name('sitesetting.update');

    // About Us Route    
    Route::get('/admin/about/edit/{slug}', 'backend\AboutController@edit')->name('about.edit');
    Route::post('/admin/about/update/{id}', 'backend\AboutController@update')->name('about.update');

    
    //FAQ Route
    Route::get('/admin/faq/create', 'backend\FAQController@create')->name('faq.create');
    Route::post('/admin/faq/store', 'backend\FAQController@store')->name('faq.store');
    Route::get('/admin/faq/index', 'backend\FAQController@index')->name('faq.index');
    Route::get('/admin/faq/show/{slug}', 'backend\FAQController@show')->name('faq.show');
    Route::get('/admin/faq/edit/{slug}', 'backend\FAQController@edit')->name('faq.edit');
    Route::post('/admin/faq/update/{id}', 'backend\FAQController@update')->name('faq.update');
    Route::get('/admin/faq/destroy/{id}', 'backend\FAQController@destroy')->name('faq.destroy');

    //Terms and Conditions Route
    Route::get('/admin/tac/create', 'backend\TACController@create')->name('tac.create');
    Route::post('/admin/tac/store', 'backend\TACController@store')->name('tac.store');
    Route::get('/admin/tac/index', 'backend\TACController@index')->name('tac.index');
    Route::get('/admin/tac/show/{slug}', 'backend\TACController@show')->name('tac.show');
    Route::get('/admin/tac/edit/{slug}', 'backend\TACController@edit')->name('tac.edit');
    Route::post('/admin/tac/update/{id}', 'backend\TACController@update')->name('tac.update');
    Route::get('/admin/tac/destroy/{id}', 'backend\TACController@destroy')->name('tac.destroy');
    
    //Slider Route
    Route::post('/admin/slider/store', 'backend\SliderController@store')->name('slider.store');
    Route::get('/admin/slider/index', 'backend\SliderController@index')->name('slider.index');
    Route::get('/admin/slider/show/{slug}', 'backend\SliderController@show')->name('slider.show');
    Route::post('/admin/slider/update/{id}', 'backend\SliderController@update')->name('slider.update');
    Route::get('/admin/slider/destroy/{id}', 'backend\SliderController@destroy')->name('slider.destroy');

    //Advertisement
    Route::post('/admin/advertisement/store','backend\AdvertisementController@store')->name('advertisement.store');
    Route::get('/admin/advertisement/index','backend\AdvertisementController@index')->name('advertisement.index');
    Route::get('/admin/advertisement/update{id}','backend\AdvertisementController@index')->name('advertisement.update');
    Route::get('/admin/advertisement/destroy/{id}','backend\AdvertisementController@destroy')->name('advertisement.destroy');

    //Brands Route
    Route::post('/admin/brand/store', 'backend\BrandController@store')->name('brand.store');
    Route::get('/admin/brand/index', 'backend\BrandController@index')->name('brand.index');
    Route::get('/admin/brand/show/{slug}', 'backend\BrandController@show')->name('brand.show');
    Route::post('/admin/brand/update/{id}', 'backend\BrandController@update')->name('brand.update');
    Route::get('/admin/brand/destroy/{id}', 'backend\BrandController@destroy')->name('brand.destroy');

    //Coupon route
    Route::get('admin/coupon/index', 'backend\CouponController@index')->name('coupon.index');
    Route::post('admin/coupon/store','backend\CouponController@store')->name('coupon.store');
    Route::post('admin/coupon/update/{id}','backend\CouponController@update')->name('coupon.update');
    Route::get('admin/coupon/delete/{id}', 'backend\CouponController@destroy')->name('coupon.destroy');

    Route::get('admin/coupon/edit/{id}', 'backend\CouponController@editusercoupon')->name('coupon.edit');
    
    //Product Coupon route
    //Route::get('/admin/productcoupon/index','backend\CouponController@index')->name(productcoupon.index);


    //Order Route
    Route::get('/admin/order/index','backend\OrderController@index')->name('order.index');
    Route::match(['get', 'post'], '/admin/edit-order/', 'backend\OrderController@editOrder')->name('order.update');


});
Route::get('/customer/order/cancel-order/{id}', 'backend\OrderController@customerCancelOrder')->name('order.customer.delete');


Auth::routes();

//admin login and logout
Route::match(['get', 'post'],'zeal/login-zeal', 'UsersController@login')->name('admin.login')->middleware('guest');
Route::get('/admin/logout', 'UsersController@logout')->name('admin.logout');

//get product price from size
Route::post('/product/single/get-product-price', 'backend\ProductController@getProductPrice');

//Add to Cart Route
Route::match(['get', 'post'], '/add-cart', 'CartController@addtocart')->name('cart.add');
Route::get('/cart', 'CartController@index')->name('cart');
//Update cart after login
Route::match(['get', 'post'], '/update-cart', 'CartController@updateCart')->name('cart.update');

//Apply Coupon
Route::post('/cart/apply-coupon', 'backend\CouponController@applyCoupon')->name('coupon.apply');
Route::post('/cart/apply-user-coupon', 'backend\CouponController@applyuserCoupon')->name('usercoupon.apply');



//Delete Product from Cart Page
Route::get('/cart/delete-product/{id}', 'CartController@deleteCartProduct')->name('cart.delete');

//Update Product Quantity in Cart
Route::post('/cart/update-quantity/{id}/{quantity}','CartController@updateCartQuantity');

//Checkout Routes
Route::get('/checkout','CheckoutController@checkout')->name('checkout')->middleware('auth:customer');
Route::post('/checkout/store','CheckoutController@store')->name('checkout.store');

// Customer Route
Route::post('/customer/store','CustomerController@store')->name('customer.store');
Route::get('/customer/dashboard','CustomerController@dashboard')->name('customer.dashboard')->middleware('auth:customer');
Route::match(['get', 'post'],'/customer/dashboard/edit/{id}','CustomerController@update')->name('customer.update')->middleware('auth:customer');
Route::post('/customer/check-pwd', 'CustomerController@chkUserPassword');


Route::match(['get', 'post'],'/customer/login','CustomerController@login')->name('customer.login');
Route::get('/customer/logout','CustomerController@logout')->name('customer.logout');

//Order Route
Route::get('/confirm-order','backend\OrderController@confirmOrder')->name('order.confirm')->middleware('auth:customer');

// Wishlist
Route::post('product/single/wishlist/store','backend\WishlistController@store')->name('wishlist.store');
Route::post('product-subcategory/wishlist/save','backend\WishlistController@save')->name('wishlist.save');
Route::get('/wishlist/destroy/{id}','backend\WishlistController@destroy')->name('wishlist.destroy');

// Ratings
Route::post('/rating/store','backend\ProductController@ratingStore')->name('rating.store');


//Password reset routes
Route::post('customer/password/email', 'Auth\CustomerForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
Route::get('customer/password/reset', 'Auth\CustomerForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
Route::post('customer/password/reset', 'Auth\CustomerResetPasswordController@reset')->name('customer.password.update');
Route::get('customer/password/reset/{token}','Auth\CustomerResetPasswordController@showResetForm')->name('customer.password.reset');

//
Route::get('/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');


//Social Account Login
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');






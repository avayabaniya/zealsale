-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2019 at 06:12 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlinezeal_zealsale`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image_text` text COLLATE utf8mb4_unicode_ci,
  `about_first_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_first_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_first_text` text COLLATE utf8mb4_unicode_ci,
  `about_second_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_second_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_second_text` text COLLATE utf8mb4_unicode_ci,
  `about_third_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_third_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_third_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `banner_image`, `banner_image_title`, `banner_image_text`, `about_first_image`, `about_first_title`, `about_first_text`, `about_second_image`, `about_second_title`, `about_second_text`, `about_third_image`, `about_third_title`, `about_third_text`, `created_at`, `updated_at`) VALUES
(1, 'banner.jpg', 'About Us', 'Passion may be a friendly or eager interest in or admiration for a proposal, cause, discovery, or activity or love to a feeling of unusual excitement.', 'what_we_do.jpg', 'What we really do?', 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.', 'our_vision.jpg', 'Our Vision', 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.', 'history.jpg', 'History of Beginning', 'Donec libero dolor, tincidunt id laoreet vitae, ullamcorper eu tortor. Maecenas pellentesque, dui vitae iaculis mattis, tortor nisi faucibus magna, vitae ultrices lacus purus vitae metus.', '2018-12-31 11:04:32', '2018-12-31 11:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `product_name`, `product_code`, `product_color`, `size`, `discount`, `price`, `quantity`, `user_email`, `session_id`, `created_at`, `updated_at`) VALUES
(2, 2, 'Women Brown Ankle Shoes', 'WBAS-001-40', 'Brown', '40', 0, 1900, 1, 'sachin.siwakoti@gmail.com', 'evig48TYqpwZdPyrEBRNNHzu0e78b2MAJe2Kyor3', NULL, NULL),
(5, 12, 'Woman\'s Black Ankle Boot- 011', 'WBAB-011-37', 'Black', '37', 0, 2250, 1, '', 'WLiX9ZWzIg9lXK5bq6Ws2EwWHGnZgHAZyLE5Ibyp', NULL, NULL),
(9, 28, 'Stainless Steel Electric Kettle', 'EK-001', 'Silver and Black', 'Medium', 0, 650, 1, '', 'irmvpClgg9E3oHv0oHKhMBGvMiwCLflKY2E26if2', NULL, NULL),
(10, 53, 'Lg Washing Machine 1475s4w', 'fc1475s4w', 'white', 'Medium', 0, 80490, 4, '', 'QjVKkuMRteTD4tlGcbWmuaSZXHsltSKB20TPtjhl', NULL, NULL),
(11, 138, 'Soft Yellow Full Printed Gown', 'SYFPG-001', 'Soft Yellow', 'Medium', 0, 4000, 1, '', 'g19M8eNF7qd93vSmqz1bVru2FTEVTbNXgwubKZM7', NULL, NULL),
(12, 77, 'Twinwash Mini', 'T2735NTWV', 'Black', 'Medium', 0, 464890, 1, '', 'g19M8eNF7qd93vSmqz1bVru2FTEVTbNXgwubKZM7', NULL, NULL),
(13, 2, 'Women Brown Ankle Shoes', 'WBAS-001-39', 'Brown', '39', 0, 1900, 1, '', 'RPwM6GR2ziJtf9A8GsKZ3qEHvWL6pZuQbXgJe49p', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_content` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `description`, `featured_image`, `featured`, `slug`, `meta_title`, `meta_description`, `meta_content`, `meta_keywords`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '0', 'Woman\'s Fashion', 'In This Folder, You Can Buy The Woman Accessories&nbsp;', NULL, 0, 'womans-fashion', '', '', '', NULL, NULL, '2019-01-01 10:48:49', '2019-01-01 15:04:27', '2019-01-01 15:04:27'),
(2, '1', 'Jewellery', 'You Can Buy Jewellery From Here.&nbsp;', NULL, 0, 'jewellery', '', '', '', NULL, NULL, '2019-01-01 10:52:13', '2019-01-01 15:04:36', '2019-01-01 15:04:36'),
(3, '1', 'Clothing', 'You Can Buy Woman Clothing.', NULL, 0, 'clothing', '', '', '', NULL, NULL, '2019-01-01 10:56:38', '2019-01-01 15:04:40', '2019-01-01 15:04:40'),
(4, '1', 'Make Up Product', 'You Can Buy Make Up Product From Here&nbsp;', '15933.jpg', 0, 'make-up-product', '', '', '', NULL, NULL, '2019-01-01 11:08:13', '2019-01-01 15:04:32', '2019-01-01 15:04:32'),
(5, '0', 'Women\'s Fashion', 'You Can Buy Woman Accessories Here.&nbsp;', '51006.jpg', 0, 'womens-fashion', '', '', '', NULL, NULL, '2019-01-01 15:10:00', '2019-01-01 15:12:05', NULL),
(6, '5', 'Make Up Items', 'You Can Buy Makeup Products.&nbsp;', '24871.jpg', 0, 'make-up-items', '', '', '', NULL, NULL, '2019-01-01 15:14:10', '2019-01-01 15:14:10', NULL),
(7, '5', 'Shoes', 'You Can Buy Shoes From Here', '96606.jpg', 0, 'shoes', '', '', '', NULL, NULL, '2019-01-01 15:15:21', '2019-01-01 15:15:21', NULL),
(8, '5', 'Clothing', 'You Can Buy Woman Clothes From Here.&nbsp;', '79408.jpg', 0, 'clothing', '', '', '', NULL, NULL, '2019-01-01 15:19:25', '2019-01-01 15:19:25', NULL),
(9, '5', 'Watches', 'You Can Search Attractive Watches From Here.', '16884.jpg', 0, 'watches', '', '', '', NULL, NULL, '2019-01-01 15:22:38', '2019-01-01 15:22:38', NULL),
(10, '5', 'Jewellery\'s', 'You Can Buy Jewellery Items From Here.', '28212.jpg', 0, 'jewellerys', '', '', '', NULL, NULL, '2019-01-01 15:28:09', '2019-01-01 15:28:09', NULL),
(11, '0', 'Electronic', 'You Can Buy Electronics Items From Here.', '8240.jpg', 0, 'electronic', '', '', '', NULL, NULL, '2019-01-01 15:30:45', '2019-01-01 15:30:45', NULL),
(12, '11', 'Mobiles', 'You Can Find Mobiles From Here&nbsp;', '55912.jpg', 0, 'mobiles', '', '', '', NULL, NULL, '2019-01-01 15:32:22', '2019-01-01 15:32:22', NULL),
(13, '11', 'Tablets', 'You Can Find Tablets From Here.', '38904.jpg', 0, 'tablets', '', '', '', NULL, NULL, '2019-01-01 15:33:38', '2019-01-01 15:33:38', NULL),
(14, '11', 'Laptops', 'You Can Find Laptops From Here.', '54820.png', 0, 'laptops', '', '', '', NULL, NULL, '2019-01-01 15:34:56', '2019-01-01 15:34:56', NULL),
(15, '11', 'Gaming Consoles', 'You Can Find Gaming Accessories Here.', '47707.jpg', 0, 'gaming-consoles', '', '', '', NULL, NULL, '2019-01-01 15:37:17', '2019-01-01 15:37:17', NULL),
(16, '11', 'Cameras', 'You Can Find Cameras From Here', '37788.png', 0, 'cameras', '', '', '', NULL, NULL, '2019-01-01 15:39:35', '2019-01-01 15:39:35', NULL),
(17, '0', 'Tv & Home Appliances', 'You Can Find Tv &amp; Home Appliances Here.', '16810.jpg', 0, 'tv-home-appliances', '', '', '', NULL, NULL, '2019-01-01 15:41:48', '2019-01-01 15:41:48', NULL),
(18, '17', 'Televisions', 'You Can Find Televisions Here&nbsp;', '54892.jpg', 0, 'televisions', '', '', '', NULL, NULL, '2019-01-01 15:43:25', '2019-01-01 15:43:25', NULL),
(19, '17', 'Audio & Video Devices', 'You Can Find Audio And Video Appliances Here.', '59623.png', 0, 'audio-video-devices', '', '', '', NULL, NULL, '2019-01-01 15:45:29', '2019-01-01 15:45:29', NULL),
(20, '17', 'Kitchen Appliance', 'You Can Find All Types Of Kitchen Appliances Here&nbsp;', '61753.jpg', 0, 'kitchen-appliance', '', '', '', NULL, NULL, '2019-01-01 15:48:21', '2019-01-01 15:48:21', NULL),
(21, '17', 'Iron', 'You Can Find All Types Of Iron Here.', '25196.jpg', 0, 'iron', '', '', '', NULL, NULL, '2019-01-01 15:49:55', '2019-01-01 15:49:55', NULL),
(22, '17', 'Fans And Heaters', 'You Can Find Every Brands Heaters And Fans', '42721.jpg', 0, 'fans-and-heaters', '', '', '', NULL, NULL, '2019-01-01 15:52:02', '2019-01-01 15:52:02', NULL),
(23, '0', 'Health & Beauty', 'You Can Find All Types Of Product Related&nbsp;to Health And Beauty&nbsp;', '55798.jpeg', 0, 'health-beauty', '', '', '', NULL, NULL, '2019-01-01 15:53:55', '2019-01-01 15:53:55', NULL),
(24, '23', 'Beauty Tools', 'You Can Find All Types Of Beauty Tools Product Here&nbsp;', '54149.jpg', 0, 'beauty-tools', '', '', '', NULL, NULL, '2019-01-01 15:55:20', '2019-01-01 15:55:20', NULL),
(25, '23', 'Skin Care', 'Here You Can Find Every Product Related To Skin Care .', '24517.jpg', 0, 'skin-care', '', '', '', NULL, NULL, '2019-01-01 15:58:18', '2019-01-01 15:58:18', NULL),
(26, '23', 'Bath & Body', 'You Can Find Product Related To Bath And Body In This Category.&nbsp;', '56259.jpg', 0, 'bath-body', '', '', '', NULL, NULL, '2019-01-01 16:00:34', '2019-01-01 16:00:34', NULL),
(27, '0', 'Sports & Outdoors', 'You Can Find Product Related To Sports, Indoors And Outdoors In This Category.&nbsp;', '30268.jpg', 0, 'sports-outdoors', '', '', '', NULL, NULL, '2019-01-01 16:04:53', '2019-01-01 21:35:25', NULL),
(28, '27', 'Men\'s Jersey', 'You Can Find All Kinds Of Men\'s Jersey In This Category.', '69780.jpg', 0, 'mens-jersey', '', '', '', NULL, NULL, '2019-01-01 16:07:32', '2019-01-01 16:07:32', NULL),
(29, '27', 'Women\'s Jersey', 'You Can Find All Kind Of Women\'s Jersey In This Category&nbsp;', '27657.jpg', 0, 'womens-jersey', '', '', '', NULL, NULL, '2019-01-01 16:11:09', '2019-01-01 16:11:09', NULL),
(30, '27', 'Water Bottles', 'You Can Find Sports Bottles In This Category.', '89435.jpg', 0, 'water-bottles', '', '', '', NULL, NULL, '2019-01-01 16:13:32', '2019-01-01 16:13:32', NULL),
(31, '0', 'Exercise & Fitness', 'You Can Find Fitness And Exercise Items In This Category&nbsp;', '64802.jpg', 0, 'exercise-fitness', '', '', '', NULL, NULL, '2019-01-01 16:13:58', '2019-01-01 16:15:47', '2019-01-01 16:15:47'),
(32, '27', 'Exercise & Fitness', 'You Can Find All Kinds Of Fitness And Exercise Items In This Category.&nbsp;', '71981.jpg', 0, 'exercise-fitness', '', '', '', NULL, NULL, '2019-01-01 16:16:41', '2019-01-01 16:16:41', NULL),
(33, '0', 'Home & Lifestyle', 'You Can Find All Kinds Of Home &amp; Lifestyle Products In This Category&nbsp;', '41922.jpg', 0, 'home-lifestyle', '', '', '', NULL, NULL, '2019-01-02 11:49:44', '2019-01-02 11:49:44', NULL),
(34, '33', 'Tools, Diy & Outdoors', 'You Can Find All Home Uses A Product In This Category.', '35964.jpg', 0, 'tools-diy-outdoors', '', '', '', NULL, NULL, '2019-01-02 11:53:15', '2019-01-02 11:53:15', NULL),
(35, '23', 'Hair Care', 'You Can Find All Kinds Of Hair Care Products In This Category&nbsp;', '43743.jpg', 0, 'hair-care', '', '', '', NULL, NULL, '2019-01-02 13:35:30', '2019-01-02 13:35:30', NULL),
(36, '17', 'Mixer', 'You Can Find All Kind Of Mixer Items In This Category.', '79594.jpg', 0, 'mixer', '', '', '', NULL, NULL, '2019-01-02 15:06:06', '2019-01-02 15:06:06', NULL),
(37, '23', 'Body Shaper', 'You Can Find All Kind Of Body Shaper In This Category&nbsp;', '65005.jpg', 0, 'body-shaper', '', '', '', NULL, NULL, '2019-01-02 15:27:35', '2019-01-02 15:27:35', NULL),
(38, '17', 'Washing Machine', 'Branded Washing Machine&nbsp;available Here.', '92416.jpg', 0, 'washing-machine', '', '', '', NULL, NULL, '2019-01-03 11:27:35', '2019-01-03 11:27:35', NULL),
(39, '17', 'Vacuum Cleaner', 'You Can Find Vacuum Cleaner In This Category.', '60974.jpg', 0, 'vacuum-cleaner', '', '', '', NULL, NULL, '2019-01-03 13:23:43', '2019-01-03 13:23:43', NULL),
(40, '17', 'Micro Oven', 'You Can Find Any Kind Of Micro Oven In This Category&nbsp;', '96075.JPG', 0, 'micro-oven', '', '', '', NULL, NULL, '2019-01-03 13:46:16', '2019-01-03 13:46:16', NULL),
(41, '11', 'Speaker', 'You Can Find Any Kind Of Speaker In This Category.', '85819.jpg', 0, 'speaker', '', '', '', NULL, NULL, '2019-01-03 15:06:53', '2019-01-03 15:06:53', NULL),
(42, '11', 'Trolly Speaker', 'You Can Find Any Kind Of Trolly Speaker In This Category&nbsp;', '81990.jpg', 0, 'trolly-speaker', '', '', '', NULL, NULL, '2019-01-03 15:08:20', '2019-01-03 15:08:20', NULL),
(43, '17', 'Refrigerator', 'Branded Refrigerator Available Here.&nbsp;', '47683.jpg', 0, 'refrigerator', '', '', '', NULL, NULL, '2019-01-03 16:06:23', '2019-01-03 16:34:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `checkouts`
--

CREATE TABLE `checkouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_notes` text COLLATE utf8mb4_unicode_ci,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `checkouts`
--

INSERT INTO `checkouts` (`id`, `cart_id`, `customer_id`, `phone`, `delivery_address`, `order_notes`, `payment_type`, `created_at`, `updated_at`) VALUES
(1, NULL, 8, '9803345815', 'thimi, bhaktapur', 'please deliver it by tomorrow', 'cash-on-delivery', '2019-01-01 16:45:50', '2019-01-01 16:45:50'),
(2, NULL, 1, '9844674167', 'Ganesthan, Chabahil-07, Kathmandu', NULL, 'cash-on-delivery', '2019-01-01 22:05:39', '2019-01-01 22:05:39'),
(3, NULL, 1, '9844674167', 'Ganesthan, Chabahil-07, Kathmandu', NULL, 'cash-on-delivery', '2019-01-01 22:23:30', '2019-01-01 22:23:30'),
(4, NULL, 2, '9803345815', 'thimi bhaktappur', NULL, 'cash-on-delivery', '2019-01-02 10:52:28', '2019-01-02 10:52:28'),
(5, NULL, 2, '9803345815', 'thimi bhaktapur', NULL, 'cash-on-delivery', '2019-01-02 12:21:28', '2019-01-02 12:21:28'),
(6, NULL, 10, '9844674167', 'Chabahil, Kathmandu', NULL, 'cash-on-delivery', '2019-01-02 12:44:05', '2019-01-02 12:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `expiry_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `slug`, `email`, `phone`, `password`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Ritendra Kumar Das', 'ritendra-kumar-dasi4XvnUkBkb', 'director@onlinezeal.com', NULL, '$2y$10$9ZgkT/5uLhhQY6BE/SXUtOjW9b/IWM9gcoQT5o9Ud35p65KP8Mcce', NULL, NULL, '2018-12-31 23:57:02', '2018-12-31 23:57:02'),
(3, 'Ritendra Kumar Das', 'ritendra-kumar-dasPisAwgldg7', 'ritendra69@gmail.com', NULL, '$2y$10$ku/97cU5y8cSS9VnoLbjAOAOaOfIKHX6znIxQEK0y5/lEJhUZV5h2', NULL, NULL, '2019-01-01 12:54:01', '2019-01-01 12:54:01'),
(4, 'Sushan Paudyal', 'sushan-paudyaloMpaSxpSOU', 'sushanpaudyal@gmail.com', NULL, '$2y$10$M9duh0ghs5jwxsr6gJBrRO0dnNEB3qs38e31TwIx9RtmcACsvd5Xe', NULL, NULL, '2019-01-01 13:20:26', '2019-01-01 13:20:26'),
(5, 'Sushan Paudyal', 'sushan-paudyali50jCKxfiV', 'sushan.paudyal@gmail.com', NULL, '$2y$10$xXnv2fbJ8KrWa5TFsq81werqlzguqyYmX/5/21z8EvSs.FSzo8EMy', NULL, NULL, '2019-01-01 13:22:42', '2019-01-01 13:22:42'),
(6, 'Sushan Paudyal', 'sushan-paudyalR5r88QymG2', 'cto@onlinezeal.com', NULL, '$2y$10$K1fvoIVlph8o0Qn50dsEKuum2ciPBejDyCjRDA23Ql7vF2xOLFIOG', NULL, NULL, '2019-01-01 13:24:33', '2019-01-01 13:24:33'),
(7, 'Avaya Baniya', 'avaya-baniyakovmj8sQDN', 'avaya.baniya@onlinezeal.com', NULL, '$2y$10$CJgzD3eSFwO6nraqKx8piO4aKbDRBVelmbAmKpIqtstNg10.cEGaC', 'Basantapur, Kathmandu', 'Ta5Dsc8aaRKHlNg3WUlO2DGumaPukiSN7JyR6tBqLMFby32U4ubkQsKqGUCs', '2019-01-01 15:29:55', '2019-01-01 15:33:27'),
(8, 'Pramit Das', 'pramit-daseeHhYMiIpN', 'trymail@jeevanayurveda.com.np', NULL, '$2y$10$zWKddAHo58pRD2rgUySOdOdonCkvuYHbvi6F7qoGXF7cu4ysJSo1K', NULL, 'eRf9KukCtQCmTPT8LPsRJVIOYOoj8OQFbeQ2TZGt7OreTzcj9zSX9nsJsPtZ', '2019-01-01 16:41:05', '2019-01-01 16:44:12'),
(10, 'Sachin Siwakoti', 'sachin-siwakoti0YT4TdHeKL', 'sachin.siwakoti@onlinezeal.com', NULL, '$2y$10$bVFluszS.lZviu/crDjNneuzwgXJlS4WWCw9sFvw142uznnXMS99q', NULL, NULL, '2019-01-02 12:39:09', '2019-01-02 12:39:09');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(317, '2014_10_12_000000_create_users_table', 1),
(318, '2014_10_12_100000_create_password_resets_table', 1),
(319, '2018_12_03_060737_create_user_profiles_table', 1),
(320, '2018_12_05_064637_create_categories_table', 1),
(321, '2018_12_06_060121_create_products_table', 1),
(322, '2018_12_09_065721_create_product_attributes_table', 1),
(323, '2018_12_09_120538_create_site_settings_table', 1),
(324, '2018_12_10_091325_create_faqs_table', 1),
(325, '2018_12_10_093710_create_product_images_table', 1),
(326, '2018_12_10_162608_create_tacs_table', 1),
(327, '2018_12_11_050641_create_sliders_table', 1),
(328, '2018_12_11_113929_create_brands_table', 1),
(329, '2018_12_12_093922_create_carts_table', 1),
(330, '2018_12_14_045733_create_customers_table', 1),
(331, '2018_12_16_073430_create_checkouts_table', 1),
(332, '2018_12_16_073620_create_orders_table', 1),
(333, '2018_12_18_090202_create_coupons_table', 1),
(334, '2018_12_20_090538_create_wishlists_table', 1),
(335, '2018_12_21_101743_create_product_specifications_table', 1),
(336, '2018_12_24_062703_create_product_ratings_table', 1),
(337, '2018_12_26_060347_create_abouts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `checkout_id` int(10) UNSIGNED NOT NULL,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_price` double NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `product_id`, `customer_id`, `checkout_id`, `order_type`, `size`, `quantity`, `order_price`, `user_email`, `order_status`, `created_at`, `updated_at`) VALUES
(1, '3340313', 2, 8, 1, 'Cash On Delivery', '40', 1, 1900, 'trymail@jeevanayurveda.com.np', 'on-way', '2019-01-01 16:45:51', '2019-01-01 22:08:35'),
(4, '7740688', 3, 2, 4, 'Cash On Delivery', '39', 1, 2200, 'director@onlinezeal.com', 'cancelled', '2019-01-02 10:52:29', '2019-01-02 11:05:29'),
(5, '4744212', 12, 2, 5, 'Cash On Delivery', '39', 1, 2250, 'director@onlinezeal.com', 'cancelled', '2019-01-02 12:21:28', '2019-01-02 12:21:40'),
(6, '5091889', 8, 10, 6, 'Cash On Delivery', '39', 1, 2300, 'sachin.siwakoti@onlinezeal.com', 'completed', '2019-01-02 12:44:06', '2019-01-02 13:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sushan.paudyal@gmail.com', '$2y$10$T642ewyMnCVUL4FHxpOOmump2OD/JxFlPbYilQYqqDpc1nyWXi3q6', '2019-01-01 13:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `onsale` int(10) UNSIGNED DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_content` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `product_code`, `product_color`, `description`, `price`, `image`, `status`, `featured`, `onsale`, `meta_title`, `meta_description`, `meta_content`, `meta_keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 7, 'Women Brown Ankle Shoes', 'women-brown-ankle-shoes', 'WBAS-001', 'Brown', '<p>Features:</p><p>Ankle Boat&nbsp;</p><p>Closure: Zip</p><p>Tip Shape: Pointed</p>', 1900.00, '3834.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 16:30:48', '2019-01-01 21:30:47', NULL),
(3, 7, 'Women Black Ankle Shoes', 'women-black-ankle-shoes', 'WBAS-002', 'Black', '<p>Features:</p><p>Woman Blank Ankle Boat&nbsp;</p><p>Heel Shape: Block</p><p>Brand: No Brand</p><p><br></p>', 2200.00, '75646.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 16:49:25', '2019-01-01 21:30:48', NULL),
(4, 7, 'Woman Light Brown Shoes', 'woman-light-brown-shoes', 'WLBS-003', 'Light Brown', '<p>Features:</p><p>Brand: No Brand<br>Heel Shape: Block&nbsp;</p>', 1800.00, '29296.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 16:58:18', '2019-01-01 16:58:18', NULL),
(5, 7, 'Woman Black Ankle Boot', 'woman-black-ankle-boot', 'WBAB-004', 'Black', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Heel Shape: Block&nbsp;</p><p>Materials: Leathers&nbsp;</p><p><br></p>', 2200.00, '89803.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:04:03', '2019-01-01 17:04:03', NULL),
(6, 7, 'Woman Black Leather Ankle Boot', 'woman-black-leather-ankle-boot', 'WBLAB-005', 'Black', '<p>Features:</p><p>Brand: No Brand</p><p>Materials: Leathers&nbsp;</p><p>Heel Shape: Block</p>', 2000.00, '55290.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:06:35', '2019-01-01 17:06:35', NULL),
(7, 7, 'Black Leather Ankle Boot For Women', 'black-leather-ankle-boot-for-women', 'WLABFW-006', 'Black', '<p>Features:</p><p>Brand: No Brand&nbsp;</p><p>Material: Leather</p><p>Colour: Black&nbsp;</p><p>Heel Shape: Block&nbsp;</p>', 2000.00, '70258.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:12:31', '2019-01-02 11:09:50', NULL),
(8, 7, 'Black Leather Boots For Woman', 'black-leather-boots-for-woman', 'BLBFW-007', 'Black', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Heel Shape: Block</p><p>Colour: Black</p><p><br></p>', 2300.00, '29620.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:15:24', '2019-01-02 11:09:49', NULL),
(9, 7, 'Women\'s Brown Ankle Boot', 'womens-brown-ankle-boot', 'WBAB-008', 'Brown', '<p>Features:</p><p>Brand: No Brand</p><p>Heel Shape: Block</p><p>Clouser: Zip&nbsp;</p><p>Material: Leather</p>', 2100.00, '35395.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:18:54', '2019-01-01 17:18:54', NULL),
(10, 7, 'Woman\'s Leather Boot In Black', 'womans-leather-boot-in-black', 'WLBB-009', 'Black', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Heel Shape: Block&nbsp;</p>', 2000.00, '15942.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:22:58', '2019-01-02 11:09:52', NULL),
(11, 7, 'Woman\'s Ankle Boot', 'womans-ankle-boot', 'WAB -010', 'Black,', '<p>Features:</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Heel Shape: Block&nbsp;</p>', 2400.00, '34436.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:40:59', '2019-01-02 11:09:52', NULL),
(12, 7, 'Woman\'s Black Ankle Boot- 011', 'womans-black-ankle-boot-011', 'WBAB-011', 'Black', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Heel Shape: Block</p><p>Colours: Black</p>', 2250.00, '11488.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:44:33', '2019-01-01 17:44:33', NULL),
(13, 7, 'Brown Leather Boot For Women-012', 'brown-leather-boot-for-women-012', 'BLBW-012', 'Brown', '<p>Features:</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Heel Shape: Block&nbsp;</p>', 1900.00, '84390.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:50:00', '2019-01-01 17:50:00', NULL),
(14, 7, 'Woman\'s Black Shoes-013', 'womans-black-shoes-013', 'WBS-013', 'Black', '<p>Features:</p><p>Brand: No Brand</p><p>Heel Shape: Block&nbsp;</p>', 1900.00, '77201.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:52:42', '2019-01-01 17:52:42', NULL),
(15, 7, 'Woman\'s Shoes', 'womans-shoes', 'WS-014', 'Dark Brown', '<p>Features:</p><p>Brand: No Brand</p><p>Colours: Dark Brown&nbsp;</p><p>Heel Shape: Block&nbsp;</p>', 1900.00, '9368.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 17:56:52', '2019-01-01 17:56:52', NULL),
(16, 7, 'Women\'s Grey Ankle Boot', 'womens-grey-ankle-boot', 'WGAB-015', 'Grey', '<p>Features:</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Heel Shape: Block&nbsp;</p><p>Colour: Grey&nbsp;</p>', 2200.00, '324.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 18:00:03', '2019-01-01 18:00:03', NULL),
(17, 7, 'Women\'s Dark Brown Ankle Boot', 'womens-dark-brown-ankle-boot', 'WDBAB-016', 'Dark Brown', '<p>Features:</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Heel Shape: Block&nbsp;</p><p>Colours: Dark Brown&nbsp;</p>', 1900.00, '45980.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-01 18:04:02', '2019-01-01 18:04:02', NULL),
(18, 7, 'Women\'s Black Leather Shoes-017', 'womens-black-leather-shoes-017', 'WBLS-017', 'Black', '<p>Features: </p><p>Brand: No Brand&nbsp;</p><p>Colours: Black&nbsp;</p><p>Heel Shape: Block&nbsp;</p>', 2000.00, '49211.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 10:53:55', '2019-01-02 10:53:55', NULL),
(19, 7, 'Women\'s Black Shoes-018', 'womens-black-shoes-018', 'WBS-018', 'Black', '<p>Features:</p><p>Brand: No Brand</p><p>Colour: Black&nbsp;</p><p>Heel Shape: Block</p>', 1800.00, '71447.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 10:59:25', '2019-01-02 10:59:25', NULL),
(20, 7, 'Woman\'s Brown Ankle Shoes-019', 'womans-brown-ankle-shoes-019', 'WBAS-019', 'Brown', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Color: Brown</p><p>Heel Shape: Block&nbsp;</p>', 2100.00, '58170.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 11:03:53', '2019-01-02 11:03:53', NULL),
(21, 7, 'Women\'s Dark Brown Shoes-020', 'womens-dark-brown-shoes-020', 'WDBS-020', 'Dark Brown', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Colour: Dark Brown</p><p>Heel Shape: Block&nbsp;</p><p>Materials: Leather&nbsp;</p>', 2100.00, '52874.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 11:09:12', '2019-01-02 11:09:12', NULL),
(22, 7, 'Woman Brown Shoes-021', 'woman-brown-shoes-021', 'WBS-021', 'Brown', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Colour: Brown</p><p>Heel Shape: Block&nbsp;</p>', 1800.00, '97241.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 11:14:25', '2019-01-02 11:14:25', NULL),
(23, 7, 'Women Shoes-022', 'women-shoes-022', 'WS-022', 'Light Brown', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Color: Light Brown</p><p>Heel Shape: Block&nbsp;</p>', 2000.00, '70025.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 11:17:00', '2019-01-02 11:17:00', NULL),
(24, 7, 'Women\'s Shoes-023', 'womens-shoes-023', 'WS-023', 'Brown', '<p>Features:&nbsp;</p><p>Brand: No Brand</p><p>Closure: Zip&nbsp;</p><p>Color: Brown</p><p>Heel Shape: Block&nbsp;</p>', 2000.00, '17627.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 11:23:09', '2019-01-02 11:23:09', NULL),
(25, 34, 'Magic Hose Pipe (green) - 75 Ft', 'magic-hose-pipe-green-75-ft', 'HS-001', 'Green', '<p>Features:</p><ul><li>GreenMagic Hose Pipe&nbsp;</li><li>Useful for Graden use, washing vehicles, House Cleaning, Boot Cleaning etc&nbsp;</li><li>materials: Rubber</li><li>Light Weight and portable size</li><li>Easy to carry and store</li><li>Expand and drains automatically in few seconds</li><li>Multi-Use able</li></ul>', 900.00, '86254.jpeg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 12:35:05', '2019-01-02 12:35:05', NULL),
(26, 19, 'Sunglasses With Headset', 'sunglasses-with-headset', 'SWH-001', 'Black', '<p>Features:&nbsp;</p><ul><li>Sun Glass with Wireless Headset&nbsp;</li><li>Supports MP3, MP4, WAV, WMA</li><li>Battery: Rechargeable</li><li>Devices Support iPhones, Smartphones, Ipad</li><li>Easy To Use&nbsp;</li><li>&nbsp;Best Quality Sound&nbsp;</li><li>Supports Both Handset and Handsfree&nbsp;Profiles&nbsp;</li><li>Battery Duration: 2/3 hours and more&nbsp;</li></ul>', 1200.00, '32719.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 13:32:13', '2019-01-02 13:32:13', NULL),
(27, 35, 'Handy Head And Neck Massager', 'handy-head-and-neck-massager', 'HHNM-001', 'Pink', '<p>Features:</p><ul><li>Easy Hair And Neck Massager</li><li>Can be also used on knees, Neck, Hair, Ankles and Elbows</li><li>Reduces Stress</li><li>Smooth Tips&nbsp;</li><li>Good for Hair Growth</li><li>Feels really good&nbsp;</li><li>Available in other different Colors&nbsp;</li></ul><p><br></p>', 80.00, '84130.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 13:44:26', '2019-01-02 13:44:26', NULL),
(28, 20, 'Stainless Steel Electric Kettle', 'stainless-steel-electric-kettle', 'EK-001', 'Silver and Black', '<p>Features:&nbsp;</p><ul><li>High-Quality Stainless Steel Material&nbsp;</li><li>Fast Water Boiling&nbsp;</li><li>Automatically cutoff while boiling&nbsp;</li><li>Boil-Dry Protection&nbsp;</li></ul>', 650.00, '19608.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 14:48:42', '2019-01-02 15:02:48', NULL),
(29, 6, 'Naked Make Up Brush', 'naked-make-up-brush', 'NMUB-001', 'Silver', '<p><b>Features:</b></p><p>Highlights :</p><ul><li>Packs of 12</li><li>Contains: Loose, Foundation, Trimming, Flat, Medium, Oblique, Eyebrow, Small Eye Shadow, Eye Shadow, Flat Eyeliner, Lip, Flap Lip</li><li>For Profession Care&nbsp;</li><li>Multicolor&nbsp;</li><li>Multishape&nbsp;</li></ul><p><br></p><p><br></p>', 700.00, '9428.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 15:19:09', '2019-01-02 15:19:09', NULL),
(30, 6, 'Farsali Rose Gold Elixir', 'farsali-rose-gold-elixir', 'FRGE-001', 'White', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">This elixir is infused with 24k gold, vitamins, antioxidants, and essential fatty acids for hydration and a healthy-looking glow. Enriched with rosehip seed oil, it penetrates quickly to nourish your skin while visibly brightening and improving texture. This product is packed with linoleic and linolenic acids, vitamin C, and vitamin A to help reduce the appearance of fine lines and discolouration, while the pure 24k gold flakes absorb right into your skin for a visibly glowing and radiant complexion</span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 24px; font-size: 14px; color: rgb(89, 89, 89); font-family: &quot;Droid Arabic Kufi&quot;, sans-serif;\"><span style=\"font-weight: bold;\">Ingredients</span></p><ul style=\"margin-bottom: 10px; color: rgb(89, 89, 89); font-family: &quot;Droid Arabic Kufi&quot;, sans-serif; font-size: 14px;\"><li style=\"font-size: 16px; line-height: 30px;\">Rosehip Seed Oil</li><li style=\"font-size: 16px; line-height: 30px;\">Pumpkin Seed Oil</li><li style=\"font-size: 16px; line-height: 30px;\">24k Gold</li><li style=\"font-size: 16px; line-height: 30px;\">Vitamin E</li><li style=\"font-size: 16px; line-height: 30px;\">Lemongrass Oil</li><li style=\"font-size: 16px; line-height: 30px;\">Orange Peel Oil</li></ul><p style=\"font-size: 16px; line-height: 30px;\"><br></p><p style=\"font-size: 16px; line-height: 30px;\">Can be used as:</p><ul><li style=\"font-size: 16px; line-height: 30px;\">Daily moisturizer&nbsp;</li><li style=\"font-size: 16px; line-height: 30px;\">Hydrating Lips&nbsp;</li><li style=\"font-size: 16px; line-height: 30px;\">Mix With Foundation&nbsp;</li><li style=\"font-size: 16px; line-height: 30px;\">Revive Dry Cream Product&nbsp;</li></ul><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 24px; font-size: 14px; color: rgb(89, 89, 89); font-family: &quot;Droid Arabic Kufi&quot;, sans-serif;\">&nbsp;</p>', 700.00, '77918.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 15:59:46', '2019-01-02 15:59:46', NULL),
(31, 6, 'Hello Kitty Brush', 'hello-kitty-brush', 'HKB-001', 'Pink', '<span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">HELLO KITTY BRUSH</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Very Cute High-Quality Professional Hello Kitty Makeup Brushes set. Great as gift &amp; travel. 7pcs brushes set includes all the basics for daily applications. Each brush is individually packed inside a plastic cover and mark with the function or name of the brush.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Made with Coat hair &amp; synthetic fibres. Very soft.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Box Size: 5\"x 2 3/4\"</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Set included:</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-Face</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-Foundation</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-Lip</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-Angled eyeliner&nbsp;</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-eye shades stick</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-EyeShadow</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1-Shader</span>', 550.00, '91247.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:05:07', '2019-01-02 16:05:07', NULL),
(32, 6, 'Zoeva Brush Set', 'zoeva-brush-set', 'ZBS-001', 'Black', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">15 pcs Makeup Brush Kit Suitable for many kinds of casual occasions brush hair is made of high-quality synthetic fibre hair which is super soft and comfortable for the skin good quality package.</span></p><ol><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Silk Finish Brush&nbsp;</span></li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Buffer&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Powder&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Face Shape&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Cream Cheek&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Concealer Buffer&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Petit Eye Blender&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Luxe Soft Definer&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Luxe Crease&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Luxe Petit Crease&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Luxe Smoky Shader&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Contour Shader&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Fine Liner&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Wing Liner&nbsp;</span>Brush</li><li><span style=\"font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif, &quot;Times New Roman&quot;, Times, serif; letter-spacing: 0.52px;\">Brow Line&nbsp;</span><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Brush<br></span><br></li></ol>', 1800.00, '31969.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:17:54', '2019-01-02 16:17:54', NULL),
(33, 6, 'Darmacol Makeup Cover', 'darmacol-makeup-cover', 'DMUC-001', 'Golden', '<p>Features:&nbsp;</p><ul><li>Full coverage&nbsp;</li><li>Long Lasting&nbsp;</li><li>Natural Look Effect&nbsp;</li><li>Effective for All Kind of Skin&nbsp;</li><li>Covers the acne, tattoos, surgical Bruising, loss of pigmentation.</li><li>Waterproof</li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Hypoallergenic</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">SPF 30</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Preservative-free</span><br></li></ul>', 2000.00, '7566.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:28:06', '2019-01-02 16:29:30', NULL),
(34, 6, 'Ladista Unicorn Colorful Makeup Brushes', 'ladista-unicorn-colorful-makeup-brushes', 'LUCMB-001', 'Multi Colour', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Ladista Unicorn Colorful Makeup Brushes Thread Rainbow Professional Make Up Brushes Set Blending Powder Foundation Contour Brush (Pack of 7)</span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Features:</span></p><ul><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Soft- multi colours&nbsp;brush&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Smooth Brush&nbsp;</span></li></ul><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><br></span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><br></span></p><p><br></p>', 1000.00, '14368.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:34:50', '2019-01-02 16:34:50', NULL),
(35, 6, 'Fit Me Foundation', 'fit-me-foundation', 'FMF-001', 'multi color', '<p style=\"margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">DESCRIPTION:<br>Fit Me® Matte + Poreless Foundation face makeup. This lightweight foundation mattifies and refines pores and leaves a natural, seamless finish.</p><p style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">BENEFITS<br>Ideal for normal to oily skin, our exclusive matte foundation formula features micro-powders to control shine and blur pores. Pore minimizing foundation. Dermatologist tested. Allergy tested. Non-comedogenic.</p><p style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">HOW TO USE/APPLY<br>Apply foundation onto skin and blend with fingertips or an applicatoBest Shopping Experience.</p>', 1100.00, '87007.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:36:35', '2019-01-02 16:36:35', NULL),
(36, 6, 'Maybelline Fit Me Matte & Poreless Foundation', 'maybelline-fit-me-matte-poreless-foundation', 'MFMMPF-001', 'Multi Colours', '<p style=\"margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">DESCRIPTION:<br>Fit Me Matte &amp; Poreless has been designed for normal to oily skin. Its blurring micro powders refines pores while shine is being absorbed for a natural matte finish.</p><p><br></p><p style=\"margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">Fit Me Matte &amp; Poreless foundation fits both skin and texture for a natural finish and poreless looking skin.</p>', 800.00, '6552.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:40:15', '2019-01-02 16:40:15', NULL),
(37, 6, 'Hd Pro Concealer', 'hd-pro-concealer', 'HPC-001', 'Multi Color', '<p><span style=\"color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">HD Pro Concealers are crease-resistant with opaque coverage in a creamy, yet lightweight texture. The long-wearing concealer formula camouflages darkness under the eyes, redness and skin imperfections.</span></p><ul><li><span style=\"color: rgb(28, 30, 33); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Available in different Color</span></li></ul><p><br></p>', 500.00, '78002.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:45:33', '2019-01-02 16:45:33', NULL),
(38, 6, 'Meet Matte Hughes', 'meet-matte-hughes', 'MMH-001', 'Multi Color', '<p><p style=\"margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"></p></p><p style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">DESCRIPTION:-<br>Introducing the Meet Matt(e) Hughes mini set from theBalm Cosmetics, featuring six brand new shades of our truly loyal liquid lipstick. Whether you’re single and ready to mingle, or dedicated to finding “the one”, this limited edition kit is the perfect ‘matte‘ch-maker.</p><ul><li style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Available in Different Color&nbsp;</li><li style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">6 in 1 Pack&nbsp;</li><li style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Long-Lasting&nbsp;</li><li style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">High-Quality&nbsp;</li></ul>', 1200.00, '33201.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 16:56:58', '2019-01-02 16:56:58', NULL),
(39, 6, 'Natural Flower Jelly Lipstick', 'natural-flower-jelly-lipstick', 'NFJL-001', 'Pink', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">All the lipstick lovers, listen up: You\'re about to fall head over heels in love with this lipstick because it\'s pure magic. This clear lipstick is infused with jelly, gold specks, and a tiny flower placed right in the centre. It\'s basically the glass-encased rose from Beauty and the Beast, the lipstick of your dreams.</span></p><ul><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Colour Changeable&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Longlasting</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">high-quality&nbsp;<br></span><br></li></ul>', 500.00, '97990.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:05:28', '2019-01-02 17:05:28', NULL),
(40, 6, 'Naked Eye Shadow And Brush Collection', 'naked-eye-shadow-and-brush-collection', 'NESBC-001', 'Multi Color', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Collection of Naked Eyeshadow and Brush&nbsp; Product&nbsp;</span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1) Kylie Brush</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">2) Naked 08 Eyeshadow</span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Features:</span></p><ul><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">High-Quality&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Set of 12 brush&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Attractive eyeshadow</span></li></ul><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><br></span><br></p>', 800.00, '17519.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:14:41', '2019-01-02 17:14:41', NULL),
(41, 6, 'Castor And Olive Oil', 'castor-and-olive-oil', 'COO-001', 'Oil Color', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">The combo of castor oil and olive oil is useful for a long smooth and shiny hair. It can be also used for youthful and healthy skin. If it is used in eyebrows and eyelashes, it will make the hair grow thicker and faster. Mainly these oils are used for a home remedy for healthy hair and skin.</span></p><ul><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Useful for Hair Growth&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">,Makes Hair Smooth and Shiny&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Used for Skin Health&nbsp;</span></li><li><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Makes Hair Thicker&nbsp;<br></span><br></li></ul>', 800.00, '9147.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:18:06', '2019-01-02 17:18:06', NULL),
(42, 6, 'Beauty Product Of Morphe And Nars', 'beauty-product-of-morphe-and-nars', 'BPMN-001', 'Multi color', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Beauty product of Nars and Morphe, Included product detail are given Below:</span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">1) Morphe Foundation</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">2) NARS Primer</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">3) NARS Concealer</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">4) Morphie Loose Powder</span></p><p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><br></span><br></p>', 1300.00, '70116.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:22:11', '2019-01-02 17:22:11', NULL),
(43, 6, 'Morphe Foundation, Mac Ovel Brush And Nyx Wonderstick', 'morphe-foundation-mac-ovel-brush-and-nyx-wonderstick', 'MFMOBNW-001', 'multi color', '<p>Morphe Foundation, Mac Ovel Brush and NYX Wonderstick&nbsp;is a combination of beauty products which makes you more beautiful, increase your face glow, remove darkspot&nbsp;..<br></p>', 1000.00, '68434.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:26:48', '2019-01-02 17:26:48', NULL),
(44, 6, 'Maybelline Eyestudio', 'maybelline-eyestudio', 'MES-001', 'Black and brown', '<p><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">Description:</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px;\">The most intense line for lasting drama. Maybelline Eye Studio Lasting Drama Gel Eyeliner holds highly concentrated pigments in a clear gel base, so colour is more intense and lines resist wear for 24 hours.</span><br></p>', 500.00, '35274.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:29:07', '2019-01-02 17:29:07', NULL),
(45, 6, 'Bridal Beauty Product', 'bridal-beauty-product', 'BBP-001', 'Multi Colour', '<p>Bridal Beauty Product is a Combination of :</p><p style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">Description:</p><p style=\"margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;\">1) Nars eyeshadow<br>2) Glow kits<br>3) 24k make up fixer<br>4) Wonderstick&nbsp;<br>5) Gelliner&nbsp;<br>6) Foundation<br>7) Primer&nbsp;<br>8) Eyelash&nbsp;<br>9) compact&nbsp;<br>10) Water lipstick&nbsp;<br>11) Lipstick&nbsp;<br>12) Brush set<br>13) Eyeliner (pencil)<br>14) Eye curl<br>15) Highlighter&nbsp;<br>16) Double gel liner&nbsp;<br>17) Body spray</p>', 5000.00, '86812.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:32:20', '2019-01-02 17:32:20', NULL),
(46, 6, 'Mac Bridal Beauty Items', 'mac-bridal-beauty-items', 'MBBI-002', 'Multi color', '<p><p style=\"margin: 6px 0px 0px; display: inline; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"></p></p><p style=\"margin: 0px 0px 6px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">MAC BRIDAL MAKEUP<br>Description:<br>1) Mac-15s<br>2) Mac foundation<br>3) Mac primer<span>&nbsp;</span><br>4) Mac brush set<br>5) Mac eyelash<span>&nbsp;</span><br>6) Mac loos powder<br>7) Mac gloss<br>8) Mac blender</p>', 2500.00, '90834.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-02 17:36:38', '2019-01-02 17:36:38', NULL),
(47, 18, 'Cg 22d1504 22\'\' Led Tv', 'cg-22d1504-22-led-tv', 'CG 22D1504 22\'\' Led Tv', 'Black', '<p><b>Features:</b></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Battery Compatible (12V)</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Compatible with Solar Home System</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">A+Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Zero Bright Dot</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Front Facing ox Speaker</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li></ul>', 17290.00, '46515.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 10:49:19', '2019-01-03 11:07:35', NULL),
(48, 18, 'Cg 20d1504 20\'\' Led Tv', 'cg-20d1504-20-led-tv', 'CG 20D1504 20\'\' LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><b>Features:&nbsp;</b></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Compatible with Solar Home System</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">A+Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Zero Bright Dot</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Front Facing ox Speaker</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li></ul>', 14990.00, '69680.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:07:05', '2019-01-03 11:07:05', NULL),
(49, 18, 'Lg 24\" Led Tv', 'lg-24-led-tv', '24LH454A', 'black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">HD Ready</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Protection</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Games</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Regional Language Option</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">PC Input</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Bollywood Mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cricket Mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Slim Depth &amp; Narrow Bezel</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Energy Saving</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Triple XD Engine</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">USB Movie</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">DIVX HD</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">audio Out</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\"><br></strong></li></ul>', 26990.00, '63409.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:09:09', '2019-01-03 11:27:39', NULL),
(50, 18, 'Cg 20 D 3305/3205 Led Tv', 'cg-20-d-33053205-led-tv', 'CG 20 D 3305/3205 20\'\' LED TV', 'Black', '<div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px; text-align: center;\">&nbsp;</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Screen Size : 43″</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Resolution : 1920*1080p</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Response time : 8ms</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">USB : Yes (Music+Photo+Movie)</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Analog AV Out : Yes</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Power Supply : 110~240V 50-60Hz</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Battery Compatible</div>', 15390.00, '43591.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:32:20', '2019-01-03 11:32:20', NULL),
(51, 18, 'Lg 32\" Led Tv', 'lg-32-led-tv', '32lh514D', 'grey black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Enriches all colors to previously unseen vibrancy</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Improve any image with Resolution Upscaler</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Virtual Surround sound spreads out the space</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Plug in a USB drive for content</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Enjoy top-quality AV over a single HDMI cable</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Games make more entertaining with your TV</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Virtual Surround</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Game TV</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">HD Ready Metallic Design</strong></li></ul>', 38190.00, '7478.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:34:13', '2019-01-03 11:34:13', NULL),
(52, 18, 'Cg 22 D 3305 Led Tv', 'cg-22-d-3305-led-tv', 'CG 22 D 3305', 'Black', '<div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Features:</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Screen Size : 22″</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Resolution : 1920*1080p</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Response time : 8ms</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">USB : Yes (Music+Photo+Movie)</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Analog AV Out : Yes</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Power Supply : 110~240V 50-60Hz</div><div style=\"outline: 0px; margin: 0px; padding: 0px; border: 0px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\">Battery Compatible</div>', 16890.00, '52111.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:39:10', '2019-01-03 11:39:10', NULL),
(53, 38, 'Lg Washing Machine 1475s4w', 'lg-washing-machine-1475s4w', 'fc1475s4w', 'white', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Less vibration, Less noise</strong><br style=\"margin: 0px; padding: 0px;\">The Inverter Direct Drive™ Motor that powers our washing machines is super reliable and really quiet. We know it is one of the best washing machine motors on the market, which is why all our machines come with a standard 10 years warranty on the motor. Nothing standard about that now, is there?</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Combat allergens with the power of Steam</strong><br style=\"margin: 0px; padding: 0px;\">The Allergy Care cycle uses water and heat to create a super-disinfecting steam that sanitizes clothing while still being gentle on it. This reduces 99.9% allergens, such as dust mites that can cause allergy or respiratory issues. Improve your quality of life by maintaining a healthy, allergen-free environment.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Cycle Download</strong><br style=\"margin: 0px; padding: 0px;\">Cycle Download lets users download new wash program such as Wool, Baby Care or Cold Wash.&nbsp;&nbsp; &nbsp;</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™</strong><br style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™ quickly troubleshoots almost any minor issue before it becomes a bigger problem.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">OPTIMAL WASH for fabrics with 6 Motion DD</strong><br style=\"margin: 0px; padding: 0px;\">Select a wash program and 6 Motion Direct Drive technology moves the wash drum in multiple directions, giving fabrics the proper care while getting clothes ultra clean.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Add forgotten items mid-cycle</strong><br style=\"margin: 0px; padding: 0px;\">If you miss to put laundry during washing, just press \"Add Item\" and add any laundry from small socks to big jacket. Cotton, Mix, Easy Care, Speed14 whichever you selected, door can be opened* immediately(less than 3 sec)** during washing.</p>', 80490.00, '73776.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:41:44', '2019-01-03 11:41:44', NULL),
(54, 18, 'Cg 24 Dn 407 24\'\' Normal Led Tv', 'cg-24-dn-407-24-normal-led-tv', 'CG 24 DN 407', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">A+Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Zero Bright Dot</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">True HDSpeaker</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Full USB Support</strong></li></ul>', 18790.00, '44934.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:44:05', '2019-01-03 17:38:12', NULL),
(55, 18, 'Cg 24 D 1905 24\'\' Led Tv', 'cg-24-d-1905-24-led-tv', 'CG 24 D 1905 24\'\' LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">HD Ready</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bright Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">16.7M Colors</strong></li></ul>', 18790.00, '49466.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:48:40', '2019-01-03 17:34:43', NULL);
INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `product_code`, `product_color`, `description`, `price`, `image`, `status`, `featured`, `onsale`, `meta_title`, `meta_description`, `meta_content`, `meta_keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(56, 18, 'Lg 32lk526 (b)', 'lg-32lk526-b', '32LK526', 'black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">20W Powerful Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Protection Plus</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built In &amp; Upgradable Games</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">IPS Display</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Quick Access</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Live Zoom (To See Details)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">BollyWood mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cricket Mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Resolution Upscaler</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Color Enhancer</strong></li><li style=\"margin: 0px; padding: 0px;\"><br></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">&nbsp;</p>', 41990.00, '68636.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:50:58', '2019-01-03 11:50:58', NULL),
(57, 18, 'Cg 32 D 1905 32\'\' Led Tv', 'cg-32-d-1905-32-led-tv', 'CG 32 D 1905 32\'\' LED TV', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">HD Ready</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bright Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">16.7M Colors</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Full USB Support</strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 26990.00, '82228.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:51:55', '2019-01-03 11:53:07', NULL),
(58, 18, 'Cg32din08 32\'\' Led Tv', 'cg32din08-32-led-tv', 'CG32DIN08 32\'\' LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><b>Features:</b></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Picture Mode</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Sound Mode</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Color Temp</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Aspect Ratio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Noise Reduction</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bass/ Treble Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Vol Control (AVL)</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Surround Sound</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Equalizer</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Speaker ON/OFF</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\"><br></strong></li></ul>', 26990.00, '37948.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:56:01', '2019-01-03 11:56:01', NULL),
(59, 18, '32\" Led Tv Cg32d0003', '32-led-tv-cg32d0003', '32\" LED TV CG32D0003', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FRONT COVER</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">STAND BASE</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SCREEN SIZE</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">RESOLUTION</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">RESPONSE TIME</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SOUND OUTPUT(RMS)</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">HDMI</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">ANALOG AV TV</strong></li></ul>', 26690.00, '3418.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 11:58:33', '2019-01-03 11:58:33', NULL),
(60, 18, 'Cg 32 D 1604 S Smart Tv', 'cg-32-d-1604-s-smart-tv', 'CG 32 D 1604 S Smart TV', 'Black', '<div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px; text-align: center;\"><strong>CG 32 D 1604 SMART LED TV</strong></div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px; text-align: center;\">&nbsp;</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Screen Size : 32″</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Resolution : 1920*1080p</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Response time : 8ms</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">USB : Yes (Music+Photo+Movie)</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Analog AV Out : Yes</div><div style=\"color: rgb(51, 51, 51); font-family: Lato, sans-serif; font-size: 16px;\">Power Supply : 110~240V 50-60Hz</div>', 30790.00, '31863.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:02:45', '2019-01-03 12:02:45', NULL),
(61, 18, 'Cg 32 Dc 100 S Smart Led Tv', 'cg-32-dc-100-s-smart-led-tv', 'CG 32 DC 100 S SMART LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stunning Full HD Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dual Core processor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Narrow Bezel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stylish Stand</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\"><br></strong></li></ul>', 32290.00, '45372.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:06:53', '2019-01-03 12:06:53', NULL),
(62, 18, 'Cg 40 Din 09 S Smart Led Tv', 'cg-40-din-09-s-smart-led-tv', 'CG 40 DIN 09 S Smart Led Tv', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">HD Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Google Play Store</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB to USB Copy Function</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Miracase- Wireless Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Core Processor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Thin Bezel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Slim TV</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bright Panel</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">WiFI</strong></li></ul>', 48190.00, '77135.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:10:42', '2019-01-03 12:10:42', NULL),
(63, 18, 'Cg 43 D 9904 Smart Tv', 'cg-43-d-9904-smart-tv', 'CG 43 D 9904 Smart TV', 'Black', '<ul style=\"margin-bottom: 10px; padding: 0px; list-style: none; font-size: 14px; color: rgb(102, 102, 102); font-family: Roboto;\"><li style=\"margin-top: 0px; margin-bottom: 1rem;\">Brand Name: CG</li><li style=\"margin-top: 0px; margin-bottom: 1rem;\">Type:LED TV</li><li style=\"margin-top: 0px; margin-bottom: 1rem;\">Screen Size : 43 Inch</li><li style=\"margin-top: 0px; margin-bottom: 1rem;\">Type: Normal</li><li style=\"margin-top: 0px; margin-bottom: 1rem;\">1 year warranty</li></ul>', 51390.00, '2614.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:15:48', '2019-01-03 12:15:48', NULL),
(64, 18, 'Cg 43 Dn 407 Led Tv', 'cg-43-dn-407-led-tv', 'CG 43 DN 407 LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stunning Full HD Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Contrast</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Backlit Adjustable</strong></li></ul>', 44990.00, '81768.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:21:14', '2019-01-03 12:21:14', NULL),
(65, 18, 'Cg 43 Dc 100 S Smart Led Tv', 'cg-43-dc-100-s-smart-led-tv', 'CG 43 DC 100 S SMART LED TV', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stunning Full HD Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Contrast</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Backlit Adjustable</strong></li></ul>', 51890.00, '73466.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:25:28', '2019-01-03 12:25:28', NULL),
(66, 18, 'Cg 43 D 9904 S Smart Led Tv', 'cg-43-d-9904-s-smart-led-tv', 'CG 43 D 9904 S SMART LED TV', 'Black', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\"><br></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\">Resolution : 1920*1080p</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\">Response time : 8ms</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\">USB : Yes (Music+Photo+Movie)</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\">Analog AV Out : Yes</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; line-height: 1.8em; font-size: 14px; color: rgb(153, 153, 153); font-family: Lato, sans-serif;\">Power Supply : 110~240V 50-60Hz</p>', 55790.00, '94959.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:31:44', '2019-01-03 12:31:44', NULL),
(67, 18, 'Cg 49 D 1004 49\" Led Tv', 'cg-49-d-1004-49-led-tv', 'CG 49 D 1004', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">A+ Panel&nbsp;</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Zero Bright Dot</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">16.7M Colors</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide View Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Full USB Support</strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 64790.00, '59810.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:39:24', '2019-01-03 12:39:24', NULL),
(68, 18, 'Cg 43 Dc 200 U 43\" 4k Smart Led Tv', 'cg-43-dc-200-u-43-4k-smart-led-tv', 'CG 43 DC 200 U', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">4K UltraHD</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dual Core Processor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bezel Less</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Ultra Slim</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">MHL Compatible</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stylish Metal Bridge Stan</strong>d</li></ul>', 58690.00, '71897.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:47:39', '2019-01-03 12:47:39', NULL),
(69, 18, 'Cg 49 Dc 100 S 49\" Smart Led Tv', 'cg-49-dc-100-s-49-smart-led-tv', 'CG 49 DC 100 S', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:<br>Stunning Full HD Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Wide Viewing Angle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Contrast</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Backlit Adjustable</strong></li></ul>', 64190.00, '2372.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:49:19', '2019-01-03 12:49:19', NULL),
(70, 18, 'Lg 32lj523d Led Tv', 'lg-32lj523d-led-tv', '32LJ523D', 'grey', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">IPS Panel</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Color Master Engine</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">20W Powerful Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dolby Digital + DTS Codec Support</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Safety</strong></li></ul>', 51590.00, '20820.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:51:03', '2019-01-03 12:51:03', NULL),
(71, 18, '4k Ultra Hd Resolution', '4k-ultra-hd-resolution', 'CG 49 DC 200 U', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">ART Design</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Perfect Picture Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Perfect Sound Quality</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Function</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Multiple Connectivity</strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 71690.00, '96987.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:54:40', '2019-01-03 12:54:40', NULL),
(72, 38, 'Lg Washing Machine 14.0kg', 'lg-washing-machine-140kg', 'F2514NTGW', 'White', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">FAST AND CLEAN WITH TURBOWASH™</strong><br style=\"margin: 0px; padding: 0px;\">TurboWash™ integrates the Jet Spray feature to help significantly reduce total washing times, create impressive energy savings and deliver cleaner clothes. Spraying water directly onto fabrics for approximately 120 seconds, the Jet Spray enhances the exceptional washing and rinsing performance of 6 Motion Direct Drive technology and enables TurboWash™ to fast forward an entire washing cycle.&nbsp;<br style=\"margin: 0px; padding: 0px;\">* Tested by Intertek ; Cotton Cycle with TurboWash™ option is finished within 59 ± 5% minutes.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">TURBOWASH™ Efficacy</strong><br style=\"margin: 0px; padding: 0px;\">TurboWash™ enables not only regular washing program to finish in 59 minutes, also create impressive energy savings and maintain washing performance. LG Turbowash™ reduces energy consumption up to 15 percent and water consumption up to 40 percent. Faster, clean, lower water and electricity bills.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">* Tested by Intertek ; Based on Cotton cycle/40℃/1400rpm, comparison between ‘with TurboWash™’ and ‘without TurboWash™’.<br style=\"margin: 0px; padding: 0px;\">** The washing performance result within ±10% and the rinsing performance result within ±5% against Cotton cycle without TurboWash™ option.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">6 MOTION DD, MORE MOTION BETTER CARE</strong><br style=\"margin: 0px; padding: 0px;\">Due to its design and unique to LG, 6 Motion Direct Drive is able to perform various drum motions or a combination of different motions depending on the wash programme selected. Combined with a controlled spin speed and the ability of the drum to rotate both left and right, the wash performance of the machine is greatly improved, giving you perfect results every time.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">INVERTER DIRECT DRIVE™</strong><br style=\"margin: 0px; padding: 0px;\">The direct drive motor that runs our washing machines is reliable as well as quiet. We guarantee it won’t let you down. In fact, we trust it so much that all machines come with a 10 year warranty on the motor and all its parts as standard – even though there’s nothing standard about it.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">No belt &amp; Pulley&nbsp;<br style=\"margin: 0px; padding: 0px;\">-Less noise&nbsp;<br style=\"margin: 0px; padding: 0px;\">-Energy Saving&nbsp;<br style=\"margin: 0px; padding: 0px;\">-Capacity&nbsp;<br style=\"margin: 0px; padding: 0px;\">-Durability</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">SMART CONVENIENCE WITH NFC</strong><br style=\"margin: 0px; padding: 0px;\">Tag On<br style=\"margin: 0px; padding: 0px;\">Using NFC tagging technology, the user can download new washing programs such as Wool, Baby Care and Cold Wash. Then, they can then apply these programs simply by touching their smartphone to the NFC Tag On symbol on the washing machine.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">*NFC<br style=\"margin: 0px; padding: 0px;\">Near field communication (NFC) is a set of standards for smartphones and similar devices to establish radio communication with each other by touching them together or bringing them into proximity, usually no more than a few inches.</p>', 168090.00, '79411.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 12:58:43', '2019-01-03 12:58:43', NULL),
(73, 18, '55\" 4k Smart Uhd Led Tv', '55-4k-smart-uhd-led-tv', 'CG 55 D 1004 U', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">4K Ultra HD</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dual Core Processor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Kitkat Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">1 GB DDR3 RAM</strong></li></ul>', 95190.00, '47056.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:00:52', '2019-01-03 13:00:52', NULL),
(74, 18, 'Lg 32\" Mosquito Away Led Tv', 'lg-32-mosquito-away-led-tv', 'Lg 32lj525D', 'grey', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Mosquito Away</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">IPS Panel</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Color Master Engine</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">PMI 300</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">20W Powerful Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Safety</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Live Zoom(To see Details)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Games make anytime more entertaining...</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cricket Mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Audio Out</strong></li></ul>', 57490.00, '53834.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:04:30', '2019-01-03 13:07:41', NULL),
(75, 18, '55\" 4k Smar Led', '55-4k-smar-led', 'CG 55 DC 200 U', 'Black', '<ol><li style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Features:</li><li style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">4K UltraHD</li><li style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Dual Core Processor</li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Android Platform</li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Bezel Less</li><li style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Ultra Slim</li><li style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">MHL Compatible</li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">Stylish Metal Bridge Stand</li><li class=\"\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i3.30e044af3wPSGX\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li><li class=\"\" data-spm-anchor-id=\"a2a0e.pdp.product_detail.i2.30e044af3wPSGX\" style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li><li style=\"line-height: 18px; margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\"><br></li></ol>', 87090.00, '99910.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:06:49', '2019-01-03 13:06:49', NULL),
(76, 18, '65\" 4k Smart Led Tv', '65-4k-smart-led-tv', 'CG 65 DC 200 U', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">4K UltraHD</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Dual Core Processor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Android Platform</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Bezel Less</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Ultra Slim</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">MHL Compatible</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Stylish Metal Bridge Stan</strong>d</li></ul>', 136690.00, '72550.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:09:42', '2019-01-03 13:09:42', NULL),
(77, 38, 'Twinwash Mini', 'twinwash-mini', 'T2735NTWV', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><h2 style=\"margin: 0px 0px 5px; padding: 0px; font-family: inherit; font-weight: bold; line-height: 1.35; color: rgb(51, 62, 72); font-size: 15px; text-transform: uppercase;\">QUICK OVERVIEW</h2><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Convenience</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Right Size Capacity</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Direct Drive Motor 10 Year Warranty</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Magnetic Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Specialty Cycle</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Sold in bundle with TwinWash WM&nbsp;<a href=\"https://cgdigital.com.np/product-twinwash-washing-machine-56\" style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); transition: all 0.5s ease 0s;\">F2721STWV</a></strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 464890.00, '1805.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:12:00', '2019-01-03 13:12:00', NULL),
(78, 18, 'Cg 86 D 8900 S Smart Tv', 'cg-86-d-8900-s-smart-tv', 'CG 86 D 8900 S Smart Tv', 'Black', '<p>Features:</p><ul><li>App store&nbsp;</li><li>Games&nbsp;</li><li>Universal Search&nbsp;</li><li>Media Player&nbsp;</li><li>Music Streaming etc.</li></ul>', 665490.00, '99298.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:21:02', '2019-01-03 13:21:02', NULL),
(79, 39, 'Cg 1400 Watt Vacuum Cleaner Cg-vc14f01', 'cg-1400-watt-vacuum-cleaner-cg-vc14f01', 'CG-VC14F01', 'Multi color', '<p><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">Features:</span></p><p><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">5 Stage Filter</span><br style=\"margin: 0px 0px 5px; padding: 0px; color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\"><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">Metal Telescopic Tube</span><br style=\"margin: 0px 0px 5px; padding: 0px; color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\"><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">Overheat Protection</span><br style=\"margin: 0px 0px 5px; padding: 0px; color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\"><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">Speed Control</span><br style=\"margin: 0px 0px 5px; padding: 0px; color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\"><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">Dust Full Indicator</span><br style=\"margin: 0px 0px 5px; padding: 0px; color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\"><span style=\"color: rgb(102, 102, 102); font-family: GothamBookRegular, Arial; font-size: 14px; background-color: rgb(250, 250, 250);\">5 Meter Supply Cord</span><br></p>', 7090.00, '96155.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:26:23', '2019-01-03 13:26:23', NULL),
(80, 38, 'Washing Machine 8.0 Kg', 'washing-machine-80-kg', 'T2108VSAR', 'Red', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Energy Saving with Smart Inverter Control</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Restart</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Stand-by Power Save</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Motion</strong><ul style=\"margin-right: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Agitating</li><li style=\"margin: 0px; padding: 0px;\">Rotating</li><li style=\"margin: 0px; padding: 0px;\">Swing</li></ul></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">TurboDrumTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Punch+3</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Pre Wash</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Side Waterfall</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">LoDecibel&nbsp;&amp; Less Vibration</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart DiagnosisTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Safe &amp; Convenient Design</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Child Lock</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Tub Clean</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Delay Start</strong></li></ul>', 48990.00, '35611.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:29:11', '2019-01-03 13:29:11', NULL),
(81, 39, 'Cg Vacuum Cleaner 1600 W Max Bagless Cg-vc16kb01', 'cg-vacuum-cleaner-1600-w-max-bagless-cg-vc16kb01', 'CG-VC16KB01', 'Multi color', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\"><span style=\"font-weight: 700;\">CG Vacuum Cleaner 1600 W Max &nbsp;Bagless CG-VC16KB01</span></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">Brand CG&nbsp;<br>Max Power (Watt) 1600W&nbsp;<br>Dust Indicator Yes&nbsp;<br>Auto Cord Rewind Yes&nbsp;<br>360 Deg Swivel Hose Yes<br>Speed Control&nbsp;<br>Upholstery Nozzle&nbsp;<br>Wet &amp; Dry Function<br>Ergonomic Folded Handle<br>Waterproof Power Switch<br>Independent Dust Control<br>System<br>4 x 360° Rotated Wheels for<br>easy Moving<br>21 ltr dust collection</p>', 7790.00, '50377.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:29:11', '2019-01-03 13:29:11', NULL),
(82, 39, 'Cg 1800 Watt Vaccum Cleaner Cg-vc18d01', 'cg-1800-watt-vaccum-cleaner-cg-vc18d01', 'CG-VC18D01', 'Multi color', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\"><span style=\"font-weight: 700;\">CG Vacuum Cleaner 1600W CG-VC18D01&nbsp;</span></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">&nbsp;</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">• Color : Blue/ Red<br>• 360 Swivel Hose<br>• 5 Stage filter<br>• Metal Telescopic Tube</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">• Overheat Protection<br>• Speed Control<br>• Dust Full Indicator<br>• 5 Meter Supply Cord</p>', 8290.00, '39964.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:32:02', '2019-01-03 13:54:45', NULL),
(83, 39, 'Cg Vaccum Cleaner Vc18hb01', 'cg-vaccum-cleaner-vc18hb01', 'VC18HB01', 'MULTI COLOR', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"><li style=\"margin: 0px; padding: 0px;\">Features:<br>Metallic Finish</li><li style=\"margin: 0px; padding: 0px;\">Hepa Filter</li><li style=\"margin: 0px; padding: 0px;\">Dust Full Indicator</li><li style=\"margin: 0px; padding: 0px;\">Speed Control</li><li style=\"margin: 0px; padding: 0px;\">5 Stage Filter</li><li style=\"margin: 0px; padding: 0px;\">Metal Telescopic Tube</li><li style=\"margin: 0px; padding: 0px;\">Overheat Protection</li><li data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.2dd86454xN4ypl\" style=\"margin: 0px; padding: 0px;\">5 Meter Supply Cord</li></ul>', 8690.00, '64424.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:35:59', '2019-01-03 13:54:49', NULL),
(84, 18, 'Lg43 Inch Led Tv', 'lg43-inch-led-tv', '43lj523T', 'grey', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">IPS Panel</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dolby Digital + DTS Codec Support</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Color Master Engine</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">20W Powerful Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Safety</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Live Zoom(To see Details)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Virtual Surround sound spreads out the space</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Games make your TV more entertaining</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Regional Language Options</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cricket Mode</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Audio Out</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">PMI 300</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Energy Saving</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Simplink (HDMI CEC)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built - In Games</strong></li></ul>', 74990.00, '17222.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:36:35', '2019-01-03 13:54:53', NULL);
INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `product_code`, `product_color`, `description`, `price`, `image`, `status`, `featured`, `onsale`, `meta_title`, `meta_description`, `meta_content`, `meta_keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(85, 39, 'Cg Vacuum Cleaner 2000w Drum Type Vc20td01 - Black', 'cg-vacuum-cleaner-2000w-drum-type-vc20td01-black', 'VC20TD01', 'Black', '<p><span style=\"margin: 0px; padding: 0px; font-weight: bolder; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\">Features</span></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"><li style=\"margin: 0px; padding: 0px;\">Wet &amp; Dry Function</li><li style=\"margin: 0px; padding: 0px;\">Waterproof Power Switch</li><li style=\"margin: 0px; padding: 0px;\">Washable Dust Bag</li><li style=\"margin: 0px; padding: 0px;\">Blower Option</li><li style=\"margin: 0px; padding: 0px;\">Independent Dust Control System</li><li data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.79e738570MZAZA\" style=\"margin: 0px; padding: 0px;\">4 x 360 Degree Rotated Wheels for easy Moving</li></ul>', 11090.00, '73135.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:38:55', '2019-01-03 13:54:52', NULL),
(86, 39, 'Cg Vacuum Cleaner 2200 Watt Bag Type Vc22e01', 'cg-vacuum-cleaner-2200-watt-bag-type-vc22e01', 'CG-VC22E01', 'BLACK', '<p><span style=\"margin: 0px; padding: 0px; font-weight: bolder; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\">**SPECIFIATIONS**</span><span style=\"margin: 0px; padding: 0px; font-weight: bolder; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\">General</span></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"><li style=\"margin: 0px; padding: 0px;\">Brand: CG</li><li style=\"margin: 0px; padding: 0px;\">Model: VC22E01</li><li style=\"margin: 0px; padding: 0px;\">Color: Black/Orange</li><li style=\"margin: 0px; padding: 0px;\">Bag: Yes</li><li style=\"margin: 0px; padding: 0px;\">Maximum Power: 2200 Watt</li><li style=\"margin: 0px; padding: 0px;\">5 Stage Filter</li><li style=\"margin: 0px; padding: 0px;\">5 Meter Suppli Cord</li><li style=\"margin: 0px; padding: 0px;\">Power Supply: 220-240V AC 50 Hz</li></ul><p><span style=\"margin: 0px; padding: 0px; font-weight: bolder; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\">Features</span></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"><li style=\"margin: 0px; padding: 0px;\">High Suction Power</li><li style=\"margin: 0px; padding: 0px;\">Hepa Filter</li><li style=\"margin: 0px; padding: 0px;\">Dust Full Indicator</li><li style=\"margin: 0px; padding: 0px;\">Speed Control</li><li style=\"margin: 0px; padding: 0px;\">5 Stage Filter</li><li style=\"margin: 0px; padding: 0px;\">Metal Telescopic Tube</li><li style=\"margin: 0px; padding: 0px;\">5 Meter Supply Cord</li></ul>', 9390.00, '8763.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:45:02', '2019-01-03 13:54:55', NULL),
(87, 40, 'Cg-mw20a01s 20 Ltr. Solo Microwave Oven', 'cg-mw20a01s-20-ltr-solo-microwave-oven', 'CG-MW20A01S', 'Black', '<p><span class=\"heading\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 700; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; display: inline-block;\">HIGHLIGHTS</span><span style=\"font-family: Roboto, sans-serif; font-size: medium;\"></span></p><ul style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; list-style: none; float: left; width: 456.141px;\"><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Digital clock %7C Digital timer</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Digital control</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Express cooking %7C Child safety lock</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Defrost %7C End cooking signal</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Glass turntable</li></ul>', 9690.00, '78481.JPG', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:49:19', '2019-01-03 13:54:57', NULL),
(88, 40, 'Cg-mw25b01g 25 Ltr. Grill Microwave Oven', 'cg-mw25b01g-25-ltr-grill-microwave-oven', 'CG-MW25B01G', 'Black', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\"><span style=\"font-weight: 700;\">CG Microwave Oven 25 Ltr. CG-MW25B01G</span></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">Express Cooking Yes<br>Brand CG<br>Output Power 900 W<br>Capacity (Ltr.) 25 Ltrs<br>Mirror Look No<br>Control (Manual/Digital) Digital<br>Auto Cook Menus 10<br>Auto Defrost No<br>No. of Power Levels 10</p>', 13999.00, '97885.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:51:56', '2019-01-03 13:54:42', NULL),
(89, 38, 'Washing Machine 9.0 Kg', 'washing-machine-90-kg', 'T2109VSAL', 'Silver', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Energy Saving with Smart Inverter™ Control</strong><br style=\"margin: 0px; padding: 0px;\">Smart Inverter™ Technology eliminates wasted operation by efficiently controlling energy use.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Restart</strong><br style=\"margin: 0px; padding: 0px;\">When a washing machine is turned off due to power failure, it will restart automatically from the position it stopped to adjust accordingly.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Standby Power Save</strong><br style=\"margin: 0px; padding: 0px;\">Even if the power cord is plugged in while power is off, only extremely little electricity will run through the washer. You don’t need to worry about wasted electricity.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Motion</strong><br style=\"margin: 0px; padding: 0px;\">Smart Motion is 3 kinds of motions created by Smart Inverter™ for optimized washing by fabric type. Enjoy better combination for better care.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">TurboDrum™</strong><br style=\"margin: 0px; padding: 0px;\">TurboDrum™ enables the most powerful wash and removes the toughest dirt through strong water stream of rotating drum and pulsator in the opposite direction.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Punch+3</strong><br style=\"margin: 0px; padding: 0px;\">Punch+3 creates powerful streams of water which mix laundry up and down repeatedly for even washing result.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Pre Wash</strong><br style=\"margin: 0px; padding: 0px;\">With one touch, tough stains are ready to be gone. Let your hands free, let your washing machine do the laundry.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Side WaterfallSmart Diagnosis™</strong><br style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™ helps diagnose and troubleshoot mechanical issues, limiting costly and inconvenient service visits<br style=\"margin: 0px; padding: 0px;\">Side Waterfall enables the best mixing of detergent with water while minimizing detergent residue that can cause skin irritation and allergy.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">LoDecibel™ &amp; Less Vibration</strong><br style=\"margin: 0px; padding: 0px;\">BMC Motor Protection holds the motor tightly so that it can minimize the noise and vibration level. Plus more durability and 10 year motor warranty.</p>', 59890.00, '77977.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:53:27', '2019-01-03 13:54:41', NULL),
(90, 40, 'Cg-mw30c01c 30ltr. Convection Microwave Oven', 'cg-mw30c01c-30ltr-convection-microwave-oven', 'CG-MW30C01C', 'BLACK', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\"><span style=\"font-weight: 700;\">CG CONVECTION 30 Ltr. CG-MW30C01C</span></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(111, 111, 111); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;\">Output Power: 900 W<br>Express Cooking: Yes<br>Brand: CG<br>Capacity (Ltr.): 30 Ltrs<br>Mirror Look: No<br>Control (Manual/Digital): Digital<br>Auto Cook Menus: 10<br>Auto Defrost: Yes<br>No. of Power Levels: 5</p>', 19890.00, '72811.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 13:54:31', '2019-01-03 13:54:40', NULL),
(91, 38, 'Washing Machine 6.0 Kg', 'washing-machine-60-kg', 'F1006NMTS', 'Silver', '<h2 style=\"margin: 0px 0px 5px; padding: 0px; font-family: Arimo, sans-serif; font-weight: bold; line-height: 1.35; color: rgb(51, 62, 72); font-size: 15px; text-transform: uppercase;\">QUICK OVERVIEW</h2><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter DD for a Powerful Wash with Less Noise</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">OPTIMAL WASH for fabrics with 6motion DD</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FULL TOUCH CONTROL</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Award and Proven</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™</strong></li></ul>', 64790.00, '19512.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:06:05', '2019-01-03 15:06:05', NULL),
(92, 18, 'Lg 43\"lj525led Tv', 'lg-43lj525led-tv', 'Lg43lj525t', 'grey black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Mosquito Away Technology</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">IPS Panel</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Color Master Engine</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">PMI 300</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">20W Powerful Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">All Round Safety</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Live Zoom(To see details)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Virtual Surround sound spreads out the space</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Built-in Games make anytime more entertaining...</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cricket Mode</strong></li></ul>', 76990.00, '35005.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:08:34', '2019-01-03 15:08:34', NULL),
(93, 42, 'Trolley Speaker 55w', 'trolley-speaker-55w', 'CGTS12A01', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 13490.00, '75552.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:11:07', '2019-01-03 15:11:07', NULL),
(94, 42, 'Trolley Speaker 60w', 'trolley-speaker-60w', 'CGTS12A02', 'Black', '<div class=\"short-description\" style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></div><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 15090.00, '33592.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:13:07', '2019-01-03 15:13:07', NULL),
(95, 38, 'Washing Machine 6.5 Kg', 'washing-machine-65-kg', 'F1265NMTS', 'White', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">6 Motion Direct Drive</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Fault Diagnosis (Digital LED Display)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Remaining Time Display</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Trio-Chrome Door Type</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Rinse Hold</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Balance</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Load Detect</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Foam Sensing &amp; Removal</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Restart</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Beeper On/Off</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Fuzzy Logic</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Pre Wash</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Crease Care</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Child Lock</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Time Delay 3~19 Hrs</strong></li></ul>', 70690.00, '13340.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:14:19', '2019-01-03 15:14:19', NULL),
(96, 42, 'Trolley Speaker With Digital 60w', 'trolley-speaker-with-digital-60w', 'CGTS12E01', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li></ul>', 16390.00, '33034.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:18:30', '2019-01-03 15:18:30', NULL),
(97, 42, 'Double Trolley Speaker', 'double-trolley-speaker', 'CGTS12B01D', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li></ul>', 17890.00, '84016.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:20:17', '2019-01-03 15:20:17', NULL),
(98, 42, 'Cg Ts12b02t 50w+30wx2 Trolley Speaker - (black)', 'cg-ts12b02t-50w30wx2-trolley-speaker-black', 'CG TS12B02T', 'Black', '<p><span style=\"margin: 0px; padding: 0px; font-weight: bolder; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\">FEATURES</span><br style=\"margin: 0px; padding: 0px; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; font-family: Roboto-Regular, &quot;Helvetica Neue&quot;, Helvetica, Tahoma, Arial, sans-serif; font-size: 12px;\"><li style=\"margin: 0px; padding: 0px;\">LED Display</li><li style=\"margin: 0px; padding: 0px;\">USB Disk</li><li style=\"margin: 0px; padding: 0px;\">SD/MMC Card</li><li style=\"margin: 0px; padding: 0px;\">FM Radio</li><li style=\"margin: 0px; padding: 0px;\">Remote Control</li><li style=\"margin: 0px; padding: 0px;\">12\" + (8\"x2) Woofer</li><li style=\"margin: 0px; padding: 0px;\">Sound Output: 50W+(30Wx2)</li><li style=\"margin: 0px; padding: 0px;\">Frequency Response: 35 Hz - 20 Khz</li><li style=\"margin: 0px; padding: 0px;\">Bluetooth</li><li style=\"margin: 0px; padding: 0px;\">Guitar Input</li><li style=\"margin: 0px; padding: 0px;\">Disco Light</li><li style=\"margin: 0px; padding: 0px;\">Mic Input</li><li style=\"margin: 0px; padding: 0px;\">Rechargeable Battery</li><li style=\"margin: 0px; padding: 0px;\">2 HiFi Mic</li><li style=\"margin: 0px; padding: 0px;\">Karaoke</li><li data-spm-anchor-id=\"a2a0e.pdp.product_detail.i0.59b24872aWevqy\" style=\"margin: 0px; padding: 0px;\">Equalizer</li><li data-spm-anchor-id=\"a2a0e.pdp.product_detail.i1.59b24872aWevqy\" style=\"margin: 0px; padding: 0px;\">Audio Output</li></ul>', 19890.00, '24090.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:23:22', '2019-01-03 15:23:22', NULL),
(99, 42, 'Trolley Speaker 120 W', 'trolley-speaker-120-w', 'CG-TS15B01D', 'Black', '<p>Features:&nbsp;</p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\"><br></strong></li></ul>', 24390.00, '70476.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:25:51', '2019-01-03 15:25:51', NULL),
(100, 42, 'Trolley Speaker 80 W', 'trolley-speaker-80-w', 'CGTS15E01', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Features:</strong></li></ul><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></p>', 22390.00, '63388.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:27:50', '2019-01-03 15:27:50', NULL),
(101, 38, 'Washing Machine 7.0 Kg', 'washing-machine-70-kg', 'F1207NMTW', 'Silver', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">LESS VIBRATION,LESS NOISE</strong><br style=\"margin: 0px; padding: 0px;\">The direct drive motor that powers our washing machines is super reliable and<br style=\"margin: 0px; padding: 0px;\">really quiet. We know it is one of the best wash machine motors on the market,<br style=\"margin: 0px; padding: 0px;\">which is why all our machines come with a standard 10-year warranty on the motor<br style=\"margin: 0px; padding: 0px;\">and parts. Nothing standard about that now, is there?</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">OPTIMAL WASH for fabrics with 6 Motion DD</strong><br style=\"margin: 0px; padding: 0px;\">Select a wash program and 6 Motion Direct Drive technology moves the wash drum in<br style=\"margin: 0px; padding: 0px;\">multiple directions, giving fabrics the proper care while getting clothes ultra clean.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">FULL TOUCH CONTROL</strong><br style=\"margin: 0px; padding: 0px;\">New full touch control boast a seamless and sleek design that brings<br style=\"margin: 0px; padding: 0px;\">a touch of sophistication to the home. Each model in the lineup<br style=\"margin: 0px; padding: 0px;\">offers a full touch control panel angled for maximum visibility.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter Control</strong><br style=\"margin: 0px; padding: 0px;\">Inverter Control System generates less speed fluctuations and consumes the exact amount of electricity needed at every step. This not only saves you water and energy but also ensures optimum washing performance.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis System</strong><br style=\"margin: 0px; padding: 0px;\">At some point in your washer’s life it will need some love and care. We just made the service easier and smarter for you. Now there is no need to wait for an engineer to detect a problem. Just dial smart diagnosis and wait for the telephone to tell you what to do or for a single visit by the service engineer with the solution.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Rinse Hold</strong><br style=\"margin: 0px; padding: 0px;\">After a rinse cycle, clothes stay suspended in the water without entering into the spin mode. The rinse hold cycle gives the option of an additional rinse cycle to release the rinse hold before entering the spin cycle. It prevents foul smell thereby maintaining the freshness of the clothes.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Baby Care&nbsp;</strong><br style=\"margin: 0px; padding: 0px;\">With the baby care feature the temperature reached upto 95 degree celsius, all enzymes &amp; bacteria are removed,also the gentle motor takes care of the quality of the clothes.Also no detergent residue remains in the clothes.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Tub Clean&nbsp;</strong><br style=\"margin: 0px; padding: 0px;\">Constant use of the washing machine results in the development of fungus and toxic substances in the tub area of the ordinary washing machine. The Tub Clean function in the LG Steam Washer Dryer uses hot steam with high spin speed to sterlize every corner of the tub. The tub remains disinfected, &amp; so do the clothes.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Pre Wash&nbsp;</strong><br style=\"margin: 0px; padding: 0px;\">The clothes are given a preliminary wash and dirty water is drained out to removes the toughest of the stains &amp; dirt.</p>', 73490.00, '51999.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:29:11', '2019-01-03 15:29:11', NULL),
(102, 18, 'Lg 43\" Smart Led Tv', 'lg-43-smart-led-tv', 'lg 43lk5700', 'grey', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Active HDR for a lifelike scene</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Color</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Full HD 1080P</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Immersive home entertainment</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">The core of authentic image</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Virtual Surround Sound</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Sophesticated Inside and Out</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Simple ways for your convenience</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Resolution Upscaler</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dynamic Color Enhancer</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dolby Digital Decoder</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Clear Voice III</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">webOS 4.0</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Full Web Browser</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Game World</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Live Zoom</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Magic Mobile Connection</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">MiracastTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Intel Widi</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Motion Eco Sensor</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Intelligent Sensor</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Simplink</strong></li></ul>', 92990.00, '99451.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:29:18', '2019-01-03 15:29:18', NULL),
(103, 42, 'Trolley Speaker', 'trolley-speaker', 'CGTS15A01', 'Black', '<ul><li style=\"margin: 0px 0px 15px; padding: 12px 0px 0px; border-top: 1px solid rgb(229, 229, 229);\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><b>Features:</b></li><ul><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disc</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Guitar Input</strong></li></ul></ul><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px;\"></p></li></ul><div class=\"product-variation\" style=\"margin: 0px 0px 15px; padding: 15px 0px; display: inline-block; width: 616.594px; border-top: 1px solid rgb(229, 229, 229); border-bottom: 1px solid rgb(229, 229, 229);\"><form action=\"https://cgdigital.com.np/cart\" method=\"post\" class=\"ng-pristine ng-valid\" style=\"margin: 0px; padding: 0px; display: inline;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></span><div class=\"cart-plus-minus\" style=\"margin: 0px 18px 0px 0px; padding: 0px; display: inline-block; float: left; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"></div></form></div><div class=\"product-color-size-area\" style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; display: inline-block; width: 616.594px;\"></div>', 17590.00, '82973.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:30:51', '2019-01-03 15:30:51', NULL),
(104, 42, 'Cg 15 Inch Trolley Speaker Big Cabinet Cgts15f01', 'cg-15-inch-trolley-speaker-big-cabinet-cgts15f01', 'CGTS15F01', 'Black', '<p><span class=\"heading\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 700; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; display: inline-block;\">HIGHLIGHTS</span><span style=\"font-family: Roboto, sans-serif; font-size: medium;\"></span></p><ul style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; list-style: none; float: left; width: 456.141px;\"><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">15%22 Woofer %7C LED display %7C 2 Wireless Mic</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">USB disk %7C Bluetooth %7C Karaoke</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">SD%2FMMCard %7C Frequency Response%3A 40Hz - 20KHz</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">5 Band Equalizer %7C Rechargeable Battery</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Guitar Input %7C Mic Input</li></ul>', 19490.00, '57961.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:38:01', '2019-01-03 15:38:01', NULL),
(105, 18, 'Lg 43\" Smart Led Tv 43lj550', 'lg-43-smart-led-tv-43lj550', 'Lg43lj550', 'grey', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">webOS 3.5 simple and fun to use</strong><br style=\"margin: 0px; padding: 0px;\">With the uniquely refined webOS 3.5 launcher bar, dive into a limitless world of premium content from all of the most popular entertainment providers.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Enriches all colors to previously unseen vibrancy</strong><br style=\"margin: 0px; padding: 0px;\">LG TV’s unique process not only enhances colors, but also adjusts color saturation, hue and luminance. Colors come alive with more intensity and depth.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Full HD revolutionizing image clarity and color</strong><br style=\"margin: 0px; padding: 0px;\">With high resolution that doubles the picture definition of HD TVs, Full HD 1080p displays richer color and minute details for a more enjoyable viewing experience.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Improve any image with Resolution Upscaler</strong><br style=\"margin: 0px; padding: 0px;\">Enjoy any image with LG’s Resolution Upscaler, which enhances and optimizes the image no matter thel picture quality of the image.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Be amused with Virtual Surround Plus</strong><br style=\"margin: 0px; padding: 0px;\">Virtual Surround Plus distinctively improves sound directionality for an immersive listening experience. The sound performance makes you feel as if you are at a concert or recording studio.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Plug in a USB drive for content</strong><br style=\"margin: 0px; padding: 0px;\">Simply insert a USB stick or external hard drive containing videos, photos and music into the USB port to play and view the files.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Enjoy top-quality AV over a single HDMI cable</strong><br style=\"margin: 0px; padding: 0px;\">HDMI (high-definition multimedia interface) is a modern interface standard that allows a variety of audio and video signals to be transmitted over a single cable connecting your LG Full HD TV to other devices.</p>', 92990.00, '2375.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:40:06', '2019-01-03 15:40:06', NULL),
(106, 38, 'Washing Machine Fc1408s3e', 'washing-machine-fc1408s3e', 'FC1408S3E', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Less vibration, Less noise</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Combat allergens with the power of Steam</strong><ul style=\"margin-right: 0px; margin-left: 0px; padding: 0px;\"><li style=\"margin: 0px; padding: 0px;\">Allergen Sanitization</li><li style=\"margin: 0px; padding: 0px;\">Allergen Dissolution</li><li style=\"margin: 0px; padding: 0px;\">Allergen Removal</li></ul></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Steam Refresh</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Steam SoftenerTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Add forgotten items mid-cycle</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">OPTIMAL WASH for fabrics with 6 Motion DD</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Convenience with SmartThinQTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Remote Control</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Cycle Download</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis</strong></li></ul>', 88990.00, '2878.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:41:00', '2019-01-03 15:41:00', NULL);
INSERT INTO `products` (`id`, `category_id`, `product_name`, `slug`, `product_code`, `product_color`, `description`, `price`, `image`, `status`, `featured`, `onsale`, `meta_title`, `meta_description`, `meta_content`, `meta_keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(107, 41, 'Cg Speaker Cga2061', 'cg-speaker-cga2061', 'CG-A2061', 'Black', '<p><span class=\"heading\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 700; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; display: inline-block;\">HIGHLIGHTS</span><span style=\"font-family: Roboto, sans-serif; font-size: medium;\"></span></p><ul style=\"margin: 20px 0px 5px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: medium; line-height: inherit; vertical-align: baseline; font-family: Roboto, sans-serif; overflow-wrap: break-word; text-size-adjust: 100%; list-style: none; float: left; width: 456.141px;\"><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Warranty: 1 year</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Sound output (RMS): 25W+10W*2</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">FM only</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">Wooden cabinet</li><li style=\"margin: 0px 0px 9px; padding: 0px 0px 0px 15px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; vertical-align: baseline; overflow-wrap: break-word; text-size-adjust: 100%; color: rgb(51, 51, 51); float: left; width: 456.141px; position: relative;\">USB/SD card read</li></ul>', 4690.00, '62088.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:44:00', '2019-01-03 15:44:00', NULL),
(108, 41, '2.1 Multimedia Speaker', '21-multimedia-speaker', 'CGA2071', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Highlights:</strong></li></ul><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">LED Display</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">USB Disk</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">SD/MMC Card</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">FM Radio</strong></p><p style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Remote Control</strong></p>', 4390.00, '36637.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:46:06', '2019-01-03 15:46:06', NULL),
(109, 38, 'Commercial Washing Machine 10.2 Kg', 'commercial-washing-machine-102-kg', 'F1069FD3PS', 'Silver', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter DD Motor</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi heat system</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Premium coated material body</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Forced Drain System</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dual Lock System</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Top Mounted Dispenser</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Multiple damper vibration reduction</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Tempered glass</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Atomizing system</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Customized program</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Quick wash (34 min.)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Steel Control panel</strong></li></ul>', 345790.00, '53637.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:49:10', '2019-01-03 15:49:10', NULL),
(110, 41, 'Lg Dvd Home Theatre System', 'lg-dvd-home-theatre-system', 'DH6631T', 'black', '<div class=\"product-overview-tab\" style=\"margin: 18px 0px 15px; padding: 0px;\"><div class=\"container\" style=\"margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px; width: 1170px;\"><div class=\"row\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px;\"><div class=\"col-xs-12\" style=\"margin: 0px; padding: 0px 15px; position: relative; min-height: 1px; float: left; width: 1170px;\"><div class=\"product-tab-inner\" style=\"margin: 0px; padding: 22px; border: 1px solid rgb(229, 229, 229);\"><div id=\"productTabContent\" class=\"tab-content\" style=\"margin: 20px 0px 0px; padding: 5px 0px 0px; overflow: hidden; font-size: 14px; line-height: 20px;\"><div class=\"tab-pane fade in active\" id=\"description\" style=\"margin: 15px 0px 0px; padding: 0px; opacity: 1;\"><div class=\"std\" style=\"margin: 0px; padding: 0px;\"><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><strong style=\"margin: 0px; padding: 0px;\">Bluetooth Compatibility</strong><br style=\"margin: 0px; padding: 0px;\">Stream music wirelessly from smart phones and tablets for a more versatile listening experience.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">1080p upscaling</strong><br style=\"margin: 0px; padding: 0px;\">Upscales your standard definition DVD discs to near-Full HD quality when connected to an HDTV, breathing new life into your DVD collection.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">KARAOKE</strong><br style=\"margin: 0px; padding: 0px;\">Enjoy high quality entertainment. Enjoy LG Audio\'s karaoke function at anytime.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Portable in</strong><br style=\"margin: 0px; padding: 0px;\">Simply plug in and play your mobile devices (MP3 player, mobile phone, etc) for convenient and flexible music sharing with the 3.5mm jack.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Simplink</strong><br style=\"margin: 0px; padding: 0px;\">One remote, total control. You can easily manage your entire AV system with SIMPLINK, which uses control signals transmitted through an HDMI cable. SIMPLINK is compatible with all LG products.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">USB DIRECT RECORDING &amp; PLAYBACK</strong><br style=\"margin: 0px; padding: 0px;\">With the LG Home Theater System, Micro and Mini Audio you can not only play files stored on USB devices, but record CD tracks or FM radio programs on your USB, while listening at the same time.</p></div></div></div></div></div></div></div></div>', 38490.00, '37178.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:53:25', '2019-01-03 15:53:25', NULL),
(111, 8, 'Red Party Wear Kurtha', 'red-party-wear-kurtha', 'RPWK-001', 'Red', '<p>Features:</p><ul><li>Red Party Wear Kurtha&nbsp;</li><li>Easy and comfortable</li><li>Semi Stitched&nbsp;</li></ul>', 3800.00, '3296.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:54:34', '2019-01-03 15:54:34', NULL),
(112, 8, 'Dark Blue Party Wear Grown', 'dark-blue-party-wear-grown', 'DBPWG-001', 'Dark Blue', '<p>Features:</p><ul><li>Semi stitch&nbsp;</li><li>Attractive and Comfortable&nbsp;</li><li>High Quality Cloth&nbsp;</li></ul><p><br></p>', 5200.00, '7272.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 15:58:19', '2019-01-03 15:58:19', NULL),
(113, 8, 'Dark Blue Semi-stitch Gown', 'dark-blue-semi-stitch-gown', 'DBSSG-001', 'Dark Blue', '<p>Features:</p><ul><li>U-shape Neck Design</li><li>Design in hand Full design in middle and lower part of clothes</li><li>Semi-stitch</li></ul>', 3600.00, '69990.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:05:18', '2019-01-03 16:05:18', NULL),
(114, 43, 'Refrigerator 285 Ltr.', 'refrigerator-285-ltr', 'GLT302RPZN', 'Silver', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter Linear Compressor (ILC)</strong><br style=\"margin: 0px; padding: 0px;\">ILC Functions within a temperature range of ±0.5°C*, that’s almost half the temperature variation as compared to the conventional compressor. It reduces noise by 25%***, comes with 10 year warranty and is certified for a lifespan of 20 years. Not just this, LG comes with 51%** energy saving in refrigerator with ILC.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Door Cooling+™</strong></p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">More even and 35%^ faster cooling<br style=\"margin: 0px; padding: 0px;\">LG Door Cooling+™ makes inside temperature more even and cools the refrigerator 35%^ faster than the conventional cooling system. This reduces the temperature gap between the inner part and the door side of the compartment; thus letting the food remain fresh for long.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong><br style=\"margin: 0px; padding: 0px;\">Multiple cooling air vents distribute and circulate cool air to every corner of the refrigerator, ensuring that each and every food item is properly cooled.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™</strong><br style=\"margin: 0px; padding: 0px;\">LG\'s Smart Diagnosis™ is a fast and easy way to troubleshoot issues. Simply call the LG Customer Service Helpline and place the phone on the appliance. The appliance then communicates with a computer that produces a diagnosis within seconds and provides an immediate solution.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Works Without Stabilizer</strong><br style=\"margin: 0px; padding: 0px;\">LG refrigerators perform optimally even in low voltage conditions. They operate within the range of 100~290V &amp; can withstand voltage fluctuations making it virtually stabilizer free.</p>', 60990.00, '54278.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:09:57', '2019-01-03 16:09:57', NULL),
(115, 8, 'Light Pink Silk Sari', 'light-pink-silk-sari', 'LPSS-001', 'Light Pink', '<p>Features:&nbsp;</p><ul><li>Light pink color&nbsp;</li><li>Printed Design&nbsp;</li><li>Attractive and Simple&nbsp;</li></ul>', 2200.00, '44453.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:12:01', '2019-01-03 17:45:15', '2019-01-03 17:45:15'),
(116, 41, 'Lg Sound Bar', 'lg-sound-bar', 'lgNB2550A', 'black', '<p><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">3D&nbsp; compatibility<br style=\"margin: 0px; padding: 0px;\">2.1 Channel Sound System<br style=\"margin: 0px; padding: 0px;\">Dolby Digital&nbsp;<br style=\"margin: 0px; padding: 0px;\">Silk Dome Speaker Technology<br style=\"margin: 0px; padding: 0px;\">Bluetooth Streaming<br style=\"margin: 0px; padding: 0px;\">Wireless Subwoofer<br style=\"margin: 0px; padding: 0px;\">Bass Blast<br style=\"margin: 0px; padding: 0px;\">Portable in<br style=\"margin: 0px; padding: 0px;\">External HDD Playback</strong><br></p>', 24290.00, '15710.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:13:31', '2019-01-03 16:13:31', NULL),
(117, 41, 'Lgsj4 Sound Bar', 'lgsj4-sound-bar', 'LG sj4', 'black', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">HIGH RESOLUTION AUDIO<br style=\"margin: 0px; padding: 0px;\">True to the Original High Resolution</strong><br style=\"margin: 0px; padding: 0px;\">A new standard of sound quality. With advanced audio processing technology delivering sound of 24bit/192kHz quality you have some of the best components available for Hi-Res Audio. Now you can listen to studio standards of sound with lossless playback, experiencing a new level of audio that is true to the original.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">ADAPTIVE SOUND CONTROL<br style=\"margin: 0px; padding: 0px;\">Adaptive Audio for what you Watch</strong><br style=\"margin: 0px; padding: 0px;\">Intelligently adaptive audio. LG\'s unique audio enhancing technology analyses frequency levels as you listen to provide an optimum sound mix based on what you are watching. This advanced process ensures your audio instantly adapts to provide crystal clear dialogue or punchy powerful action whatever you choose to watch.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">AUTO SOUND ENGINE<br style=\"margin: 0px; padding: 0px;\">Optimised Audio</strong><br style=\"margin: 0px; padding: 0px;\">Automatically adjusted audio accuracy. LG Auto Sound Engine optimizes the audio at every volume level keeping the frequencies accurate in the right places. This delivers the correct sound balance no matter what volume your entertainment is playing at.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">TV MATCHING<br style=\"margin: 0px; padding: 0px;\">Picture Perfect Audio Partner</strong><br style=\"margin: 0px; padding: 0px;\">Picture perfect audio partner. Matching 43 inch LG TVs this soundbar mirrors the size and design of your television for the optimum audio visual partnership. Bring symphony to your home entertainment experience with an integrated solution.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Bluetooth Stand-by, wake up your bar on demand</strong><br style=\"margin: 0px; padding: 0px;\">Sound starts the moment you transfer audio to the Soundbar. The Soundbar remains in sleep mode but turns on and begins playing when the audio is sent via Bluetooth.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Music Play, music that follows you</strong><br style=\"margin: 0px; padding: 0px;\">Sound starts the moment you enter the room With Auto Music Play activated the bar can automatically take over the playback of the audio from a connected device the moment it sense the device is nearby.<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Control with your TV Remote</strong><br style=\"margin: 0px; padding: 0px;\">LG Soundbar comes with a remote but you can choose to use your own *TV remote as well.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">(*LG, Sony, Philips, Sharp, Panasonic, Vizio, Toshiba and Samsung brand remotes).<br style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Connected to your entertainment</strong><br style=\"margin: 0px; padding: 0px;\">Feel free to connect to the device you wish with HDMI, USB, Optical, Portable In and Bluetooth connectivity</p>', 31790.00, '8498.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:18:10', '2019-01-03 16:18:10', NULL),
(118, 8, 'Light Blue Gortagge Sari', 'light-blue-gortagge-sari', 'LBGS-001', 'Light Blue', '<p>Features:&nbsp;</p><ul><li>Light Blue color&nbsp;</li><li>Geortagge&nbsp;Material&nbsp;</li><li>Printed Design</li></ul>', 2800.00, '35388.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:18:32', '2019-01-03 16:18:32', NULL),
(119, 43, 'Lg Refrigerator', 'lg-refrigerator', 'GL-C292RLBN', 'Silver', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px;\">Inverter Linear Compressor (ILC)</span><br style=\"margin: 0px; padding: 0px;\">ILC Functions within a temperature range of ±0.5°C*, that’s almost half the temperature variation as compared to the conventional compressor. It reduces noise by 25%***, comes with&nbsp;10&nbsp;year&nbsp;warranty and is certified for a lifespan of 20 years. Not just this, LG comes with 51%** energy saving in&nbsp;refrigerator&nbsp;with ILC.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px;\">Door Cooling+™</span><br style=\"margin: 0px; padding: 0px;\">More even and 35%^ faster cooling<br style=\"margin: 0px; padding: 0px;\">LG Door Cooling+™ makes&nbsp;inside&nbsp;temperature more even and cools the refrigerator 35%^ faster than the conventional cooling system.&nbsp;</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px;\">Smart Diagnosis™</span><br style=\"margin: 0px; padding: 0px;\">LG\'s Smart Diagnosis™ is a fast and easy way to troubleshoot issues. Simply call the LG Customer Service Helpline and place the phone on the appliance. The appliance then communicates with a computer that produces a diagnosis within seconds and provides an immediate solution.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"font-weight: 600; margin: 0px; padding: 0px;\">Works Without Stabilizer</span><br style=\"margin: 0px; padding: 0px;\">LG refrigerators perform optimally even in low voltage conditions. They operate within the range of 100~290V &amp; can withstand voltage fluctuations making it virtually stabilizer free.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><br></p>', 56590.00, '75318.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:22:51', '2019-01-03 16:22:51', NULL),
(120, 8, 'Siffon Sari', 'siffon-sari', 'SS-001', 'Multi Color', '<p>Features:</p><ul><li>Siffon Sari</li><li>Available in different Color&nbsp;</li><li>Blouse Piece also available&nbsp;</li></ul>', 3000.00, '19832.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:26:20', '2019-01-03 16:26:20', NULL),
(121, 8, 'Sky Blue Siffon Sari', 'sky-blue-siffon-sari', 'SKSS-001', 'Sky Blue', '<p>Features:</p><ul><li>Sky Blue Siffon Sari&nbsp;</li><li>Available in Different Color&nbsp;</li><li>Blouse Piece also availble&nbsp;</li></ul>', 2500.00, '9316.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:29:43', '2019-01-03 16:29:43', NULL),
(122, 8, 'Red Georgette Sari', 'red-georgette-sari', 'RGS-001', 'Red', '<p>Features:</p><ul><li>Available in Red color&nbsp;</li><li>Simple and attractive&nbsp;</li><li>Printed Design&nbsp;</li></ul>', 2500.00, '87464.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:31:41', '2019-01-03 16:31:41', NULL),
(123, 41, 'Lgsj5 Sound Bar', 'lgsj5-sound-bar', 'LG sj5', 'black', '<p><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">True to the Original High Resolution</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">A new standard of sound quality. With advanced audio processing technology delivering sound of 24bit/192kHz quality you have some of the best components available for Hi-Res Audio. Now you can listen to studio standards of sound with lossless playback, experiencing a new level of audio that is true to the original.</span></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">HIGH RESOLUTION AUDIO</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">WIRELESS SUBWOOFER</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">ADAPTIVE SOUND CONTROL</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">AUTO SOUND ENGINE</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">TV MATCHING</strong></li></ul><p><br></p>', 36190.00, '12490.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:36:35', '2019-01-03 16:36:35', NULL),
(124, 41, 'Lg P5 Smart Hi-fi Audio Bluetooth Speaker', 'lg-p5-smart-hi-fi-audio-bluetooth-speaker', 'LGnp55550B', 'vlack', '<h2 style=\"margin: 0px 0px 15px; padding: 0px; font-family: Arimo, sans-serif; line-height: 1.35; color: rgb(51, 62, 72); font-size: 30px;\"><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Lightweight Portable Bluetooth™ Speaker</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Ultra Long Lasting Battery Life</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Simultaneous Connection</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Pair and Play</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">TV Sound Sync</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">LG Blutooth™ Remote App</strong></li></ul></h2>', 15090.00, '34689.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:42:49', '2019-01-03 16:42:49', NULL),
(125, 8, 'Sea Green Georgette Sari', 'sea-green-georgette-sari', 'SGGS-001', 'Sea Green', '<p><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Type:Sarees&nbsp;</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Color:Sea Green&nbsp;</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Saree Fabric:georgette With satin Patta&nbsp;</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Blouse Fabric:Georgette</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Saree Length:5.5mtr</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Blouse Length:0.8mtr</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Occasion:Party Wear&nbsp;</span><br style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\"><span style=\"color: rgb(14, 13, 13); font-family: Verdana, Helvetica, Arial, sans-serif; font-size: 12px;\">Work:Print</span><br></p>', 2500.00, '70896.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:44:41', '2019-01-03 16:46:49', NULL),
(126, 43, '422 Ltr Lg Refrigerator', '422-ltr-lg-refrigerator', 'GL-B432BS', 'Black', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; font-size: 14px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><strong style=\"margin: 0px; padding: 0px;\">Key Features</strong></p><ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif;\"><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Water Dispenser</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter Linear Compressor</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">Hygiene Fresh+</strong></li><li style=\"margin: 0px; padding: 0px; list-style: none;\"><strong style=\"margin: 0px; padding: 0px;\">I-Micom</strong></li></ul>', 87590.00, '99811.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:46:41', '2019-01-03 16:46:41', NULL),
(127, 8, 'White Satin Patta Sari', 'white-satin-patta-sari', 'WSPS-001', 'White', '<p>White satin patta sari&nbsp;</p><p>Material: Georgette&nbsp;</p><p>Color White&nbsp;</p>', 2500.00, '67530.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:50:57', '2019-01-03 16:50:57', NULL),
(128, 41, 'Lg Sh5 Sound Bar', 'lg-sh5-sound-bar', 'Lg Sh5', 'silver', '<p><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">ASC (Adaptive Sound Control)</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">Adaptive Sound Control is LG\'s unique or differentiated voice enhancing technology. It actively analyzes the vocal levels in real time.When it senses high vocal levels, it automatically lowers the bass power to make dialogue crystal-clear. (ex : News, Narration)</span><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">Auto Sound Engine</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">LG Auto Sound Engine optimizes the audio at every volume level keeping the frequencies accurate in the right places.This delivers the correct sound balance no matter the sound volume.</span><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">TV Matching Design</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">The sound bar perfectly matches your TV and delivers an elegant and simple atmosphere. It was designed to fit with your TV stand.</span><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">Control with your TV Remote</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">LG Sound bars come with a remote but even allow you to use the remote you know and love, your TV remote*. (* LG, Sony, Philips, Sharp, Panasonic, Vizio, Toshiba and Samsung)</span><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">Connectivity</strong><br style=\"margin: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><span style=\"color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\">No matter what you want to connect, be it an HDMI, optical or portable type of device, an LG sound bar has you cover</span><br></p>', 37990.00, '64162.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:51:12', '2019-01-03 16:51:12', NULL),
(129, 43, 'Refrigerator 360 Ltr.', 'refrigerator-360-ltr', 'GL-C402RPCN', 'Glass Shelves', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter Linear Compressor</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Linear CoolingTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Door CoolingTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">LG Dual FridgeTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart DiagnosisTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Smart ConnectTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Eco Friendly Refrigerant</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">I-Micom</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Catechin Deodorizer</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Anti Bacterial Gasket</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Moist Balance Crisper</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Humidity Controller</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Ever Fresh Zone</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Double Twist Ice Tray</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">2 L Bottle Storage</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Top LED</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Pull Out Tray</strong></li></ul>', 76190.00, '70788.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:57:55', '2019-01-03 17:20:08', NULL),
(130, 41, 'Lg Portable All In One Sound System', 'lg-portable-all-in-one-sound-system', 'Lg 0j98', 'black', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Powerful Bass</strong><br style=\"margin: 0px; padding: 0px;\">In addition to 1800 cranking watts, this powerful all-in-one speaker pumps out thumping, thunderous low-end from its two mighty woofers for bass you can feel as well as hear.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Party Thruster</strong><br style=\"margin: 0px; padding: 0px;\">Build up the energy with the Party Thruster controller. Simply slide the throttle forward and hear the party build up until you hit maximum party at which time the system booms and the lights go crazy.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Dance Lighting</strong><br style=\"margin: 0px; padding: 0px;\">This system offers multiple lighting modes and a variety of effects, including a feature that lets the light pulse to the beat of the music so everyone can feel and see the rhythm.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Karaoke Creator</strong><br style=\"margin: 0px; padding: 0px;\">Get ready to croon along with any tune: This feature suppresses the vocal frequencies of a song, making any track ready to play behind a new lead singer! It can even change the key of the song to better suit the voice of the singer.*</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Voice Filters</strong><br style=\"margin: 0px; padding: 0px;\">The karaoke feature includes built-in sound-enhancing voice filters to make any singer sound even more like a star. They can even change voices completely for added fun.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Bluetooth Connectivity</strong><br style=\"margin: 0px; padding: 0px;\">Wirelessly stream music directly from your Smartphone or other compatible device for a seamless listening experience</p>', 56990.00, '87789.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:58:09', '2019-01-03 17:20:02', NULL),
(131, 8, 'Golden Sari', 'golden-sari', 'GS-001', 'Golden', '<p>Highlights:</p><p>Material: Chiffon&nbsp;</p><p>Color: Golden&nbsp;</p><p>High-Quality Material&nbsp;</p>', 3000.00, '76004.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:58:23', '2019-01-03 17:20:05', NULL),
(132, 8, 'Blue Sari', 'blue-sari', 'BS-001', 'Blue', '<p>Blue Sari&nbsp;</p><p>Highquality material&nbsp;</p><p>Chiffon Sari&nbsp;</p>', 3300.00, '83264.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 16:59:15', '2019-01-03 17:19:59', NULL),
(133, 8, 'Orange Sari', 'orange-sari', 'OS-001', 'Orange', '<p>Orange siffon sari</p>', 3000.00, '61737.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:01:16', '2019-01-03 17:19:57', NULL),
(134, 43, 'Lg Refrigeraor 310 Ltr.', 'lg-refrigeraor-310-ltr', 'GLT322RPZN', 'Silver', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Linear Inverter Compressor</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Linear CoolingTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Door Cooling+TM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">LG Dual FridgeTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto Smart ConnectTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart DiagnosisTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Eco Friendly Refrigerant</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Ever Fresh Zone</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">I-Micom</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Moist Balance CrisperTM</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Double Twist Ice Tray</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Humidity Controller</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Top LED</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">2 L Bottle Storage</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Works Without Stabilizer</strong></li></ul>', 61990.00, '79703.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:10:42', '2019-01-03 17:19:56', NULL),
(135, 41, 'Lg X-boom Sound System', 'lg-x-boom-sound-system', 'lg DM8360', 'black & red', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Auto DJ</strong><br style=\"margin: 0px; padding: 0px;\">You can put your party on Auto DJ and let the system take over while you enjoy yourself..</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">TV Sound Sync</strong><br style=\"margin: 0px; padding: 0px;\">Connect your compatible LG TV to the system without ugly wires.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Bluetooth Stand-by</strong><br style=\"margin: 0px; padding: 0px;\">The party starts the moment you send music to it. The system sleeps but when music is sent via bluetooth the unit wakes and plays.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Bluetooth Audio Streaming</strong><br style=\"margin: 0px; padding: 0px;\">Enjoy your favorite music on mobile devices through LG sound system with wireless audio streaming via Bluetooth.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">USB Playback</strong><br style=\"margin: 0px; padding: 0px;\">Music straight from a USB storage device.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Sound EQ</strong><br style=\"margin: 0px; padding: 0px;\">Customize the way your music sounds. LED pannel clearly shows the sound mode that you are in.</p>', 60990.00, '98144.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:11:16', '2019-01-03 17:19:55', NULL),
(136, 8, 'Net Sari', 'net-sari', 'NS-001', 'Multi Color', '<p>Available in Different color&nbsp;</p>', 3600.00, '51830.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:11:28', '2019-01-03 17:19:54', NULL),
(137, 8, 'Dark Blue Party Wear Gown', 'dark-blue-party-wear-gown', 'DBPWG', 'Dark Blue', '<ul><li>Dark Blue Party Wear Gown&nbsp;</li><li>High-Quality Product&nbsp;</li></ul><p><br></p>', 5000.00, '26594.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:15:29', '2019-01-03 17:44:30', '2019-01-03 17:44:30'),
(138, 8, 'Soft Yellow Full Printed Gown', 'soft-yellow-full-printed-gown', 'SYFPG-001', 'Soft Yellow', '<ul><li>Soft Yellow Color</li><li>High-Quality Materials</li><li>Printed Design</li><li>Party Wear&nbsp;</li></ul>', 4000.00, '11885.jpg', 1, 1, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:19:09', '2019-01-03 17:19:52', NULL),
(139, 8, 'New Ladies Stylish Pants', 'new-ladies-stylish-pants', 'NLSP-001', 'multi-color', '<p>Japanese Plus Size / Free Size / High waist / Stretchable skinny jeans</p><p>✨ Size A = WAIST 26-32 Inch Hip 32-42 inch</p><p>Length 38 Inch Thigh 17-26 inch</p>', 1700.00, '33870.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:31:25', '2019-01-03 17:31:25', NULL),
(140, 43, 'Lg Refrigerator 258 Ltr', 'lg-refrigerator-258-ltr', 'GLC292RHPN', 'Glass Shelves', '<p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Inverter Linear Compressor (ILC)</strong><br style=\"margin: 0px; padding: 0px;\">ILC Functions within a temperature range of ±0.5°C*, that’s almost half the temperature variation as compared to the conventional compressor. It reduces noise by 25%***, comes with 10 year warranty and is certified for a lifespan of 20 years. Not just this, LG comes with 51%** energy saving in refrigerator with ILC.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Door Cooling+™</strong><br style=\"margin: 0px; padding: 0px;\">More even and 35%^ faster cooling<br style=\"margin: 0px; padding: 0px;\">LG Door Cooling+™ makes inside temperature more even and cools the refrigerator 35%^ faster than the conventional cooling system. This reduces the temperature gap between the inner part and the door side of the compartment; thus letting the food remain fresh for long.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong><br style=\"margin: 0px; padding: 0px;\">Multiple cooling air vents distribute and circulate cool air to every corner of the refrigerator, ensuring that each and every food item is properly cooled.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Smart Diagnosis™</strong><br style=\"margin: 0px; padding: 0px;\">LG\'s Smart Diagnosis™ is a fast and easy way to troubleshoot issues. Simply call the LG Customer Service Helpline and place the phone on the appliance. The appliance then communicates with a computer that produces a diagnosis within seconds and provides an immediate solution.</p><p style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><br></p>', 57690.00, '32651.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:39:43', '2019-01-03 17:39:43', NULL),
(141, 43, 'Refrigerator 240 Ltr.', 'refrigerator-240-ltr', 'GLB252VLGY', 'Black', '<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; color: rgb(51, 62, 72); font-family: Arimo, sans-serif; font-size: 14px;\"><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">10 Yrwarranty</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Works without Stabilizer (135V~290 V)</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Multi Air Flow</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Egg cum Ice Tray</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Moist Balance Crisper with</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Humidity Controller</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">High Gloss Finish</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Lock &amp; Key</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Colour: Wine Blossom</strong></li><li style=\"margin: 0px; padding: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">Dimension : 550 x 1450 x 685</strong></li></ul>', 42890.00, '43670.jpg', 1, 0, NULL, NULL, NULL, NULL, NULL, '2019-01-03 17:54:27', '2019-01-03 17:54:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `sku`, `size`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 'CM001', 'Medium', 2000.00, 1, '2019-01-01 14:51:25', '2019-01-01 14:51:25'),
(3, 2, 'WBAS-001-36', '36', 1900.00, 10, '2019-01-01 16:34:19', '2019-01-01 16:34:19'),
(4, 2, 'WBAS-001-40', '40', 1900.00, 9, '2019-01-01 16:34:19', '2019-01-01 22:08:34'),
(5, 2, 'WBAS-001-39', '39', 1900.00, 10, '2019-01-01 16:34:19', '2019-01-02 11:07:31'),
(6, 2, 'WBAS-001-38', '38', 1900.00, 10, '2019-01-01 16:34:19', '2019-01-01 16:34:19'),
(7, 2, 'WBAS-001-37', '37', 1900.00, 10, '2019-01-01 16:34:20', '2019-01-01 16:34:20'),
(9, 3, 'WBAS-002-39', '39', 2200.00, 10, '2019-01-01 16:53:51', '2019-01-02 11:05:30'),
(10, 3, 'WBAS-002-36', '36', 2200.00, 10, '2019-01-01 16:53:51', '2019-01-01 22:24:22'),
(11, 3, 'WBAS-002-37', '37', 2200.00, 10, '2019-01-01 16:53:51', '2019-01-01 22:21:14'),
(12, 3, 'WBAS-002-38', '38', 2200.00, 10, '2019-01-01 16:53:51', '2019-01-01 22:21:14'),
(13, 4, 'WLBS-003', 'Medium', 1800.00, 1, '2019-01-01 16:58:18', '2019-01-01 16:58:18'),
(21, 5, 'WBAB-004-39', '39', 2200.00, 10, '2019-01-01 17:41:03', '2019-01-01 17:41:03'),
(22, 5, 'WBAB-004-38', '38', 2200.00, 10, '2019-01-01 17:41:03', '2019-01-01 17:41:03'),
(23, 5, 'WBAB-004-37', '37', 2200.00, 10, '2019-01-01 17:41:03', '2019-01-01 17:41:03'),
(24, 5, 'WBAB-004-36', '36', 2200.00, 10, '2019-01-01 17:41:03', '2019-01-01 17:41:03'),
(25, 6, 'WBLAB-005-39', '39', 2000.00, 10, '2019-01-01 17:43:54', '2019-01-01 17:43:54'),
(26, 6, 'WBLAB-005-38', '38', 2000.00, 10, '2019-01-01 17:43:55', '2019-01-01 17:43:55'),
(27, 6, 'WBLAB-005-37', '37', 2000.00, 10, '2019-01-01 17:43:55', '2019-01-01 17:43:55'),
(28, 6, 'WBLAB-005-36', '36', 2000.00, 10, '2019-01-01 17:43:55', '2019-01-01 17:43:55'),
(30, 7, 'WLABFW-006-39', '39', 2000.00, 10, '2019-01-01 17:48:06', '2019-01-01 17:48:06'),
(31, 7, 'WLABFW-006-38', '38', 2000.00, 10, '2019-01-01 17:48:07', '2019-01-01 17:48:07'),
(32, 7, 'WLABFW-006-37', '37', 2000.00, 10, '2019-01-01 17:48:07', '2019-01-01 17:48:07'),
(33, 7, 'WLABFW-006-36', '36', 2000.00, 10, '2019-01-01 17:48:07', '2019-01-01 17:48:07'),
(35, 8, 'BLBFW-007-39', '39', 2300.00, 9, '2019-01-01 17:50:30', '2019-01-02 13:17:22'),
(36, 8, 'BLBFW-007-38', '38', 2300.00, 10, '2019-01-01 17:50:30', '2019-01-01 17:50:30'),
(37, 8, 'BLBFW-007-37', '37', 2300.00, 10, '2019-01-01 17:50:30', '2019-01-01 17:50:30'),
(38, 8, 'BLBFW-007-36', '36', 2300.00, 100, '2019-01-01 17:50:30', '2019-01-01 17:50:30'),
(40, 9, 'WBAB-008-39', '39', 2100.00, 10, '2019-01-01 17:53:26', '2019-01-01 17:53:26'),
(41, 9, 'WBAB-008-38', '38', 2100.00, 10, '2019-01-01 17:53:27', '2019-01-01 17:53:27'),
(42, 9, 'WBAB-008-37', '37', 2100.00, 10, '2019-01-01 17:53:27', '2019-01-01 17:53:27'),
(43, 9, 'WBAB-008-36', '36', 2100.00, 10, '2019-01-01 17:53:27', '2019-01-01 17:53:27'),
(44, 10, 'WLBB-009-39', '39', 2000.00, 10, '2019-01-01 17:55:54', '2019-01-01 17:55:54'),
(45, 10, 'WLBB-009-38', '38', 2000.00, 10, '2019-01-01 17:55:54', '2019-01-01 17:55:54'),
(46, 10, 'WLBB-009-37', '37', 2000.00, 10, '2019-01-01 17:55:54', '2019-01-01 17:55:54'),
(47, 10, 'WLBB-009-36', '36', 2000.00, 10, '2019-01-01 17:55:54', '2019-01-01 17:55:54'),
(49, 11, 'WAB-010-39', '39', 2400.00, 10, '2019-01-01 17:58:41', '2019-01-01 17:58:41'),
(50, 11, 'WAB-010-38', '38', 2400.00, 10, '2019-01-01 17:58:41', '2019-01-01 17:58:41'),
(51, 11, 'WAB-010-37', '37', 2400.00, 10, '2019-01-01 17:58:41', '2019-01-01 17:58:41'),
(52, 11, 'WAB-010-36', '36', 2400.00, 10, '2019-01-01 17:58:42', '2019-01-01 17:58:42'),
(53, 16, 'WGAB-015', 'Medium', 2200.00, 1, '2019-01-01 18:00:03', '2019-01-01 18:00:03'),
(54, 12, 'WBAB-011-39', '39', 2250.00, 10, '2019-01-01 18:01:46', '2019-01-02 12:21:40'),
(55, 12, 'WBAB-011-38', '38', 2250.00, 10, '2019-01-01 18:01:46', '2019-01-01 18:01:46'),
(56, 12, 'WBAB-011-37', '37', 2250.00, 10, '2019-01-01 18:01:46', '2019-01-01 18:01:46'),
(57, 12, 'WBAB-011-36', '36', 2250.00, 10, '2019-01-01 18:01:46', '2019-01-01 18:01:46'),
(58, 17, 'WDBAB-016', 'Medium', 1900.00, 1, '2019-01-01 18:04:02', '2019-01-01 18:04:02'),
(59, 13, 'BLBW-012-39', '39', 1900.00, 10, '2019-01-01 18:04:24', '2019-01-01 18:04:24'),
(60, 13, 'BLBW-012-38', '38', 1900.00, 10, '2019-01-01 18:04:24', '2019-01-01 18:04:24'),
(61, 13, 'BLBW-012-37', '37', 1900.00, 10, '2019-01-01 18:04:24', '2019-01-01 18:04:24'),
(62, 13, 'BLBW-012-36', '36', 1900.00, 10, '2019-01-01 18:04:24', '2019-01-01 18:04:24'),
(63, 14, 'WBS-013-39', '39', 1900.00, 10, '2019-01-01 18:07:27', '2019-01-01 18:07:27'),
(64, 14, 'WBS-013-38', '38', 1900.00, 10, '2019-01-01 18:07:27', '2019-01-01 18:07:27'),
(65, 14, 'WBS-013-37', '37', 1900.00, 10, '2019-01-01 18:07:27', '2019-01-01 18:07:27'),
(66, 14, 'WBS-013-36', '36', 1900.00, 10, '2019-01-01 18:07:27', '2019-01-01 18:07:27'),
(67, 15, 'WS-014-39', '39', 1900.00, 10, '2019-01-01 18:10:01', '2019-01-01 18:10:01'),
(68, 15, 'WS-014-38', '38', 1900.00, 10, '2019-01-01 18:10:01', '2019-01-01 18:10:01'),
(69, 15, 'WS-014-37', '37', 1900.00, 10, '2019-01-01 18:10:01', '2019-01-01 18:10:01'),
(70, 15, 'WS-014-36', '36', 1900.00, 10, '2019-01-01 18:10:01', '2019-01-01 18:10:01'),
(71, 16, 'WGAB-015-39', '39', 2200.00, 10, '2019-01-01 18:12:29', '2019-01-01 18:12:29'),
(72, 16, 'WGAB-015-38', '38', 2200.00, 10, '2019-01-01 18:12:29', '2019-01-01 18:12:29'),
(73, 16, 'WGAB-015-37', '37', 2200.00, 10, '2019-01-01 18:12:29', '2019-01-01 18:12:29'),
(74, 16, 'WGAB-015-36', '36', 2200.00, 10, '2019-01-01 18:12:29', '2019-01-01 18:12:29'),
(75, 18, 'WBLS-017', 'Medium', 2000.00, 1, '2019-01-02 10:53:55', '2019-01-02 10:53:55'),
(76, 19, 'WBS-018', 'Medium', 1800.00, 1, '2019-01-02 10:59:26', '2019-01-02 10:59:26'),
(77, 20, 'WBAS-019', 'Medium', 2100.00, 1, '2019-01-02 11:03:53', '2019-01-02 11:03:53'),
(78, 21, 'WDBS-020', 'Medium', 2100.00, 1, '2019-01-02 11:09:12', '2019-01-02 11:09:12'),
(79, 22, 'WBS-021', 'Medium', 1800.00, 1, '2019-01-02 11:14:25', '2019-01-02 11:14:25'),
(80, 23, 'WS-022', 'Medium', 2000.00, 1, '2019-01-02 11:17:00', '2019-01-02 11:17:00'),
(81, 24, 'WS-023', 'Medium', 2000.00, 1, '2019-01-02 11:23:09', '2019-01-02 11:23:09'),
(83, 25, 'HS-001-75', '75 Ft', 900.00, 10, '2019-01-02 12:38:35', '2019-01-02 12:38:35'),
(84, 25, 'HS-001-50', '50 Ft', 700.00, 10, '2019-01-02 12:38:35', '2019-01-02 12:38:35'),
(85, 26, 'SWH-001', 'Medium', 1200.00, 1, '2019-01-02 13:32:14', '2019-01-02 13:32:14'),
(86, 27, 'HHNM-001', 'Medium', 80.00, 1, '2019-01-02 13:44:26', '2019-01-02 13:44:26'),
(87, 28, 'EK-001', 'Medium', 650.00, 1, '2019-01-02 14:48:42', '2019-01-02 14:48:42'),
(88, 29, 'NMUB-001', 'Medium', 700.00, 1, '2019-01-02 15:19:09', '2019-01-02 15:19:09'),
(89, 30, 'FRGE-001', 'Medium', 700.00, 1, '2019-01-02 15:59:46', '2019-01-02 15:59:46'),
(90, 31, 'HKB-001', 'Medium', 550.00, 1, '2019-01-02 16:05:07', '2019-01-02 16:05:07'),
(91, 32, 'ZBS-001', 'Medium', 1800.00, 1, '2019-01-02 16:17:54', '2019-01-02 16:17:54'),
(92, 33, 'DMUC-001', 'Medium', 2000.00, 1, '2019-01-02 16:28:06', '2019-01-02 16:28:06'),
(93, 34, 'LUCMB-001', 'Medium', 1000.00, 1, '2019-01-02 16:34:50', '2019-01-02 16:34:50'),
(94, 35, 'FMF-001', 'Medium', 1100.00, 1, '2019-01-02 16:36:35', '2019-01-02 16:36:35'),
(95, 36, 'MFMMPF-001', 'Medium', 800.00, 1, '2019-01-02 16:40:15', '2019-01-02 16:40:15'),
(96, 37, 'HPC-001', 'Medium', 500.00, 1, '2019-01-02 16:45:33', '2019-01-02 16:45:33'),
(97, 38, 'MMH-001', 'Medium', 1200.00, 1, '2019-01-02 16:56:58', '2019-01-02 16:56:58'),
(98, 39, 'NFJL-001', 'Medium', 500.00, 1, '2019-01-02 17:05:28', '2019-01-02 17:05:28'),
(99, 40, 'NESBC-001', 'Medium', 800.00, 1, '2019-01-02 17:14:41', '2019-01-02 17:14:41'),
(100, 41, 'COO-001', 'Medium', 800.00, 1, '2019-01-02 17:18:06', '2019-01-02 17:18:06'),
(101, 42, 'BPMN-001', 'Medium', 1300.00, 1, '2019-01-02 17:22:11', '2019-01-02 17:22:11'),
(102, 43, 'MFMOBNW-001', 'Medium', 1000.00, 1, '2019-01-02 17:26:48', '2019-01-02 17:26:48'),
(103, 44, 'MES-001', 'Medium', 500.00, 1, '2019-01-02 17:29:07', '2019-01-02 17:29:07'),
(104, 45, 'BBP-001', 'Medium', 5000.00, 1, '2019-01-02 17:32:20', '2019-01-02 17:32:20'),
(105, 46, 'MBBI-002', 'Medium', 2500.00, 1, '2019-01-02 17:36:38', '2019-01-02 17:36:38'),
(106, 47, '22\'\' LED TV', 'Medium', 14999.00, 1, '2019-01-03 10:49:19', '2019-01-03 10:49:19'),
(107, 48, 'CG 20D1504 20\'\' LED TV', 'Medium', 14990.00, 1, '2019-01-03 11:07:06', '2019-01-03 11:07:06'),
(108, 49, '24LH454A', 'Medium', 26990.00, 20, '2019-01-03 11:09:09', '2019-01-03 11:25:49'),
(111, 50, 'Cg 20 D 3305/3205', '43\'\'', 15390.00, 20, '2019-01-03 11:34:00', '2019-01-03 11:34:00'),
(112, 51, '32lh514D', 'Medium', 38190.00, 1, '2019-01-03 11:34:13', '2019-01-03 11:34:13'),
(113, 51, 'LG 32 inch Led TV', '32', 38190.00, 20, '2019-01-03 11:36:14', '2019-01-03 11:36:14'),
(114, 52, 'CG 22 D 3305', 'Medium', 16890.00, 1, '2019-01-03 11:39:11', '2019-01-03 11:39:11'),
(115, 53, 'fc1475s4w', 'Medium', 80490.00, 15, '2019-01-03 11:41:44', '2019-01-03 13:43:13'),
(116, 54, 'CG 24 DN 407', 'Medium', 18.00, 1, '2019-01-03 11:44:06', '2019-01-03 11:44:06'),
(117, 55, 'CG 24 D 1905 24\'\' LED TV', 'Medium', 18.00, 1, '2019-01-03 11:48:40', '2019-01-03 11:48:40'),
(118, 56, '32LK526', 'Medium', 41990.00, 1, '2019-01-03 11:51:03', '2019-01-03 11:51:03'),
(119, 57, 'CG 32 D 1905 32\'\' LED TV', 'Medium', 26.00, 1, '2019-01-03 11:51:57', '2019-01-03 11:51:57'),
(120, 56, '32\" inch LED TV', '32', 41990.00, 20, '2019-01-03 11:52:14', '2019-01-03 11:52:14'),
(121, 58, 'CG32DIN08 32\'\' LED TV', 'Medium', 26990.00, 1, '2019-01-03 11:56:19', '2019-01-03 11:56:19'),
(122, 59, '32\" LED TV CG32D0003', 'Medium', 26690.00, 1, '2019-01-03 11:58:38', '2019-01-03 11:58:38'),
(123, 60, 'CG 32 D 1604 S Smart TV', 'Medium', 30790.00, 1, '2019-01-03 12:04:06', '2019-01-03 12:04:06'),
(124, 61, 'CG 32 DC 100 S SMART LED TV', 'Medium', 32290.00, 1, '2019-01-03 12:07:11', '2019-01-03 12:07:11'),
(125, 62, 'CG 40 DIN 09 S Smart Led Tv', 'Medium', 48190.00, 1, '2019-01-03 12:10:44', '2019-01-03 12:10:44'),
(126, 63, 'CG 43 D 9904 Smart TV', 'Medium', 51390.00, 1, '2019-01-03 12:17:23', '2019-01-03 12:17:23'),
(127, 64, 'CG 43 DN 407 LED TV', 'Medium', 44990.00, 1, '2019-01-03 12:21:34', '2019-01-03 12:21:34'),
(128, 65, 'CG 43 DC 100 S SMART LED TV', 'Medium', 51890.00, 1, '2019-01-03 12:25:46', '2019-01-03 12:25:46'),
(129, 66, 'CG 43 D 9904 S SMART LED TV', 'Medium', 55790.00, 1, '2019-01-03 12:31:47', '2019-01-03 12:31:47'),
(130, 67, 'CG 49 D 1004', 'Medium', 64790.00, 1, '2019-01-03 12:41:18', '2019-01-03 12:41:18'),
(131, 68, 'CG 43 DC 200 U', 'Medium', 58690.00, 1, '2019-01-03 12:47:56', '2019-01-03 12:47:56'),
(132, 69, 'CG 49 DC 100 S', 'Medium', 64190.00, 1, '2019-01-03 12:49:27', '2019-01-03 12:49:27'),
(133, 70, '32LJ523D', 'Medium', 51590.00, 1, '2019-01-03 12:51:09', '2019-01-03 12:51:09'),
(134, 70, 'Led 32\"tv', '32', 51590.00, 20, '2019-01-03 12:52:48', '2019-01-03 12:52:48'),
(135, 71, 'CG 49 DC 200 U', 'Medium', 71690.00, 1, '2019-01-03 12:54:44', '2019-01-03 12:54:44'),
(136, 72, 'F2514NTGW', 'Medium', 168090.00, 15, '2019-01-03 12:58:43', '2019-01-03 13:40:05'),
(137, 73, 'CG 55 D 1004 U', 'Medium', 95190.00, 1, '2019-01-03 13:00:52', '2019-01-03 13:00:52'),
(138, 74, 'Lg 32lj525D', 'Medium', 57490.00, 1, '2019-01-03 13:04:30', '2019-01-03 13:04:30'),
(139, 74, 'Lg mosquito away 32\"tv', '32', 57490.00, 20, '2019-01-03 13:06:12', '2019-01-03 13:06:12'),
(140, 75, 'CG 55 DC 200 U', 'Medium', 87090.00, 1, '2019-01-03 13:06:49', '2019-01-03 13:06:49'),
(141, 76, 'CG 65 DC 200 U', 'Medium', 136690.00, 1, '2019-01-03 13:09:42', '2019-01-03 13:09:42'),
(142, 77, 'T2735NTWV', 'Medium', 464890.00, 15, '2019-01-03 13:12:00', '2019-01-03 13:39:13'),
(143, 78, 'CG 86 D 8900 S Smart Tv', 'Medium', 665490.00, 1, '2019-01-03 13:21:02', '2019-01-03 13:21:02'),
(144, 79, 'CG-VC14F01', 'Medium', 7090.00, 1, '2019-01-03 13:26:23', '2019-01-03 13:26:23'),
(145, 80, 'T2108VSAR', 'Medium', 48990.00, 15, '2019-01-03 13:29:11', '2019-01-03 13:38:16'),
(146, 81, 'CG-VC16KB01', 'Medium', 7790.00, 1, '2019-01-03 13:29:11', '2019-01-03 13:29:11'),
(147, 82, 'CG-VC18D01', 'Medium', 8290.00, 1, '2019-01-03 13:32:02', '2019-01-03 13:32:02'),
(148, 83, 'VC18HB01', 'Medium', 8690.00, 1, '2019-01-03 13:35:59', '2019-01-03 13:35:59'),
(149, 84, '43lj523T', 'Medium', 74990.00, 1, '2019-01-03 13:36:38', '2019-01-03 13:36:38'),
(150, 84, 'Lg 43 LJ523Tv', '43', 74990.00, 20, '2019-01-03 13:38:15', '2019-01-03 13:38:15'),
(151, 85, 'VC20TD01', 'Medium', 11090.00, 1, '2019-01-03 13:38:55', '2019-01-03 13:38:55'),
(152, 86, 'CG-VC22E01', 'Medium', 9390.00, 1, '2019-01-03 13:45:03', '2019-01-03 13:45:03'),
(153, 87, 'CG-MW20A01S', 'Medium', 9690.00, 1, '2019-01-03 13:49:19', '2019-01-03 13:49:19'),
(154, 88, 'CG-MW25B01G', 'Medium', 13999.00, 1, '2019-01-03 13:51:56', '2019-01-03 13:51:56'),
(155, 89, 'T2109VSAL', 'Medium', 59890.00, 15, '2019-01-03 13:53:28', '2019-01-03 13:58:48'),
(156, 90, 'CG-MW30C01C', 'Medium', 19890.00, 1, '2019-01-03 13:54:31', '2019-01-03 13:54:31'),
(157, 91, 'F1006NMTS', 'Medium', 64790.00, 15, '2019-01-03 15:06:06', '2019-01-03 15:20:10'),
(158, 92, 'Lg43lj525t', 'Medium', 76990.00, 1, '2019-01-03 15:08:34', '2019-01-03 15:08:34'),
(159, 93, 'CGTS12A01', 'Medium', 13490.00, 1, '2019-01-03 15:11:07', '2019-01-03 15:11:07'),
(160, 94, 'CGTS12A02', 'Medium', 15090.00, 1, '2019-01-03 15:13:08', '2019-01-03 15:13:08'),
(161, 92, 'lg43inch ledtv 43lh525t', '43', 76990.00, 20, '2019-01-03 15:13:59', '2019-01-03 15:13:59'),
(162, 95, 'F1265NMTS', 'Medium', 70690.00, 15, '2019-01-03 15:14:19', '2019-01-03 15:19:18'),
(163, 96, 'CGTS12E01', 'Medium', 16390.00, 1, '2019-01-03 15:18:30', '2019-01-03 15:18:30'),
(164, 97, 'CGTS12B01D', 'Medium', 17890.00, 1, '2019-01-03 15:20:17', '2019-01-03 15:20:17'),
(165, 98, 'CG TS12B02T', 'Medium', 19890.00, 1, '2019-01-03 15:23:22', '2019-01-03 15:23:22'),
(166, 99, 'CG-TS15B01D', 'Medium', 24390.00, 1, '2019-01-03 15:25:51', '2019-01-03 15:25:51'),
(167, 100, 'CGTS15E01', 'Medium', 22390.00, 1, '2019-01-03 15:27:50', '2019-01-03 15:27:50'),
(168, 101, 'F1207NMTW', 'Medium', 73490.00, 15, '2019-01-03 15:29:12', '2019-01-03 15:29:33'),
(169, 102, 'lg 43lk5700', 'Medium', 92990.00, 1, '2019-01-03 15:29:18', '2019-01-03 15:29:18'),
(170, 102, '43lk5700', '43', 92990.00, 20, '2019-01-03 15:30:06', '2019-01-03 15:30:06'),
(171, 103, 'CGTS15A01', 'Medium', 17590.00, 1, '2019-01-03 15:30:51', '2019-01-03 15:30:51'),
(172, 104, 'CGTS15F01', 'Medium', 19490.00, 1, '2019-01-03 15:38:01', '2019-01-03 15:38:01'),
(173, 105, 'Lg43lj550', 'Medium', 92990.00, 1, '2019-01-03 15:40:06', '2019-01-03 15:40:06'),
(174, 106, 'FC1408S3E', 'Medium', 88990.00, 15, '2019-01-03 15:41:00', '2019-01-03 15:41:20'),
(175, 105, '43lj550', '43', 92990.00, 20, '2019-01-03 15:41:03', '2019-01-03 15:41:03'),
(176, 107, 'CG-A2061', 'Medium', 4690.00, 1, '2019-01-03 15:44:00', '2019-01-03 15:44:00'),
(177, 108, 'CGA2071', 'Medium', 4390.00, 1, '2019-01-03 15:46:06', '2019-01-03 15:46:06'),
(178, 109, 'F1069FD3PS', 'Medium', 345790.00, 15, '2019-01-03 15:49:10', '2019-01-03 15:49:39'),
(179, 110, 'DH6631T', 'Medium', 38490.00, 1, '2019-01-03 15:53:25', '2019-01-03 15:53:25'),
(180, 111, 'RPWK-001', 'Medium', 3800.00, 1, '2019-01-03 15:54:35', '2019-01-03 15:54:35'),
(181, 112, 'DBPWG-001', 'Medium', 5200.00, 1, '2019-01-03 15:58:20', '2019-01-03 15:58:20'),
(182, 113, 'DBSSG-001', 'Medium', 3600.00, 1, '2019-01-03 16:05:18', '2019-01-03 16:05:18'),
(183, 114, 'GLT302RPZN', 'Medium', 60990.00, 15, '2019-01-03 16:10:05', '2019-01-03 16:10:36'),
(184, 115, 'LPSS-001', 'Medium', 2200.00, 1, '2019-01-03 16:12:02', '2019-01-03 16:12:02'),
(185, 116, 'lgNB2550A', 'Medium', 24290.00, 1, '2019-01-03 16:13:31', '2019-01-03 16:13:31'),
(186, 117, 'LG sj4', 'Medium', 31790.00, 1, '2019-01-03 16:18:10', '2019-01-03 16:18:10'),
(187, 118, 'LBGS-001', 'Medium', 2800.00, 1, '2019-01-03 16:18:33', '2019-01-03 16:18:33'),
(188, 119, 'GL-C292RLBN', 'Medium', 56590.00, 15, '2019-01-03 16:22:55', '2019-01-03 16:24:20'),
(189, 120, 'SS-001', 'Medium', 3000.00, 1, '2019-01-03 16:26:29', '2019-01-03 16:26:29'),
(190, 121, 'SKSS-001', 'Medium', 2500.00, 1, '2019-01-03 16:29:46', '2019-01-03 16:29:46'),
(191, 122, 'RGS-001', 'Medium', 2500.00, 1, '2019-01-03 16:32:17', '2019-01-03 16:32:17'),
(192, 123, 'LG sj5', 'Medium', 36190.00, 1, '2019-01-03 16:36:39', '2019-01-03 16:36:39'),
(193, 124, 'LGnp55550B', 'Medium', 15090.00, 1, '2019-01-03 16:42:58', '2019-01-03 16:42:58'),
(194, 125, 'SGGS-001', 'Medium', 2500.00, 1, '2019-01-03 16:44:45', '2019-01-03 16:44:45'),
(195, 126, 'GL-B432BS', 'Medium', 87590.00, 15, '2019-01-03 16:46:42', '2019-01-03 16:47:07'),
(196, 127, 'WSPS-001', 'Medium', 2500.00, 1, '2019-01-03 16:51:03', '2019-01-03 16:51:03'),
(197, 128, 'Lg Sh5', 'Medium', 37990.00, 1, '2019-01-03 16:51:12', '2019-01-03 16:51:12'),
(198, 129, 'GL-C402RPCN', 'Medium', 76190.00, 15, '2019-01-03 16:57:55', '2019-01-03 16:58:30'),
(199, 130, 'Lg 0j98', 'Medium', 56990.00, 1, '2019-01-03 16:58:09', '2019-01-03 16:58:09'),
(200, 131, 'GS-001', 'Medium', 3000.00, 1, '2019-01-03 16:58:24', '2019-01-03 16:58:24'),
(201, 132, 'BS-001', 'Medium', 3300.00, 1, '2019-01-03 16:59:15', '2019-01-03 16:59:15'),
(202, 133, 'OS-001', 'Medium', 3000.00, 1, '2019-01-03 17:01:21', '2019-01-03 17:01:21'),
(203, 134, 'GLT322RPZN', 'Medium', 61990.00, 15, '2019-01-03 17:10:42', '2019-01-03 17:11:04'),
(204, 135, 'lg DM8360', 'Medium', 60990.00, 1, '2019-01-03 17:11:16', '2019-01-03 17:11:16'),
(205, 136, 'NS-001', 'Medium', 3600.00, 1, '2019-01-03 17:11:29', '2019-01-03 17:11:29'),
(206, 137, 'DBPWG', 'Medium', 5000.00, 1, '2019-01-03 17:15:29', '2019-01-03 17:15:29'),
(207, 138, 'SYFPG-001', 'Medium', 4000.00, 1, '2019-01-03 17:19:09', '2019-01-03 17:19:09'),
(208, 139, 'NLSP-001', 'Medium', 1700.00, 1, '2019-01-03 17:31:26', '2019-01-03 17:31:26'),
(209, 140, 'GLC292RHPN', 'Medium', 57690.00, 15, '2019-01-03 17:39:43', '2019-01-03 17:41:07'),
(210, 141, 'GLB252VLGY', 'Medium', 42890.00, 15, '2019-01-03 17:54:27', '2019-01-03 17:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 2, '98551.jpg', '2019-01-01 16:39:28', '2019-01-01 16:39:28'),
(2, 2, '55392.jpg', '2019-01-01 16:39:28', '2019-01-01 16:39:28'),
(3, 2, '54066.jpg', '2019-01-01 16:39:29', '2019-01-01 16:39:29'),
(4, 3, '2795.jpg', '2019-01-01 16:50:12', '2019-01-01 16:50:12'),
(5, 3, '36144.jpg', '2019-01-01 16:50:13', '2019-01-01 16:50:13'),
(6, 25, '5564.jpg', '2019-01-02 12:41:55', '2019-01-02 12:41:55'),
(7, 25, '46015.jpg', '2019-01-02 12:41:55', '2019-01-02 12:41:55'),
(8, 25, '55461.jpg', '2019-01-02 12:41:55', '2019-01-02 12:41:55'),
(9, 26, '82801.jpg', '2019-01-02 13:33:03', '2019-01-02 13:33:03'),
(10, 26, '70046.jpg', '2019-01-02 13:33:03', '2019-01-02 13:33:03'),
(11, 26, '77221.jpg', '2019-01-02 13:33:03', '2019-01-02 13:33:03'),
(12, 27, '8434.jpeg', '2019-01-02 13:45:06', '2019-01-02 13:45:06'),
(13, 27, '49797.jpg', '2019-01-02 13:45:06', '2019-01-02 13:45:06'),
(14, 28, '41855.jpg', '2019-01-02 14:49:50', '2019-01-02 14:49:50'),
(15, 28, '48320.jpg', '2019-01-02 14:49:51', '2019-01-02 14:49:51'),
(16, 32, '79517.jpg', '2019-01-02 16:18:26', '2019-01-02 16:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_ratings`
--

INSERT INTO `product_ratings` (`id`, `product_id`, `customer_id`, `rating`, `created_at`, `updated_at`) VALUES
(1, 4, 6, 5, '2019-01-01 17:05:53', '2019-01-01 17:05:53'),
(2, 4, 6, 5, '2019-01-01 17:06:08', '2019-01-01 17:06:08'),
(3, 4, 6, 5, '2019-01-01 17:06:18', '2019-01-01 17:06:18'),
(4, 17, 2, 5, '2019-01-02 11:59:06', '2019-01-02 11:59:06'),
(5, 3, 2, 5, '2019-01-02 12:00:25', '2019-01-02 12:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_specifications`
--

CREATE TABLE `product_specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_specifications`
--

INSERT INTO `product_specifications` (`id`, `product_id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 27, 'Hair Massager', 'This product is most suitable for the hair, neck, ankles and elbows massage. It has smooth tips and makes feel cool and relaxed as well as removes stress', '2019-01-02 13:47:12', '2019-01-02 13:47:12'),
(2, 28, 'Brand', 'AB Tech', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(3, 28, 'Operation Mode', 'Bottom Type', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(4, 28, 'Material', 'Stainless Steel', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(5, 28, 'Color', 'Black and Silver', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(6, 28, 'SKU', 'Electric Water Kettle', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(7, 28, 'Heating Speed', '5-10 Minutes', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(8, 28, 'Capacity', '1-2 liter', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(9, 28, 'Heating Method', 'Pallet Heating', '2019-01-02 14:58:35', '2019-01-02 14:58:35'),
(12, 32, 'Brand', 'Zoeva', '2019-01-02 16:19:30', '2019-01-02 16:19:30'),
(13, 32, 'Shape', 'Multishape', '2019-01-02 16:19:30', '2019-01-02 16:19:30'),
(14, 32, 'Color', 'Black', '2019-01-02 16:19:30', '2019-01-02 16:19:30'),
(15, 32, 'Pack of', '15 Brush', '2019-01-02 16:19:30', '2019-01-02 16:19:30'),
(16, 47, 'Brand', 'CG', '2019-01-03 10:57:48', '2019-01-03 10:57:48'),
(17, 47, 'Service Warranty', '1 Years', '2019-01-03 10:57:48', '2019-01-03 10:57:48'),
(18, 47, 'Screen Size', '22\'\'', '2019-01-03 10:57:48', '2019-01-03 10:57:48'),
(19, 47, 'Model', 'CG 22 D 1504', '2019-01-03 10:57:49', '2019-01-03 10:57:49'),
(20, 47, 'Battery Compatible', '12 V', '2019-01-03 10:57:49', '2019-01-03 10:57:49'),
(21, 48, 'Brand', 'CG', '2019-01-03 11:10:09', '2019-01-03 11:10:09'),
(22, 48, 'Battery Compatible', '12 V', '2019-01-03 11:10:09', '2019-01-03 11:10:09'),
(23, 48, 'Service Warranty', '1 Years', '2019-01-03 11:10:09', '2019-01-03 11:10:09'),
(24, 48, 'Price', '14990', '2019-01-03 11:10:09', '2019-01-03 11:10:09'),
(25, 48, 'Model', 'CG 20 D 1504', '2019-01-03 11:10:09', '2019-01-03 11:10:09'),
(26, 48, 'Screen Size', '20 inch', '2019-01-03 11:25:15', '2019-01-03 11:25:15'),
(27, 48, 'Analog AV Out', 'Yes', '2019-01-03 11:25:17', '2019-01-03 11:25:17'),
(28, 48, 'USB', 'Yes (Music+Photo+Movie)', '2019-01-03 11:25:20', '2019-01-03 11:25:20'),
(29, 48, 'Response time', '8ms', '2019-01-03 11:25:28', '2019-01-03 11:25:28'),
(30, 48, 'Resolution', '1920*1080p', '2019-01-03 11:25:29', '2019-01-03 11:25:29'),
(31, 51, 'resolution', '1366*768', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(32, 51, 'USB', 'Yes (Music+Photo+Movie)', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(33, 51, 'Sound Output (RMS)', '10W+10W', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(34, 51, 'PC Input', 'No', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(35, 51, 'HDMI', 'Yes (2)', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(36, 51, 'Analog AV Out', 'Yes', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(37, 51, 'Power Supply', '110~240V 50-60Hz', '2019-01-03 11:43:54', '2019-01-03 11:43:54'),
(38, 56, 'Resolution', '1366*768', '2019-01-03 12:05:20', '2019-01-03 12:05:20'),
(39, 56, 'USB', 'Yes (Music+Photo+Movie)', '2019-01-03 12:05:29', '2019-01-03 12:05:29'),
(41, 56, 'Sound Output (RMS)', '10W+10W', '2019-01-03 12:05:59', '2019-01-03 12:05:59'),
(42, 56, 'PC Input', 'No', '2019-01-03 12:06:02', '2019-01-03 12:06:02'),
(43, 53, 'Capacity', '7.5 kg', '2019-01-03 12:06:02', '2019-01-03 12:06:02'),
(44, 53, 'Display', 'LED Display', '2019-01-03 12:06:07', '2019-01-03 12:06:07'),
(45, 56, 'HDMI', 'Yes (1)', '2019-01-03 12:06:07', '2019-01-03 12:06:07'),
(46, 53, 'Motor', '6 Motion Direct Drive', '2019-01-03 12:06:13', '2019-01-03 12:06:13'),
(47, 56, 'Analog AV Out', 'Yes', '2019-01-03 12:06:13', '2019-01-03 12:06:13'),
(48, 53, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 12:06:14', '2019-01-03 12:06:14'),
(49, 56, 'Power Supply', '110~240V 50-60Hz', '2019-01-03 12:06:14', '2019-01-03 12:06:14'),
(50, 53, 'TrueSteam', 'No', '2019-01-03 12:06:16', '2019-01-03 12:06:16'),
(52, 53, 'Max. Spin Speed', '1400 RMP', '2019-01-03 12:07:11', '2019-01-03 12:07:11'),
(53, 57, 'Resolution', '1366*768', '2019-01-03 12:10:21', '2019-01-03 12:10:21'),
(55, 70, 'Response Time', 'N/A', '2019-01-03 12:58:35', '2019-01-03 12:58:35'),
(56, 70, 'USB', 'Yes (Music+Photo+Movie)', '2019-01-03 12:58:42', '2019-01-03 12:58:42'),
(57, 70, 'Sound Output (RMS)', '10W+10W', '2019-01-03 12:58:42', '2019-01-03 12:58:42'),
(58, 70, 'PC Input', 'No', '2019-01-03 12:58:43', '2019-01-03 12:58:43'),
(59, 70, 'HDMI', 'Yes (2)', '2019-01-03 12:58:44', '2019-01-03 12:58:44'),
(60, 70, 'Analog AV Out', 'Yes', '2019-01-03 12:58:45', '2019-01-03 12:58:45'),
(61, 70, 'Power Supply', '110~240V 50-60Hz', '2019-01-03 12:58:50', '2019-01-03 12:58:50'),
(62, 72, 'Display', 'LED Display', '2019-01-03 13:03:59', '2019-01-03 13:03:59'),
(63, 72, 'Motor', '6 Motion Inverter Direct Drive', '2019-01-03 13:03:59', '2019-01-03 13:03:59'),
(64, 72, 'TrueSteam', 'NO', '2019-01-03 13:03:59', '2019-01-03 13:03:59'),
(65, 72, 'Tub Clean', 'Yes', '2019-01-03 13:03:59', '2019-01-03 13:03:59'),
(66, 75, 'Response Time', 'N/A', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(67, 75, 'Resolution', '1366*768', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(68, 75, 'USB', 'Yes (Music+Photo+Movie)', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(69, 75, 'Sound Output (RMS)', '10W+10W', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(70, 75, 'PC Input', 'No', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(71, 75, 'HDMI', 'Yes (2)', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(72, 75, 'Analog AV Out', 'Yes', '2019-01-03 13:12:07', '2019-01-03 13:12:07'),
(73, 75, 'Power Supply', '110~240V 50-60Hz', '2019-01-03 13:12:08', '2019-01-03 13:12:08'),
(74, 77, 'Brand', 'LG', '2019-01-03 13:22:24', '2019-01-03 13:22:24'),
(75, 77, 'Number of Programs', '14', '2019-01-03 13:22:25', '2019-01-03 13:22:25'),
(76, 77, 'Power Supply', '220V,50Hz', '2019-01-03 13:22:25', '2019-01-03 13:22:25'),
(77, 77, 'Display', 'Touch LED', '2019-01-03 13:22:26', '2019-01-03 13:22:26'),
(78, 77, 'Capacity', '3.5 KG', '2019-01-03 13:22:26', '2019-01-03 13:22:26'),
(79, 77, 'Motor', '6 Motion Direct Drive', '2019-01-03 13:22:26', '2019-01-03 13:22:26'),
(80, 77, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 13:22:26', '2019-01-03 13:22:26'),
(81, 77, 'Max. Spin Speed', '1000RPM', '2019-01-03 13:22:26', '2019-01-03 13:22:26'),
(82, 80, 'Pulsator Type', 'Punch+3', '2019-01-03 13:32:12', '2019-01-03 13:32:12'),
(83, 80, 'Blanket Option', 'No', '2019-01-03 13:32:13', '2019-01-03 13:32:13'),
(84, 80, 'Brand', 'LG', '2019-01-03 13:36:27', '2019-01-03 13:36:27'),
(85, 80, 'Power', '230V/50Hz AC', '2019-01-03 13:36:31', '2019-01-03 13:36:31'),
(86, 80, 'Soak', 'Yes', '2019-01-03 13:36:33', '2019-01-03 13:36:33'),
(87, 80, 'No. Of Water Inlet', '2', '2019-01-03 13:36:37', '2019-01-03 13:36:37'),
(88, 84, 'Resolution', '1920*1080p', '2019-01-03 13:54:04', '2019-01-03 13:54:04'),
(89, 84, 'Response Time', '8.5 ms', '2019-01-03 13:54:05', '2019-01-03 13:54:05'),
(90, 84, 'USB', 'Yes', '2019-01-03 13:54:05', '2019-01-03 13:54:05'),
(91, 84, 'Sound Output (RMS)', '10W+10W', '2019-01-03 13:54:05', '2019-01-03 13:54:05'),
(92, 84, 'PC Input', 'No', '2019-01-03 13:54:05', '2019-01-03 13:54:05'),
(93, 84, 'HDMI', 'Yes (2)', '2019-01-03 13:54:05', '2019-01-03 13:54:05'),
(94, 89, 'Brand', 'LG', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(95, 89, 'Display', 'LED, Tact Panel (1888)', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(96, 89, 'Motor', 'NDD Inverter', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(97, 89, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(98, 89, 'Dimensions', '540×540×925 mm', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(99, 89, 'Power Supply', '230V/50Hz AC', '2019-01-03 13:57:09', '2019-01-03 13:57:09'),
(100, 91, 'Brand', 'LG', '2019-01-03 15:10:39', '2019-01-03 15:10:39'),
(101, 91, 'Display', 'LED Display', '2019-01-03 15:10:39', '2019-01-03 15:10:39'),
(102, 91, 'Motor', '6 Motion Direct Drive', '2019-01-03 15:10:39', '2019-01-03 15:10:39'),
(103, 91, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 15:10:39', '2019-01-03 15:10:39'),
(104, 91, 'Max. Spin Speed', '1000RPM', '2019-01-03 15:10:39', '2019-01-03 15:10:39'),
(105, 91, 'No. Of Wash Programs', '13', '2019-01-03 15:10:40', '2019-01-03 15:10:40'),
(106, 91, 'No. Of Water Inlet', '1', '2019-01-03 15:10:40', '2019-01-03 15:10:40'),
(107, 91, 'Power Supply', '220V,50Hz', '2019-01-03 15:10:41', '2019-01-03 15:10:41'),
(108, 91, 'Capacity', '6.0Kg', '2019-01-03 15:10:41', '2019-01-03 15:10:41'),
(109, 95, 'Brand', 'LG', '2019-01-03 15:18:26', '2019-01-03 15:18:26'),
(110, 95, 'Display', 'LED Display', '2019-01-03 15:18:26', '2019-01-03 15:18:26'),
(111, 95, 'Motor', '6 Motion Direct Drive', '2019-01-03 15:18:26', '2019-01-03 15:18:26'),
(112, 95, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 15:18:27', '2019-01-03 15:18:27'),
(113, 95, 'TrueSteam', 'No', '2019-01-03 15:18:27', '2019-01-03 15:18:27'),
(114, 95, 'Max. Spin Speed', '1000RPM', '2019-01-03 15:18:27', '2019-01-03 15:18:27'),
(116, 92, 'Response Time', '8.5 ms', '2019-01-03 15:20:31', '2019-01-03 15:20:31'),
(117, 92, 'USB', 'Yes', '2019-01-03 15:20:31', '2019-01-03 15:20:31'),
(118, 92, 'Sound Output (RMS)', '10W+10W', '2019-01-03 15:20:31', '2019-01-03 15:20:31'),
(119, 92, 'HDMI', 'No', '2019-01-03 15:20:31', '2019-01-03 15:20:31'),
(120, 101, 'Brand', 'LG', '2019-01-03 15:32:56', '2019-01-03 15:32:56'),
(121, 101, 'Display', 'LED Display', '2019-01-03 15:32:57', '2019-01-03 15:32:57'),
(122, 101, 'Motor', '6 Motion Direct Drive', '2019-01-03 15:32:57', '2019-01-03 15:32:57'),
(123, 101, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 15:32:57', '2019-01-03 15:32:57'),
(124, 101, 'TrueSteam', 'No', '2019-01-03 15:32:57', '2019-01-03 15:32:57'),
(126, 102, 'USB', 'Yes (Music+Movies+Images)', '2019-01-03 15:33:53', '2019-01-03 15:33:53'),
(127, 102, 'Response Time', '8.5 ms', '2019-01-03 15:33:54', '2019-01-03 15:33:54'),
(128, 102, 'Sound Output (RMS)', '5W+5W', '2019-01-03 15:33:54', '2019-01-03 15:33:54'),
(129, 102, 'PC Input', 'No', '2019-01-03 15:33:55', '2019-01-03 15:33:55'),
(130, 102, 'HDMI', 'Yes (2)', '2019-01-03 15:33:55', '2019-01-03 15:33:55'),
(132, 105, 'Response Time', '8.5 ms', '2019-01-03 15:45:12', '2019-01-03 15:45:12'),
(133, 105, 'USB', 'Yes (Music+Movies+Images)', '2019-01-03 15:45:12', '2019-01-03 15:45:12'),
(134, 105, 'Sound Output (RMS)', '10W+10W', '2019-01-03 15:45:12', '2019-01-03 15:45:12'),
(135, 105, 'PC Input', 'No', '2019-01-03 15:45:13', '2019-01-03 15:45:13'),
(136, 105, 'HDMI', 'Yes (2)', '2019-01-03 15:45:13', '2019-01-03 15:45:13'),
(137, 106, 'Brand', 'LG', '2019-01-03 15:45:20', '2019-01-03 15:45:20'),
(138, 106, 'Display', 'Touch LED + DIAL', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(139, 106, 'Motor', '6 Motion Direct Drive', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(140, 106, 'Max. Spin Speed', '1400RPM', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(141, 106, 'Power Supply', '220V,50Hz', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(142, 106, 'Number Of Programs', '14', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(143, 106, 'Capacity', '8,0KG', '2019-01-03 15:45:21', '2019-01-03 15:45:21'),
(144, 109, 'Brand', 'LG', '2019-01-03 15:52:45', '2019-01-03 15:52:45'),
(145, 109, 'Display', 'LED Display', '2019-01-03 15:52:45', '2019-01-03 15:52:45'),
(146, 109, 'Motor', '6 Motion Direct Drive', '2019-01-03 15:52:45', '2019-01-03 15:52:45'),
(147, 109, 'Motor Warranty', '10 years on Direct Dirve motor', '2019-01-03 15:52:46', '2019-01-03 15:52:46'),
(148, 109, 'Max. Spin Speed', '1150RPM', '2019-01-03 15:52:46', '2019-01-03 15:52:46'),
(149, 109, 'TrueSteam', 'Yes', '2019-01-03 15:52:46', '2019-01-03 15:52:46'),
(150, 110, 'Sound Output (RMS)', '1000 W', '2019-01-03 16:09:07', '2019-01-03 16:09:07'),
(151, 110, 'Channel', '5.1', '2019-01-03 16:09:08', '2019-01-03 16:09:08'),
(153, 110, 'Power Output - Center', '167W (Peak : 184W)', '2019-01-03 16:09:45', '2019-01-03 16:09:45'),
(154, 116, 'USB', 'Yes', '2019-01-03 16:14:57', '2019-01-03 16:14:57'),
(155, 116, 'Sound Output (RMS)', '80 W', '2019-01-03 16:14:59', '2019-01-03 16:14:59'),
(156, 117, 'Dimensions', '890 x 55 x 85 mm', '2019-01-03 16:20:38', '2019-01-03 16:20:38'),
(158, 117, 'Sound Output (RMS)', '300 W', '2019-01-03 16:20:39', '2019-01-03 16:20:39'),
(159, 119, 'Brand', 'LG', '2019-01-03 16:29:52', '2019-01-03 16:29:52'),
(160, 119, 'Gross Capacity (Ltr.)', '258', '2019-01-03 16:29:52', '2019-01-03 16:29:52'),
(161, 119, 'Door Finish', 'High Gloss Finish', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(162, 119, 'Cooling Type', 'Frost Free', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(163, 119, 'Door Baskets', 'Transparent', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(164, 119, 'Shelves', 'Trimless Toughened Glass Shelves', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(165, 119, 'Chiller', 'N/R', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(166, 119, 'Low Voltage Startup (Range)', '100V~290V', '2019-01-03 16:29:53', '2019-01-03 16:29:53'),
(167, 123, 'Dimensions', '950 x 55 x 85 mm', '2019-01-03 16:39:04', '2019-01-03 16:39:04'),
(169, 123, 'Sound Output (RMS)', '320 W', '2019-01-03 16:39:05', '2019-01-03 16:39:05'),
(170, 114, 'Brand', 'LG', '2019-01-03 16:42:04', '2019-01-03 16:42:04'),
(171, 114, 'Capacity', '285 Ltrs', '2019-01-03 16:42:04', '2019-01-03 16:42:04'),
(172, 114, 'Door Finish', 'High Gloss Finish', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(173, 114, 'Cooling Type', 'Door Cooling+', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(174, 114, 'Door Baskets', 'Transparent', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(175, 114, 'Shelves', 'Trimless Toughened Glass', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(176, 114, 'Low Voltage Startup (Range)', '100v~290v', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(177, 114, 'Dimensions', '1575x585x703 mm', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(178, 114, 'Condensor', 'Concealed', '2019-01-03 16:42:05', '2019-01-03 16:42:05'),
(179, 124, 'Sound Output (RMS)', '10 W', '2019-01-03 16:47:27', '2019-01-03 16:47:27'),
(181, 124, 'Dimensions', '153 x 58 x 59.5 mm', '2019-01-03 16:47:27', '2019-01-03 16:47:27'),
(182, 124, 'Channel', '2 Ch', '2019-01-03 16:47:27', '2019-01-03 16:47:27'),
(183, 124, 'Charging Time', '2h 30min.', '2019-01-03 16:47:27', '2019-01-03 16:47:27'),
(184, 126, 'Brand', 'LG', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(185, 126, 'Gross Capacity (Ltr.)', '422', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(186, 126, 'Door Finish', 'High Gloss Finish', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(187, 126, 'Cooling Type', 'Door Cooling+', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(188, 126, 'Door Baskets', 'Transparent', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(189, 126, 'Shelves', 'Trimless Toughened Glass Shelves', '2019-01-03 16:51:41', '2019-01-03 16:51:41'),
(191, 128, 'Sound Output (RMS)', '320 W', '2019-01-03 16:53:15', '2019-01-03 16:53:15'),
(192, 129, 'Brand', 'LG', '2019-01-03 17:04:02', '2019-01-03 17:04:02'),
(193, 129, 'Gross Capacity (Ltr.)', '360 Ltrs.', '2019-01-03 17:04:09', '2019-01-03 17:04:09'),
(194, 129, 'Door Finish', 'High Gloss Finish', '2019-01-03 17:04:13', '2019-01-03 17:04:13'),
(195, 129, 'Cooling Type', 'Door Cooling+', '2019-01-03 17:04:14', '2019-01-03 17:04:14'),
(196, 129, 'Door Baskets', 'Transparent', '2019-01-03 17:04:16', '2019-01-03 17:04:16'),
(197, 129, 'Shelves', 'Trimless Toughened Glass Shelves', '2019-01-03 17:04:17', '2019-01-03 17:04:17'),
(198, 129, 'Vegetable Tray', 'N/A', '2019-01-03 17:04:17', '2019-01-03 17:04:17'),
(199, 129, 'Condensor', 'Concealed', '2019-01-03 17:04:18', '2019-01-03 17:04:18'),
(200, 129, 'Low Voltage Startup (Range)', '100v~290v', '2019-01-03 17:04:19', '2019-01-03 17:04:19'),
(202, 135, 'USB', '2.0', '2019-01-03 17:13:40', '2019-01-03 17:13:40'),
(203, 135, 'Sound Output (RMS)', '515W', '2019-01-03 17:13:41', '2019-01-03 17:13:41'),
(205, 135, 'Karaoke', 'Yes', '2019-01-03 17:13:43', '2019-01-03 17:13:43'),
(206, 134, 'Brand', 'LG', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(207, 134, 'Gross Capacity (Ltr.)', '310 Ltrs', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(208, 134, 'Door Finish', 'High Gloss Finish', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(209, 134, 'Cooling Type', 'Frost Free', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(210, 134, 'Door Baskets', 'Transparent', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(211, 134, 'Shelves', 'Trimless Toughened Glass Shelves', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(212, 134, 'Chiller', 'N/A', '2019-01-03 17:16:38', '2019-01-03 17:16:38'),
(213, 140, 'Brand', 'LG', '2019-01-03 17:46:11', '2019-01-03 17:46:11'),
(214, 140, 'Door Baskets', 'Transparent', '2019-01-03 17:46:11', '2019-01-03 17:46:11'),
(215, 140, 'Low Voltage Startup (Range)', '100v-290v', '2019-01-03 17:48:43', '2019-01-03 17:48:43'),
(216, 140, 'Shelves', 'Trimless Toughened Glass Shelves', '2019-01-03 17:48:43', '2019-01-03 17:48:43'),
(217, 140, 'Cooling Type', 'Frost Free', '2019-01-03 17:48:43', '2019-01-03 17:48:43'),
(218, 140, 'Gross Capacity (Ltr.)', '258 Ltr', '2019-01-03 17:48:43', '2019-01-03 17:48:43');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viber_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `organization_name`, `email`, `logo`, `support_time`, `phone`, `address`, `about`, `facebook_uri`, `instagram_uri`, `linkedin_uri`, `youtube_uri`, `twitter_uri`, `viber_uri`, `whatsapp_uri`, `created_at`, `updated_at`) VALUES
(1, 'Zeal Sale', 'contact@onlinezeal.com', '81037.png', '9:00am - 7:00pm', '(+977) 01 5199625', 'Near Prabhu Bank, Tinkune, Kathmandu', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://www.linkedin.com/feed/', 'https://www.youtube.com/', 'https://twitter.com/', 'https://www.viber.com/', 'https://www.whatsapp.com/', '2018-12-31 11:04:32', '2019-01-04 06:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tacs`
--

CREATE TABLE `tacs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `admin`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@onlinezeal.com', '1', NULL, '$2y$10$YznGNrB79oUPw/sKnmWMdeLIAJaia7KD1grj47H7/tBu6iLMAJjJm', 'AaUJQURp4vaPOIpOcBcZmb4du7y26yL7b3NdzTvjJs0waJE5u2NGAmmKOlUB', '2018-12-31 11:04:32', '2018-12-31 11:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `avatar`, `address`, `phone`, `user_id`, `about`, `created_at`, `updated_at`) VALUES
(1, 'public/backend/images/avatar.png', 'Tinkune, Kathmandu', '9803961736', '1', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, blanditiis.', '2018-12-31 11:04:32', '2018-12-31 11:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkouts`
--
ALTER TABLE `checkouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tacs`
--
ALTER TABLE `tacs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `checkouts`
--
ALTER TABLE `checkouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_specifications`
--
ALTER TABLE `product_specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tacs`
--
ALTER TABLE `tacs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

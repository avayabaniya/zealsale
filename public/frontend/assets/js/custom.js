	$("#slider-range").slider({
02
	  range: true,
03
	  orientation: "horizontal",
04
	  min: 0,
05
	  max: 10000,
06
	  values: [0, 10000],
07
	  step: 100,
08
	 
09
	  slide: function (event, ui) {
10
	    if (ui.values[0] == ui.values[1]) {
11
	      return false;
12
	    }
13
	     
14
	    $("#min_price").val(ui.values[0]);
15
	    $("#max_price").val(ui.values[1]);
16
	  }
17
	});
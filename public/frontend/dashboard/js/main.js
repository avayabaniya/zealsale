$ = jQuery.noConflict();

$(document).ready(function(){
    $('.cart').hide();
    $('.details-content').hide();
    $('.payment-methods').hide();

    $('.dashBoard').on('click',function(){
        $('.dashboard-content-cont').show();
        $('.details-content').hide();
        $('.cart').hide();
        $('.payment-methods').hide();
        $('.title').html("<h4>My Account</h4>");
        $('.dashboard-location').html("Dashboard");
        $('.dashBoard').addClass('dash-active');
        $('.orders').removeClass('dash-active');
        $('.details').removeClass('dash-active');
        $('.payment').removeClass('dash-active');

    });
    $('.orders').on('click',function(){
        $('.dashboard-content-cont').hide();
        $('.details-content').hide();
        $('.payment-methods').hide();
        $('.cart').show();
        $('.title').html("<h4>Orders</h4>");
        $('.dashboard-location').html("Orders");
        $('.orders').addClass('dash-active');
        $('.dashBoard').removeClass('dash-active');
        $('.details').removeClass('dash-active');
        $('.payment').removeClass('dash-active');
    });
    $('.payment').on('click',function(){
        $('.dashboard-content-cont').hide();
        $('.cart').hide();
        $('.details-content').hide();
        $('.payment-methods').show();
        $('.title').html("<h4>Payment</h4>");
        $('.dashboard-location').html("Payment Methods");
        $('.payment').addClass('dash-active');
        $('.details').removeClass('dash-active');
        $('.dashBoard').removeClass('dash-active');
        $('.orders').removeClass('dash-active');
    });
    $('.details').on('click',function(){
        $('.dashboard-content-cont').hide();
        $('.cart').hide();
        $('.payment-methods').hide();
        $('.details-content').show();
        $('.title').html("<h4>Accounts Details</h4>");
        $('.dashboard-location').html("Account Details");
        $('.details').addClass('dash-active');
        $('.dashBoard').removeClass('dash-active');
        $('.orders').removeClass('dash-active');
        $('.payment').removeClass('dash-active');
    });
});

$(document).ready(function(){
    $('.additional-info').hide();
    $('.payment-methods').on('click',function(){
        var infoToShow=$(this).attr('data-infoClass')
        $('.'+infoToShow).toggle();
    });
});


            // Instantiate EasyZoom instances
            var $easyzoom = $('.easyzoom').easyZoom();
    
            // Setup thumbnails example
            var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
    
            $('.thumbnails').on('click', 'a', function(e) {
                var $this = $(this);
    
                e.preventDefault();
    
                // Use EasyZoom's `swap` method
                api1.swap($this.data('standard'), $this.attr('href'));
            });
    
            // Setup toggles example
            var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');
    
            $('.toggle').on('click', function() {
                var $this = $(this);
    
                if ($this.data("active") === true) {
                    $this.text("Switch on").data("active", false);
                    api2.teardown();
                } else {
                    $this.text("Switch off").data("active", true);
                    api2._init();
                }
            });
        



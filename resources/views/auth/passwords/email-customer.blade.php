@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">

        <div class="container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <section class="section">
                        <div class="box" style="margin:20px;margin-top:60px;">
                            <div class="card">
                                <div class="text-center">
                                    
                    <div class="card-header" style="text-align:center">{{ __('Reset Password') }}</div>
    
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h3 style="text-align:center">Customer Reset Password</h3>
                        <form method="POST" action="{{ route('customer.password.email') }}">
                            @csrf
    
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
    
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <p style="text-align:center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
                            </div>
                        </div>
                        <!-- End of .container -->
                    </section>
                    <!-- End of .search-results -->
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->

@endsection

    

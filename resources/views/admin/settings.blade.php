@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Admin Account Setting</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Admin Account Setting</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">


        <div class="row">

            <!-- right column -->
            <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Update Email / Password </h4>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-6" style="margin-left: 20px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))

                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>

                    <form id="update_password" action="{{ route('admin.settings') }}" method="post">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label>Email:</label>
                                <input name="email" class="form-control" placeholder="Email" id="inputEmail3" autocomplete="off" value="{{old('email')}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('email') }}</p>
                                <p id="correct_email" style="color: green;"></p>
                            </div>

                            <div class="form-group">
                                <label>Current Password:</label>
                                <input type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="Current Password">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('oldPassword') }}</p>
                                <p id="correct_pwd" style="color: green;"></p>
                            </div>

                            <div class="form-group">
                                <label>New Password:</label>
                                <input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="New Password">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('newPassword') }}</p>
                            </div>

                            <div class="form-group">
                                <label>Confirm Password:</label>
                                <input class="form-control" name="confirmPassword" id="confirmPassword" type="password" placeholder="Confirm Password">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('confirmPassword') }}</p>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Update Account</button>
                        </div>
                    </form>

                </div>
                <!-- /.box -->
            </div>
        </div>


    </section>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#update_password').validate({
                rules: {
                    email:{
                        required: true,
                        email: true
                    },
                    oldPassword:{
                        required: true,
                        minlength:6,
                        maxlength:20
                    },
                    newPassword:{
                        required: true,
                        minlength:6,
                        maxlength:20
                    },
                    confirmPassword:{
                        required: true,
                        minlength:6,
                        maxlength:20,
                        equalTo:"#newPassword"
                    }
                },
                messages: {
                    email: {
                        required: "Your email is required"
                    },
                    oldPassword: {
                        required: "Your Current Password is required"
                    },
                    newPassword: {
                        required: "Please enter your new password"
                    },
                    confirmPassword: {
                        required: "Please confirm your new password",
                        equalTo: "This password should match your new password"
                    }
                }
            });
        });
    </script>

    {{--Check current password--}}
    <script>
        $("#oldPassword").keyup(function(){
		var current_pwd = $("#oldPassword").val();
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type:'post',
			url:'check-pwd',
			data:{current_pwd:current_pwd},
			success:function(resp){

				if(resp=="false"){

                    $("#correct_pwd").text("Entered value does not match the current password").css("color","red");

				}else if(resp=="true"){

					$("#correct_pwd").text("Current password matched").css("color","green");
				}
			},error:function(resp){

			}
		});
	});
    </script>

    {{--Check current email--}}
    <script>
        $("#inputEmail3").keyup(function(){
            var current_email = $("#inputEmail3").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'check-email',
                data:{current_email:current_email},
                success:function(resp){
                    if(resp=="false"){

                        $("#correct_email").text("Entered email does not match the current email").css("color","red");

                    }else if(resp=="true"){

                        $("#correct_email").text("Current email matched").css("color","green");
                    }
                },error:function(resp){

                }
            });
        });
    </script>
@endsection

@section('styles')
    <style>
        .error{
            color: red;
        }
    </style>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}">

    <title>Log In - {{$sites_data->organization_name }} </title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')}}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{asset('public/backend/css/bootstrap-extend.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/backend/css/master_style.css')}}">

    <!-- Superieur Admin skins -->
    <link rel="stylesheet" href="{{asset('public/backend/css/skins/_all-skins.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition bg-img" style="background-image: url({{ asset('public/images/backend_images/ecommerce-banner.jpg') }})" data-overlay="4">

<div class="auth-2-outer row align-items-center h-p100 m-0">
    <div class="auth-2">
        <div class="text-center" style="margin-bottom:20px;">
            <img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}" alt="" class="img-responsive" width="150" height="100">
        </div>
        <div class="auth-logo font-size-40">
            {{-- <a href="#" class="text-white"><b>Zeal Sale</b></a> --}}
        </div>
        <!-- /.login-logo -->
        <div class="auth-body">
            <p class="auth-msg">Sign in to start your session</p>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_error') !!} </strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_success') !!} </strong>
                </div>
            @endif

            <form action="{{route('admin.login')}}" method="post" class="form-element">
                @csrf
                <div class="form-group has-feedback">
                    <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                    <span class="ion ion-email form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                    <span class="ion ion-locked form-control-feedback"></span>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="checkbox">
                            <input type="checkbox" id="basic_checkbox_1">
                            <label for="basic_checkbox_1">Remember Me</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <div class="fog-pwd">
                            <a href="{{route('password.request')}}" class="text-white"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-block mt-10 btn-success">SIGN IN</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>

</div>


<!-- jQuery 3 -->
<script src="{{asset('public/backend/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js')}}"></script>

<!-- popper -->
<script src="{{asset('public/backend/assets/vendor_components/popper/dist/popper.min.js')}}"></script>

<!-- Bootstrap 4.0-->
<script src="{{asset('public/backend/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

</body>
</html>

@extends('layouts.adminLayout.admin_design')
@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto w-p50">
                <h3 class="page-title">e-Commerce</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                            <li class="breadcrumb-item active" aria-current="page">Control</li>
                        </ol>
                    </nav>
                </div>
            </div>
			<div class="right-title w-170">
				<span class="subheader_daterange font-weight-600" id="dashboard_daterangepicker">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title"></span>
						<span class="subheader_daterange-date text-primary"></span>
					</span>
					<a href="#" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box box-body">
                    <h6 class="mb-0">
                        <span class="text-uppercase">ORDER RECEIVED</span>
                        <span class="float-right"><a class="btn btn-xs btn-primary" href="{{ route('order.index') }}">View</a></span>
                    </h6>
                    <br>
                    
                    <small>Total Order</small>
                    <p class="font-size-26">{{ $order_received->count() }}</p>

                    <div class="progress progress-xxs mt-0 mb-10">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    {{-- <div class="font-size-12"><i class="ion-arrow-graph-up-right text-success mr-1"></i> %18 decrease from last month</div> --}}
                </div>
            </div>
            
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box box-body">
                    <h6 class="mb-0">
                        <span class="text-uppercase">SALES STATUS</span>
                        <span class="float-right"><a class="btn btn-xs btn-success" href="{{ route('order.index') }}">View</a></span>
                    </h6>
                    <br>
                    <small>Total Sales</small>
                    
                    <p class="font-size-26"> {{ $total_sales->count() }}</p>

                    <div class="progress progress-xxs mt-0 mb-10">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 52%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="flexbox font-size-12">
                        {{-- <span><i class="ion-arrow-graph-down-right text-success mr-1"></i> %37 up last year</span> --}}
                    </div>
                </div>
            </div>
            <!-- /.col -->
            
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box box-body">
                    <h6 class="mb-0">
                        <span class="text-uppercase">SALES CANCELLED</span>
                        <span class="float-right"><a class="btn btn-xs btn-danger" href="{{ route('order.index') }}">View</a></span>
                    </h6>
                    <br>
                    <small>Order Cancelled</small>
                   
                    <p class="font-size-26">  {{ $total_cancelled->count() }}</p>

                    <div class="progress progress-xxs mt-0 mb-10">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: 52%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="flexbox font-size-12">
                        {{-- <span><i class="ion-arrow-graph-down-right text-success mr-1"></i> %37 up last year</span> --}}
                    </div>
                </div>
            </div>
            <!-- /.col -->

            <div class="col-xl-3 col-md-6 col-12">
                <div class="box box-body">
                    <h6 class="mb-0">
                        <span class="text-uppercase">REVENUE STATUS</span>
                        <span class="float-right"><a class="btn btn-xs btn-info" href="{{ route('order.index') }}">View</a></span>
                    </h6>
                    <br>
                    
                    <?php $total_price = 0; ?>
                    @foreach($total_revenue as $att)
                            <?php $total_price = $att->order_price + $total_price; ?>
                    @endforeach
                    <small>Total Revenue</small>
                    <p class="font-size-26">Rs. {{ $total_price }}</p>

                    <div class="progress progress-xxs mt-0 mb-10">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 55%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    {{-- <div class="font-size-12"><i class="ion-arrow-graph-down-right text-danger mr-1"></i> %41 down last year</div> --}}
                </div>
            </div>
            <!-- /.col -->

        </div>

        <div class="row">

            <div class="col-lg-12 col-12 connectedSortable">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Product Order List</h4>
                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                            <div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" id="productorder_wrapper">
                                <table id="invoice-list" class="table table-hover no-wrap product-order dataTable no-footer" data-page-size="10">
                                <thead>
                                <tr>
                                    <th>#Order</th>
                                    <th>Product Name</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Order By</th>
                                    <th>Change Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order_list as $ol)
                                <tr>
                                    <td>#{{ $ol->id }}</td>
                                    <td>{{ $ol->product->product_name }}</td>
                                    <td>{{ $ol->product->price }}</td>
                                    <td>
                                        @if($ol->order_status === 'pending')
                                            <span class="label label-warning" >
                                            Pending
                                            </span>
                                        @elseif($ol->order_status === 'on-way')
                                            <span class="label label-info">
                                            On way
                                            </span>
                                        @elseif($ol->order_status === 'completed')
                                            <span class="label label-success">
                                            Completed
                                            </span>
                                        @elseif($ol->order_status === 'cancelled')
                                            <span class="label label-danger">
                                            Cancelled
                                            </span>
                                        @endif
                                    </td>
                                    <td>{{ $ol->customer->full_name }}</td>
                                    <td>
                                        <form id="store" name="store" method="post" action="{{ route('order.update') }}">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{ $ol->id }}" name="order_id">
                                            <input type="hidden" value="{{ $ol->order_status }}" name="old_status">
                                            <select name="status" id="status" onchange="this.form.submit()">
                                                <option value="pending" @if($ol->order_status == 'pending') selected @endif>Pending</option>
                                                <option value="on-way" @if($ol->order_status == 'on-way') selected @endif>On way</option>
                                                <option value="completed" @if($ol->order_status == 'completed') selected @endif>Completed</option>
                                                <option value="cancelled" @if($ol->order_status == 'cancelled') selected @endif>Cancelled</option>
                                            </select>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>

            <div class="col-lg-6 col-12 connectedSortable">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Recent Product Ratings</h4>
                        <div class="box-controls pull-right">
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="media-list media-list-divided">
                            @foreach($recent_ratings as $rating)
                            <div class="media p-0">
                                <div class="media-body m-0">
                                    <div class="flexbox">
                                        <div>
                                            <h4 class="mb-0">For {{ $rating->product->product_name }}</h4>
                                        </div>
                                        <div>
                                            @if($rating->rating == 1)
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star"></a>
                                            <a class="fa fa-star"></a>
                                            <a class="fa fa-star"></a>
                                            <a class="fa fa-star"></a>
                                            @endif
                                            @if($rating->rating == 2)
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star"></a>
                                            <a class="fa fa-star"></a>
                                            <a class="fa fa-star"></a>
                                            @endif
                                            @if($rating->rating == 3)
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" ></a>
                                            <a class="fa fa-star" ></a>
                                            @endif
                                            @if($rating->rating == 4)
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" ></a>
                                            @endif
                                            @if($rating->rating == 5)
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            <a class="fa fa-star" style="color:gold"></a>
                                            @endif
                                        </div>
                                    </div>
                                    <p>By<strong>&nbsp;{{ $rating->customer->full_name }}</strong> </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-lg-6 connectedSortable">
                <div class="box">
                    <div class="box-header with-border">
                        <h5 class="box-title">Resent Notifications</h5>
                    </div>
                    <div class="box-body p-15">
                        <div class="media-list media-list-hover">
                            <a class="media media-single" href="#">
                                <h4 class="w-50 text-gray font-weight-500">10:10</h4>
                                <div class="media-body pl-15 bl-5 rounded border-primary">
                                    <p>Morbi quis ex eu arcu auctor sagittis.</p>
                                    <span class="text-fade">by Johne</span>
                                </div>
                            </a>

                            <a class="media media-single" href="#">
                                <h4 class="w-50 text-gray font-weight-500">08:40</h4>
                                <div class="media-body pl-15 bl-5 rounded border-success">
                                    <p>Proin iaculis eros non odio ornare efficitur.</p>
                                    <span class="text-fade">by Amla</span>
                                </div>
                            </a>

                            <a class="media media-single" href="#">
                                <h4 class="w-50 text-gray font-weight-500">07:10</h4>
                                <div class="media-body pl-15 bl-5 rounded border-info">
                                    <p>In mattis mi ut posuere consectetur.</p>
                                    <span class="text-fade">by Josef</span>
                                </div>
                            </a>

                            <a class="media media-single" href="#">
                                <h4 class="w-50 text-gray font-weight-500">01:15</h4>
                                <div class="media-body pl-15 bl-5 rounded border-danger">
                                    <p>Morbi quis ex eu arcu auctor sagittis.</p>
                                    <span class="text-fade">by Rima</span>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

@endsection
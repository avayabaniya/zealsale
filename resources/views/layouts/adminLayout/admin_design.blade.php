<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}">

    <title>{{$sites_data->organization_name}} - Dashboard</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('public/backend/assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('public/backend/css/bootstrap-extend.css') }}">

    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('public/backend/css/master_style.css') }}">

    <!-- Superieur Admin skins -->
    <link rel="stylesheet" href="{{ asset('public/backend/css/skins/_all-skins.css') }}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('public/backend/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

    <!-- Morris charts -->
    <link rel="stylesheet" href="{{ asset('public/backend/assets/vendor_components/morris.js/morris.css') }}">

    <!-- Data Table-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/backend/assets/vendor_components/datatable/datatables.min.css') }}"/>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">


    <!--alerts CSS -->
    <link href="{{asset('public/backend/assets/vendor_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">

    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>


    <!--alerts CSS -->
    <link href="{{ asset('public/backend/assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    {{-- Select 2 --}}
    <link rel="stylesheet" href="{{ asset('public/backend/assets/vendor_components/select2/dist/css/select2.min.css') }}">
    
	<!-- Bootstrap tagsinput -->
	<link rel="stylesheet" href="{{ asset('public/backend/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('styles')

</head>

<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">

    @include('layouts.adminLayout.admin_header')

    @include('layouts.adminLayout.admin_sidebar')

  

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    
    @include('layouts.adminLayout.admin_footer')

</div>
<!-- ./wrapper -->



<!-- jQuery 3 -->
<script src="{{ asset('public/backend/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js') }}"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/backend/assets/vendor_components/jquery-ui/jquery-ui.js') }}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- popper -->
<script src="{{ asset('public/backend/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('public/backend/assets/vendor_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/backend/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<!-- Bootstrap 4.0-->
<script src="{{ asset('public/backend/assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>

<!-- Slimscroll -->
<script src="{{ asset('public/backend/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- echarts -->
<script src="{{ asset('public/backend/assets/vendor_components/echarts/dist/echarts-en.min.js') }}"></script>
<script src="{{ asset('public/backend/assets/vendor_components/echarts/echarts-liquidfill.min.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('public/backend/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

<!-- Morris.js charts -->
<script src="{{ asset('public/backend/assets/vendor_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/backend/assets/vendor_components/morris.js/morris.min.js') }}"></script>

<!-- This is data table -->
<script src="{{ asset('public/backend/assets/vendor_components/datatable/datatables.min.js') }}"></script>



<!-- Superieur Admin App -->
<script src="{{ asset('public/backend/js/template.js') }}"></script>

<!-- Superieur Admin dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('public/backend/js/pages/dashboard2.js') }}"></script>

<!-- Superieur Admin for demo purposes -->
<script src="{{ asset('public/backend/js/demo.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<!-- Select2 -->
<script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>

<!-- Superieur Admin for advanced form element -->
<script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>

<!-- Bootstrap tagsinput -->
<script src="{{ asset('public/backend/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js') }}"></script>

{{--SweetAlert--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-danger',
                    buttonStyling: false,
                    reverseButtons: true
                },
                function () {
                    window.location.href="/zealsale/admin/"+deleteFunction+"/"+id;

                });
        });
    });
</script>


{{-- File Input Validation --}}
<script type="text/javascript">
    $("#file-1").fileinput({
        theme: 'fa',
        uploadUrl: "/image-view",
        uploadExtraData: function() {
            return {
                _token: $("input[name='_token']").val(),
            };
        },
        allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize:100000,
        maxFilesNum: 20,
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
</script>

<script type="text/javascript">
    function fileValidation(){
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
       alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
       fileInput.value = '';
       return false;
    }else{
       //Image preview
       if (fileInput.files && fileInput.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
           };
           reader.readAsDataURL(fileInput.files[0]);
       }
   }
}

 function fileValidation1(){
    var fileInput1 = document.getElementById('file1');
    var filePath = fileInput1.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
       alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
       fileInput1.value = '';
       return false;
    }else{
       //Image preview
       if (fileInput1.files && fileInput1.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
           };
           reader.readAsDataURL(fileInput1.files[0]);
       }
   }
}
function fileValidation2(){
    var fileInput2 = document.getElementById('file1');
    var filePath = fileInput2.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
       alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
       fileInput2.value = '';
       return false;
    }else{
       //Image preview
       if (fileInput2.files && fileInput2.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
           };
           reader.readAsDataURL(fileInput2.files[0]);
       }
   }
}
function fileValidation3(){
    var fileInput3 = document.getElementById('file1');
    var filePath = fileInput3.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
       alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
       fileInput3.value = '';
       return false;
    }else{
       //Image preview
       if (fileInput3.files && fileInput3.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
           };
           reader.readAsDataURL(fileInput3.files[0]);
       }
   }
}



</script>

<!-- Sweet-Alert  -->
<script src="{{ asset('public/backend/assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('public/backend/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>



@yield('scripts')

</body>
</html>

<?php
use \App\Http\Controllers\Controller;
$pendingOrders = Controller::pendingOrders();
?>


<!-- Left side column. contains the logo and sidebar -->
@if(!empty(auth()->user()))
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="user-profile treeview">
                <a >
                    <img src="{{ asset(auth()->user()->userprofile->avatar) }}" alt="user">
                    <span>
            <span class="d-block font-weight-600 font-size-16">{{ auth()->user()->name }}</span>
            <span class="email-id">{{ auth()->user()->email }}</span>
          </span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
                <ul class="treeview-menu">
                    {{-- <li><a href="javascript:void()"><i class="fa fa-user mr-5"></i>My Profile </a></li> --}}
                    {{-- <li><a href="javascript:void()"><i class="fa fa-money mr-5"></i>My Balance</a></li> --}}
                    {{-- <li><a href="javascript:void()"><i class="fa fa-envelope-open mr-5"></i>Inbox</a></li> --}}
                    <li><a href="{{route('admin.settings')}}"><i class="fa fa-cog mr-5"></i>Account Setting</a></li>
                    <li><a href="{{route('admin.logout')}}"><i class="fa fa-power-off mr-5"></i>Logout</a></li>
                </ul>
            </li>

            <li>
                <a href="{{url('admin/dashboard')}}">
                    <i class="mdi mdi-chart-bar"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-content-copy"></i>
                    <span>Category</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('category.create') }}"><i class="mdi mdi-toggle-switch-off"></i>Add Category</a></li>
                    <li><a href="{{ route('category.index') }}"><i class="mdi mdi-toggle-switch-off"></i>View Category</a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-medical-bag"></i>
                    <span>Product</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('product.create') }}"><i class="mdi mdi-toggle-switch-off"></i>Create Product</a></li>
                    <li><a href="{{ route('product.index') }}"><i class="mdi mdi-toggle-switch-off"></i>View Product</a></li>
                    <li><a href="{{ route('product.softdelete') }}"><i class="mdi mdi-toggle-switch-off"></i>Soft Deletes</a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-basket"></i>
                    <span>Orders &nbsp; </span> <span class="label label-danger" style="border-radius: 0;">{{$pendingOrders}} Pending</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('order.index') }}"><i class="mdi mdi-toggle-switch-off"></i> View Orders</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-ticket"></i>
                    <span>Coupons  &nbsp; </span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('coupon.index') }}"><i class="mdi mdi-toggle-switch-off"></i> Create / View Coupons</a></li>
                    <li><a href="{{ route('coupon.edit',1) }}"><i class="mdi mdi-toggle-switch-off"></i> Edit New Customer Coupons</a></li>
                    <li><a href="{{ route('coupon.edit',2) }}"><i class="mdi mdi-toggle-switch-off"></i> Edit Total Discount Coupons</a></li>
                
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-ticket"></i>
                    <span>Coupon Category  &nbsp; </span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('couponcategory.index') }}"><i class="mdi mdi-toggle-switch-off"></i>  View Coupons Category</a></li>
                    <li><a href="{{ route('couponcategory.create') }}"><i class="mdi mdi-toggle-switch-off"></i> Create Coupons Category</a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-settings"></i>
                    <span>Site Settings</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('sitesetting.edit',1) }}"><i class="mdi mdi-toggle-switch-off"></i>View / Edit</a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-information"></i>
                    <span>About Us</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('about.edit',1) }}"><i class="mdi mdi-toggle-switch-off"></i>View / Edit</a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-comment-question-outline"></i>
                    <span>FAQs</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('faq.create') }}"><i class="mdi mdi-toggle-switch-off"></i> Create </a></li>
                    <li><a href="{{ route('faq.index') }}"><i class="mdi mdi-toggle-switch-off"></i> View </a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-book-open-page-variant"></i>
                    <span>Terms & Conditions</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('tac.create') }}"><i class="mdi mdi-toggle-switch-off"></i> Create </a></li>
                    <li><a href="{{ route('tac.index') }}"><i class="mdi mdi-toggle-switch-off"></i> View </a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-cards-playing-outline"></i>
                    <span>Slider</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('slider.index') }}"><i class="mdi mdi-toggle-switch-off"></i> Create / View </a></li>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-seal"></i>
                    <span>Brands</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('brand.index') }}"><i class="mdi mdi-toggle-switch-off"></i> Create / View </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="mdi mdi-video"></i>
                    <span>Advertisement</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('advertisement.index') }}"><i class="mdi mdi-toggle-switch-off"></i> Create / View </a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
@endif

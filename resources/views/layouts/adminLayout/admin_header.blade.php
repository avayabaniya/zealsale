<header class="main-header">
    <!-- Logo -->
    <a href="{{route('admin.dashboard')}}" class="logo">
        <!-- mini logo -->
        <div class="logo-mini">
            <span class="light-logo"><img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}" style="width:60px;height:50px;" alt="logo"></span>
            <span class="dark-logo"><img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}" alt="logo"></span>
        </div>
        <!-- logo-->
        <div class="logo-lg">
            <span class="light-logo"><h2 style="color:white">{{ $sites_data->organization_name}}</h2></span>
            {{-- <span class="dark-logo">Sell</span> --}}
        </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                {{-- <li class="search-box">
                    <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
                    <form class="app-search" style="display: none;">
                        <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                    </form>
                </li> --}}
                @if(!empty(auth()->user()))
                <!-- User Account-->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset(auth()->user()->userprofile->avatar) }}" class="user-image rounded-circle" alt="User Image">
                    </a>
                    <ul class="dropdown-menu animated flipInY">
                        <!-- User image -->
                        <li class="user-header bg-img" style="background-image: url({{ asset('public/backend/images/user-info.jpg') }})" data-overlay="3">
                            <div class="flexbox align-self-center">
                                <img src="{{ asset(auth()->user()->userprofile->avatar) }}" class="float-left rounded-circle" alt="User Image">
                                <h4 class="user-name align-self-center">
                                    <span>{{ auth()->user()->name }}</span>
                                    <small>{{ auth()->user()->email }}</small>
                                </h4>
                            </div>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            {{-- <a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-person"></i> My Profile</a> --}}
                            {{-- <a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-bag"></i> My Balance</a> --}}
                            {{-- <a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-email-unread"></i> Inbox</a> --}}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('admin.settings')}}"><i class="ion ion-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('admin.logout')}}"><i class="ion-log-out"></i> Logout</a>
                            <div class="dropdown-divider"></div>
                            <div class="p-10"><a href="javascript:void(0)" class="btn btn-sm btn-rounded btn-success">View Profile</a></div>
                        </li>
                    </ul>
                </li>
                @endif

                <!-- Notifications -->
                <li class="dropdown notifications-menu" style="margin-right:30px;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="mdi mdi-bell"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInDown">

                        <li class="header">
                            <div class="bg-img text-white p-20" style="background-image: url({{ asset('public/backend/images/user-info.jpg')}})" data-overlay="5">
                                <div class="flexbox">
                                    <div>
                                        <h3 class="mb-0 mt-0">7 New</h3>
                                        <span class="font-light">Notifications</span>
                                    </div>
                                    <div class="font-size-40">
                                        <i class="mdi mdi-message-alert"></i>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu sm-scrol">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-info"></i> Curabitur id eros quis nunc suscipit blandit.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-warning"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-danger"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-success"></i> In gravida mauris et nisi
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-danger"></i> Praesent eu lacus in libero dictum fermentum.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-primary"></i> Nunc fringilla lorem
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-success"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#" class="text-white bg-danger">View all</a></li>
                    </ul>
                </li>
                
            </ul>
        </div>
    </nav>
</header>
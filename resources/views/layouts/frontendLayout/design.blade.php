<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <link rel="shortcut icon" href="{{ asset('public/frontend/assets/images/fav-icon.png') }}"> --}}

        <link rel="shortcut icon" href="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}">


        <title>{{$sites_data->organization_name}} &#8211; Nepal No. 1 Shopping Site</title>

        
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/bootstrap.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/font-awesome.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/animate.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/font-electro.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/owl-carousel.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/style.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/colors/green.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/easyzoom.css') }}" media="all" />
        
        
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,700italic,800,800italic,600italic,400italic,300italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" />


        <style>
            .error{
                color: red;
            }
        </style>

        {{--wishlist hover link--}}
        <style>
            .btn-wishlist{
                border:none;
                outline:none;
                background:none;
                cursor:pointer;
                color:grey;
                padding:0;
                font-family:inherit;
                font-size:inherit;
                text-decoration: none;
            }

            .btn-wishlist:hover{
                background-color: white;
                color: black;
            }

        </style>


        @yield('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    </head>

    <body class="about page home page-template-default single-product full-width left-sidebar">
        <div id="page" class="hfeed site">
            <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

            <div class="top-bar">
                <div class="container">
                    <nav>
                        <ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
                            <li class="menu-item animate-dropdown"><a title="Welcome to Worldwide Electronics Store" href="#">Welcome to {{ $sites_data->organization_name}}</a></li>
                        </ul>
                    </nav>

                    <nav>
                        <ul id="menu-top-bar-right" class="nav nav-inline pull-right animate-dropdown flip">
                            {{-- <li class="menu-item animate-dropdown"><a title="Store Locator" href="#"><i class="ec ec-map-pointer"></i>Store Locator</a></li> --}}
                            {{-- <li class="menu-item animate-dropdown"><a title="Track Your Order" href=""><i class="ec ec-transport"></i>Track Your Order</a></li> --}}
                            <li class="menu-item animate-dropdown"><a title="Shop" href="{{ route('super.deals') }}"><i class="ec ec-shopping-bag"></i>Shop</a></li>
                            @if(empty(Session::has('Email')))
                            <li class="menu-item animate-dropdown"><a title="My Account" href="{{route('my.account')}}"><i class="ec ec-user"></i>My Account</a></li>
                            @else 
                            <li class="menu-item animate-dropdown"><a title="Dashboard" href="{{route('customer.dashboard') }}"><i class="ec ec-user"></i>{{ Session::get('Email') }}</a></li>
                            <li class="menu-item animate-dropdown"><a title="Logout" href="{{route('customer.logout')}}"><i class="ec ec-user"></i>Logout</a></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div><!-- /.top-bar -->

            @include('layouts.frontendLayout.header')
            

            @yield('content')

            @include('layouts.frontendLayout.footer')

        </div><!-- #page -->
       
        <script type="text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/tether.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/bootstrap-hover-dropdown.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/owl.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/echo.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/wow.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/jquery.easing.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/jquery.waypoints.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/electro.js') }}"></script>
          
        <script type="text/javascript" src="{{ asset('public/frontend/assets/js/easyzoom.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('public/frontend/dashboard/js/main.js') }}"></script>        
        
        

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>

        {{--SweetAlert--}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script>
            jQuery( document ).ready(function( $ ) {

                $(document).ready(function () {
                    $(".customerCancelOrder").click(function () {
                        var id = $(this).attr('rel');
                        swal({
                                title: "Are you sure you want to cancel your order?",
                                text: "You will not be able to revert this!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, cancel my order',
                                cancelButtonText: 'No',
                                confirmButtonClass: 'btn btn-success',
                                cancelButtonClass: 'btn btn-danger',
                                buttonStyling: false,
                                reverseButtons: true
                            },
                            function () {
                                window.location.href="/zealsale/customer/order/cancel-order/"+id;

                            });
                    });
                });
            });
        </script>

        <script>
            jQuery( document ).ready(function( $ ) {
                $("#wishlistButton").click(function () {
                        //alert($(this).val());
                    var product_id=($('#product_id').val());
        
        
                   $.ajax({
                       headers:{
                           'X-CSRF-TOKEN' : $ ('meta[name="csrf-token"]').attr('content')
                       },
                       type:'post',
                       url:'wishlist/store',
                       data:{product_id:product_id},
                       success:function(resp){
                           //alert("success")
                           toastr.success('Product has been added to Wishlist');
        
                       },error:function(resp){
                           alert("error");
        
                       }
                   });
                });
            });
        </script>
        

        @yield('scripts')
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c207393a71c7747"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"> </script>
        <script >
            @if(Session::has('success'))
            
            toastr.options.positionClass = 'toast-top-right';
            toastr.success('{{ Session::get('success') }}')
            
            @endif
            @if(Session::has('error'))
            
            toastr.options.positionClass = 'toast-top-right';
            toastr.error('{{ Session::get('error') }}')
            
            @endif
        </script>

        
    </body>
</html>

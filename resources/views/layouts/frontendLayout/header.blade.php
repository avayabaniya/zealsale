
            <header id="masthead" class="site-header header-v1">
                <div class="container">
                    <div class="row">
                    @yield('header')
                    </div><!-- /.row -->
                </div>
                @yield('navbar')
            </header><!-- #masthead -->
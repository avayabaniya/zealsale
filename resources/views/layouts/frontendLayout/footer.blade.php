
            <footer id="colophon" class="site-footer">
            	<div class="footer-widgets">
            		<div class="container">
            			<div class="row">
            				<div class="col-lg-4 col-md-4 col-xs-12">
            					<aside class="widget clearfix">
            						<div class="body">
            							<h4 class="widget-title">Featured Products</h4>
            							<ul class="product_list_widget">									
											@foreach($featuredProducts as $product)
												<li>
													<a href="{{ route('product.single.details', $product->slug) }}" title="{{$product->product_name}}">
														<img class="wp-post-image" data-echo="{{ asset('public/images/backend_images/product/small/'.$product->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$product->image) }}" alt=""><span class="product-title">{{$product->product_name}}</span>
													</a>
													<span class="electro-price"><span class="amount">Rs. {{ rupee_format($product->price) }}</span></span>
												</li>
											@endforeach

            							</ul>
            						</div>
            					</aside>
            				</div>
            				<div class="col-lg-4 col-md-4 col-xs-12">
            					<aside class="widget clearfix">
            						<div class="body"><h4 class="widget-title">Onsale Products</h4>
            							<ul class="product_list_widget">
											@foreach($onsale_product as $osp)
            								<li>
            									<a href="{{ route('product.single.details',$osp->slug) }}" title="{{ $osp->product_name }}">
            										<img class="wp-post-image" data-echo="{{ asset('public/images/backend_images/product/small/'.$osp->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$osp->image) }}" alt="">
            										<span class="product-title">{{ $osp->product_name }} </span>
            									</a>
            									<span class="electro-price"><ins><span class="amount">Rs. <?php echo $osp->price - ($osp->price * ($osp->onsale/100)) ?></span></ins> &nbsp;<del><span class="amount" style="color:red;">Rs. {{ rupee_format($osp->price) }}</span></del></span>
											</li>
											@endforeach
            							</ul>
            						</div>
            					</aside>
            				</div>
            				<div class="col-lg-4 col-md-4 col-xs-12">
            					<aside class="widget clearfix">
            						<div class="body">
            							<h4 class="widget-title">Top Rated Products</h4>
            							<ul class="product_list_widget">
											@foreach($top_rated as $trp)
            										<li>
            									<a href="{{ route('product.single.details',$trp->product->slug) }}" title="{{ $trp->product->product_name }}">
            										<img class="wp-post-image" data-echo="{{ asset('public/images/backend_images/product/small/'.$trp->product->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$trp->product->image) }}" alt="">
	            										<span class="product-title">{{ $trp->product->product_name }}</span>
            									</a>
            									<div class="star-rating" title="Rated 5 out of 5">
													<span style="width:100%"><strong class="rating">5</strong> out of 5</span>
												</div>
												<span class="electro-price">
														@if($trp->product->onsale == NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount pull-left">Rs. {{ rupee_format($trp->product->price) }}</span></ins>
                                                        </span>
                                                        @endif
                                                        @if($trp->product->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $trp->product->price - ($trp->product->price * ($trp->product->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($trp->product->price) }}</span></del>
                                                        </span>
                                                        @endif
												</span>
											</li>
											@endforeach

            							</ul>
            						</div>
            					</aside>
            				</div>
            			</div>
            		</div>
            	</div>

            	<div class="footer-newsletter">
            		<div class="container">
            			<div class="row">
            				<div class="col-xs-12 col-sm-7">
            					<h5 class="newsletter-title">Sign up to Newsletter</h5>
            					<span class="newsletter-marketing-text">...and receive <strong>$20 coupon for first shopping</strong></span>
            				</div>
            				<div class="col-xs-12 col-sm-5">
            					<form>
            						<div class="input-group">
            							<input type="text" class="form-control" placeholder="Enter your email address">
            							<span class="input-group-btn">
            								<button class="btn btn-secondary" type="button">Sign Up</button>
            							</span>
            						</div>
            					</form>
            				</div>
            			</div>
            		</div>
            	</div>

            	<div class="footer-bottom-widgets">
            		<div class="container">
            			<div class="row">
            				<div class="col-xs-12 col-sm-12 col-md-7 col-md-push-5">
								@foreach($categories_data->chunk(10) as $cat)
            					<div class="columns">
            						<aside id="nav_menu-2" class="widget clearfix widget_nav_menu">
            							<div class="body">
            								<h4 class="widget-title">Find It Fast</h4>
            								<div class="menu-footer-menu-1-container">
            									<ul id="menu-footer-menu-1" class="menu">
													@foreach ($cat as $cat_data)
            										<li class="menu-item"><a href="{{route('product.category.details',$cat_data->slug)}}">{{ $cat_data->name }}</a></li>														
													@endforeach
            									</ul>
            								</div>
            							</div>
            						</aside>
								</div><!-- /.columns -->
								@endforeach

            					{{-- <div class="columns">
            						<aside id="nav_menu-3" class="widget clearfix widget_nav_menu">
            							<div class="body">
            								<h4 class="widget-title">&nbsp;</h4>
            								<div class="menu-footer-menu-2-container">
            									<ul id="menu-footer-menu-2" class="menu">
            										<li class="menu-item"><a >Printers &#038; Ink</a></li>
            									</ul>
            								</div>
            							</div>
            						</aside>
            					</div><!-- /.columns --> --}}

            					<div class="columns">
            						<aside id="nav_menu-4" class="widget clearfix widget_nav_menu">
            							<div class="body">
            								<h4 class="widget-title">Customer Care</h4>
            								<div class="menu-footer-menu-3-container">
            									<ul id="menu-footer-menu-3" class="menu">
            										<li class="menu-item"><a href="{{route('my.account')}}">My Account</a></li>
            										<li class="menu-item"><a href="">Track your Order</a></li>
            										<li class="menu-item"><a href="{{route('wishlist') }}">Wishlist</a></li>
            										<li class="menu-item"><a href="{{route('about.us')}}">About Us</a></li>
            										<li class="menu-item"><a href="{{route('contact.us')}}">Customer Service</a></li>
            										<li class="menu-item"><a href="{{route('contact.us')}}">Returns/Exchange</a></li>
            										<li class="menu-item"><a href="{{route('faq')}}">FAQs</a></li>
            										<li class="menu-item"><a href="{{route('tac')}}">Product Support</a></li>
            									</ul>
            								</div>
            							</div>
            						</aside>
            					</div><!-- /.columns -->

            				</div><!-- /.col -->

            				<div class="footer-contact col-xs-12 col-sm-12 col-md-5 col-md-pull-7">
            					<div class="footer-logo">
										<img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}" alt="" width="80" height="45">            						
            					</div><!-- /.footer-contact -->

            					<div class="footer-call-us">
            						<div class="media">
            							<span class="media-left call-us-icon media-middle"><i class="ec ec-support"></i></span>
            							<div class="media-body">
            								<span class="call-us-text">Got Questions ? Call us {{$sites_data->support_time}}!</span>
            								<span class="call-us-number">{{$sites_data->phone}}</span>
            							</div>
            						</div>
            					</div><!-- /.footer-call-us -->


            					<div class="footer-address">
            						<strong class="footer-address-title">Contact Info</strong>
            						<address>{{ $sites_data->address }}</address>
            					</div><!-- /.footer-address -->

            					<div class="footer-social-icons">
            						<ul class="social-icons list-unstyled">
            							<li><a class="fab fa-facebook" target="_blank" href="{{$sites_data->facebook_uri}}" title="Facebook"></a></li>
            							<li><a class="fab fa-twitter" target="_blank" href="{{$sites_data->twitter_uri}}" title="Twitter"></a></li>
            							<li><a class="fab fa-linkedin" target="_blank" href="{{$sites_data->linkedin_uri}}" title="Linkedin"></a></li>
            							<li><a class="fab fa-instagram" target="_blank" href="{{$sites_data->instagram_uri}}" title="Instagram"></a></li>
            							<li><a class="fab fa-youtube" target="_blank" href="{{$sites_data->youtube_uri}}" title="Youtube"></a></li>
            							<li><a class="fab fa-viber" target="_blank" href="{{$sites_data->viber_uri}}" title="Viber"></a></li>
            							<li><a class="fab fa-whatsapp" target="_blank" href="{{$sites_data->whatsapp_uri}}" title="Whatsapp"></a></li>
            						</ul>
            					</div>
            				</div>

            			</div>
            		</div>
            	</div>

            	<div class="copyright-bar">
            		<div class="container">
            			<div class="pull-left flip copyright">&copy; {{ config('app.name','laravel') }} - All Rights Reserved</div>
            			<div class="pull-right flip payment">
            				<div class="footer-payment-logo">
            					<ul class="cash-card card-inline">
            						{{-- <li class="card-item"><img src="{{ asset('public/frontend/assets/images/footer/e-sewa.png') }}" alt="" width="52" height="30"></li>
            						<li class="card-item"><img src="{{ asset('public/frontend/assets/images/footer/khalti.png') }}" alt="" width="52" height="30"></li>
            						<li class="card-item"><img src="{{ asset('public/frontend/assets/images/footer/nic-asia.png') }}" alt="" width="52" height="30"></li>
            						<li class="card-item"><img src="{{ asset('public/frontend/assets/images/footer/IME.png') }}" alt="" width="52" height="30"></li> --}}
            					</ul>
            				</div><!-- /.payment-methods -->
            			</div>
            		</div><!-- /.container -->
            	</div><!-- /.copyright-bar -->
            </footer><!-- #colophon -->
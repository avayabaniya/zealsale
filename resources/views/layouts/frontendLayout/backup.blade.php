
											@foreach($top_rated as $trp)
            								<li>
            									<a href="{{ route('product.single.details',$trp->slug) }}" title="{{ $trp->product->product_name }}">
            										<img class="wp-post-image" data-echo="{{ asset('public/images/backend_images/product/small/'.$trp->product->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$trp->product->image) }}" alt="">
	            										<span class="product-title">{{ $trp->product->product_name }}</span>
            									</a>
            									<div class="star-rating" title="Rated 5 out of 5">
													<span style="width:100%"><strong class="rating">5</strong> out of 5</span>
												</div>
												<span class="electro-price">
														@if($trp->product->onsale == NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount pull-left">Rs. {{ rupee_format($trp->product->price) }}</span></ins>
                                                        </span>
                                                        @endif
                                                        @if($trp->product->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $trp->product->price - ($trp->product->price * ($trp->product->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($trp->product->price) }}</span></del>
                                                        </span>
                                                        @endif
													{{-- <ins><span class="amount">&#36;1,999.00</span></ins>  --}}
													{{-- <del><span class="amount">&#36;2,299.00</span></del> --}}
												</span>
            								</li>
											@endforeach
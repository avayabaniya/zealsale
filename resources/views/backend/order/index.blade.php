@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="box">
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
                @if(Session::has('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_error') !!} </strong>
                    </div>
                @endif
                @if(Session::has('flash_message_success'))

                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_success') !!} </strong>
                    </div>
                @endif
            </div>
        </div>


        <div class="box-header with-border">
            <h3 class="box-title">Places Orders</h3>
            <h6 class="box-subtitle">view order details and change order status</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" id="productorder_wrapper">
                    <div class="row"><div class="col-sm-12">
                            <table class="table table-hover no-wrap product-order dataTable no-footer" id="productorder" role="grid" aria-describedby="productorder_info" data-page-size="10">
                                <thead>
                                <tr class="bg-secondary" role="row">
                                    <th rowspan="1" colspan="1">
                                        Customer
                                    </th>
                                    <th rowspan="1" colspan="2">
                                        Product
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Size
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Quantity
                                    </th>
                                    <th  rowspan="1" colspan="1">
                                        Order Price
                                    </th>
                                    <th  rowspan="1" colspan="1">
                                        Date
                                    </th>
                                    <th  rowspan="1" colspan="1">
                                        Contact Number
                                    </th>
                                    <th  rowspan="1" colspan="1">
                                        Delivery Address
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Status
                                    </th>
                                    <th rowspan="1" colspan="1">
                                        Change Status
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr class="odd" role="row">
                                        <td class="sorting_1">{{ $order->customer->email }}</td>
                                        <td style="width: 500px;">
                                            @if(!empty($order->product->image))
                                                <img src="{{ asset('public/images/backend_images/product/small/'.$order->product->image) }}" style="width: 50px; height: 50px;" class="rounded-circle">
                                            @endif
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            {{ $order->product->product_name }}
                                        </td>
                                        <td>{{ $order->size }}</td>
                                        <td>{{ $order->quantity }}</td>
                                        <td>{{ $order->order_price }}</td>
                                        <td>{{$order->created_at}}</td>
                                        <td>{{$order->checkout->phone}}</td>
                                        <td>{{$order->checkout->delivery_address}}</td>
                                        <td>
                                            @if($order->order_status === 'pending')
                                                <span class="label label-warning" >
                                                Pending
                                                </span>
                                            @elseif($order->order_status === 'on-way')
                                                <span class="label label-info">
                                                On way
                                                </span>
                                            @elseif($order->order_status === 'completed')
                                                <span class="label label-success">
                                                Completed
                                                </span>
                                            @elseif($order->order_status === 'cancelled')
                                                <span class="label label-danger">
                                                Cancelled
                                                </span>
                                            @endif

                                        </td>
                                        <td>
                                            <form id="store" name="store" method="post" action="{{ route('order.update') }}">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                <input type="hidden" value="{{ $order->order_status }}" name="old_status">
                                                <select name="status" id="status" onchange="this.form.submit()">
                                                    <option value="pending" @if($order->order_status == 'pending') selected @endif>Pending</option>
                                                    <option value="on-way" @if($order->order_status == 'on-way') selected @endif>On way</option>
                                                    <option value="completed" @if($order->order_status == 'completed') selected @endif>Completed</option>
                                                    <option value="cancelled" @if($order->order_status == 'cancelled') selected @endif>Cancelled</option>
                                                </select>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

@endsection

@section('styles')
    <style>
        .dataTable{
            overflow: auto;
            white-space: nowrap;
        }

        .dataTable tr {

        }
    </style>
@endsection

@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="box">
    <div class="box-body">
            <div class="row">
                <table class="table">
                    <tr>
                        <th><b><u>Items</u></b></th>
                        <th><b><u>Details</u></b></th>
                    </tr>
                    <tr>
                        <td>Product Name:</td>
                        <td> {{$product->product_name }}</td>
                    </tr>
                    <tr>
                        <td>Product Category:</td>
                        @foreach ($categories as $cat)
                        @if($cat->id == $product->category_id)
                        <td>{{ $cat->name }}</td>
                        @endif
                        @endforeach
                    </tr>
                </table>
            </div>
            <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;">
                        @if(Session::has('flash_message_error'))
                            <div class="alert alert-error alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                        @endif
                        @if(Session::has('flash_message_success'))
            
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                        @endif
                    </div>
            </div>
            <div class="row col-md-12" >            
                <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                    <label for=""><b>Add Attributes</b></label>
                    <div class=" increment" >
                        <input type="text" name="sku[]" id="sku" placeholder="Product Code">
                        <input type="text" name="size[]" id="size" placeholder="Size">
                        <input type="number" name="price[]" id="price" placeholder="Price">
                        <input type="number" name="stock[]" id="stock" placeholder="Stock">
                        &nbsp;
                        <button class="btn btn-info btn-sm" type="button"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Add</button>
                    </div>
                    <div class="clone hide">
                        <div class="control-group input-group" style="margin-top:10px">
                            <input type="text" name="sku[]" id="sku" placeholder="Product Code" style="margin-right: 4px;">
                            <input type="text" name="size[]" id="size" placeholder="Size" style="margin-right: 4px;">
                            <input type="number" name="price[]" id="price" placeholder="Price" style="margin-right: 4px;">
                            <input type="number" name="stock[]" id="stock" placeholder="Stock" style="margin-right: 4px;">
                            &nbsp;
                            <button class="btn btn-danger btn-sm" type="button"><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Remove</button>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <button class="btn btn-success btn-md" type="submit"> <i class="mdi mdi-content-save"></i>&nbsp;Submit</button>
                    </div>
                </form>
            </div>
        </div>
</div>

<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Hover Export Data Table</h3>
            <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="">
                <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                    <form action="{{ route('product.attribute.edit', $product->id) }}" method="post">
                        @csrf
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                        <thead>
                        <tr role="row">
                            <th >S.No</th>
                            <th >Product Code (SKU)</th>
                            <th >Size</th>
                            <th >Price</th>
                            <th >Stock</th>
                            <th >Action</th>
                        </thead>
                        <tbody>
                        @foreach($product->attributes as $attribute)
                            <tr>
                            <td><input type="hidden" name="idAttr[]" value="{{ $attribute->id }}">{{ $attribute->id }}</td>
                            <td>{{ $attribute->sku }}</td>
                            <td>{{ $attribute->size }}</td>
                            <td><input type="text" name="price[]" value="{{ $attribute->price }}"></td>
                            <td><input type="text" name="stock[]" value="{{ $attribute->stock }}"></td>
                            <td>
                                <input type="submit" value="Update" class="btn btn-primary btn-mini">
                                <a rel="{{ $attribute->id }}" rel1="delete-attribute" href="javascript:" class="btn btn-danger btn-mini deleteRecord" >Delete</a>
                               {{-- <a href="{{route('product.attribute.delete', $attribute->id)}}:" class="btn btn-danger btn-mini deleteRecord" >Delete</a>--}}
                            </td>
                            </tr>
                        @endforeach
                       </tbody>
                        <tfoot>
                        <tr>
                            <th >S.No</th>
                            <th >Product Code (SKU)</th>
                            <th >Size</th>
                            <th >Price</th>
                            <th >Stock</th>
                            <th >Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endsection




@section('scripts')
<script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $(".btn-info").click(function(){
        var html = $(".clone").html();
        $(".increment").after(html);
    });

    $("body").on("click",".btn-danger",function(){
        $(this).parents(".control-group").remove();
    });

  });

</script>
@endsection

@section('styles')

{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> --}}

@endsection

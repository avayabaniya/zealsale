@extends('layouts.adminLayout.admin_design')
@section('content')
       
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="page-title">Products</h3>
            <div class="d-inline-block align-items-center">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href=""><i class="mdi mdi-home-outline"></i></a>&nbsp;Home</li>
                        <li class="breadcrumb-item active" aria-current="page">Products</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div> 

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="box box-body b-1 text-center no-shadow">
                            <img src="{{ asset('public/images/backend_images/product/large/'.$product->image) }}" id="product-image" class="img-fluid" alt="" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <h2 class="box-title mt-0">{{ $product->product_name }}</h2>
                        @if(empty($product->onsale))
                            <h1 class="pro-price mb-0 mt-20">Rs. {{$product->price}}</h1>
                        @else
                            <h1 class="pro-price mb-0 mt-20">Rs. <?php echo $product->price - ($product->onsale * ($product->onsale/100))?>
                            <span class="old-price">Rs. {{$product->price}}</span>
                            <span class="text-danger">{{$product->onsale}}% off</span>
                            </h1>
                        @endif
                        <hr>
                        <p>{!! $product->description !!}</p>
                        <div class="row">
                            <div class="col-sm-12">
                                    <h5>Color</h5><h6>{{$product->product_color}}</h6>
                                <p>Code:{{$product->product_code}}</p>
                                <h6 class="mt-20">Available Size</h6>
                                @if(empty($product->attributes->size))

                                    <p>SKU for this product has not been added</p>

                                @else
                                    <p class="mb-0">
                                        @foreach($product->attributes as $size)
                                            <span class="badge badge-lg badge-default">{{$size->size}}</span>
                                        @endforeach
                                    </p>
                                @endif
                            </div>
                        </div>
                        <hr>
                        {{-- <button class="btn btn-success btn-outline mr-5" data-toggle="tooltip" data-container="body" title="" data-original-title="Buy Now!"><i class="mdi mdi-shopping"></i></button>
                        <button class="btn btn-primary btn-outline mr-5" data-toggle="tooltip" data-container="body" title="" data-original-title="Add to Cart"><i class="mdi mdi-cart-plus"></i> </button>
                        <button class="btn btn-info btn-outline mr-5" data-toggle="tooltip" data-container="body" title="" data-original-title="Compare"><i class="mdi mdi-compare"></i></button>
                        <button class="btn btn-danger btn-outline" data-toggle="tooltip" data-container="body" title="" data-original-title="Wishlist"><i class="mdi mdi-heart"></i></button>
                        <h4 class="box-title mt-40">Key Highlights</h4>
                        <ul class="list-icons">
                            <li><i class="fa fa-check text-danger float-none"></i> Party Wear</li>
                            <li><i class="fa fa-check text-danger float-none"></i> Nam libero tempore, cum soluta nobis est</li>
                            <li><i class="fa fa-check text-danger float-none"></i> Omnis voluptas as placeat facere possimus omnis voluptas.</li>
                        </ul> --}}
                    </div>

                    {{--product general info
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h4 class="box-title mt-40">General Info</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="390">Brand</td>
                                        <td> Brand Name </td>
                                    </tr>
                                    <tr>
                                        <td>Delivery Condition</td>
                                        <td> Lorem Ipsum </td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td> Party Wear </td>
                                    </tr>
                                    <tr>
                                        <td>Style</td>
                                        <td> Modern </td>
                                    </tr>
                                    <tr>
                                        <td>Product Number</td>
                                        <td> FG1548952 </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>--}}
                </div>
            </div>				
        </div>
    </div>
</div>

</section>
<!-- /.content -->

@endsection
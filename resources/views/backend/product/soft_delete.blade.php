@extends('layouts.adminLayout.admin_design')
@section('content')
        
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Products</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href=""><i class="mdi mdi-home-outline"></i></a>&nbsp;Home</li>
                            <li class="breadcrumb-item active" aria-current="page">Products</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>   

    <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
                @if(Session::has('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_error') !!} </strong>
                    </div>
                @endif
                @if(Session::has('flash_message_success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_success') !!} </strong>
                    </div>
                @endif
                @if(Session::has('flash_message_warning'))
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_warning') !!} </strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            @foreach($products as $data)
            <div class="col-12 col-lg-6 col-xl-4">
                <div class="box">
                    <div class="box-body">
                        <div class="product-img">
                            <img src="{{ asset('public/images/backend_images/product/small/'.$data->image) }}" alt="">
                        </div>
                        <div class="product-text">
                            <div class="pro-img-overlay">
                                <a href="{{route('product.restore',$data->id)}}" type="submit" class="btn btn-info btn-icon-circle text-inverse" title="Restore" data-toggle="tooltip" data-original-title="Restore"><i class="mdi mdi-autorenew"></i></a>
                                <a href="{{route('product.forcedelete',$data->id)}}" type="submit" class="btn btn-danger btn-icon-circle text-inverse" title="Trash Permanently" data-toggle="tooltip" data-original-title="Trash Permanently"><i class="mdi mdi-delete-empty "></i></a>
                            </div>
                            <h2 class="pro-price text-blue">{{$data->price}}</h2>
                            <h3 class="box-title mb-0">{{$data->product_name}}</h3>
                            <small class="text-muted db">Lorem Ipsum Dummy Text</small>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="pull-right">
            {!! $products->links(); !!}
        </div>
    </section>
    <!-- /.content -->
@endsection
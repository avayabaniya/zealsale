@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Create Product </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create Product</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Create Product</h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-6" style="margin-left: 20px;">
                                @if(Session::has('flash_message_error'))
                                    <div class="alert alert-error alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_error') !!} </strong>
                                    </div>
                                @endif
                                @if(Session::has('flash_message_success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_success') !!} </strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body">
                           <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Product Category:</label>
                                    <select class="form-control select2" name="category_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                         <?php echo $categories_dropdown; ?>
                                     </select>
                                </div>

                               <div class="form-group col-md-6">
                                   <label for="product_name">Product Name:</label>
                                   <input name="product_name" class="form-control" placeholder="Product Name" id="product_name" autocomplete="off" value="{{old('product_name')}}">
                                   <p style="color: red; margin-bottom: 0px;" id="product_name_val">{{ $errors -> first('product_name') }}</p>
                               </div>
                           </div>
                           
                           <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="product_code">Product Code:</label>
                                    <input name="product_code" class="form-control" placeholder="Product Code" id="product_code" autocomplete="off" value="{{old('product_code')}}">
                                    <p style="color: red; margin-bottom: 0px;" id="product_code_val">{{ $errors -> first('product_code') }}</p>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="product_color">Product Color:</label>
                                    <input name="product_color" class="form-control" placeholder="Product Color" id="product_color" autocomplete="off" value="{{old('product_color')}}">
                                    <p style="color: red; margin-bottom: 0px;" id="product_color_val">{{ $errors -> first('product_color') }}</p>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Select Image</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation()" name="image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('file') }}</p>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="product_price">Product Price:</label>
                                    <input name="product_price" class="form-control" placeholder="Product Price" id="product_price" autocomplete="off" value="{{old('product_price')}}">
                                    <p style="color: red; margin-bottom: 0px;" id="product_price_val">{{ $errors -> first('product_price') }}</p>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="Checkbox_1" value="1">Select a Status</label>
                                    <div class="checkbox">
                                        <input type="checkbox" name="status" id="Checkbox_1" checked class="filled-in chk-col-maroon">
                                        <label for="Checkbox_1" value="1">Status</label>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Description:</label>
                                    <textarea name="description" rows="7" class="form-control summernote"></textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('description') }}</p>
                                </div>
                            </div>



                            <div class="box-header with-border"style="margin-bottom: 20px;">

                                <h4 class="box-title">Product SEO</h4>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Meta Title:</label>
                                    <input name="meta_title" class="form-control" placeholder="Meta Title" id="meta_title" autocomplete="off" value="{{old('meta_title')}}">
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_title') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Meta Keywords:</label>
                                    <div class="input-group">
                                        <input type="text" name="meta_keywords" data-role="tagsinput" placeholder="Add keywords"> &nbsp; <span class="input-group-addon" style="color:white;background-color:maroon;"><i class="mdi mdi-comment-plus-outline"></i>&nbsp;Keywords</span>
                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_title') }}</p>
                                    </div>
                                </div> 
                            </div>

                            <div class="row">                            
                                <div class="form-group col-md-12">
                                    <label>Meta Description:</label>
                                    <textarea name="meta_description" class="form-control summernote"></textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_description') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Meta Content:</label>
                                    <textarea name="meta_content" class="form-control summernote"></textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_content') }}</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" id="submit" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Create</button>
                        </div>
                        <hr>  

                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#product_name').keyup(function(){
        var product_name = $('#product_name').val();

        if(product_name.length == 0){
            $('#product_name_val').text('** Product Name is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (product_name.length > 0 && product_name.length < 6)
        {
            $('#product_name_val').text('** Minimum 6 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#product_name_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 
    
    $('#product_code').keyup(function(){
        var product_code = $('#product_code').val();

        if(product_code.length == 0){
            $('#product_code_val').text('** Product code is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (product_code.length > 0 && product_code.length < 4)
        {
            $('#product_code_val').text('** Minimum 4 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#product_code_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 

    $('#product_price').keyup(function(){
        var product_price = $('#product_price').val();

        if(product_price.length == 0){
            $('#product_price_val').text('** Product price is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#product_price_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection
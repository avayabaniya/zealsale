@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="box">
    <div class="box-body">
            <div class="row">
                <table class="table">
                    <tr>
                        <th><b><u>Items</u></b></th>
                        <th><b><u>Details</u></b></th>
                    </tr>
                    <tr>
                        <td>Product Name:</td>
                        <td> {{$product->product_name }}</td>
                    </tr>
                    <tr>
                        <td>Product Category:</td>
                        @foreach ($categories as $cat)
                        @if($cat->id == $product->category_id)
                        <td>{{ $cat->name }}</td>
                        @endif
                        @endforeach
                    </tr>
                </table>
            </div>
            <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;">
                        @if(Session::has('flash_message_error'))
                            <div class="alert alert-error alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                        @endif
                        @if(Session::has('flash_message_success'))
            
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                        @endif
                    </div>
            </div>
            <div class="row col-md-12" >
                <form enctype="multipart/form-data" class="form-horizontal" method="post" action="" name="add_image" id="add_image">
                    @csrf

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Select Image(s)</label>
                            <div class="controls">
                                <input type="file" id="file" onchange="return fileValidation()" name="image[]" class="form-control" aria-invalid="false" multiple="multiple">
                                <div class="help-block"></div>
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('file') }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" value="Add Images" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
</div>

<div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Hover Export Data Table</h3>
            <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="">
                <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                    <form action="{{ route('product.attribute.edit', $product->id) }}" method="post">
                        @csrf
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                        <thead>
                        <tr role="row">
                            <th >S.No</th>
                            <th >Product Name</th>
                            <th >Images</th>
                            <th >Action</th>
                        </thead>
                        <tbody>
                        @foreach($product->images as $image)
                            <tr>
                                <td>{{ $image->id }}</td>
                                <td>{{ $image->product->product_name }}</td>
                                <td>
                                    <img src="{{ asset('public/images/backend_images/product/small/'.$image->image) }}" style="width: 75px;">
                                </td>
                                <td>
                                    <a href="{{route('product.image.delete', $image->id)}}:" class="btn btn-danger btn-mini deleteRecord" >Delete</a>
                                </td>
                            </tr>
                        @endforeach
                       </tbody>
                        <tfoot>
                        <tr>
                            <th >S.No</th>
                            <th >Product Name</th>
                            <th >Images</th>
                            <th >Action</th>
                        </tr>
                        </tfoot>
                    </table>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endsection




@section('scripts')
<script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $(".btn-success").click(function(){
        var html = $(".clone").html();
        $(".increment").after(html);
    });

    $("body").on("click",".btn-danger",function(){
        $(this).parents(".control-group").remove();
    });

  });

</script>
@endsection

@section('styles')

{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> --}}

@endsection

@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="box">
    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_error') !!} </strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))

                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_success') !!} </strong>
                </div>
            @endif
        </div>
    </div>


    <div class="box-header with-border">
        <h3 class="box-title">Products Data Table</h3>
        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        <a href="{{ route('product.create') }}" style="color:white;" class="hover"><button class="btn btn-info pull-right hover"><i class="fa fa-plus-circle"></i> &nbsp; Add New Product </button></a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="">
            <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">

                <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                    <thead>
                    <tr role="row">
                        <th tabindex="0" class="sorting_asc" aria-controls="example" style="width: 157.11px;" aria-label="Name: activate to sort column descending" aria-sort="ascending" rowspan="1" colspan="1">S.No</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 113.67px;" aria-label="Office: activate to sort column ascending" rowspan="1" colspan="1">Product Name</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 54.71px;" aria-label="Age: activate to sort column ascending" rowspan="1" colspan="1">Featured</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 54.71px;" aria-label="Age: activate to sort column ascending" rowspan="1" colspan="1">Category</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Status</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 95.4px;" aria-label="Salary: activate to sort column ascending" rowspan="1" colspan="1">Action</th></tr>
                    </thead>
                    <tbody>
                    @foreach($products as $data)
                        <tr class="odd" role="row">
                            <td class="sorting_1">{{$loop->index + 1}}</td>
                            <td>
                                @if(!empty($data->image))
                                    <img src="{{ asset('public/images/backend_images/product/small/'.$data->image) }}" style="width: 50px; height: 50px;" class="rounded-circle">
                                @endif
                                &nbsp;
                                 {{ $data->product_name }}
                            </td>
                            <td>
                                {{--<label for="Checkbox_1" value="1">Select a Status</label>--}}
                                <div class="checkbox">
                                    @if($data->featured == 1)
                                        <input class="filled-in chk-col-maroon featured" id="md_checkbox_{{$data->id}}" type="checkbox" checked>

                                    @else
                                        <input class="filled-in chk-col-maroon featured" id="md_checkbox_{{$data->id}}" type="checkbox">

                                    @endif
                                    <label for="md_checkbox_{{$data->id}}"></label>
                                </div>
                            </td>
                            <td>
                                @if($data->category_id == 0)
                                    {{ 'Main Category' }}
                                @endif
                                @foreach($categories as $c)
                                    @if($c->id == $data->category_id)
                                        {{ $c->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @if($data->status == 1)
                                    <span class="badge badge-lx badge-success">Active</span>
                                @else
                                    <span class="badge badge-lx badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('product.show', $data->slug) }}" class="text-inverse" title="Show" data-toggle="tooltip" data-original-title="Show">
                                    <i class="fa fa-eye"></i> 
                                </a>
                                &nbsp;
                                <a href="{{ route('product.edit', $data->slug) }}" class="text-inverse" title="Edit" data-toggle="tooltip" data-original-title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                &nbsp;

                                <a href="javascript:" rel="{{$data->id}}" rel1="product/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                                &nbsp;
                                <a href="{{ route('product.attribute', $data->id) }}" class="text-inverse" title="Add Attribute" data-toggle="tooltip" data-original-title="Add Attribute">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                                &nbsp;
                                <a href="{{ route('product.specification', $data->id) }}" class="text-inverse" title="Add Specification" data-toggle="tooltip" data-original-title="Add Spefication">
                                    <i class="fa fa-list-ul"></i>
                                </a>
                                &nbsp;
                                <a href="{{ route('product.image', $data->id) }}" class="text-inverse" title="Add Image" data-toggle="tooltip" data-original-title="Add Image">
                                    <i class="fa fa-camera"></i>
                                </a>
                                &nbsp;
                                <a href="" data-toggle="modal" data-target="#modal-center{{$data->id}}" class="text-inverse" title="Allow Offer (Sale)" data-original-title="Allow Offer (Sale)">
                                    <i class="fa fa-gift"></i>
                                </a>
                                @if(!empty($data->onsale))
                                    <a href="{{ route('product.onsale.remove', $data->id) }}"  class="text-inverse" title="Remove Offer" data-original-title="Remove Offer">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                        
                                        <!-- Modal -->
                                        <div class="modal center-modal fade{{$data->id}}" id="modal-center{{$data->id}}" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action="{{route('product.onsale',$data->id)}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Product Name - {{$data->product_name}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <h2>
                                                                    &nbsp;<img src="{{ asset('public/images/backend_images/product/small/'.$data->image) }}" style="width: 80px; height: 80px;" class="rounded-circle">&nbsp;&nbsp;{{ $data->product_name }}
                                                                </h2>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="old_price">Price</label>
                                                                <input type="text" name="old_price" id="old_price{{$data->id}}" class="form-control" value="{{$data->price}}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="new_price">Discount (in %)</label>
                                                                <select class="form-control select2 discount" name="discount" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                    <option value="1" selected>1</option>
                                                                    @for($i=2; $i<= 100; $i++)
                                                                    <option value="{{ $i }}-{{$data->id}}">{{ $i }}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="new_price">Final Price</label>
                                                                <input type="text" name="new_price" id="new_price{{$data->id}}" class="form-control" value="{{$data->price}}" readonly>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer modal-footer-uniform">
                                                            <button type="button" class="btn btn-bold btn-pure btn-danger" data-dismiss="modal">Close</button>
                                                            <button type="submit" id="update" class="btn btn-bold btn-pure btn-success float-right">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal -->

                    @endforeach
                   </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="1" colspan="1">S.No</th>
                        <th rowspan="1" colspan="1">Product Name</th>
                        <th rowspan="1" colspan="1">Featured</th>
                        <th rowspan="1" colspan="1">Category</th>
                        <th rowspan="1" colspan="1">Status</th>
                        <th rowspan="1" colspan="1">Action</th>
                    </tr>
                    </tfoot>
                </table>
                
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    
</div>

@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

    <script>
        $(document).ready(function () {
            console.log('document ready');
            $('.discount').change(function () {
                var a = $(this).val();
                var arr = a.split('-');

                var old_price_selector = "#old_price".concat(arr[1]);
                var new_price_selector = "#new_price".concat(arr[1]);

                var old_price = $(old_price_selector).val();
                var current_price = $(new_price_selector).val( old_price - ((parseFloat(arr[0])/100) * old_price) );

            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.featured').change(function (event) {

                console.log(event.target.id);
                var arr = event.target.id.split('_');

                if($(this).prop("checked") === true){
                    //alert("Checkbox is checked.");
                    var value = 1;
                }
                else if($(this).prop("checked") === false){
                    //alert("Checkbox is unchecked.");
                    var value = 0;
                }


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: 'featured-product/' + arr[2],
                    data:{
                        product_id:arr[2],
                        value:value
                    },
                    success:function (resp) {
                        console.log(resp);

                    }, error:function (resp) {
                        console.log('error');
                    }
                });

h

            });
        })
    </script>

@endsection

@section('styles')
    <style>
		.example-modal .modal {
			position: relative;
			top: auto;
			bottom: auto;
			right: auto;
			left: auto;
			display: block;
			z-index: 1;
		}

		.example-modal .modal {
			background: transparent !important;
		}

	</style>
@endsection

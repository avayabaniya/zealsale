@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Site Settings </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Update Site Settings</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form action="{{ route('sitesetting.update',1) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Update Site Settings - {{ $sitesettings->organization_name }} </h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-6" style="margin-left: 20px;">
                                @if(Session::has('flash_message_error'))
                                    <div class="alert alert-error alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_error') !!} </strong>
                                    </div>
                                @endif
                                @if(Session::has('flash_message_success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_success') !!} </strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body">
                           <div class="row">
                               <div class="form-group col-md-12">
                                   <label for="organization_name">Organization Name:</label>
                                   <input name="organization_name" class="form-control" placeholder="Organization Name" id="organization_name" autocomplete="off" value="{{$sitesettings->organization_name}}">
                                   <p style="color: red; margin-bottom: 0px;" id="organization_name_val">{{ $errors -> first('organization_name') }}</p>
                               </div>
                           </div>
                           
                           <div class="row">                       
                                <div class="form-group col-md-4">
                                    <label>Select Logo</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation()" name="image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <input type="hidden" name="current_image" value="{{$sitesettings->logo}}">
                                    @if(!empty($sitesettings->logo))
                                        <img src="{{ asset('public/images/backend_images/settings/small/'.$sitesettings->logo) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email Address:</label>
                                    <input name="email" class="form-control" placeholder="Email Address" id="email" autocomplete="off" value="{{$sitesettings->email}}">
                                    <p style="color: red; margin-bottom: 0px;" id="email_val">{{ $errors -> first('email') }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="support_time">Support Time:</label>
                                    <input name="support_time" class="form-control" placeholder="Product Price" id="support_time" autocomplete="off" value="{{$sitesettings->support_time}}">
                                    <p style="color: red; margin-bottom: 0px;" id="support_time_val">{{ $errors -> first('support_time') }}</p>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="address">Address:</label>
                                    <input name="address" class="form-control" placeholder="Address" id="address" autocomplete="off" maxlength="100" value="{{$sitesettings->address}}">
                                    <p style="color: red; margin-bottom: 0px;" id="address_val">{{ $errors -> first('address') }}</p>
                                </div>
                            </div>
                            <?php 
                            $number = explode(',',$sitesettings->phone);
                            ?>
                            
                            <div class="row">                 
                                <div class="form-group col-md-12">
                                    <label>Phone Number :</label>
                                    <div class="input-group">
                                        <input type="text" id="phone" name="phone" data-role="tagsinput" placeholder="Add Numbers" value="@foreach($number as $n) {{$n}}, @endforeach" style="font-size:16px;"> &nbsp; <span class="input-group-addon" style="color:white;background-color:maroon;"><i class="mdi mdi-comment-plus-outline"></i>&nbsp;Numbers</span>
                                        <p style="color: red; margin-bottom: 0px;" id="phone_val">{{ $errors -> first('phone') }}</p>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>About Us:</label>
                                    <textarea name="about" rows="7" class="form-control summernote">{!! $sitesettings->about !!}</textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('about') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="facebook">Facebook URI</label>
                                    <input name="facebook" class="form-control" placeholder="Facebook URI" id="facebook" autocomplete="off" value="{{$sitesettings->facebook_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="facebook_val">{{ $errors -> first('facebook') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="instagram">Instagram URI</label>
                                    <input name="instagram" class="form-control" placeholder="Instagram URI" id="instagram" autocomplete="off" value="{{$sitesettings->instagram_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="instagram_val">{{ $errors -> first('facebook') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="linkedin">Linkedin URI</label>
                                    <input name="linkedin" class="form-control" placeholder="linkedin URI" id="linkedin" autocomplete="off" value="{{$sitesettings->linkedin_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="linkedin_val">{{ $errors -> first('linkedin') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="twitter">Twitter URI</label>
                                    <input name="twitter" class="form-control" placeholder="twitter URI" id="twitter" autocomplete="off" value="{{$sitesettings->twitter_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="twitter_val">{{ $errors -> first('twitter') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="youtube">Youtube URI</label>
                                    <input name="youtube" class="form-control" placeholder="Youtube URI" id="youtube" autocomplete="off" value="{{$sitesettings->youtube_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="youtube_val">{{ $errors -> first('youtube') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="viber">Viber URI</label>
                                    <input name="viber" class="form-control" placeholder="Viber URI" id="viber" autocomplete="off" value="{{$sitesettings->viber_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="viber_val">{{ $errors -> first('viber') }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="whatsapp">Whatsapp URI</label>
                                    <input name="whatsapp" class="form-control" placeholder="whatsapp URI" id="whatsapp" autocomplete="off" value="{{$sitesettings->whatsapp_uri}}">
                                    <p style="color: red; margin-bottom: 0px;" id="whatsapp_val">{{ $errors -> first('whatsapp') }}</p>
                                </div>
                            </div>
                </div>
                <!-- /.box -->

                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <button type="submit" id="update" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Update</button>
                </div>
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#organization_name').keyup(function(){
        var organization_name = $('#organization_name').val();

        if(organization_name.length == 0){
            $('#organization_name_val').text('** Organization Name is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (organization_name.length > 0 && organization_name.length < 6)
        {
            $('#organization_name_val').text('** Minimum 6 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#organization_name_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    $('#email').keyup(function(){
        var email = $('#email').val();

        if(email.length == 0){
            $('#email_val').text('** Valid Email Address is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (email.length > 0 && email.length < 6)
        {
            $('#email_val').text('** Minimum 6 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#email_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 

    $('#address').keyup(function(){
        var address = $('#address').val();

        if(address.length == 0){
            $('#address_val').text('** Address is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (address.length > 0 && address.length < 10)
        {
            $('#address_val').text('** Minimum 10 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#address_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 

    
    $('#support_time').keyup(function(){
        var support_time = $('#support_time').val();

        if(support_time.length == 0){
            $('#support_time_val').text('** Support Time is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (support_time.length > 0 && support_time.length < 4)
        {
            $('#support_time_val').text('** Minimum 4 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#support_time_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    });

    
    $('#phone').keyup(function(){
        var phone = $('#phone').val();

        if(phone.length == 0){
            $('#phone_val').text('** Phone Number is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (phone.length > 0 && phone.length < 8)
        {
            $('#phone_val').text('** Minimum 8 numbers').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#phone_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection

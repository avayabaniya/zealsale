@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Update Coupon Category</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create Category</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form id="edit_category" action="{{ route('couponcategory.edit', ['id'=> $couponcategory->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">

            <!-- right column -->
            <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Edit Category</h4>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-6" style="margin-left: 20px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))

                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>


                        <div class="box-body">
                           <div class="row">

                                   <div class="form-group col-md-6">
                                       <label for="name">Category Name:</label>
                                       <input name="name" class="form-control" placeholder="Coupon Category Name" id="name" autocomplete="off" value="{{$couponcategory->name}}">
                                       <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('name') }}</p>
                                   </div>



                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Select Image</label>
                                        <div class="controls">
                                            <input type="file" name="featured_image" class="form-control" aria-invalid="false"> <div class="help-block"></div></div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <input type="hidden" name="current_image" value="{{$couponcategory->featured_image}}">
                                        @if(!empty($couponcategory->featured_image))
                                            <img src="{{ asset('public/images/backend_images/couponcategory/small/'.$couponcategory->featured_image) }}" style="width: 50px; height: 50px; margin-top: 1px;">
                                        @endif
                                    </div>

                                </div>


                            <div class="row">

                                <div class="form-group col-md-12">
                                    <label>Description:</label>
                                    <textarea name="description" id="description" rows="7" class="form-control summernote">
                                        {{ $couponcategory->description }}
                                </textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('description') }}</p>
                                </div>
                            </div>


                        <div>
                            <div class="box-header with-border"style="margin-bottom: 20px;">

                                <h4 class="box-title">Category SEO</h4>
                            </div>
                       
                         
                            <div class="row">

                                <div class="form-group col-md-12">
                                    <label>Meta Title:</label>
                                    <input name="meta_title" class="form-control" placeholder="Meta Title" id="meta_title" autocomplete="off" value="{{ $couponcategory->meta_title }}">
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('name') }}</p>
                                </div>
                                <?php
                                $tags = explode(',',$couponcategory->meta_keywords);
                                ?>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Meta Keywords:</label>
                                        <div class="input-group">
                                            <input type="text" name="meta_keywords" data-role="tagsinput" value="@foreach($tags as $t) {{$t}}, @endforeach" placeholder="Add keywords"> &nbsp; <span class="input-group-addon" style="color:white;background-color:maroon;"><i class="mdi mdi-comment-plus-outline"></i>&nbsp;Keywords</span>
                                            <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_title') }}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Meta Description:</label>
                                    <textarea name="meta_description" id="meta_description" rows="5" class="form-control summernote">
                                        {{ $couponcategory->meta_description }}
                                    </textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_description') }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Meta Content:</label>
                                    <textarea name="meta_content" id="meta_content" rows="5" class="form-control summernote">
                                        {{ $couponcategory->meta_content }}
                                    </textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('meta_content') }}</p>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Update Category</button>
                        </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#create_category').validate({
                rules: {
                    category_name:{
                        required: true,
                    }
                },
                messages: {
                    category_name: {
                        required: "Please enter the category name"
                    }
                }
            });
        });
    </script>

    <!-- Bootstrap Select -->
    <!-- Select2 -->
    <script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
    <!-- Superieur Admin for advanced form element -->
    <script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>






@endsection

@section('styles')

    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/select2/dist/css/select2.css')}}">


    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection

@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="box">

    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_error') !!} </strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))

                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_success') !!} </strong>
                </div>
            @endif
        </div>
    </div>


    <div class="box-header with-border">
        <h3 class="box-title">Categories Data Table</h3>
        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        <a href="{{ route('couponcategory.create') }}" style="color:white;" class="hover"><button class="btn btn-info pull-right hover"><i class="fa fa-plus-circle"></i> &nbsp; Add New Category </button></a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="">
            <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                    <thead>
                    <tr role="row">
                        <th>S.No</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Action</th></tr>
                    </thead>
                    <tbody>
                    @foreach($couponcategories as $category)
                        <tr class="odd" role="row">
                            <td class="sorting_1">{{$loop->index + 1}}</td>
                            <td>
                                @if(!empty($category->featured_image))
                                    <img src="{{ asset('public/images/backend_images/couponcategory/small/'.$category->featured_image) }}" style="width: 50px; height: 50px;" class="rounded-circle">
                                @endif
                                &nbsp;
                                 {{ $category->name }}

                            </td>
                           
                            <td>{{ $category->description}} </td>
                            <td>
                                <a href="{{ route('couponcategory.edit', $category->id) }}">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                &nbsp;
                                <a rel = "{{$category->id}}" rel1 = "couponcategory/delete" id="sa-warning" class="model_img img-fluid deleteRecord" href="javascript:">
                                    <i class="fa fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                   </tbody>
                    <tfoot>
                    <tr>
                        <th >S.No</th>
                        <th >Category</th>
                        <th >Description</th>
                        <th >Action</th>
                    </tr>
                    </tfoot>
                </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
</div>
@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.featured').change(function (event) {

                console.log(event.target.id);
                var arr = event.target.id.split('_');

                if($(this).prop("checked") === true){
                    //alert("Checkbox is checked.");
                    var value = 1;
                }
                else if($(this).prop("checked") === false){
                    //alert("Checkbox is unchecked.");
                    var value = 0;
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: 'featured-category/' + arr[2],
                    data:{
                        category_id:arr[2],
                        value:value
                    },
                    success:function (resp) {
                        console.log(resp);

                    }, error:function (resp) {
                        console.log('error');
                    }
                });
            });
        })
    </script>

@endsection

@section('styles')
    <style>
        a:hover{
            background-color:blue;
        }
    </style>
@endsection


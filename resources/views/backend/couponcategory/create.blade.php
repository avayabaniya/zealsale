@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Create Coupon Category</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create Category</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form id="create_category" action="{{ route('couponcategory.create') }}" method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">

            <!-- right column -->
            <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Create Category</h4>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-6" style="margin-left: 20px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))

                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>


                        <div class="box-body">
                           <div class="row">

                                   <div class="form-group col-md-6">
                                       <label for="name">Category Name:</label>
                                       <input name="name" class="form-control" placeholder="Coupon Category Name" id="name" autocomplete="off" value="{{old('name')}}">
                                       <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('name') }}</p>
                                   </div>



                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Select Image</label>
                                        <div class="controls">
                                            <input type="file" name="featured_image" class="form-control" aria-invalid="false"> <div class="help-block"></div></div>
                                    </div>

                                </div>


                            <div class="row">

                                <div class="form-group col-md-12">
                                    <label>Description:</label>
                                    <textarea name="description" id="description" rows="7" class="form-control summernote">

                                </textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('description') }}</p>
                                </div>
                                <!-- /.box-body -->
                                 <div class="box-footer">
                                     <button type="submit" class="btn btn-success">Add Category</button>
                                 </div>
                            </div>
                       </div>
                <!-- /.box -->
            </div>
        </div>
        </form>
    </section>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#create_category').validate({
                rules: {
                    category_name:{
                        required: true,
                    }
                },
                messages: {
                    category_name: {
                        required: "Please enter the category name"
                    }
                }
            });
        });
    </script>

    <!-- Bootstrap Select -->
    <!-- Select2 -->
    <script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
    <!-- Superieur Admin for advanced form element -->
    <script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>






@endsection

@section('styles')

    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/select2/dist/css/select2.css')}}">


    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection

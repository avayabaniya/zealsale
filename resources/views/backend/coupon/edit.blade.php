@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title" id="mainheader">Edit User Coupon</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" id="submainheader" aria-current="page">Create Coupon</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        
 <div class="row">
     <div class="col-lg-12 col-xl-12">

    <form id="create_coupon" action="{{ route('coupon.update',$coupons->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">

            <!-- right column -->
            <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title" id="subheader">Edit Coupon</h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))    
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                  
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="coupon_name">Welcome Coupon Code: </label>
                                <input name="coupon_code" class="form-control" placeholder="Coupon Code" id="coupon_code" autocomplete="off" minlength="5" maxlength="15" value="{{$coupons->coupon_code}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('coupon_amount') }}</p>
                            </div>
        

                            <div class="form-group col-md-12">
                                <label>Amount Type</label>
                                <select class="form-control select2" name="amount_type" id="amount_type" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="{{$coupons->amount_type}}" selected>{{$coupons->amount_type}}</option>                                   
                                    <option value="Percentage">Percentage</option>
                                    <option value="Fixed">Fixed</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label >Amount: </label>
                                <input name="amount" class="form-control" placeholder="Coupon Amount" id="coupon_amount" autocomplete="off" value="{{$coupons->amount}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('coupon_amount') }}</p>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Expiry Date</label>
                                <input  type="date" name="expiry_date" class="form-control datepicker" placeholder="Expiry Date" id="expiry_date" autocomplete="off" value="{{$coupons->expiry_date}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('expiry_date') }}</p>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="Checkbox_1" value="1">Select a Status</label>
                                <div class="checkbox">
                                    @if($coupons->status == 1)
                                        <input type="checkbox" id="Checkbox_1" name="new_status" checked class="filled-in chk-col-maroon">
                                    @else
                                        <input type="checkbox" id="Checkbox_1" name="new_status" class="filled-in chk-col-maroon">
                                    @endif
                                    <label for="Checkbox_1" value="1">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <button type="submit" id="submit" class="btn btn-success"><i class="mdi mdi-content-save"></i>&nbsp; Update Coupon</button>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </form>
</div>

</div>
</section>

@endsection


@section('scripts')

<script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#create_coupon').validate({
                rules: {
                    coupon_code:{
                        required: true
                    }
                },
                messages: {
                    coupon_code: {
                        required: "Please enter the coupon code"
                    }
                }
            });
        });

       

    </script>

    <!-- Bootstrap Select -->
    <!-- Select2 -->
    <script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
    <!-- Superieur Admin for advanced form element -->
    <script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>

@endsection


@section('styles')

    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/select2/dist/css/select2.css')}}">


    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>

@endsection

@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title" id="mainheader">Create Coupon</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" id="submainheader" aria-current="page">Create Coupon</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        
 <div class="row">
     <div class="col-lg-4 col-xl-4">

    <form id="create_coupon" action="{{ route('coupon.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">

            <!-- right column -->
            <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title" id="subheader">Create Coupon</h4>
                    </div>
                    <!-- /.box-header -->
                 
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="coupon_name">Coupon Code: </label>
                                <input name="coupon_code" class="form-control" placeholder="Coupon Code" id="coupon_code" autocomplete="off" minlength="5" maxlength="15" value="{{old('coupon_code')}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('coupon_amount') }}</p>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>Coupon Type</label>
                                <select class="form-control select2" name="couponcategory_id" id="couponcategory_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    @foreach($couponcategories as $coupon)
                                    <option value="{{$coupon->id}}">{{$coupon->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Amount Type</label>
                                <select class="form-control select2" name="amount_type" id="amount_type" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="Percentage">Percentage</option>
                                    <option value="Fixed">Fixed</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label >Amount: </label>
                                <input name="amount" class="form-control" placeholder="Coupon Amount" id="coupon_amount" autocomplete="off" value="{{old('amount')}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('coupon_amount') }}</p>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Expiry Date</label>
                                <input  type="date" name="expiry_date" class="form-control datepicker" placeholder="Expiry Date" id="expiry_date" autocomplete="off" value="{{old('expiry_date')}}">
                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('expiry_date') }}</p>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="Checkbox_1">Select Status</label>

                                <div class="checkbox" id="checkhide">
                                    <input type="checkbox" id="Checkbox_11" checked name="status" class="filled-in chk-col-maroon">
                                    <label for="Checkbox_11" value="1">Status</label>
                                </div>
                                
                                <div class="checkbox" id="check">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <button type="submit" id="submit" class="btn btn-success"><i class="mdi mdi-content-save"></i>&nbsp; Create Coupon</button>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </form>
</div>
<div class="col-lg-8 col-xl-8">
            
    <div class="box">
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
                @if(Session::has('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_error') !!} </strong>
                    </div>
                @endif
                @if(Session::has('flash_message_success'))    
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert"> X </button>
                        <strong> {!! session('flash_message_success') !!} </strong>
                    </div>
                @endif
            </div>
        </div>

        <div class="box-header with-border">
            <h3 class="box-title">Coupon Data Table</h3>
            <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="">
                <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" class="sorting_asc" aria-controls="example" style="width: 157.11px;" aria-label="Name: activate to sort column descending" aria-sort="ascending" rowspan="1" colspan="1">S.No</th>
                                <th tabindex="0" class="sorting" aria-controls="example" style="width: 113.67px;" aria-label="Office: activate to sort column ascending" rowspan="1" colspan="1">Coupon Code</th>
                                <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Amount</th>
                                <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Expiry Date</th>
                                <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Status</th>
                                <th tabindex="0" class="sorting" aria-controls="example" style="width: 95.4px;" aria-label="Salary: activate to sort column ascending" rowspan="1" colspan="1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($coupons as $coupon_data)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                
                                <td id="coupon_code{{$coupon_data->id}}">{{$coupon_data->coupon_code}}</td>
                                
                                @if($coupon_data->amount_type == 'Percentage')
                                <td class="amount{{$coupon_data->id}}">{{$coupon_data->amount}}% </td>
                                @endif
                                @if($coupon_data->amount_type == 'Fixed')
                                <td class="amount{{$coupon_data->id}}">Rs.{{$coupon_data->amount}}</td>
                                @endif
                                </td>
                                
                                <td style="display:none;" id="amount_type{{$coupon_data->id}}">{{$coupon_data->amount_type}}</td>
                                
                                <td class="expiry_date{{$coupon_data->id}}">{{$coupon_data->expiry_date}}</td>

                                @if($coupon_data->status == 1)                                
                                <td style="display:none;" id="status{{$coupon_data->id}}">
                                    {{$coupon_data->status}}
                                </td>
                                <td> <span class="badge badge-success badge-pill"> Active </span></td>
                                @endif
                                @if($coupon_data->status == 0)
                                <td  style="display:none;" id="status{{$coupon_data->id}}">
                                    {{$coupon_data->status}}
                                </td>
                                <td> <span class="badge badge-danger badge-pill"> Inactive </span></td>
                                @endif
                                </td>
                                <td>
                                    {{-- <a href="" data-toggle="modal" data-target="#modal-center{{$coupon_data->id}}" class="text-inverse" title="Edit" data-original-title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a> --}}
                                    <a data-toggle="tooltip" class="text-inverse edit" value="{{ $coupon_data->id }}" title="Edit" data-original-title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    &nbsp;
                                    {{--<a href="{{ route('coupon.destroy', $coupon_data->id) }}" class="text-inverse" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </a>--}}

                                    <a href="javascript:" rel = "{{$coupon_data->id}}" rel1 = "coupon/delete" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                       </tbody>
                        <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">S.No</th>
                                <th rowspan="1" colspan="1">Coupon Code</th>
                                <th rowspan="1" colspan="1">Amount</th>
                                <th rowspan="1" colspan="1">Expiry Date</th>
                                <th rowspan="1" colspan="1">Status</th>
                                <th rowspan="1" colspan="1">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
</div>
</section>

@endsection


@section('scripts')

<script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#create_coupon').validate({
                rules: {
                    coupon_code:{
                        required: true
                    }
                },
                messages: {
                    coupon_code: {
                        required: "Please enter the coupon code"
                    }
                }
            });
        });

        $(document).ready(function () {
            console.log('document ready');
            $('.edit').click(function () {
                var id = $(this).attr('value');
                // alert(parseInt(id));
                $('#mainheader').html('Update Coupon');
                $('#subheader').html('Update Coupon');
                $('#submainheader').html('Update Coupon');
                $('#submit').html('<i class="mdi mdi-content-save"></i>&nbsp; Update Coupon');
                

                var url = '{{ route("coupon.update", ":id") }}';
                url = url.replace(':id', id);
                

                $('#create_coupon').attr("action",url);

                // Expire Date
                var expiry_date_id = ".expiry_date".concat(id);
                var expiry_date = $(expiry_date_id).html();
                // alert(expiry_date);
                $('#expiry_date').val(expiry_date);



                // Coupon Code
                var coupon_code_id = "#coupon_code".concat(id);
                var coupon_code = $(coupon_code_id).html();
                // alert(coupon_code);
                $('#coupon_code').val(coupon_code);


                // Coupon Amount
                var amount_id = ".amount".concat(id);
                var amount = $(amount_id).html();
                // alert(amount);
                $('#coupon_amount').val(amount);
                

                // Amount type
                var amount_type_id = "#amount_type".concat(id);
                var amount_type = $(amount_type_id).html();
                // alert(amount_type);
                $('#amount_type').append('<option value="'+amount_type+'" selected="selected">'+amount_type+'</option>');
                // $('#amount_type').append('<option value="'++'">Hello</option>');

                // Status
                var status_id = "#status".concat(id);
                var status = $(status_id).html();
                // alert(status);
                if(status == 0 ){
                // alert('LOL');
                $('#checkhide').hide();
                $('#check').html('<input type="checkbox" id="Checkbox_1" name="new_status" class="filled-in chk-col-maroon"><label for="Checkbox_1" value="1">Status</label>');
                }else{
                // alert('Tero Tauko');
                $('#checkhide').hide();
                $('#check').html('<input type="checkbox" id="Checkbox_1" checked  name="new_status" class="filled-in chk-col-maroon"><label for="Checkbox_1" value="1">Status</label>');
                }
                // $('#Checkbox_1').attr("value",checked);
                
            });
        });

    </script>

    <!-- Bootstrap Select -->
    <!-- Select2 -->
    <script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
    <!-- Superieur Admin for advanced form element -->
    <script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>

@endsection


@section('styles')

    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/select2/dist/css/select2.css')}}">


    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>

    
@endsection

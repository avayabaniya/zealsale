@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">About Us </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Update About Us</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form action="{{ route('about.update',1) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Update About Us </h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-12" style="margin-left: 20px;">
                                @if(Session::has('flash_message_error'))
                                    <div class="alert alert-error alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_error') !!} </strong>
                                    </div>
                                @endif
                                @if(Session::has('flash_message_success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_success') !!} </strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="box-header with-border">
                                <h4 class="box-title">Banner Section</h4>
                                <br>
                            </div>
                           <div class="row">
                               <div class="form-group col-md-6">
                                   <label for="banner_image_title">Banner Image Title:</label>
                                   <input name="banner_image_title" class="form-control" placeholder="Organization Name" id="banner_image_title" autocomplete="off" value="{{$about->banner_image_title}}">
                                   <p style="color: red; margin-bottom: 0px;" id="banner_image_title_val">{{ $errors -> first('banner_image_title') }}</p>
                                </div>                       
                                <div class="form-group col-md-4">
                                    <label>Select Banner Image</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation()" name="banner_image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="hidden" name="banner_current_image" value="{{$about->banner_image}}">
                                    @if(!empty($about->banner_image))
                                        <img src="{{ asset('public/images/backend_images/about/small/'.$about->banner_image) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">Banner Description</label>
                                        <textarea id="" cols="30" rows="10" name="banner_image_text" class="form-control summernote">{!! $about->banner_image_text !!}</textarea>
                                       <p style="color: red; margin-bottom: 0px;" id="banner_image_title_val">{{ $errors -> first('banner_image_text') }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="box-header with-border">
                                <h4 class="box-title">First Section</h4>
                                <br>
                            </div>
                           <div class="row">
                               <div class="form-group col-md-6">
                                   <label for="about_first_title">First Section Title:</label>
                                   <input name="about_first_title" class="form-control" placeholder="First Section Title" id="about_first_title" autocomplete="off" value="{{$about->about_first_title}}">
                                   <p style="color: red; margin-bottom: 0px;" id="about_first_title_val">{{ $errors -> first('about_first_title') }}</p>
                                </div>                       
                                <div class="form-group col-md-4">
                                    <label>Select First Section Image</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation1()" name="about_first_image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="hidden" name="about_first_current_image" value="{{$about->about_first_image}}">
                                    @if(!empty($about->about_first_image))
                                        <img src="{{ asset('public/images/backend_images/about/small/'.$about->about_first_image) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">First Section Description</label>
                                        <textarea id="" cols="30" rows="10" name="about_first_text" class="form-control summernote">{!! $about->about_first_text !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="box-header with-border">
                                <h4 class="box-title">Second Section</h4>
                                <br>
                            </div>
                           <div class="row">
                               <div class="form-group col-md-6">
                                   <label for="about_second_title">Second Section Title:</label>
                                   <input name="about_second_title" class="form-control" placeholder="Second Section Title" id="about_second_title" autocomplete="off" value="{{$about->about_second_title}}">
                                   <p style="color: red; margin-bottom: 0px;" id="about_second_title_val">{{ $errors -> first('about_second_title') }}</p>
                                </div>                       
                                <div class="form-group col-md-4">
                                    <label>Select Second Section Image</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation2()" name="about_second_image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="hidden" name="about_second_current_image" value="{{$about->about_second_image}}">
                                    @if(!empty($about->about_second_image))
                                        <img src="{{ asset('public/images/backend_images/about/small/'.$about->about_second_image) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">Second Section Description</label>
                                        <textarea id="" cols="30" rows="10" name="about_second_text" class="form-control summernote">{!! $about->about_second_text !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="box-header with-border">
                                <h4 class="box-title">Third Section</h4>
                                <br>
                            </div>
                           <div class="row">
                               <div class="form-group col-md-6">
                                   <label for="about_third_title">Third Section Title:</label>
                                   <input name="about_third_title" class="form-control" placeholder="Third Section Title" id="about_third_title" autocomplete="off" value="{{$about->about_third_title}}">
                                   <p style="color: red; margin-bottom: 0px;" id="about_third_title_val">{{ $errors -> first('about_third_title') }}</p>
                                </div>                       
                                <div class="form-group col-md-4">
                                    <label>Select Third Section Image</label>
                                    <div class="controls">
                                        <input type="file" id="file" onchange="return fileValidation3()" name="about_third_image" class="form-control" aria-invalid="false"> 
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="hidden" name="about_third_current_image" value="{{$about->about_third_image}}">
                                    @if(!empty($about->about_third_image))
                                        <img src="{{ asset('public/images/backend_images/about/small/'.$about->about_third_image) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">Third Section Description</label>
                                        <textarea id="" cols="30" rows="10" name="about_third_text" class="form-control summernote">{!! $about->about_third_text !!}</textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.box -->

                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <button type="submit" id="update" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Update</button>
                </div>
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#banner_image_title').keyup(function(){
        var banner_image_title = $('#banner_image_title').val();

        if(banner_image_title.length == 0){
            $('#banner_image_title_val').text('** Banner Title is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (banner_image_title.length > 0 && banner_image_title.length < 6)
        {
            $('#banner_image_title_val').text('** Minimum 6 characters required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#banner_image_title_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 

        $('#about_first_title').keyup(function(){
        var about_first_title = $('#about_first_title').val();

        if(about_first_title.length == 0){
            $('#about_first_title_val').text('** First Section Title is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (about_first_title.length > 0 && about_first_title.length < 6)
        {
            $('#about_first_title_val').text('** Minimum 6 characters required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#about_first_title_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    $('#about_second_title').keyup(function(){
        var about_second_title = $('#about_second_title').val();

        if(about_second_title.length == 0){
            $('#about_second_title_val').text('** Second Section Title is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (about_second_title.length > 0 && about_second_title.length < 6)
        {
            $('#about_second_title_val').text('** Minimum 6 characters required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#about_second_title_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    $('#about_third_title').keyup(function(){
        var about_third_title = $('#about_third_title').val();

        if(about_third_title.length == 0){
            $('#about_third_title_val').text('** Third Section Title is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (about_third_title.length > 0 && about_third_title.length < 6)
        {
            $('#about_third_title_val').text('** Minimum 6 characters required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#about_third_title_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    
    
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection

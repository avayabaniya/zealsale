@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Update FAQs </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Update FAQ</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form action="{{ route('faq.update',$faq->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Update FAQ</h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-6" style="margin-left: 20px;">
                                @if(Session::has('flash_message_error'))
                                    <div class="alert alert-error alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_error') !!} </strong>
                                    </div>
                                @endif
                                @if(Session::has('flash_message_success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_success') !!} </strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body">
                           <div class="row">
                               <div class="form-group col-md-12">
                                   <label for="question">Question:</label>
                                   <input name="question" class="form-control" placeholder="Write Question" id="question" autocomplete="off" value="{{$faq->question}}">
                                   <p style="color: red; margin-bottom: 0px;" id="question_val">{{ $errors -> first('question') }}</p>
                               </div>
                           </div>
                           
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Answer:</label>
                                    <textarea name="answer" rows="7" maxlength="1000" placeholder="Answer of the Questions" class="form-control summernote">{{$faq->answer}}</textarea>
                                    <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('answer') }}</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" id="update" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Update</button>
                        </div>
                        <hr>  

                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#question').keyup(function(){
        var question = $('#question').val();

        if(question.length == 0){
            $('#question_val').text('** Question is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (question.length > 0 && question.length < 10)
        {
            $('#question_val').text('** Question should be Minimum 10 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#question_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection
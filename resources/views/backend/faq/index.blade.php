@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="box">
    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_error') !!} </strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))

                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert"> X </button>
                    <strong> {!! session('flash_message_success') !!} </strong>
                </div>
            @endif
        </div>
    </div>


    <div class="box-header with-border">
        <h3 class="box-title">Frequently Asked Questions Data Table</h3>
        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
        <a href="{{ route('faq.create') }}" style="color:white;" class="hover"><button class="btn btn-info pull-right hover"><i class="fa fa-plus-circle"></i> &nbsp; Add New FAQ </button></a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="">
            <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">

                <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                    <thead>
                    <tr role="row">
                        <th tabindex="0" class="sorting_asc" aria-controls="example" style="width: 157.11px;" aria-label="Name: activate to sort column descending" aria-sort="ascending" rowspan="1" colspan="1">S.No</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 113.67px;" aria-label="Office: activate to sort column ascending" rowspan="1" colspan="1">Questions</th>
                        <th tabindex="0" class="sorting" aria-controls="example" style="width: 95.4px;" aria-label="Salary: activate to sort column ascending" rowspan="1" colspan="1">Action</th></tr>
                    </thead>
                    <tbody>
                    @foreach ($faqs as $data)
                        <tr>
                            <td>{{ $loop->index+1}}</td>
                            <td>{{$data->question}}</td>
                            <td>
                                <a href="{{ route('faq.edit', $data->slug) }}" class="text-inverse" title="Edit" data-toggle="tooltip" data-original-title="Edit">
                                    <i class="fa fa-edit"></i> 
                                </a>
                                &nbsp;
                                {{--<a href="{{ route('faq.destroy', $data->id) }}" class="text-inverse" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                    <i class="fa fa-trash"></i> 
                                </a>--}}

                                <a href="javascript:" rel="{{$data->id}}" rel1="faq/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                                &nbsp;
                            </td>
                        </tr>
                    @endforeach                        
                   </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="1" colspan="1">S.No</th>
                        <th rowspan="1" colspan="1">Questions</th>
                        <th rowspan="1" colspan="1">Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>

@endsection

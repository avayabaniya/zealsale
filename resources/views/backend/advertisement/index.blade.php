@extends('layouts.adminLayout.admin_design')
@section('content')
    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title" id="mainheader">Create Advertisement</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" id="submainheader" aria-current="page">Create Advertisement</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <section class="content">

        <div class="row">
            <div class="col-lg-5 col-xl-5">
                <form id="create_advertisement" action="{{ route('advertisement.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <!-- right column -->
                        <div class="col-xl-12 col-lg-12">
                            <!-- Horizontal Form -->
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title" id="subheader">Create Advertisement</h4>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Select Category</label>
                                            <select class="form-control select2" name="category_id" id="category_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                @foreach($categories as $cat)
                                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-10">
                                            <label>Select Image (size: 1170 x 305) </label>
                                            <div class="controls">
                                                <input type="file" id="file" onchange="return fileValidation()" name="image" class="form-control" aria-invalid="false">
                                                <div class="help-block"></div>
                                                <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('file') }}</p>
                                            </div>
                                        </div>

                                        <div class="col-md-2" id="image_div">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer text-center">
                                    <button type="submit" id="submit" class="btn btn-success"><i class="mdi mdi-content-save"></i>&nbsp; Create </button>
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-7 col-xl-7">

                <div class="box">
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="box-header with-border">
                        <h3 class="box-title">Advertisement Data Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="">
                            <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                                <thead>
                                <tr role="row">
                                    <th >S.No</th>
                                    <th> Category Name </th>
                                    <th> Image </th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($advertisements as $advertisement_data)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>

                                        <input type="hidden" value="{{ $advertisement_data->category->id }}" name="category_id" id="category_id{{$advertisement_data->id}}">
                                        <td class="category{{$advertisement_data->id}}">{{$advertisement_data->category->name}}</td>
                                        <td class="image{{$advertisement_data->id}}">
                                            <img src="{{ asset('public/images/backend_images/setting/small/'.$advertisement_data->image) }}" alt="" style="width:60px;30px;" name="old_image">
                                        </td>
                                        <td>
                                            <a data-toggle="tooltip" class="text-inverse edit" value="{{ $advertisement_data->id }}" title="Edit" data-original-title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            &nbsp;
                                            <a href="javascript:" rel = "{{$advertisement_data->id}}" rel1 = "advertisement/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th> Category Name </th>
                                    <th> Image </th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#create_advertisement').validate({
                rules: {
                    coupon_code:{
                        required: true
                    }
                },
                messages: {
                    coupon_code: {
                        required: "Please enter the coupon code"
                    }
                }
            });
        });
        $(document).ready(function () {
            console.log('document ready');
            $('.edit').click(function () {
                var id = $(this).attr('value');
                // alert(parseInt(id));
                $('#mainheader').html('Update Advertisement');
                $('#subheader').html('Update Advertisement');
                $('#submainheader').html('Update Advertisement');
                $('#submit').html('<i class="mdi mdi-content-save"></i>&nbsp; Update Advertisement');

                var url = '{{ route("advertisement.update", ":id") }}';
                url = url.replace(':id', id);

                $('#create_advertisement').attr("action",url);

                // Branch Name
                var category_id = ".category".concat(id);
                var category = $(category_id).html();
                var b_id = $("#category_id".concat(id)).val();
                // alert(b_id);
                $('#category_id').append('<option value="'+b_id+'" selected="selected">'+category+'</option>');
                // $('#category_id').prop('selectedIndex', b_id - 1);

                // Image
                var image_id = ".image".concat(id);
                var image = $(image_id).html();
                // alert(image);
                @if($advertisement_count == 0)
                @else
                $('#image_div').append(image);
                @endif
            });
        });
    </script>
    <!-- Bootstrap Select -->
    <!-- Select2 -->
    <script src="{{asset('public/backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
    <!-- Superieur Admin for advanced form element -->
    <script src="{{asset('public/backend/js/pages/advanced-form-element.js')}}"></script>
    <script>
        $(function() {
            $("#datepicker").datepicker();
        });
    </script>
@endsection
@section('styles')
    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('public/backend/assets/vendor_components/select2/dist/css/select2.css')}}">
    <style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{
            display: none;
        }
    </style>
@endsection
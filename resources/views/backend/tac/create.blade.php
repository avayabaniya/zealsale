@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Create Terms & Conditions </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create TAC</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">

        <form action="{{ route('tac.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Create Terms & Conditions</h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-6" style="margin-left: 20px;">
                                @if(Session::has('flash_message_error'))
                                    <div class="alert alert-error alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_error') !!} </strong>
                                    </div>
                                @endif
                                @if(Session::has('flash_message_success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert"> X </button>
                                        <strong> {!! session('flash_message_success') !!} </strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body">
                           <div class="row">
                               <div class="form-group col-md-12">
                                   <label for="title">Title:</label>
                                   <input name="title" class="form-control" placeholder="Write Title" id="title" autocomplete="off" value="{{old('title')}}">
                                   <p style="color: red; margin-bottom: 0px;" id="title_val">{{ $errors -> first('title') }}</p>
                               </div>
                           </div>
                           
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Description:</label>
                                    <textarea name="description" rows="7" placeholder="Description of the Title" class="form-control summernote"></textarea>
                                    <p style="color: red; margin-bottom: 0px;" id="description_val">{{ $errors -> first('description') }}</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" id="submit" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Create</button>
                        </div>
                        <hr>  

                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>
    </section>

@endsection

@section('scripts')

    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#title').keyup(function(){
        var title = $('#title').val();

        if(title.length == 0){
            $('#title_val').text('** Title is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (title.length > 0 && title.length < 6)
        {
            $('#title_val').text('** Title should be Minimum 6 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#title_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 

    $('#description').keyup(function(){
        var description = $('#description').val();

        if(description.length == 0){
            $('#description_val').text('** Description is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (description.length > 0 && description.length < 10)
        {
            $('#description_val').text('** Description should be Minimum 10 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#description_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 
    
    
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

    </style>
@endsection
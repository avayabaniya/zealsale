@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Create / View Brands </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create / View Brands</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="row">
            <div class="col-lg-5 col-xl-5">
                    
                
        <form action="{{ route('brand.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Create Brand</h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body">
                            <div class="form-group">
                               <label for="name">Brand Name:</label>
                               <input name="name" class="form-control" placeholder="Brand Name" id="name" autocomplete="off" value="{{old('name')}}">
                               <p style="color: red; margin-bottom: 0px;" id="name_val">{{ $errors -> first('name') }}</p>
                            </div>

                            <div class="form-group">
                                <label>Upload Logo</label>
                                <div class="controls">
                                    <input type="file" id="file" onchange="return fileValidation()" name="image" class="form-control" aria-invalid="false"> 
                                    <div class="help-block"></div>
                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('file') }}</p>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" id="submit" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Create</button>
                        </div>
                        <hr>  
                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>

            </div>
            <div class="col-lg-7 col-xl-7">
                        
                <div class="box">
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))    
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
    
                    <div class="box-header with-border">
                        <h3 class="box-title">Brands Data Table</h3>
                        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="">
                            <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                                <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" class="sorting_asc" aria-controls="example" style="width: 157.11px;" aria-label="Name: activate to sort column descending" aria-sort="ascending" rowspan="1" colspan="1">S.No</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 113.67px;" aria-label="Office: activate to sort column ascending" rowspan="1" colspan="1">Name</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Logo</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 95.4px;" aria-label="Salary: activate to sort column ascending" rowspan="1" colspan="1">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($brands as $brands_data)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$brands_data->name}}</td>
                                            <td>
                                                <img src="{{ asset('public/images/backend_images/brand/small/'.$brands_data->logo) }}" alt="{{$brands_data->name}}" width="70px" height="50px">
                                            </td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#modal-center{{$brands_data->id}}" class="text-inverse" title="Edit" data-original-title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                &nbsp;
                                                {{--<a href="{{ route('brand.destroy', $brands_data->id) }}" class="text-inverse" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a> --}}

                                                <a href="javascript:" class="text-inverse deleteRecord" rel = "{{$brands_data->id}}" rel1 = "brand/destroy" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class="modal center-modal fade{{$brands_data->id}}" id="modal-center{{$brands_data->id}}" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action="{{route('brand.update',$brands_data->id)}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Brand - {{$brands_data->title}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="new_name">Brand Name</label>
                                                                <input type="text" name="new_name" id="new_name" class="form-control" value="{{$brands_data->name}}">
                                                                <p style="color: red; margin-bottom: 0px;" id="new_name_val">{{ $errors -> first('new_title') }}</p>
                                                            </div>                                
                                                            
                                                            <div class="row">
                                                                <div class="form-group col-md-9">
                                                                    <label>Select Image</label>
                                                                    <div class="controls">
                                                                        <input type="file" id="file1" onchange="return fileValidation1()" name="image" class="form-control" aria-invalid="false"> 
                                                                        <div class="help-block"></div>
                                                                    </div>
                                                                </div>
        
                                                                <div class="form-group col-md-3">
                                                                    <input type="hidden" name="current_image" value="{{$brands_data->logo}}">
                                                                    @if(!empty($brands_data->logo))
                                                                        <img src="{{ asset('public/images/backend_images/brand/small/'.$brands_data->logo) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                                                    @endif
                                                               </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer modal-footer-uniform">
                                                            <button type="button" class="btn btn-bold btn-pure btn-danger" data-dismiss="modal">Close</button>
                                                            <button type="submit" id="update" class="btn btn-bold btn-pure btn-success float-right">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal -->
                                    @endforeach
                                   </tbody>
                                    <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">S.No</th>
                                            <th rowspan="1" colspan="1">Name</th>
                                            <th rowspan="1" colspan="1">Logo</th>
                                            <th rowspan="1" colspan="1">Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>


    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#name').keyup(function(){
        var name = $('#name').val();

        if(name.length == 0){
            $('#name_val').text('** Brand name is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (name.length > 0 && name.length < 2)
        {
            $('#name_val').text('** Minimum 2 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#name_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 

    
    $('#new_name').keyup(function(){
        var new_name = $('#new_name').val();

        if(new_name.length == 0){
            $('#new_name_val').text('** Brand name is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (new_name.length > 0 && new_name.length < 2)
        {
            $('#new_name_val').text('** Minimum 2 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#new_name_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

		.example-modal .modal {
			position: relative;
			top: auto;
			bottom: auto;
			right: auto;
			left: auto;
			display: block;
			z-index: 1;
		}

		.example-modal .modal {
			background: transparent !important;
		}

    </style>
@endsection
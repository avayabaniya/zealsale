@extends('layouts.adminLayout.admin_design')
@section('content')

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Create / View Slider </h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="mdi mdi-home-outline"></i> Home</a></li>
                            <li class="breadcrumb-item" aria-current="page">Create / View Slider</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>

    <section class="content">
        <div class="row">
            <div class="col-lg-5 col-xl-5">
                    
                
        <form action="{{ route('slider.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                <!-- Horizontal Form -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h4 class="box-title">Create Slider</h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body">
                            <div class="form-group">
                               <label for="title">Slider Title:</label>
                               <input name="title" class="form-control" placeholder="Slider Title" id="title" autocomplete="off" value="{{old('title')}}">
                               <p style="color: red; margin-bottom: 0px;" id="title_val">{{ $errors -> first('title') }}</p>
                            </div>
                           
                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input name="price" class="form-control" placeholder="Price" id="price" autocomplete="off" value="{{old('price')}}">
                                <p style="color: red; margin-bottom: 0px;" id="price_val">{{ $errors -> first('price') }}</p>
                            </div>

                            <div class="form-group">
                                <label>Select Upload</label>
                                <div class="controls">
                                    <input type="file" id="file" onchange="return fileValidation()" name="image" class="form-control" aria-invalid="false"> 
                                    <div class="help-block"></div>
                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('file') }}</p>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" id="submit" class="btn btn-success btn-lg"><i class="mdi mdi-content-save"></i> Create</button>
                        </div>
                        <hr>  
                </div>
                <!-- /.box -->
            </div>
        </div>

        </form>

            </div>
            <div class="col-lg-7 col-xl-7">
                        
                <div class="box">
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_error') !!} </strong>
                                </div>
                            @endif
                            @if(Session::has('flash_message_success'))    
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert"> X </button>
                                    <strong> {!! session('flash_message_success') !!} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
    
                    <div class="box-header with-border">
                        <h3 class="box-title">Sliders Data Table</h3>
                        <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF &amp; Print</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="">
                            <div class="dataTables_wrapper container-fluid dt-bootstrap4" id="example_wrapper">
                                <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100 dataTable" id="example" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" class="sorting_asc" aria-controls="example" style="width: 157.11px;" aria-label="Name: activate to sort column descending" aria-sort="ascending" rowspan="1" colspan="1">S.No</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 113.67px;" aria-label="Office: activate to sort column ascending" rowspan="1" colspan="1">Title</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 54.71px;" aria-label="Age: activate to sort column ascending" rowspan="1" colspan="1">Price</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 103.17px;" aria-label="Start date: activate to sort column ascending" rowspan="1" colspan="1">Image</th>
                                            <th tabindex="0" class="sorting" aria-controls="example" style="width: 95.4px;" aria-label="Salary: activate to sort column ascending" rowspan="1" colspan="1">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($sliders as $slider_data)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$slider_data->title}}</td>
                                            <td>{{$slider_data->price}}</td>
                                            <td>
                                                <img src="{{ asset('public/images/backend_images/slider/small/'.$slider_data->image) }}" alt="{{$slider_data->title}}" width="70px" height="50px">
                                            </td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#modal-center{{$slider_data->id}}" class="text-inverse" title="Edit" data-original-title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                &nbsp;
                                                {{--<a href="{{ route('slider.destroy', $slider_data->id) }}" class="text-inverse" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>--}}

                                                <a href="javascript:" rel="{{$slider_data->id}}" rel1="slider/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class="modal center-modal fade{{$slider_data->id}}" id="modal-center{{$slider_data->id}}" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form action="{{route('slider.update',$slider_data->id)}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Slider - {{$slider_data->title}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="new_title">Slider Title</label>
                                                                <input type="text" name="new_title" id="new_title" class="form-control" value="{{$slider_data->title}}">
                                                                <p style="color: red; margin-bottom: 0px;" id="new_title_val">{{ $errors -> first('new_title') }}</p>
                                                            </div>                                
                                                            
                                                            <div class="form-group">
                                                                <label for="new_price">Price</label>
                                                                <input type="text" name="new_price" id="new_price" class="form-control" value="{{$slider_data->price}}">
                                                                <p style="color: red; margin-bottom: 0px;" id="new_price_val">{{ $errors -> first('new_title') }}</p>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-group col-md-9">
                                                                    <label>Select Image</label>
                                                                    <div class="controls">
                                                                        <input type="file" id="file1" onchange="return fileValidation1()" name="image" class="form-control" aria-invalid="false"> 
                                                                        <div class="help-block"></div>
                                                                    </div>
                                                                </div>
        
                                                                <div class="form-group col-md-3">
                                                                    <input type="hidden" name="current_image" value="{{$slider_data->image}}">
                                                                    @if(!empty($slider_data->image))
                                                                        <img src="{{ asset('public/images/backend_images/slider/small/'.$slider_data->image) }}" style="width: 100px; height: 70px; margin-top: 10px;">
                                                                    @endif
                                                               </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer modal-footer-uniform">
                                                            <button type="button" class="btn btn-bold btn-pure btn-danger" data-dismiss="modal">Close</button>
                                                            <button type="submit" id="update" class="btn btn-bold btn-pure btn-success float-right">Save changes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal -->
                                    @endforeach
                                   </tbody>
                                    <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">S.No</th>
                                            <th rowspan="1" colspan="1">Title</th>
                                            <th rowspan="1" colspan="1">Price</th>
                                            <th rowspan="1" colspan="1">Image</th>
                                            <th rowspan="1" colspan="1">Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script src="{{asset('public/backend/js/pages/data-table.js')}}"></script>


    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 300
            });
        }); 
    </script>

    <script>
        $('#title').keyup(function(){
        var title = $('#title').val();

        if(title.length == 0){
            $('#title_val').text('** Slider Title is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else if (title.length > 0 && title.length < 6)
        {
            $('#title_val').text('** Minimum 6 characters').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#title_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 
    
    $('#price').keyup(function(){
        var price = $('#price').val();

        if(price.length == 0){
            $('#price_val').text('** Price is required').css("color","red");
            $('#submit').prop('disabled', true);
            $('#submit').css('background-color', 'red');
        }
        else 
        {
            $('#price_val').text('').css("color","green");
            $('#submit').prop('disabled', false);
            $('#submit').css('background-color', 'green');
        }
    }); 

    
    $('#new_title').keyup(function(){
        var new_title = $('#new_title').val();

        if(new_title.length == 0){
            $('#new_title_val').text('** Slider Title is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else if (new_title.length > 0 && new_title.length < 6)
        {
            $('#new_title_val').text('** Minimum 6 characters').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#new_title_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
    
    $('#new_price').keyup(function(){
        var new_price = $('#new_price').val();

        if(new_price.length == 0){
            $('#new_price_val').text('** Price is required').css("color","red");
            $('#update').prop('disabled', true);
            $('#update').css('background-color', 'red');
        }
        else 
        {
            $('#new_price_val').text('').css("color","green");
            $('#update').prop('disabled', false);
            $('#update').css('background-color', 'green');
        }
    }); 
  </script>  
@endsection

@section('styles')

<style>
        .error{
            color: red;
        }
        .modal-backdrop{
            z-index: 1 !important;
        }
        .note-popover.popover{

            display: none;
        }

		.example-modal .modal {
			position: relative;
			top: auto;
			bottom: auto;
			right: auto;
			left: auto;
			display: block;
			z-index: 1;
		}

		.example-modal .modal {
			background: transparent !important;
		}

    </style>
@endsection
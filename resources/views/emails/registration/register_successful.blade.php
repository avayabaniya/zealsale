@component('mail::message')
# Introduction

Hello {{$customer->full_name}}, you have successfully registered to ZealSale.
Your email address is {{ $customer->email }}
<hr>

@component('mail::button', ['url' => 'http://localhost/zealsale/'])
Keep Shopping
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Introduction

Hello {{$customer->full_name}}, your order has been successfully delivered to you.
Thankyou, for shopping with us.
<hr>

<strong>Order Code:</strong> {{ $order->order_code }}

<strong>Product Name:</strong> {{ $order->product->product_name }}

![Product Image]({{ asset('http://localhost/zealsale/public/images/backend_images/product/small/'.$order->product->image) }} "{{ $order->product->product_name}}")

<strong>Order Price:</strong> {{ $order->order_price }}
<hr>



@component('mail::button', ['url' => 'http://localhost/zealsale/'])
Keep Shopping
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')

<img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo)}}" alt="{{ $sites_data->organization_name}}" style="width:100px;height:auto">

# Introduction

One of our order has been canceled by {{$customer->full_name}}.
<strong>Phone:</strong> {{ $order->checkout->phone }}


<hr>

<strong>Order Code:</strong> {{ $order->order_code }}

<strong>Product Name:</strong> {{ $order->product->product_name }}

![Product Image]({{ asset('http://localhost/zealsale/public/images/backend_images/product/small/'.$order->product->image) }} "{{ $order->product->product_name}}")

<strong>Order Price:</strong> {{ $order->order_price }}
<hr>

@component('mail::button', ['url' => 'http://localhost/zealsale/'])
Keep Shopping
@endcomponent

Thanks,<br>
{{ config('app.name') }}
01-5199625
zealsalee@gmail.com 
@endcomponent

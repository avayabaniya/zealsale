@component('mail::message')
# Contact Us Mail

<h3>The body of your message: {{ $information['subject'] }}</h3>

<h2><b>Name:</b> {{ $information['first_name'] }} {{$information['last_name']}}</h2>

<p>
    <b>Content:</b> <br>
    {{$information['your_message']}}

</p>

Thanks,<br>
{{ config('app.name') }}

@endcomponent

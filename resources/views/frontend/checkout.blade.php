@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">
        <div class="container">

            <nav class="woocommerce-breadcrumb"><a href="{{route('index')}}">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Checkout</nav>

            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <article class="page type-page status-publish hentry">
                        <header class="entry-header"><h1 itemprop="name" class="entry-title">Checkout</h1></header><!-- .entry-header -->

                        <form id="checkout_form" enctype="multipart/form-data" action="{{route('checkout.store')}}" class="checkout woocommerce-checkout" method="post" name="checkout">
                            @csrf
                            <div id="customer_details" class="col2-set">
                                <div class="col-1">
                                    <div class="woocommerce-billing-fields">

                                        <h3>Billing Details</h3>

                                        <p id="phone_field" class="form-row form-row form-row-last validate-required validate-phone">
                                            <label class="" for="phone">Phone <abbr title="required" class="required">*</abbr></label>
                                            <input type="text" value="{{$customer->phone}}" placeholder="Your Phone Number" id="phone" name="phone" class="input-text ">
                                        </p>
                                        <div class="clear"></div>

                                        <p id="delivery_address_field" class="form-row form-row form-row-wide address-field validate-required">
                                            <label class="" for="delivery_address">Address <abbr title="required" class="required">*</abbr></label>
                                            <input type="text" value="{{$customer->address}}" placeholder="Street address, District Name" id="delivery_address" name="delivery_address" class="input-text ">
                                        </p>
                                        <div class="clear"></div>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <h3>Shipping Details</h3>
                                    <div class="woocommerce-shipping-fields">
                                        {{-- <h3 id="ship-to-different-address">
                                            <label class="checkbox" for="ship-to-different-address-checkbox">Ship to a different address?</label>
                                            <input type="checkbox" value="1" name="ship_to_different_address" class="input-checkbox" id="ship-to-different-address-checkbox">
                                        </h3> --}}
                                 
                                        <p id="order_notes_field" class="form-row form-row notes">
                                            <label class="" for="order_notes">Order Notes</label>
                                            <textarea cols="5" rows="2" placeholder="Notes about your order, e.g. special notes for delivery." id="order_notes" class="input-text " name="order_notes">
                                            </textarea>
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <h3 id="order_review_heading">Your order</h3>

                            <div class="woocommerce-checkout-review-order" id="order_review">
                                <table class="shop_table woocommerce-checkout-review-order-table">
                                    <thead>
                                        <tr>
                                            <th class="product-name">Product</th>
                                            <th class="product-total">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cartProducts as $cart)
                                        <tr class="cart_item">
                                            <td class="product-name">
                                                {{ $cart->product_name }}
                                                <strong class="product-quantity">× {{ $cart->quantity }}</strong>
                                            </td>
                                            <td class="product-total">
                                                <span class="amount">Rs. <?php echo rupee_format($cart->quantity * $cart->price) ?></span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>

                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td><span class="amount">Rs. {{ rupee_format($totalPrice) }}</span></td>
                                        </tr>

                                        <tr class="shipping">
                                            <th>Shipping</th>
                                            <td data-title="Shipping">Free shipping inside Ringroad
                                            </td>
                                        </tr>

                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td><strong><span class="amount">Rs. {{ rupee_format($totalPrice) }}</span></strong> </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <div class="woocommerce-checkout-payment" id="payment">
                                    <ul class="wc_payment_methods payment_methods methods">
                                        <li class="wc_payment_method payment_method_cod">
                                            <input type="radio" checked data-order_button_text="" value="cash-on-delivery" name="payment_method" class="input-radio" id="payment_method_cod">

                                            <label for="payment_method_cod">Cash on Delivery</label>
                                            <div style="display:none;" class="payment_box payment_method_cod">
                                                <p>Pay with cash upon delivery.</p>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="form-row place-order">

                                        <p class="form-row terms wc-terms-and-conditions">
                                            <input type="checkbox" id="terms" name="terms" value="1" required class="input-checkbox">
                                            <label class="checkbox" for="terms">I’ve read and accept the <a target="_blank" href="{{ route('tac') }}">terms &amp; conditions</a> <span class="required">*</span></label>
                                            <input type="hidden" value="1" name="terms-field">
                                        </p>

                                        <input type="submit" id="submit" name="submit" data-value="Place order" value="Place order" class="button alt">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </article>
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->

    @include('frontend.includes.brands')

@endsection


@section('scripts')
    <script>
        jQuery( document ).ready(function( $ ) {
            $(document).ready(function () {
                $('#checkout_form').validate({
                    rules: {
                        phone: {
                            required: true
                        },
                        delivery_address: {
                            required: true
                        }
                    },
                    messages: {
                        phone: {
                            required: "Please enter your phone number"
                        },
                        delivery_address: {
                            required: "Please enter your new password"
                        }
                    }
                });
            });
        });
    </script>
@endsection
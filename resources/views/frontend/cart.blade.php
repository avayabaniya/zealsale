@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div class="box">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;">
                        @if(Session::has('flash_message_error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                        @endif
                        @if(Session::has('flash_message_success'))

                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <nav class="woocommerce-breadcrumb"><a href="{{route('index')}}">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>

            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <article class="page type-page status-publish hentry">
                        <header class="entry-header"><h1 itemprop="name" class="entry-title">Cart</h1></header><!-- .entry-header -->

                            

                            <table class="shop _table shop_ table_responsive ca rt" style="display: block! important" >
                                <thead>
                                    <tr>

                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name"><b>Product</b></th>
                                        <th class="product-name"><b>Size</b></th>
                                        <th class="product-name"><b>Discount Code</b></th>
                                        <th class="product-price"><b>Price</b></th>
                                        <th class="product-quantity"><b>Quantity</b></th>
                                        <th class="product-subtotal"><b>Total</b></th>
                                        <th class="product-remove">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $total_amount = 0; ?>
                                    @foreach($cartProducts as $product)
                                        <tr class="cart_item">

                                        <td class="product-thumbnail">
                                            <a href=""><img width="180" height="180" src="{{ asset('public/images/backend_images/product/small/'.$product->product->image) }}" alt=""></a>
                                        </td>

                                        <td data-title="Product" class="product-name">
                                            <a href="">{{ $product->product_name }}</a>
                                        </td>

                                            <td data-title="Size" class="product-size">
                                                <a href="">{{ $product->size }}</a>
                                            </td>

                                            <td data-title="Size" class="product-size">

                                                <?php if (!empty($product->discount)){ Session::put('CouponAmount', $product->discount); ?>

                                                    <b><p id="coupon-discount{{$product->id}}">Rs. {{$product->discount}}</p></b>
                                                    <p>Discount Applied</p>
                                                <?php }else{ ?>
                                                    <div class="coupon" style="width: 250px;">
                                                        <form id="apply_coupon" action="{{ route('usercoupon.apply') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="cart_id" value="{{ $product->id }}">
                                                            <input type="text" placeholder="Coupon Code"  id="coupon_code" class="input-text" name="coupon_code" style="display: inline-block; width: 150px; height: 46px;">
                                                            <input style="display: inline-block;" type="submit" value="Apply" name="apply_coupon" class="button">
                                                        </form>
                                                    </div>
                                                <?php }?>
                                            </td>                               


                                        <td data-title="Price" class="product-price">
                                            <span class="amount">Rs. {{ rupee_format($product->price) }}</span>
                                        </td>

                                        <td data-title="Quantity" class="product-quantity">
                                            <div class="quantity buttons_added">
                                                <input type="button" class="minus" value="-" onclick="minusQuantity(this)" id="{{$product->id}}">
                                                <label>Quantity:</label>
                                                <input type="number" size="4" class="input-text qty text quantity{{$product->id}}" title="Qty" value="{{$product->quantity}}" name="quantity" max="29" min="1" step="1" readonly>
                                                <input type="button" class="plus" value="+" onclick="plusQuantity(this)" id="{{$product->id}}">
                                            </div>
                                        </td>

                                        <td data-title="Total" class="product-subtotal">
                                            <span class="amount total_amount{{$product->id}}">

                                                 Rs.<?php
                                                if (Session::get('CartId') == $product->id)
                                                {
                                                    echo  ($product -> price*$product -> quantity) - Session::get('CouponAmount');
                                                    //$total_discount = $total_discount + Session::get('CouponAmount');
                                                }else{
                                                    echo rupee_format(($product -> price*$product->quantity) - $product->discount);

                                                } ?>

                                            </span>
                                        </td>

                                            <td class="product-remove">
                                                <a class="remove" href="{{route('cart.delete', $product->id)}}">×</a>
                                            </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="actions" colspan="6">

                                            <input type="submit" value="Update Cart" name="update_cart" class="button">

                                            <br><br>
                                            <div class="wc-proceed-to-checkout">
                                                <a class="checkout-button button alt wc-forward" href="{{ route('checkout') }}">&nbsp;Proceed to Checkout&nbsp;&nbsp;</a>
                                            </div>

                                            <input type="hidden" value="1eafc42c5e" name="_wpnonce"><input type="hidden" value="/electro/cart/" name="_wp_http_referer">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        <div class="cart-collaterals">

                            <div class="cart_totals ">

                                <h2>Cart Totals</h2>

                                <table class="shop_table shop_table_responsive">

                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>Subtotal</th>
                                            <td data-title="Subtotal"><span id="subTotalAmount" class="amount">Rs. {{ rupee_format($totalPrice) }}</span></td>
                                        </tr>

                                        <tr class="order-total">
                                            <th>Total</th>
                                            <td data-title="Total"><strong><span class="amount totalAmount">Rs. {{ rupee_format($totalPrice) }}</span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="wc-proceed-to-checkout">

                                    <a class="checkout-button button alt wc-forward" href="{{ route('checkout') }}">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->

    @include('frontend.includes.brands')

@endsection

        @section('styles')
            <style>
                input#coupon_code.input-text{
                    padding-left: 18px;
                    font-size: 0.95em;
                }
            </style>
        @endsection


@section('scripts')
    <script>
        function minusQuantity(value) {

            jQuery( document ).ready(function( $ ) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: 'cart/update-quantity/'+value.id+'/-1',
                    data:{
                        cartId:value.id,
                        quantityChange:value.value
                    },
                    success:function (resp) {
                        console.log(resp);
                        if (resp != 0) {
                            var quantity_class = '.quantity'+value.id;
                            var total_id = '.total_amount'+value.id;


                            $(quantity_class).val(resp[0].quantity);
                            $(quantity_class).html(resp[0].quantity + '× <span class="amount">'+ resp[0].price+'</span>');
                            var totalQuantity = parseFloat(resp[0].quantity) * parseFloat(resp[0].price) - parseFloat(resp[0].discount);

                            $(total_id).html("Rs." + totalQuantity);

                            var discount_applied_id = '#coupon-discount' + value.id;
                            $(discount_applied_id).html("Rs. " + resp[0].discount);

                            $('#subTotalAmount').html("Rs." + resp[1]);
                            $('.totalAmount').html("Rs." + resp[1]);
                        }

                    }, error:function (resp) {
                        console.log(resp);
                        alert("Error");
                    }
                });
            });
        }


        function plusQuantity(value) {
            jQuery( document ).ready(function( $ ) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: 'cart/update-quantity/'+value.id+'/+1',
                    data:{
                        cartId:value.id,
                        quantityChange:value.value
                    },
                    success:function (resp) {
                        console.log(resp);
                        if (resp != 0) {
                            var quantity_class = '.quantity' + value.id;
                            $(quantity_class).html(resp[0].quantity + '× <span class="amount">'+ resp[0].price+'</span>');
                            var total_id = '.total_amount' + value.id;

                            $(quantity_class).val(resp[0].quantity);
                            var totalQuantity = parseFloat(resp[0].quantity) * parseFloat(resp[0].price) - parseFloat(resp[0].discount);

                            $(total_id).html("Rs." + totalQuantity);

                            var discount_applied_id = '#coupon-discount' + value.id;
                            $(discount_applied_id).html("Rs. " + resp[0].discount);

                            $('#subTotalAmount').html("Rs." + resp[1]);
                            $('.totalAmount').html("Rs." + resp[1]);
                        }

                    }, error:function (resp) {
                        console.log(resp);
                        alert("Error");
                    }
                });
            });
        }

    </script>


   



@endsection
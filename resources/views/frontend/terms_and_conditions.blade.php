@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')


<div id="content" class="site-content" tabindex="-1">
    <div class="container">

        <nav class="woocommerce-breadcrumb" >
            <a href="{{route('index')}}">Home</a>
            <span class="delimiter"><i class="fa fa-angle-right"></i></span>
            Terms and Conditions
        </nav><!-- .woocommerce-breadcrumb -->


        <div id="primary" class="content-area">
            <main id="main" class="site-main">


                <article id="post-2183" class="hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Terms and Conditions</h1>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        @foreach($terms as $terms_data)
                        <section class="section inner-bottom-xs">
                            <h2>{{$terms_data->title }}</h2>
                            <p>{!! $terms_data->description !!}</p>
                        </section><!-- .section -->
                        @endforeach

                        <section class="section intellectual-property">
                            <h2>Contact Us</h2>
                            <p>If you have any questions about this Agreement, please contact us filling this <a href="{{route('contact.us')}}"><strong>contact form</strong></a></p>
                        </section><!-- .section -->

                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->


    </div><!-- .col-full -->
</div><!-- #content -->

@include('frontend.includes.brands')

@endsection
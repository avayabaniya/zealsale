@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')


<div id="content" class="site-content" tabindex="-1">
    <div class="container">
        <div class="box">
            <div class="row">
                <div class="col-md-12" style="margin-top: 10px;">
                    @if(Session::has('flash_message_error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_error') !!} </strong>
                        </div>
                    @endif
                    @if(Session::has('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                    @endif
                </div>
            </div>
        <nav class="woocommerce-breadcrumb" ><a href="{{route('index')}}">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>My Account</nav><!-- .woocommerce-breadcrumb -->

        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <article id="post-8" class="hentry">

                    <div class="entry-content">
                        <div class="woocommerce">
                            <div class="customer-login-form">
                                <span class="or-text">or</span>

                                <div class="col2-set" id="customer_login">

                                    <div class="col-1">


                                        <h2>Login</h2>

                                        <form method="post" class="login" action="{{ route('customer.login')}}" enctype="multipart/form-data">
                                            @csrf
                                            <p class="before-login-text">Welcome back! Sign in to your account</p>

                                            <p class="form-row form-row-wide">
                                                <label for="email">Email address<span class="required">*</span></label>
                                                <input type="text" class="input-text" name="email" id="email" value="" />
                                            </p>

                                            <p class="form-row form-row-wide">
                                                <label for="password">Password<span class="required">*</span></label>
                                                <input class="input-text" type="password" name="password" id="password" />
                                            </p>

                                            <p class="form-row">
                                                <input class="button" type="submit" value="Login" name="login">
                                                
                                                <label for="rememberme" class="inline"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember me</label>
                                            </p>

                                            <p class="lost_password"><a href="{{route('customer.password.request')}}">Lost your password?</a></p>

                                            <button class="loginBtn loginBtn--facebook"><a href="login/facebook" > Login with Facebook  </a></button>
                                            
                                              
                                              <button class="loginBtn loginBtn--google"><a href="login/google" > Login with Google  </a></button>
                                               
                                        </form>

                                        

                                    </div><!-- .col-1 -->

                                    <div class="col-2">

                                        <h2>Register</h2>

                                        <form method="post" class="register" action="{{ route('customer.store') }}" enctype="multipart/form-data">
                                            @csrf

                                            <p class="before-register-text">Create your very own account</p>

                                            <p class="form-row form-row-wide">
                                                <label for="full_name">Full Name<span class="required">*</span></label>
                                                <input type="full_name" class="input-text" name="full_name" id="full_name" value="{{old('full_name')}}" />
                                                <strong style="color: red; margin-bottom: 0px;">{{ $errors -> first('full_name') }}</strong>
                                            </p>

                                            <p class="form-row form-row-wide">
                                                <label for="email">Email address<span class="required">*</span></label>
                                                <input type="email" class="input-text" name="email" id="email" value="{{old('email')}}" />
                                                <strong style="color: red; margin-bottom: 0px;">{{ $errors -> first('email') }}</strong>
                                            </p>
                                            
                                            <p class="form-row form-row-wide">
                                                <label for="password">Password<span class="required">*</span></label>
                                                <input type="password" class="input-text" name="password" id="password" value="" />
                                                <strong style="color: red; margin-bottom: 0px;">{{ $errors -> first('password') }}</strong>
                                            </p>

                                            <p class="form-row"><input type="submit" class="button" name="register" value="Register"></button></p>

                                            <div class="register-benefits">
                                                <h3>Sign up today and you will be able to :</h3>
                                                <ul>
                                                    <li>Speed your way through checkout</li>
                                                    <li>Track your orders easily</li>
                                                    <li>Keep a record of all your purchases</li>
                                                </ul>
                                            </div>

                                        </form>

                                    </div><!-- .col-2 -->

                                </div><!-- .col2-set -->

                            </div><!-- /.customer-login-form -->
                        </div><!-- .woocommerce -->
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .col-full -->
</div><!-- #content -->

@include('frontend.includes.brands')

<style>
        body { padding: 2em; }

        a {
            color: white;

        }
        /* Shared */
        .loginBtn {
          box-sizing: border-box;
          position: relative;
          /* width: 13em;  - apply for fixed size */
          margin: 0.2em;
          padding: 0 15px 0 46px;
          border: none;
          text-align: left;
          line-height: 34px;
          white-space: nowrap;
          border-radius: 0.2em;
          font-size: 16px;
          color: #FFF;
        }
        .loginBtn:before {
          content: "";
          box-sizing: border-box;
          position: absolute;
          top: 0;
          left: 0;
          width: 34px;
          height: 100%;
        }
        .loginBtn:focus {
          outline: none;
        }
        .loginBtn:active {
          box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
        }
        
        
        /* Facebook */
        .loginBtn--facebook {
          background-color: #4C69BA;
          background-image: linear-gradient(#4C69BA, #3B55A0);
          {{-- font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif; --}}
          text-shadow: 0 -1px 0 #354C8C;
        }
        .loginBtn--facebook:before {
          border-right: #364e92 1px solid;
          background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
        }
        .loginBtn--facebook:hover,
        .loginBtn--facebook:focus {
          background-color: #5B7BD5;
          background-image: linear-gradient(#5B7BD5, #4864B1);
        }
        
        
        /* Google */
        .loginBtn--google {
          font-family: "Roboto", Roboto, arial, sans-serif;
          background: #DD4B39;
        }
        .loginBtn--google:before {
          border-right: #BB3F30 1px solid;
          background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
        }
        .loginBtn--google:hover,
        .loginBtn--google:focus {
          background: #E74B37;
        }

</style>

@endsection


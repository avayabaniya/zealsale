@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

    {{-- @include('frontend.account.sidebar') --}}

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">

            <div class="row">
                <div class="col-md-12" style="margin-top: 10px;">
                    @if(Session::has('flash_message_error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_error') !!} </strong>
                        </div>
                    @endif
                    @if(Session::has('flash_message_success'))

                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                    @endif
                </div>
            </div>

            <section id="heading">
                <div class="container">
                    <div class="dash-map">
                        <small class="dash-location text-muted"><a href="{{ route('index') }}" style="color:black;"> Home </a><i class="fa fa-chevron-right"></i>My Account <i class="fa fa-chevron-right"></i> <span class="dashboard-location">Dashboard</span></small>
                    </div>
                    <div class="title">
                        <h4>My Account </h4>
                    </div>
                </div>
            </section>
            <section id="dashboard">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div id="dashboard-navigation">
                                <div class="dash-navigation">
                                    <ul>
                                        <li class="dashBoard dash-active"><span>Dashboard<i class="fa fa-tachometer"></i></span></li>
                                        <li class="orders">Orders<i class="fa fa-shopping-basket"></i>
                                        </li>
                                        <li class="payment">Payment methods<i class="fa fa-credit-card-alt"></i></li>
                                        <li class="details">Account Details<i class="fa fa-user"></i></li>
                                        <li class="logout"><a href="{{ route('customer.logout') }}" style="color:black"> Logout<i class="fa fa-sign-out"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="dashboard-content">
                                <!---------------------_DashBoard content--------------------->
                                <div class="dashboard-content-cont">
                                    <p>Hello <strong>{{ Session::get('Email') }}</strong> (not <strong>{{ Session::get('Email') }} &nbsp;?</strong> <a href="{{ route('customer.logout') }}">Log out</a>)</p>
                                    <p>From your account dashboard you can view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a>, and <a href="#">edit your password and account details</a>.</p>
                                </div>
                                <!---------------------_Orders--------------------->
                                <table class="cart table table-striped">
                                    <thead>
                                    <tr class="table-active">
                                        <th scope="col">Product</th>
                                        {{-- <th scope="col">Price</th> --}}
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Cancel/Reorder</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody  class="cart-body">
                                    @foreach($orders as $order_lists)
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4 product-image">
                                                        <img src="{{ asset('public/images/backend_images/product/small/'.$order_lists->product->image) }}" alt="{{ $order_lists->product->product_name }}">
                                                    </div>
                                                    <div class="col-md-6 offset-md-2 product-desc">
                                                        <p>{{ $order_lists->product->product_name }}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p>{{ $order_lists->quantity }}</p></td>
                                            <td><p>Rs.&nbsp;{{ $order_lists->order_price }}</p></td>
                                            <td><p>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    @if($order_lists->order_status == 'pending')
                                                        <a class="customerCancelOrder text-danger" rel="{{$order_lists->id}} " title="Cancel Order" href="javascript:" style="color: black;">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @elseif($order_lists->order_status == 'cancelled')
                                                        <a title="Re-Order this product" href="{{route('product.single.details',$order_lists->product->slug )}}" style="color: black;"><i class="fa fa-refresh"></i></a>
                                                    @endif
                                                </p>
                                            </td>
                                            <td class="status"><p>
                                                    @if($order_lists->order_status === 'pending')
                                                        <span class="label label-warning" >
                                        Pending
                                        </span>
                                                    @elseif($order_lists->order_status === 'on-way')
                                                        <span class="label label-info">
                                        On way
                                        </span>
                                                    @elseif($order_lists->order_status === 'completed')
                                                        <span class="label label-success">
                                        Completed
                                        </span>
                                                    @elseif($order_lists->order_status === 'cancelled')
                                                        <span class="label label-danger">
                                        Cancelled
                                        </span>
                                                    @endif
                                                </p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!---------------------_Payment Methods--------------------->
                            <div class="payment-methods methods1" data-infoClass="info1">
                                <div class="container-fluid payment-content">
                                    <i class="fa fa-truck"></i><h5>Cash On Delivery</h5>
                                </div>
                                <div class="additional-info info1">
                                    <p>Pay Cash On <span>Delivery</span></p>
                                </div>
                            </div>
                            <!---Next-->
                            <div class="payment-methods methods2" data-infoClass="info2">
                                <div class="container-fluid payment-content">
                                    <i class="fa fa-credit-card-alt"></i><h5>Account Payment</h5>
                                </div>
                                <div class="additional-info info2">
                                    <div class="info-title">
                                        <h5>Bank Details</h5>
                                    </div>
                                    <ol class="bank-name">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <li><strong>Prabhu Bank</strong>
                                                    <ul class="bank-details bank-list">
                                                        <li><small class="text-muted">Account Number: </small>02000101000228000001</li>
                                                        <li><small class="text-muted">Account Name: </small>Online Zeal Pvt. Ltd.</li>
                                                        <li><small class="text-muted">Branch: </small>Koteshwor, Kathmandu</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>Jyoti Bikash Bank</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Account Number: </small>00100100148901000001</li>
                                                        <li><small class="text-muted">Account Name: </small>Online Zeal Pvt. Ltd.</li>
                                                        <li><small class="text-muted">Branch: </small>Kamladhi, Kathmandu</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>Laxmi Bank</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Account Number: </small>01224006900</li>
                                                        <li><small class="text-muted">Account Name: </small>Ritendra Kumar Das</li>
                                                        <li><small class="text-muted">Branch: </small>New Baneshwor, Kathmandu</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>Siddhratha Bank</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Account Number: </small>00615076224</li>
                                                        <li><small class="text-muted">Account Name: </small>Mahendra Kumar Das</li>
                                                        <li><small class="text-muted">Branch: </small>Teku, Kathmandu</li>
                                                    </ul>
                                                </li>
                                            </div>
                                        </div>
                                    </ol>
                                </div>
                            </div>
                            <!--Again-->
                            <div class="payment-methods methods2" data-infoClass="info3">
                                <div class="container-fluid payment-content">
                                    <i class="fa fa-money"></i><h5>Non Account Payment</h5>
                                </div>
                                <div class="additional-info info3">
                                    <div class="info-title">
                                        <h5>Payment Details</h5>
                                    </div>
                                    <ol class="bank-name">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <li><strong>E-Sewa</strong>
                                                    <ul class="bank-details bank-list">
                                                        <li><small class="text-muted">E-Sewa ID: </small>9801079604</li>
                                                        <li><small class="text-muted">Name: </small>Ritendra Kumar Das</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>Khalti</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Khalti ID: </small>9801079604</li>
                                                        <li><small class="text-muted">Name: </small>Ritendra Kumar Das</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>IME</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Receiver's Name: </small>Ramesh Tharu</li>
                                                        <li><small class="text-muted">Mobile Number: </small>9801079604</li>
                                                        <li><small class="text-muted">Address: </small>Kathmandu, Nepal</li>
                                                    </ul>
                                                </li>
                                            </div>
                                            <div class="col-md-6 bank-list">
                                                <li><strong>Citi Express</strong>
                                                    <ul class="bank-details">
                                                        <li><small class="text-muted">Receiver's Name: </small>Ramesh Tharu</li>
                                                        <li><small class="text-muted">Mobile Number: </small>9801079604</li>
                                                        <li><small class="text-muted">Address: </small>Kathmandu, Nepal</li>
                                                    </ul>
                                                </li>
                                            </div>
                                        </div>
                                    </ol>
                                </div>
                            </div>
                            <!---------------------_Account Details--------------------->
                            <div class="details-content">
                                <form method="post" action="{{route('customer.update', $customer->id)}}" id="customer_dashboard" >
                                    @csrf
                                    <div class="details-content-title">
                                        <h2>General</h2>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstName4">Full Name *</label>
                                            <input type="text" value="{{$customer->full_name}}" class="form-control" id="inputFirstName4" name="full_name" placeholder="First Name">
                                            <p style="color: red; margin-bottom: 0px;" id="first_name_val"></p>
                                            <p style="color: red; margin-bottom: 0px;" id="email_val">{{ $errors -> first('full_name') }}</p>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Email Address *</label>
                                        <input type="email" value="{{$customer->email}}" class="form-control" id="inputEmail4" name="email" placeholder="Your Email Address">
                                        <p style="color: red; margin-bottom: 0px;" id="email_val">{{ $errors -> first('email') }}</p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone">Phone *</label>
                                        <input type="text" value="{{$customer->phone}}" class="form-control" id="phone" name="phone" placeholder="Your Mobile Number,Telephone">
                                        <p style="color: red; margin-bottom: 0px;" id="phone">{{ $errors -> first('phone') }}</p>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputDisplayName">Address *</label>
                                        <input type="text" value="{{$customer->address}}" name="address" class="form-control" id="inputDisplayName" placeholder="Street Name, District">
                                        <p style="color: red; margin-bottom: 0px;" id="address_val"></p>
                                    </div>
                                    <button type="submit" id="update" class="detail-submit btn btn-info">Save Changes</button>
                                </form>
                                <br>
                                <form method="post" action="{{route('customer.update', $customer->id)}}" id="cutomer_password" >
                                    @csrf
                                    <div class="details-content-title">
                                        <h2>Password</h2>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="oldPassword">Current password (leave blank to leave unchanged)</label>
                                        <input type="password" name="oldPassword" class="form-control" id="oldPassword" placeholder="">
                                        <b><p id="correct_pwd" style="color: green;"></p></b>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="newPassword">New password (leave blank to leave unchanged)</label>
                                        <input type="password" name="newPassword" class="form-control" id="newPassword" placeholder="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="confirmPassword">Confirm new password</label>
                                        <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="">
                                    </div>
                                    <button type="submit" id="update" class="detail-submit btn btn-info">Update Password</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



        </div><!-- .col-full -->
    </div><!-- #content -->

    @include('frontend.includes.brands')

@endsection

@section('styles')

    <link rel="stylesheet" href="{{ asset('public/frontend/dashboard/css/style.css') }}" type="text/css">
    {{-- <link rel="stylesheet" href="{{ asset('public/frontend/dashboard/css/bootstrap.min.css') }}" type="text/css"> --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

@endsection

@section('scripts')

    {{-- <script src="{{ asset('public/frontend/dashboard/js/bootstrap.min.js') }}"></script> --}}
    <script src="{{ asset('public/frontend/dashboard/js/main.js') }}"></script>


    <script>

        $('#inputFirstName4').keyup(function(){
            var name = $('#inputFirstName4').val();

            if(name.length == 0){
                $('#first_name_val').text('** First Name is required').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else if (name.length > 0 && name.length < 2)
            {
                $('#first_name_val').text('** Minimum 2 characters').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else
            {
                $('#first_name_val').text('').css("color","green");
                $('#update').prop('disabled', false);
                $('#update').css('background-color', 'green');
            }
        });


        $('#inputLastName4').keyup(function(){
            var name = $('#inputLastName4').val();

            if(name.length == 0){
                $('#last_name_val').text('** Last Name is required').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else if (name.length > 0 && name.length < 2)
            {
                $('#last_name_val').text('** Minimum 2 characters').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else
            {
                $('#last_name_val').text('').css("color","green");
                $('#update').prop('disabled', false);
                $('#update').css('background-color', 'green');
            }
        });


        $('#inputEmail4').keyup(function(){
            var name = $('#inputEmail4').val();

            if(name.length == 0){
                $('#email_val').text('** Email is required').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else if (name.length > 0 && name.length < 6)
            {
                $('#email_val').text('** Minimum 6 characters').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else
            {
                $('#email_val').text('').css("color","green");
                $('#update').prop('disabled', false);
                $('#update').css('background-color', 'green');
            }
        });


        $('#inputDisplayName').keyup(function(){
            var name = $('#inputDisplayName').val();

            if(name.length == 0){
                $('#address_val').text('** Address is required').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else if (name.length > 0 && name.length < 10)
            {
                $('#address_val').text('** Minimum 10 characters').css("color","red");
                $('#update').prop('disabled', true);
                $('#update').css('background-color', 'red');
            }
            else
            {
                $('#address_val').text('').css("color","green");
                $('#update').prop('disabled', false);
                $('#update').css('background-color', 'green');
            }
        });


    </script>


    <script>
        $(document).ready(function () {
            $('#cutomer_password').validate({
                rules: {
                    oldPassword:{
                        required: true,
                    },
                    newPassword:{
                        required: true,
                        minlength:6,
                        maxlength:20
                    },
                    confirmPassword:{
                        required: true,
                        minlength:6,
                        maxlength:20,
                        equalTo:"#newPassword"
                    }
                },
                messages: {
                    oldPassword: {
                        required: "Your Current Password is required"
                    },
                    newPassword: {
                        required: "Please enter your new password"
                    },
                    confirmPassword: {
                        required: "Please confirm your new password",
                        equalTo: "This password should match your new password"
                    }
                }
            });
        });
    </script>


    {{--Check current password--}}
    <script>
        $("#oldPassword").keyup(function(){
            var current_pwd = $("#oldPassword").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'check-pwd',
                data:{current_pwd:current_pwd},
                success:function(resp){

                    if(resp=="false"){

                        $("#correct_pwd").text("Entered value does not match the current password").css("color","red");

                    }else if(resp=="true"){

                        $("#correct_pwd").text("Current password matched").css("color","green");
                    }
                },error:function(resp){

                }
            });
        });
    </script>
@endsection



@section('styles')
    <style>
        .error{
            color: red !important;
        }
    </style>
@endsection


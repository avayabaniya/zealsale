@extends('layouts.frontendLayout.design')

@include('frontend.includes.mainheader')

@include('frontend.includes.mainpagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">
    <div class="container">

        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                @include('frontend.includes.slider')

                <div class="home-v3-features-block animate-in-view fadeIn animated" data-animation="fadeIn">
                        <div class="features-list columns-5">
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-transport"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Free Delivery</strong>Inside Ringroad
                                    </div>
                                </div>
                            </div><!-- .feature -->

                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-customers"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>99% Positive</strong> Feedbacks
                                    </div>
                                </div>
                            </div><!-- .feature -->

                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-returning"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>2-3 days</strong> Free Exchange
                                    </div>
                                </div>
                            </div><!-- .feature -->

                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-payment"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Payment</strong> Secure System
                                    </div>
                                </div>
                            </div><!-- .feature -->

                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-tag"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Only Best</strong> Brands
                                    </div>
                                </div>
                            </div><!-- .feature -->
                        </div><!-- .features-list -->
                </div><!-- .home-v3-features-block -->

                <div class="home-v1-deals-and-tabs deals-and-tabs row animate-in-view fadeIn animated" data-animation="fadeIn">
                    @foreach($specialOffer as $spoff_data)
                    <div class="deals-block col-lg-4 col-md-6 col-sm-12">
                        <section class="section-onsale-product">
                            <header>
                                <h2 class="h1">Special Offer</h2>
                                <div class="savings">
                                    <span class="savings-text">Save <span class="amount"> {{$spoff_data->onsale}} %</span></span>
                                </div>
                            </header><!-- /header -->

                            <div class="onsale-products">
                                <span class="loop-product-categories"><a href="{{ route('product.subcategory', $spoff_data->category->slug) }}" rel="tag">{{$spoff_data->category->name}}</a></span>
                                <div class="onsale-product">
                                    <br>
                                    <a href="{{ route('product.single.details',$spoff_data->slug) }}">
                                        <div class="product-thumbnail">
                                            <img class="wp-post-image" title="{{$spoff_data->category->name}}" data-echo="{{ asset('public/images/backend_images/product/medium/'.$spoff_data->image) }}" src="{{ asset('public/images/backend_images/product/medium/'.$spoff_data->image) }}" alt=""></div>

                                            <h3><a href="{{ route('product.single.details',$spoff_data->slug) }}">{{ $spoff_data->product_name }}</a></h3>
                                    </a>

                                    <span class="price">
                                        <span class="electro-price">
                                            <ins><span class="amount">Rs. <?php echo $spoff_data->price - ($spoff_data->price * ($spoff_data->onsale/100)) ?></span></ins>
                                             <del><span class="amount">Rs. {{ rupee_format($spoff_data->price) }}</span></del>
                                        </span>
                                        <a rel="nofollow" href="{{ route('product.single.details', $spoff_data->slug) }}" class="button add_to_cart_button">Add to cart</a>                                                    
                                    </span><!-- /.price -->
                                    
                                    <div class="deal-progress">
                                        <div class="deal-stock">

                                                <?php $total_stock = 0; ?>
                                                @foreach($spoff_data->attributes as $att)
                                                        <?php $total_stock = $att->stock + $total_stock; ?>
                                                @endforeach
                                                    <span class="stock-available" style="float: left;">Available on stock: <strong><?php echo $total_stock; ?></strong></span>

                                        </div>
                                    </div><!-- /.deal-progress -->
                                    
                                    <div class="hover-area">
                                        <div class="action-buttons" >
                                            <input type="hidden" name="product_id" class="product_id" value="{{ $spoff_data->id }}">
                                            {{-- <button name="your_name" class="wishlistButton"onclick="wishlist(this)" id="{{$spoff_data->id}}">Wishlist</button> --}}
                                            <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$spoff_data->id}}">Wishlist</button>
                                        </div>
                                    </div>
                                </div><!-- /.onsale-product -->
                            </div><!-- /.onsale-products -->
                        </section><!-- /.section-onsale-product -->
                    </div><!-- /.col -->
                    @endforeach


                    <div class="tabs-block col-lg-8">
                        <div class="products-carousel-tabs">
                            <ul class="nav nav-inline">
                                <li class="nav-item"><a class="nav-link active" href="#tab-products-1" data-toggle="tab">Featured</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab-products-2" data-toggle="tab">On Sale</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-products-1" role="tabpanel">
                                    <div class="woocommerce columns-3">

                                        <ul class="products columns-3">
                                            <?php $count=1; ?>
                                            @foreach($productsFeatured as $product)
                                                <li class="product <?php if($count==1) {?> first <?php } elseif($count == 3) {?> last <?php } ?>" class="col-sm-6 col-md-6">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory', $product->category->slug) }}" rel="tag">{{$product->category->name}}</a></span>
                                                            <a href="{{ route('product.single.details', $product->slug) }}">
                                                                <h3>{{ $product->product_name }}</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}" style="width: 250px; height: 232px;" class="img-responsive" data-echo="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}">
                                                                </div>
                                                            </a>

                                                            <div class="price-add-to-cart" style="padding-top: 30px;">
                                                                <span class="price">
                                                                    @if($product->onsale == NULL)
                                                                        <span class="electro-price">
                                                                            <ins><span class="amount">Rs. {{ rupee_format($product->price) }}</span></ins>
                                                                            {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                                        </span>
                                                                    @endif
                                                                    @if($product->onsale != NULL)
                                                                        <span class="electro-price">
                                                                            <ins><span class="amount">Rs. <?php echo $product->price - ($product->price * ($product->onsale/100)) ?></span></ins>
                                                                            <del><span class="amount">Rs. {{ rupee_format($product->price) }}</span></del>
                                                                        </span>
                                                                    @endif
                                                                </span>
                                                                <a rel="nofollow" href="{{ route('product.single.details', $product->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                                                {{--<a rel="nofollow" href="{{ route('cart.add') }}" class="button add_to_cart_button">Add to cart</a>--}}
                                                            </div><!-- /.price-add-to-cart -->


                                                            <div class="hover-area">
                                                                <div class="action-buttons">
                                                                    <input type="hidden" name="product_id" class="product_id" value="{{ $product->id }}">
                                                                    <button name="your_name" class="wishlistButton"onclick="wishlist(this)" id="{{$product->id}}">Wishlist</button>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </li><!-- /.products -->
                                                    <?php $count++;
                                                    if ($count > 3){
                                                        $count = 1;
                                                    }
                                                    ?>
                                                @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab-products-2" role="tabpanel">
                                    <div class="woocommerce columns-3">
                                        <ul class="products columns-3">
                                                <?php $count=1; ?>
                                                @foreach($productOnSale as $product)
                                                    <li class="product <?php if($count==1) {?> first <?php } elseif($count == 3) {?> last <?php } ?>">
                                                        <div class="product-outer">
                                                            <div class="product-inner">
                                                                <span class="loop-product-categories"><a href="{{ route('product.subcategory', $product->category->slug) }}" rel="tag">{{$product->category->name}}</a></span>
                                                                <a href="{{ route('product.single.details', $product->slug) }}">
                                                                    <h3>{{ $product->product_name }}</h3>
                                                                    <div class="product-thumbnail">
                                                                        <img src="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}" style="width: 250px; height: 232px;" class="img-responsive" data-echo="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}">
                                                                    </div>
                                                                </a>

                                                                <div class="price-add-to-cart" style="padding-top: 30px;">                                                                                    
                                                                    
                                                                       <span class="price">
                                                                        <span class="electro-price">
                                                                             <ins><span class="amount">Rs. <?php echo $product->price - ($product->price * ($product->onsale/100)) ?></span></ins>
                                                                             <del><span class="amount">Rs. {{ rupee_format($product->price) }}</span></del>
                                                                        </span>
                                                                    </span><!-- /.price -->
                                                                    <a rel="nofollow" href="{{ route('product.single.details', $product->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                                                </div><!-- /.price-add-to-cart -->

                                                                <div class="hover-area">
                                                                        <div class="action-buttons" >
                                                                                <input type="hidden" name="product_id" class="product_id" value="{{ $product->id }}">
                                                                                <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$product->id}}">Wishlist</button>
                                                                        </div>
                                                                    </div>
                                                            </div><!-- /.product-inner -->
                                                        </div><!-- /.product-outer -->
                                                    </li><!-- /.products -->
                                                        <?php $count++;
                                                        if ($count > 3){
                                                            $count = 1;
                                                        }
                                                        ?>
                                                    @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><!-- /.tabs-block -->
                </div><!-- /.deals-and-tabs -->

                <!-- ============================================================= 2-1-2 Product Grid ============================================================= -->
                <section class="products-2-1-2 animate-in-view fadeIn animated" data-animation="fadeIn">
                    <h2 class="sr-only">Products Grid</h2>
                    <div class="container">

                        <ul class="nav nav-inline nav-justified">

                            @foreach($mainCategories as $cat)
                                <li class="nav-item"><a class="@if(($loop->index + 1) == 1) active @endif nav-link" href="{{ route('product.category.details',$cat->slug) }}">{{$cat->name}}</a></li>
                            @endforeach

                        </ul>

                        <div class="columns-2-1-2">
                            <ul class="products exclude-auto-height">
                                @foreach($firstCat as $firstCat_data)
                                <li class="product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.single.details',$firstCat_data->slug) }}" rel="tag">{{ $firstCat_data->category->name }}</a></span>
                                            <a href="{{ route('product.single.details',$firstCat_data->slug) }}">
                                                <h3>{{ $firstCat_data->product_name }}</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="{{ asset('public/images/backend_images/product/small/'.$firstCat_data->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$firstCat_data->image) }}" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <span class="price">
                                                    @if(empty($firstCat_data->onsale))
                                                    <span class="electro-price">
                                                        <ins><span class="amount">Rs. {{ rupee_format($firstCat_data->price) }}</span></ins>
                                                        {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                    </span>
                                                    @endif
                                                    @if($firstCat_data->onsale != NULL)
                                                    <span class="electro-price">
                                                        <ins><span class="amount">Rs. <?php echo $firstCat_data->price - ($firstCat_data->price * ($firstCat_data->onsale/100)) ?></span></ins>
                                                        <del><span class="amount">Rs. {{ rupee_format($firstCat_data->price) }}</span></del>
                                                    </span>
                                                    @endif
                                                </span>
                                                <a rel="nofollow" href="{{ route('product.single.details', $firstCat_data->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                    <div class="action-buttons">
                                                            <input type="hidden" id="success" name="product_id" class="product_id" value="{{ $firstCat_data->id }}">
                                                            <button name="your_name" class="wishlistButton"onclick="wishlist(this)" id="{{$firstCat_data->id}}">Wishlist</button>
                                                    </div>
                                                </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                @endforeach
                            </ul>

                            <ul class="products exclude-auto-height product-main-2-1-2">
                                @foreach($midCat as $midCat_data)
                                <li class="last product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory',$midCat_data->category->slug) }}" rel="tag">{{ $midCat_data->category->name }}</a></span>
                                            <a href="{{ route('product.single.details',$midCat_data->slug) }}">
                                                <h3>{{ $midCat_data->product_name }}</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" title="{{ $midCat_data->product_name }}" data-echo="{{ asset('public/images/backend_images/product/medium/'.$midCat_data->image) }}" src="{{ asset('public/images/backend_images/product/medium/'.$midCat_data->image) }}" alt="">
                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <span class="price">
                                                        @if(empty($midCat_data->onsale))
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. {{ rupee_format($midCat_data->price) }}</span></ins>
                                                            {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                        </span>
                                                        @endif
                                                        @if($midCat_data->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $midCat_data->price - ($midCat_data->price * ($midCat_data->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($midCat_data->price) }}</span></del>
                                                        </span>
                                                        @endif
                                                </span>
                                                <a rel="nofollow" href="{{ route('product.single.details',$midCat_data->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                    <div class="action-buttons">
                                                            <input type="hidden" name="product_id" class="product_id" value="{{ $midCat_data->id }}">
                                                            <button name="your_name" class="wishlistButton"onclick="wishlist(this)" id="{{$midCat_data->id}}">Wishlist</button>
                                                    </div>
                                                </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                @endforeach
                            </ul>

                            <ul class="products exclude-auto-height">
                                @foreach($lastCat as $lastCat_data)
                                <li class="product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory',$lastCat_data->category->slug) }}" rel="tag">{{ $lastCat_data->category->name }}</a></span>
                                            <a href="{{ route('product.single.details',$lastCat_data->slug) }}">
                                                <h3>{{ $lastCat_data->product_name }}</h3>
                                                <div class="product-thumbnail">

                                                    <img class="wp-post-image" title="{{ $lastCat_data->product_name }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$lastCat_data->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$lastCat_data->image) }}" alt="">


                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <span class="price">
                                                    
                                                        @if($lastCat_data->onsale == NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. {{ rupee_format($lastCat_data->price) }}</span></ins>
                                                            {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                        </span>
                                                        @endif
                                                        @if($lastCat_data->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $lastCat_data->price - ($lastCat_data->price * ($lastCat_data->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($lastCat_data->price) }}</span></del>
                                                        </span>
                                                        @endif
                                                </span>
                                                <a rel="nofollow" href="{{ route('product.single.details',$lastCat_data->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                    <div class="action-buttons">
                                                            <input type="hidden" name="product_id" class="product_id" value="{{ $lastCat_data->id }}">
                                                    <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$lastCat_data->id}}">Wishlist</button>
                                                    </div>
                                                </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- ============================================================= 2-1-2 Product Grid : End============================================================= -->

                <section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">

                    <header>

                        <h2 class="h1">Best Sellers</h2>

                        <ul class="nav nav-inline">

                        </ul>
                    </header>

                    <div id="home-v1-product-cards-careousel">
                        <div class="woocommerce columns-3 home-v1-product-cards-carousel product-cards-carousel owl-carousel">

                            @foreach($bestSellerProducts->chunk(3) as $chunk)
                                <?php $count=1; ?>
                                <ul class="products columns-3">
                                    @foreach($chunk as $product)
                                        <li class="product product-card <?php if ($count == 1) { ?>first <?php }elseif ($count == 3) {?>last<?php } ?>">

                                            <div class="product-outer">
                                                <div class="media product-inner">

                                                    <a class="media-left" href="{{ route('product.single.details', $product->slug) }}" title="{{$product->product_name}}">
                                                        <img class="media-object wp-post-image img-responsive" src="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}" data-echo="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}" alt="">
                                                    </a>

                                                    <div class="media-body">
                                                        <span class="loop-product-categories">
                                                            <a href="{{ route('product.subcategory',$product->category->slug) }}" rel="tag">{{$product->category->name}}</a>
                                                        </span>

                                                        <a href="{{ route('product.single.details', $product->slug) }}">
                                                            <h3>{{$product->product_name}}</h3>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                       <span class="price">
                                                                     
                                                    @if($product->onsale == NULL)
                                                    <span class="electro-price">
                                                        <ins><span class="amount">Rs. {{ rupee_format($product->price) }}</span></ins>
                                                        {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                    </span>
                                                    @endif
                                                    @if($product->onsale != NULL)
                                                    <span class="electro-price">
                                                        <ins><span class="amount">Rs. <?php echo $product->price - ($product->price * ($product->onsale/100)) ?></span></ins>
                                                        <del><span class="amount">Rs. {{ rupee_format($product->price) }}</span></del>
                                                    </span>
                                                    @endif
                                                            </span>

                                                            <a href="{{ route('product.single.details', $product->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                                <div class="action-buttons" >
                                                                        <input type="hidden" name="product_id" class="product_id" value="{{ $product->id }}">
                                                                        <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$product->id}}">Wishlist</button>
                                                                </div>
                                                            </div>

                                                    </div>
                                                </div><!-- /.product-inner -->
                                            </div><!-- /.product-outer -->

                                        </li><!-- /.products -->
                                        <?php $count++;?>
                                    @endforeach
                                </ul>
                                @endforeach
                        </div>
                    </div><!-- #home-v1-product-cards-careousel -->
                </section>

                {{-- Banner not showing --}}
                @foreach($advertisements as $ads)
                <div class="home-v3-banner-block animate-in-view fadeIn animated" data-animation="fadeIn">
                    <div class="home-v3-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px">
                        <a href="{{ route('product.category.details',$ads->category->slug) }}"><img src="{{ asset('public/images/backend_images/setting/large/'.$ads->image) }}" data-echo="{{ asset('public/images/backend_images/setting/large/'.$ads->image) }}" class="img-responsive" alt=""></a>
                    </div>
                </div><!-- /.home-v1-banner-block -->
                @endforeach
                {{-- Banner not showing --}}


                <section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
                    <header>
                        <h2 class="h1">Recently Added</h2>
                        <div class="owl-nav">
                            <a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                            <a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </header>

                    <div id="recently-added-products-carousel">
                        <div class="woocommerce columns-6">
                            <div class="products owl-carousel recently-added-products products-carousel columns-6">

                                @foreach($latestProducts as $product)
                                    <div class="product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory',$product->category->slug) }}" rel="tag">{{$product->category->name}}</a></span>
                                            <a href="{{ route('product.single.details', $product->slug) }}">
                                                <h3>{{ $product->product_name }}</h3>
                                                <div class="product-thumbnail">
                                                    <img src="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}" style="width: 163px; height: 152px;" class="img-responsive" data-echo="{{ asset('public/images/backend_images/product/medium/'.$product->image) }}">
                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <span class="price">

                                                        @if($product->onsale == NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. {{ rupee_format($product->price) }}</span></ins>
                                                             {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                        </span>
                                                        @endif
                                                        @if($product->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $product->price - ($product->price * ($product->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($product->price) }}</span></del>
                                                        </span>
                                                        @endif
                                                </span>
                                                <a rel="nofollow" href="{{ route('product.single.details', $product->slug) }}
                                                        " class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                    <div class="action-buttons">
                                                        <input type="hidden" name="product_id"  class="product_id" value="{{ $product->id }}">
                                                        <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$product->id}}">Wishlist</button>
                                                    </div>
                                                </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </div><!-- /.products -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .container -->
</div><!-- #content -->


@include('frontend.includes.brands')

@endsection

@section('scripts')


    <script>
        jQuery( document ).ready(function( $ ) {
            $('.wishlistButton').click(function (event) {
                //alert($(this).val());
                var product_id=event.target.id;

                //alert(product_id);
                $.ajax({
                    headers:{
                        'X-CSRF-TOKEN' : $ ('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url:'product/single/wishlist/store',
                    data:{product_id:product_id},
                    success:function(resp){
                        //alert("success")
                        toastr.success('Product has been added to Wishlist');

                    },error:function(resp){
                        alert("error");

                    }
                });
            });
        });
    </script>

@endsection

@section('styles')

<style>

a:hover{
    cursor:pointer;
}

.btn-wishlist{
    border:none;
    outline:none;
    background:none;
    cursor:pointer;
    color:grey;
    padding:0;
    font-family:inherit;
    font-size:inherit;
    text-decoration: none;
}

.btn-wishlist:hover{
    background-color: white;
    color: black;
}

</style>

@endsection
@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')


<div id="content" class="site-content" tabindex="-1">
    <div class="container">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <article class="has-post-thumbnail hentry">
                    <header class="entry-header header-with-cover-image" style="background-image: url({{asset('public/images/backend_images/about/large/'.$about->banner_image)}});">
                        <div class="caption">
                            <h1 class="entry-title" itemprop="name">{{ $about->banner_image_title }}</h1>
                            <p class="entry-subtitle">{{ $about->banner_image_text }}</p>
                        </div>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <div class="row about-features inner-top-md inner-bottom-sm">
                            <div class="col-xs-12 col-md-4">
                                <figure class="wpb_wrapper vc_figure outer-bottom-xs">
                                    <div class="vc_single_image-wrapper">
                                        <img src="{{ asset('public/images/backend_images/about/medium/'.$about->about_first_image) }}" data-echo="{{ asset('public/images/backend_images/about/medium/'.$about->about_first_image) }}" class="img-responsive" alt="">
                                    </div>
                                </figure>


                                <div class="text-content">
                                    <h2 class="align-top">{{ $about->about_first_title }}</h2>
                                    <p>{!! $about->about_first_text !!}</p>
                                </div>
                            </div><!-- .col -->

                            <div class="col-xs-12 col-md-4">
                                <figure class="wpb_wrapper vc_figure outer-bottom-xs">
                                    <div class="vc_single_image-wrapper">
                                        <img src="{{ asset('public/images/backend_images/about/medium/'.$about->about_second_image) }}" data-echo="{{ asset('public/images/backend_images/about/medium/'.$about->about_second_image) }}" class="img-responsive" alt="">
                                    </div>
                                </figure>

                                <div class="text-content">
                                    <h2 class="align-top">{{ $about->about_second_title }}</h2>
                                    <p>{!! $about->about_second_text !!}</p>
                                </div>
                            </div><!-- .col -->

                            <div class="col-xs-12 col-md-4">
                                <figure class="wpb_wrapper vc_figure outer-bottom-xs">
                                    <div class="vc_single_image-wrapper">
                                        <img src="{{ asset('public/images/backend_images/about/medium/'.$about->about_third_image) }}" data-echo="{{ asset('public/images/backend_images/about/medium/'.$about->about_third_image) }}" class="img-responsive" alt="">
                                    </div>
                                </figure>

                                <div class="text-content">
                                    <h2 class="align-top">{{ $about->about_third_title }}</h2>
                                    <p>{!! $about->about_third_text !!}</p>
                                </div>
                            </div><!-- .col -->
                        </div><!-- .row -->

                    </div><!-- .entry-content -->

                </article><!-- #post-## -->
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .col-full -->
</div><!-- #content -->

@include('frontend.includes.brands')

@endsection
@extends('layouts.frontendLayout.design')

@include('frontend.includes.mainheader')

@include('frontend.includes.categorypagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
    
                    @include('frontend.includes.slider')
    
    
                    <div class="home-v3-features-block animate-in-view fadeIn animated" data-animation="fadeIn">
                        <div class="features-list columns-5">
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-transport"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Free Delivery</strong>Inside Ringroad
                                    </div>
                                </div>
                            </div><!-- .feature -->
    
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-customers"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>99% Positive</strong> Feedbacks
                                    </div>
                                </div>
                            </div><!-- .feature -->
    
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-returning"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>2-3 days</strong> Free Exchange
                                    </div>
                                </div>
                            </div><!-- .feature -->
    
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-payment"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Payment</strong> Secure System
                                    </div>
                                </div>
                            </div><!-- .feature -->
    
                            <div class="feature">
                                <div class="media">
                                    <div class="media-left media-middle feature-icon">
                                        <i class="ec ec-tag"></i>
                                    </div>
                                    <div class="media-body media-middle feature-text">
                                        <strong>Only Best</strong> Brands
                                    </div>
                                </div>
                            </div><!-- .feature -->
                        </div><!-- .features-list -->
                    </div><!-- .home-v3-features-block -->
    
                    {{-- Not Showing --}}
                    {{-- <div class="home-v1-ads-block animate-in-view fadeIn animated" data-animation=" animated fadeIn">
                        <div class="ads-block row">
                            <div class="ad col-xs-12 col-sm-6">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <img data-echo="{{ asset('public/frontend/assets/images/banner/cameras.jpg') }}" src="{{ asset('public/frontend/assets/images/blank.gif')}}" alt="">
                                    </div>
                                    <div class="media-body media-middle">
                                        <div class="ad-text">
                                            Catch Hottest <strong>Deals</strong> in Cameras Category
                                        </div>
                                        <div class="ad-action">
                                            <a href="#">Shop now</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col -->
    
                            <div class="ad col-xs-12 col-sm-6">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <img data-echo="{{ asset('public/frontend/assets/images/banner/DesktopPC.jpg') }}" src="{{ asset('public/frontend/assets/images/blank.gif')}}" alt="">
                                    </div>
                                    <div class="media-body media-middle">
                                        <div class="ad-text">
                                            Tablets, Smartphones <br><strong>and more</strong>
                                        </div>
                                        <div class="ad-action">
                                            <a href="#"><span class="from"><span class="prefix">From</span><span class="value"><sup>$</sup>749</span><span class="suffix">99</span></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.home-v3-ads-block --> --}}
                    {{-- Not Showing --}}
                    
    
                    <section class="products-carousel-tabs animate-in-view fadeIn animated" data-animation="fadeIn">
                        <h2 class="sr-only">Product Carousel Tabs</h2>
                        <ul class="nav nav-inline text-xs-left">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab-products-1" data-toggle="tab">Featured</a>
                            </li>
    
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-products-2" data-toggle="tab">On Sale</a>
                            </li>
    
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="#tab-products-3" data-toggle="tab">Top Rated</a>
                            </li> --}}
                        </ul><!-- /.nav -->
    
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-products-1" role="tabpanel">
                                <section class="section-products-carousel" >
                                    <div class="home-v3-owl-carousel-tabs">
                                        <div class="woocommerce columns-4">
    
                                            <div class="products owl-carousel home-v3-carousel-tabs products-carousel columns-4">
    
                                                @foreach($productsFeatured as $featuredProduct_data)
                                                <div class="product first">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.subcategory',$featuredProduct_data->category->slug)}}" rel="tag">{{ $featuredProduct_data->category->name }}</a></span>
                                                            <a href="{{route('product.single.details',$featuredProduct_data->slug)}}">
                                                                <h3>{{ $featuredProduct_data->product_name }}</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/images/backend_images/product/small/'.$featuredProduct_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$featuredProduct_data->image) }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    @if($featuredProduct_data->onsale == NULL)
                                                                        <span class="electro-price">
                                                                        <ins><span class="amount">Rs. {{rupee_format($featuredProduct_data->price) }}</span></ins>
                                                                            {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                                    </span>
                                                                    @endif
                                                                    @if($featuredProduct_data->onsale != NULL)
                                                                        <span class="electro-price">
                                                                        <ins><span class="amount">Rs. <?php echo $featuredProduct_data->price - ($featuredProduct_data->price * ($featuredProduct_data->onsale/100)) ?></span></ins>
                                                                        <del><span class="amount">Rs. {{ rupee_format($featuredProduct_data->price) }}</span></del>
                                                                    </span>
                                                                    @endif
                                                                </span>
                                                                <a rel="nofollow" href="{{route('cart.add')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                    <div class="action-buttons">
                                                                        <input type="hidden" name="product_id" class="product_id" value="{{ $featuredProduct_data->id }}">
                                                                    <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$featuredProduct_data->id}}">Wishlist</button>
                                                                    </div>
                                                                </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
                                                @endforeach
    
                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane -->
    
                            <div class="tab-pane" id="tab-products-2" role="tabpanel">
                                <section class="section-products-carousel">
                                    <div class="home-v3-owl-carousel-tabs">
                                        <div class="woocommerce columns-4">
    
                                            <div class="products owl-carousel home-v3-carousel-tabs products-carousel columns-4">
    
                                                @foreach($onsaleProduct as $onsaleProduct_data)
                                                <div class="product ">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.subcategory',$onsaleProduct_data->category->slug)}}" rel="tag">{{ $onsaleProduct_data->category->name}}</a></span>
                                                            <a href="{{route('product.single.details',$onsaleProduct_data->slug)}}">
                                                                <h3>{{ $onsaleProduct_data->product_name }}</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/images/backend_images/product/small/'.$onsaleProduct_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$onsaleProduct_data->image) }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    
                                                                        <span class="electro-price">
                                                                                <ins><span class="amount">Rs. <?php echo $onsaleProduct_data->price - ($onsaleProduct_data->price * ($onsaleProduct_data->onsale/100)) ?></span></ins>
                                                                                <del><span class="amount">Rs. {{ rupee_format($onsaleProduct_data->price) }}</span></del>
                                                                            </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('cart.add')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                    <div class="action-buttons">
                                                                            <input type="hidden" name="product_id" class="product_id" value="{{ $onsaleProduct_data->id }}">
                                                                    <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$onsaleProduct_data->id}}">Wishlist</button>
                                                                    </div>
                                                                </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
                                                @endforeach
    
                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane -->
    
                            {{-- <div class="tab-pane" id="tab-products-3" role="tabpanel">
                                <section class="section-products-carousel">
                                    <div class="home-v3-owl-carousel-tabs">
                                        <div class="woocommerce columns-4">
    
                                            <div class="products owl-carousel home-v3-carousel-tabs products-carousel columns-4">
    
                                                <div class="product first">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Audio Speakers</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>Wireless Audio System Multiroom 360</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/1.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/1.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $1,999.00</span></ins>
                                                                        <del><span class="amount">$2,299.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
    
    
                                                <div class="product ">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Laptops</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/2.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/2.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> </span></ins>
                                                                        <span class="amount"> $1,999.00</span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
    
    
                                                <div class="product ">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Headphones</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>Purple Solo 2 Wireless</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/3.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/3.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $1,999.00</span></ins>
                                                                        <del><span class="amount">$2,299.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
    
    
                                                <div class="product last">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Laptops</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>Tablet Red EliteBook  Revolve 810 G2</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/2.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/2.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> </span></ins>
                                                                        <span class="amount"> $1,999.00</span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
    
    
                                                <div class="product first">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Headphones</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>White Solo 2 Wireless</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/1.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/1.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $1,999.00</span></ins>
                                                                        <del><span class="amount">$2,299.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
    
    
                                                <div class="product ">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">Smartphones</a></span>
                                                            <a href="{{route('product.single')}}">
                                                                <h3>Smartphone 6S 32GB LTE</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{ asset('public/frontend/assets/images/4.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/4.jpg') }}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>
    
                                                            <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> </span></ins>
                                                                        <span class="amount"> $1,999.00</span>
                                                                    </span>
                                                                </span>
                                                                <a rel="nofollow" href="{{route('product.single')}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->
    
                                                            <div class="hover-area">
                                                                <div class="action-buttons">
    
                                                                    <a href="#" rel="nofollow" class="add_to_wishlist hover"> Wishlist</a>
    
                                                                    <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane --> --}}
                        </div><!-- /.tab-content -->
                    </section><!-- /.products-carousel-tabs -->
    
                    <section class="products-carousel-with-image animate-in-view fadeIn animated" style="background-size: cover; background-position: center center; background-image: url( assets/images/home-v3-bg.jpg );" data-animation="fadeIn">
                        <h2 class="sr-only">Products Carousel</h2>
                        <div class="container">
                            <div class="row">
    
                                @foreach($mainCategories as $mc_data)
                                <div class="image-block col-xs-12 col-sm-6">
                                    <img data-echo="{{ asset('public/images/backend_images/category/medium/'.$mc_data->featured_image) }}" src="{{ asset('public/images/backend_images/category/medium/'.$mc_data->featured_image) }}" alt="">
                                </div><!-- /.image-block -->
                                @endforeach
    
                                <div class="products-carousel-block col-xs-12 col-sm-6">
                                    <section class="home-v2-categories-products-carousel section-products-carousel" >
    
                                        @foreach($mainCategories as $mc_data)
                                        <header>
                                            <h2 class="h1">{{ $mc_data->name }}</h2>
                                            <div class="owl-nav">
                                                <a href="#products-carousel-prev" data-target="#products-carousel-with-umage" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                                                <a href="#products-carousel-next" data-target="#products-carousel-with-umage" class="slider-next"><i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </header>
                                        @endforeach
    
                                        <div id="products-carousel-with-umage">
                                            <div class="woocommerce columns-4">
    
                                                <div class="products owl-carousel home-v3-carousel-tabs products-carousel columns-4">
    
                                                    @foreach($mainCategories as $mc_data)
                                                    @foreach($subCategories as $sc_data)
                                                    @if($sc_data->parent_id == $mc_data->id)
                                                    @foreach($products as $pp_data)
                                                    @if($sc_data->id == $pp_data->category_id )
                                                    <div class="product ">
                                                        <div class="product-outer">
                                                            <div class="product-inner">
                                                                <span class="loop-product-categories"><a href="{{route('product.category')}}" rel="tag">{{ $pp_data->category->name }}</a></span>
                                                                <a href="{{route('product.single')}}">
                                                                    <h3>{{ $pp_data->product_name }}</h3>
                                                                    <div class="product-thumbnail">
                                                                        <img src="{{ asset('public/images/backend_images/product/small/'.$pp_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$pp_data->image) }}" class="img-responsive" alt="">
                                                                    </div>
                                                                </a>
    
                                                                <div class="price-add-to-cart">
                                                                    <span class="price">
                                                                        
                                                                    @if(empty($pp_data->onsale))
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount">Rs. {{ rupee_format($pp_data->price) }}</span></ins>
                                                                        {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                                    </span>
                                                                    @endif
                                                                    @if($pp_data->onsale != NULL)
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount">Rs. <?php echo $pp_data->price - ($pp_data->price * ($pp_data->onsale/100)) ?></span></ins>
                                                                        <del><span class="amount">Rs. {{ rupee_format($pp_data->price) }}</span></del>
                                                                    </span>
                                                                    @endif
                                                                    </span>
                                                                    <a rel="nofollow" href="{{route('cart.add')}}" class="button add_to_cart_button">Add to cart</a>
                                                                </div><!-- /.price-add-to-cart -->
    
                                                                <div class="hover-area">
                                                                        <div class="action-buttons">
                                                                            <input type="hidden" name="product_id" class="product_id" value="{{ $pp_data->id }}">
                                                                            <button name="your_name" class="wishlistButton"onclick="wishlist(this)" id="{{$pp_data->id}}">Wishlist</button>
                                                                        </div>
                                                                    </div>
                                                            </div><!-- /.product-inner -->
                                                        </div><!-- /.product-outer -->
                                                    </div><!-- /.product -->
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                    @endforeach
                                                    @endforeach
    
                                                </div><!-- /.products -->
                                            </div>
                                        </div>
                                    </section>
                                </div><!-- /.iproducts-carousel-block -->
                            </div><!-- /.row -->
                        </div><!-- /.conainer -->
                    </section><!-- /.products-carousel-with-image-->
    
                    <section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
                        @foreach($nextMainCategories as $nxmc)
                        <header>
                            <h2 class="h1">{{ $nxmc->name }}</h2>
    
                            <div class="owl-nav">
                                <a href="#products-cards-carousel-prev" data-target="#homev3-products-cards-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                                <a href="#products-cards-carousel-next" data-target="#homev3-products-cards-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
                            </div>
                        </header><!-- /header-->
                        @endforeach
    
    
                        <div id="homev3-products-cards-carousel">
                            <div class="woocommerce home-v3 columns-2 product-cards-carousel owl-carousel">
    
                                <ul class="products columns-2">
                                    <?php $count=1; ?>                                
                                    @foreach($nextMainCategories as $mc_data)
                                    @foreach($subCategories as $sc_data)
                                    @if($sc_data->parent_id == $mc_data->id)
                                    @foreach($products as $pp_data)
                                    @if($sc_data->id == $pp_data->category_id )
                                    <li class="product product-card <?php if($count==1) {?> first <?php } else {?> last <?php } ?>">
    
                                        <div class="product-outer">
                                            <div class="media product-inner">
    
                                                <a class="media-left" href="{{route('product.single.details',$pp_data->slug)}}" title="{{ $pp_data->product_name }}">
                                                    <img class="media-object wp-post-image img-responsive" src="{{ asset('public/images/backend_images/product/small/'.$pp_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$pp_data->image) }}" alt="">
    
                                                </a>
    
                                                <div class="media-body">
                                                    <span class="loop-product-categories">
                                                        <a href="{{route('product.subcategory',$pp_data->category->slug)}}" rel="tag">{{ $pp_data->category->name }}</a>
                                                    </span>
    
                                                    <a href="{{route('product.single.details',$pp_data->slug)}}">
                                                        <h3>{{ $pp_data->product_name }}</h3>
                                                    </a>
    
                                                    <div class="price-add-to-cart">
                                                        <span class="price">
                                                            
                                                                @if(empty($pp_data->onsale))
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">Rs. {{ rupee_format($pp_data->price) }}</span></ins>
                                                                    {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                                </span>
                                                                @endif
                                                                @if($pp_data->onsale != NULL)
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">Rs. <?php echo $pp_data->price - ($pp_data->price * ($pp_data->onsale/100)) ?></span></ins>
                                                                    <del><span class="amount">Rs. {{ rupee_format($pp_data->price) }}</span></del>
                                                                </span>
                                                                @endif
                                                        </span>
    
                                                        <a href="{{route('product.single.details',$pp_data->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->
    
                                                    <div class="hover-area">
                                                            <div class="action-buttons">
                                                                <input type="hidden" name="product_id" class="product_id" value="{{ $pp_data->id }}">
                                                                <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$pp_data->id}}">Wishlist</button>
                                                            </div>
                                                        </div>
    
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
    
                                    </li><!-- /.products -->
                                    <?php $count++;
                                        if ($count > 2){
                                        $count = 1;
                                        }
                                    ?>
                                    @endif
                                    @endforeach
                                    @endif
                                    @endforeach
                                    @endforeach
    
    
                                </ul>
    
                            </div>
                        </div><!-- /#homev3-products-cards-carousel-->
    
                    </section><!-- /.section-product-cards-carousel-->
    
                    <section class="products-6-1 animate-in-view fadeIn animated" data-animation="fadeIn">
                        <div class="container">
                            <header>
                                <h2 class="h1">Bestsellers</h2>
                                {{-- <ul class="nav nav-inline">
                                    <li class="nav-item active">
                                        <span class="nav-link">Top 7</span>
                                    </li>
    
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('product.category')}}">Smart Phones &amp; Tablets</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('product.category')}}">Laptops &amp; Computers</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('product.category')}}">Video Cameras</a>
                                    </li>
                                </ul> --}}
                            </header>
    
                            <div class="columns-6-1">
                                <ul class="products exclude-auto-height products-6">
                                    @foreach($bestSellers as $bs_data)
                                    <li class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="{{route('product.subcategory',$bs_data->category->slug)}}" rel="tag">{{ $bs_data->category->name }}</a></span>
                                                <a href="{{route('product.single.details',$bs_data->slug)}}">
                                                    <h3>{{ $bs_data->product_name }}</h3>
                                                    <div class="product-thumbnail">
    
                                                        <img data-echo="{{ asset('public/images/backend_images/product/small/'.$bs_data->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$bs_data->image) }}" alt="">
    
    
                                                    </div>
                                                </a>
    
                                                <div class="price-add-to-cart">
                                                    <span class="price">
                                                        
                                                            @if(empty($bs_data->onsale))
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. {{ rupee_format($bs_data->price) }}</span></ins>
                                                                {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                            </span>
                                                            @endif
                                                            @if($bs_data->onsale != NULL)
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. <?php echo $bs_data->price - ($bs_data->price * ($bs_data->onsale/100)) ?></span></ins>
                                                                <del><span class="amount">Rs. {{ rupee_format($bs_data->price) }}</span></del>
                                                            </span>
                                                            @endif
                                                    </span>
                                                    <a rel="nofollow" href="{{route('product.single.details',$bs_data->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->
    
                                                <div class="hover-area">
                                                        <div class="action-buttons">
                                                                <input type="hidden" name="product_id"  class="product_id" value="{{ $bs_data->id }}">
                                                                <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$bs_data->id}}">Wishlist</button>
                                                        </div>
                                                    </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </li>
                                    @endforeach
                                </ul>
    
                                <ul class="products exclude-auto-height product-main-6-1">
                                    @foreach($bestSellerSingle as $bss_data)
                                    <li class="first product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories">
                                                    <a href="{{route('product.subcategory',$bss_data->category->slug)}}" rel="tag">{{ $bss_data->category->name }}</a>
                                                </span>
    
                                                <a href="{{route('product.single.details',$bss_data->slug)}}">
                                                    <h3>{{ $bss_data->product_name }}</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="{{ asset('public/images/backend_images/product/large/'.$bss_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/large/'.$bss_data->image) }}" class="wp-post-image" alt="">
                                                    </div>
                                                </a>
    
                                                {{-- <div class="thumbnails columns-3">
                                                    <a href="{{ asset('public/frontend/assets/images/3.jpg') }}" class="zoom first" title="" data-rel="prettyPhoto[product-gallery]">
                                                        <img src="{{ asset('public/frontend/assets/images/3.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/3.jpg') }}"  alt="">
                                                    </a>
                                                    <a href="{{ asset('public/frontend/assets/images/2.jpg') }}" class="zoom" title="" data-rel="prettyPhoto[product-gallery]">
                                                        <img src="{{ asset('public/frontend/assets/images/2.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/2.jpg') }}"  alt="">
                                                    </a>
                                                    <a href="{{ asset('public/frontend/assets/images/5.jpg') }}" class="zoom last" title="" data-rel="prettyPhoto[product-gallery]">
                                                        <img src="{{ asset('public/frontend/assets/images/5.jpg') }}" data-echo="{{ asset('public/frontend/assets/images/5.jpg') }}"  alt="">
                                                    </a>
                                                </div> --}}
    
                                                <div class="price-add-to-cart">
                                                    <span class="price">
                                                        
                                                            @if(empty($bss_data->onsale))
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. {{ rupee_format($bss_data->price) }}</span></ins>
                                                                {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                            </span>
                                                            @endif
                                                            @if($bss_data->onsale != NULL)
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. <?php echo $bss_data->price - ($bss_data->price * ($bss_data->onsale/100)) ?></span></ins>
                                                                <del><span class="amount">Rs. {{ rupee_format($bss_data->price) }}</span></del>
                                                            </span>
                                                            @endif
                                                    </span>
    
                                                    <a rel="nofollow" href="{{ route('product.single.details',$bss_data->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->
    
                                                <div class="hover-area">
                                                        <div class="action-buttons">
                                                                <input type="hidden" name="product_id" class="product_id" value="{{ $bss_data->id }}">
                                                                <button name="your_name" class="wishlistButton" onclick="wishlist(this)" id="{{$bss_data->id}}">Wishlist</button>
                                                        </div>
                                                    </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </li><!-- /.product -->
                                    @endforeach
                                </ul><!-- /.products-->
                            </div><!-- /.columns-6-1 -->
                        </div><!-- /.container-->
                    </section><!-- /.products-6-1 -->
    
                    <section class="home-list-categories animate-in-view fadeIn animated" data-animation="fadeIn">
                        <header>
                            <h2 class="h1">Top Categories this Month</h2>
                        </header>
                        <ul class="categories">
                            @foreach($cat as $cat_data)
                            <li class="category">
                                <div class="media">
                                    <a class="media-left" href="{{route('product.category.details',$cat_data->slug)}}">
                                        <img data-echo="{{ asset('public/images/backend_images/category/small/'.$cat_data->featured_image) }}" src="{{ asset('public/images/backend_images/category/small/'.$cat_data->featured_image) }}" alt="">
                                    </a><!-- /.media-left -->
    
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="{{route('product.category.details',$cat_data->slug)}}">{{ $cat_data->name }}</a></h4>
                                        <ul class="sub-categories list-unstyled">
                                            @foreach($subcat as $subcat_data)
                                            @if($cat_data->id == $subcat_data->parent_id)
                                            <li class="cat-item">
                                                <a href="{{route('product.subcategory',$subcat_data->slug)}}" >{{ $subcat_data->name }}</a>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div><!-- /.media-body -->
                                </div><!-- /.media -->
                                <a class="see-all" href="{{ route('product.category.details',$cat_data->slug) }}">See all</a>
                            </li>
                            @endforeach
                        </ul>
                    </section>
    
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->

@foreach($advertisements as $ads)
    <div class="home-v3-banner-block animate-in-view fadeIn animated" data-animation="fadeIn">
        <div class="home-v3-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px">
            <a href="{{ route('product.category.details',$ads->category->slug) }}"><img src="{{ asset('public/images/backend_images/setting/large/'.$ads->image) }}" data-echo="{{ asset('public/images/backend_images/setting/large/'.$ads->image) }}" class="img-responsive" alt=""></a>
        </div>
    </div><!-- /.home-v1-banner-block -->
@endforeach

    @include('frontend.includes.brands')
@endsection

@section('scripts')


<script>
    jQuery( document ).ready(function( $ ) {
        $('.wishlistButton').click(function (event) {
            //alert($(this).val());
            //alert (event.target.id);
            var product_id= event.target.id;
            //alert(product_id);
            $.ajax({
                headers:{
                    'X-CSRF-TOKEN' : $ ('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'product/single/wishlist/store',
                data:{product_id:product_id},
                success:function(resp){
                    //alert("success")
                    toastr.success('Product has been added to Wishlist');

                },error:function(resp){
                    //alert("error");

                }
            });
        });
    });
</script>



@endsection


@section('styles')

<style>

a:hover{
    cursor:pointer;
}

</style>

@endsection
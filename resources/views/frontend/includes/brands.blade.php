    <section class="brands-carousel">
        <h2 class="sr-only">Brands Carousel</h2>
        <div class="container">

            @foreach($brand_slider as $bsd)
            <div id="owl-brands" class="owl-brands owl-carousel unicase-owl-carousel owl-outer-nav">
                <div class="item">
                    <a>
                        <figure>
                            <figcaption class="text-overlay">
                                <div class="info">
                                    <h4>{{$bsd->name}}</h4>
                                </div><!-- /.info -->
                            </figcaption>
                             <img src="{{ asset('public/images/backend_images/brand/small/'.$bsd->logo) }}" data-echo="{{ asset('public/images/backend_images/brand/small/'.$bsd->logo)}}" class="img-responsive" alt="">
                        </figure>
                    </a>
                </div><!-- /.item -->
            </div><!-- /.owl-carousel -->
            @endforeach

        </div>
    </section>

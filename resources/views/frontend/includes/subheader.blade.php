@section('header')

    <!-- ============================================================= Header Logo ============================================================= -->
    <div class="header-logo">
        <a href="{{ route('index') }}" class="header-logo-link">
                <img src="{{ asset('public/images/backend_images/settings/small/'.$sites_data->logo) }}" alt="" width="80" height="45">
        </a>
    </div>
    <!-- ============================================================= Header Logo : End============================================================= -->

    <div class="primary-nav animate-dropdown">
        <div class="clearfix">
             <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                    &#9776;
             </button>
         </div>

        <div class="collapse navbar-toggleable-xs" id="default-header">
            <nav>
                <ul id="menu-main-menu" class="nav nav-inline yamm">
                    <li class="menu-item"><a title="Home" href="{{ route('index') }}" >Home</a></li>
                    <li class="menu-item animate-dropdown"><a title="About Us" href="{{route('about.us')}}">About Us</a></li>

                    <li class="menu-item menu-item-has-children animate-dropdown dropdown"><a title="Blog" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Categories</a>
                        <ul role="menu" class=" dropdown-menu">
                            @foreach($categories_data as $catMain_data)
                            <li class="menu-item animate-dropdown"><a title="Blog v1" href="{{ route('product.category.details',$catMain_data->slug) }}">{{ $catMain_data->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="menu-item"><a title="Contact Us" href="{{ route('contact.us') }}">Contact Us</a></li>
                </ul>
            </nav>   
        </div>
    </div>

    <div class="header-support-info">
        <div class="media">
            <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
            <div class="media-body">
                <span class="support-number"><strong>Support</strong> &nbsp;{{ $sites_data->phone }}</span><br/>
                <span class="support-email"><strong>Email:</strong> {{ $sites_data->email}}</span>
            </div>
        </div>
    </div>

@endsection
@section('navbar')

    <?php
    use \App\Http\Controllers\Controller;

    $cartCount = Controller::cartCount();
    $categories = Controller::categories();
    $navCart = Controller::navCart();
    $totalAmount = Controller::totalCartAmount();
    $mainCategories = Controller::mainCategories();
    $wishlistCount = Controller::wishlistCount();
    ?>


<nav class="navbar navbar-primary navbar-full">
    <div class="container">
        <ul class="nav navbar-nav departments-menu animate-dropdown">
            <li class="nav-item dropdown ">

                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle" >Shop by Department</a>
                <ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown">
                    @foreach($mainCategories as $cat)
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="{{$cat->name}}" data-hover="dropdown" href="" data-toggle="dropdown"  aria-haspopup="true">{{$cat->name}}</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                    <img style="width: 200px; height:150px; margin-right: 10px;" src="{{ asset('public/images/backend_images/category/small/'.$cat->featured_image) }}" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title"> {{$cat->name}}</li>
                                                                    <li class="nav-divider" style="margin-right: 10px;"></li>
                                                                    @foreach($categories as $subcat)
                                                                        @if($cat->id == $subcat->parent_id)
                                                                            <li><a href="{{route('product.subcategory',$subcat->slug)}}">{{ $subcat->name }}</a></li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    @endforeach

                </ul>
            </li>
        </ul>
        <form class="navbar-search" method="POST" action="{{ url('/search') }}">
            @csrf
            <label class="sr-only screen-reader-text" for="search">Search for:</label>
            <div class="input-group">
                <input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="q" placeholder="Search for products" />
                <div class="input-group-addon search-categories">
                    <select name='category_id' id='category_id' class='postform resizeselect' >
                        {{-- <option value='0' selected='selected'>All Categories</option> --}}
                        @foreach($categories as $cat)
                        @if($cat->parent_id == 0)
                        <option value="{{ $cat->id }}">{{$cat->name}}</option>
                        @endif
                        @endforeach      
                    </select>
                </div>
                <div class="input-group-btn">
                    <input type="hidden" id="search-param" name="post_type" value="product" />
                    <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
                </div>
            </div>
        </form>
        
        <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
            <li class="nav-item dropdown">
                <a href="" class="nav-link" data-toggle="dropdown">
                    <i class="ec ec-shopping-bag"></i>
                    <span class="cart-items-count count">{{ $cartCount }}</span>
                    <span class="cart-items-total-price total-price"><span class="amount totalAmount">Rs. {{ rupee_format($totalAmount) }}</span></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-mini-cart">
                    <li>

                        <div class="widget_shopping_cart_content">
                            @if(!empty($navCart))
                            <ul class="cart_list product_list_widget ">
                                @foreach($navCart as $cart)
                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="{{route('cart.delete',$cart->id)}}">×</a>
                                        <a href="">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="{{ asset('public/images/backend_images/product/small/'.$cart->product->image) }}" alt="{{$cart->product_name}}">{{$cart->product_name}}&nbsp;
                                        </a>

                                        <span class="quantity quantity{{$cart->id}}">{{$cart->quantity}} × <span class="amount">{{$cart->price}}</span></span>
                                    </li>
                                @endforeach
                            </ul>
                                <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                                <p class="buttons">
                                    <a class="button wc-forward" href="{{route('cart') }}">View Cart</a>
                                    <a class="button checkout wc-forward" href="{{route('checkout')}}">Checkout</a>
                                </p>
                            @else
                                <ul class="cart_list product_list_widget ">
                                    <li class="mini_cart_item">
                                        <h3>Cart is empty</h3>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
            <li class="nav-item">
                <a href="{{ route('wishlist') }}" class="nav-link" title="Wishlist">
                    <i class="ec ec-favorites"> </i>
                    <span class="cart-items-count count">{{ $wishlistCount }}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>

@endsection
<?php

/* moneyformat.php */

     function rupee_format($num) {
        $explrestunits = "" ;
        if(strlen($num)>3){
            $lastthree = substr($num, strlen($num)-3, strlen($num));
            $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
            $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for($i=0; $i<sizeof($expunit); $i++){
                // creates each of the 2's group and adds a comma to the end
                if($i==0)
                {
                    $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
                }else{
                    $explrestunits .= $expunit[$i].",";
                }
            }
            $thecash = $explrestunits.$lastthree;
        } else {
            $thecash = $num;
        }
        return $thecash; // writes the final format where $currency is the currency symbol.
    }
?>



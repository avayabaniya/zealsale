<?php
use \App\Http\Controllers\Controller;

$mainCategories = Controller::mainCategories();
$categories = Controller::categories();



//echo $url;die;
?>
@section('navbar')

<div class="row" style="margin-left:20px;margin-right:25px;">
    <div class="col-xs-12 col-lg-3">
        <nav>
            <ul class="list-group vertical-menu yamm make-absolute" >
                <li class="list-group-item"><span><i class="fa fa-list-ul"></i> All Departments</span></li>

                @foreach($mainCategories as $cat)
                <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                    <a title="{{$cat->name}}" data-hover="dropdown" href="" data-toggle="dropdown"  aria-haspopup="true">{{$cat->name}}</a>
                    <ul role="menu" class=" dropdown-menu">
                        <li class="menu-item animate-dropdown menu-item-object-static_block">
                            <div class="yamm-content">
                                <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                    <div class="col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_single_image wpb_content_element vc_align_left">
                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper vc_box_border_grey">
                                                            <img style="width: 200px; height:150px; margin-right: 10px;" src="{{ asset('public/images/backend_images/category/small/'.$cat->featured_image) }}" class="vc_single_image-img attachment-full" alt="">
                                                        </div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <ul>
                                                            <li class="nav-title"> {{$cat->name}}</li>
                                                            <li class="nav-divider" style="margin-right: 10px;"></li>
                                                            @foreach($categories as $subcat)
                                                                @if($cat->id == $subcat->parent_id)
                                                                    <li><a href="{{route('product.subcategory',$subcat->slug)}}">{{ $subcat->name }}</a></li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                @endforeach
            </ul>
        </nav>
    </div>

    <div class="col-xs-12 col-lg-9">
        <nav>
            <ul id="menu-secondary-nav" class="secondary-nav">
                <li class="highlight menu-item"><a href="{{route('super.deals')}}">Super Deals</a></li>
                <li class="menu-item"><a href="{{ route('featured.brands') }}">Featured Brands</a></li>
                <li class="menu-item"><a href="{{ route('trending.styles')}}">Trending Styles</a></li>
                <li class="pull-right menu-item">Free Shipping inside Ringroad</li>
            </ul>
        </nav>
    </div>
</div>

@endsection



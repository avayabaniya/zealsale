
    <div class="home-v1-slider" >
        <!-- ========================================== SECTION – HERO : END========================================= -->

        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

            <?php
            $count = $sliders->count();
            ?>
            @if($count === 0)
            <div class="item" style="background-image: url({{ asset('public/images/backend_images/slider/large/default.jpg')}});">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-5">
                            <div class="caption vertical-center text-left">
                                <div class="hero-1 fadeInDown-1" style="color:white;font-weight:bold">
                                    {{ $sites_data->organization_name}}
                                </div>
                                <br><br>
                                <div class="hero-v2-price fadeInDown-3">
                                    {{-- Nepal No. 1 Shopping Site --}}
                                </div>
                                <br><br><br><br>
                                <div class="hero-action-btn fadeInDown-4">
                                    <a href="{{ route('super.deals') }}" class="big le-button ">Start Buying</a>
                                </div>
                            </div><!-- /.caption -->
                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.item -->
            @else
            @foreach ($sliders as $slider_data)
            <div class="item" style="background-image: url({{ asset('public/images/backend_images/slider/large/'.$slider_data->image)}});">
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-5">
                            <div class="caption vertical-center text-left">
                                <div class="hero-1 fadeInDown-1" style="color:white;font-weight:bold">
                                    {{$slider_data->title}}
                                </div>
                                <br><br>
                                <div class="hero-v2-price fadeInDown-3" style="color:white;">
                                    Starts from <br><span>Rs. {{$slider_data->price }}</span>
                                </div>
                                <br><br>
                                <div class="hero-action-btn fadeInDown-4">
                                    <a href="{{ route('super.deals') }}" class="big le-button ">Start Buying</a>
                                </div>
                            </div><!-- /.caption -->
                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.item -->
            @endforeach
            @endif

        </div><!-- /.owl-carousel -->

        <!-- ========================================= SECTION – HERO : END ========================================= -->

    </div><!-- /.home-v1-slider -->
@section('navbar')

<nav class="navbar navbar-primary navbar-full yamm">
    <div class="container">
        <div class="clearfix">
            <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#header-v3">
                &#9776;
            </button>
        </div>

        <div class="collapse navbar-toggleable-xs" id="header-v3">
            <ul class="nav navbar-nav">
                @foreach($categories_data as $cat_data)
                <li class="menu-item animate-dropdown"><a title="{{ $cat_data->name }}" href="{{ route('product.category.details',$cat_data->slug) }}">{{ $cat_data->name }}</a></li>
                @endforeach
            </ul>
        </div><!-- /.collpase -->
    </div><!-- /.-container -->
</nav><!-- /.navbar -->


@endsection
@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    @if(isset($details))
                    <!-- ++++ banner ++++ -->
                    <section class="banner  o-hidden banner-inner search-results-banner">
                        <div class="container">
                            <!--banner text-->
                            <div class="banner-txt text-center">
                                <br>
                                <h1 class="text-center">Search Results</h1>
                            </div>
                            <!--end banner text-->
                        </div>
                    <!-- End of .container -->
                    </section>
                    <section class="products-6-1 animate-in-view fadeIn animated" data-animation="fadeIn">
                        <div class="container">
                            <header>
                                <h2 class="h1">{{$details->count()}} search results found</h2>
                            </header>
    
                            <div class="columns-6-1">
                                <ul class="products exclude-auto-height products-6">
                                    @foreach($details as $bs_data)
                                    <li class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="{{route('product.subcategory',$bs_data->category->slug)}}" rel="tag">{{ $bs_data->category->name }}</a></span>
                                                <a href="{{route('product.single.details',$bs_data->slug)}}">
                                                    <h3>{{ $bs_data->product_name }}</h3>
                                                    <div class="product-thumbnail">
                                                        <img data-echo="{{ asset('public/images/backend_images/product/small/'.$bs_data->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$bs_data->image) }}" alt="">
                                                    </div>
                                                </a>
    
                                                <div class="price-add-to-cart">
                                                    <span class="price">
                                                            @if($bs_data->onsale == NULL)
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. {{ rupee_format($bs_data->price) }}</span></ins>
                                                                {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                            </span>
                                                            @endif
                                                            @if($bs_data->onsale != NULL)
                                                            <span class="electro-price">
                                                                <ins><span class="amount">Rs. <?php echo $bs_data->price - ($bs_data->price * ($bs_data->onsale/100)) ?></span></ins>
                                                                <del><span class="amount">Rs. {{ rupee_format($bs_data->price) }}</span></del>
                                                            </span>
                                                            @endif
                                                    </span>
                                                    <a rel="nofollow" href="{{route('product.single.details',$bs_data->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->
    
                                                <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <form id="wishlist" action="{{ route('wishlist.store') }}" method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="hidden" name="product_id" value="{{ $bs_data->id }}">
                                                                <button type="submit" name="your_name" value="your_value" class="add_to_wishlist btn-wishlist hoverhover">Wishlist</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </li>
                                    @endforeach
                                </ul>
                            </div><!-- /.columns-6-1 -->
                        </div><!-- /.container-->
                    </section><!-- /.products-6-1 -->
                    @elseif(isset($message))
                    <!-- ++++ contact-us-now ++++ -->
                    <section class="section">
                        <div class="box" style="margin:20px;margin-top:60px;">
                            <div class="card">
                                <div class="text-center">
                                    <br>
                                    <h2 style="text-align:center">{{$message}}</h2>
                                    <p style="text-align:center">
                                        <button class="btn btn-warning btn-lg" style="text-align:center;background-color:gold"><a href="{{ route('index') }}" style="color:black" >Shop Now</a></button>
                                    </p>
                                    <h3 style="text-align:center;">or</h3>  
                                    <p style="text-align:center">
                                        <button class="btn btn-warning" style="text-align:center;background-color:gold"><a href="{{ route('contact.us') }}" style="color:black">Contact Us Now</a></button>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End of .container -->
                    </section>
                    <!-- End of .search-results -->
                    @endif
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->
    
@endsection

@section('styles')

<style>

a:hover{
    cursor:pointer;
}

</style>

@endsection
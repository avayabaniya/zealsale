@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')


@section('content')

<div tabindex="-1" class="site-content" id="content">
    <div class="container">

        <nav class="woocommerce-breadcrumb"><a href="{{route('index')}}">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Wishlist</nav>
        <div class="content-area" id="primary">
            <main class="site-main" id="main">
                <article class="page type-page status-publish hentry">
                    <div itemprop="mainContentOfPage" class="entry-content">
                        <div id="yith-wcwl-messages"></div>
                        <form class="woocommerce" method="post" id="yith-wcwl-form" action="{{ route('cart.add') }}" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" value="68bc4ab99c" name="yith_wcwl_form_nonce" id="yith_wcwl_form_nonce"><input type="hidden" value="/electro/wishlist/" name="_wp_http_referer">
                            <!-- TITLE -->
                            <div class="wishlist-title ">
                                <h2>My wishlist on {{ $sites_data->organization_name }}</h2>
                            </div>
                            
            <div class="box">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;">
                        @if(Session::has('flash_message_error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                        @endif
                        @if(Session::has('flash_message_success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert"> X </button>
                                <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

                            <!-- WISHLIST TABLE -->
                            <table data-token="" data-id="" data-page="1" data-per-page="5" data-pagination="no" class="shop_ table ca rt wish list_ ta ble">

                                <thead>
                                    <tr>

                                        <th class="product-remove"></th>

                                        <th class="product-thumbnail"></th>

                                        <th class="product-name">
                                            <span class="nobr">Product Name</span>
                                        </th>

                                        <th class="product-price">
                                            <span class="nobr">Unit Price</span>
                                        </th>
                                        <th class="product-stock-stauts">
                                            <span class="nobr">Stock Status</span>
                                        </th>

                                        <th class="product-add-to-cart"></th>

                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach($wishlists as $wl)
                                    <tr>
                                        <td class="product-remove">
                                            <div>
                                                <a title="Remove this product" class="remove remove_from_wishlist" href="{{ route('wishlist.destroy',$wl->id) }}">×</a>
                                            </div>
                                        </td>

                                        <td class="product-thumbnail">
                                            <a href="{{ route('product.single.details',$wl->product->slug) }}"><img width="180" height="180" alt="1" class="wp-post-image" src="{{ asset('public/images/backend_images/product/small/'.$wl->product->image) }}"></a>
                                        </td>

                                        <td class="product-name">
                                            <a href="{{ route('product.single.details',$wl->product->slug) }}">{{ $wl->product->product_name }}</a>
                                        </td>

                                        <td class="product-price">
                                            <span class="electro-price"><span class="amount">Rs. {{ rupee_format($wl->product->price) }}</span></span>
                                        </td>

                                        <td class="product-stock-status">
                                            <?php $total_stock = 0; ?>
                                            @foreach($wl->product->attributes as $att)
                                            <?php $total_stock = $att->stock + $total_stock; ?>
                                            @endforeach
                                            @if($total_stock == 0)
                                            <span id="unavailable" class="in-stock">Unavailable</span>
                                            @else
                                            <span class="in-stock">Available</span>
                                            @endif
                                        </td>

                                        <td class="product-add-to-cart">
                                            <!-- Add to cart button -->
                                            @if($total_stock != 0)
                                                <a href="{{ route('product.single.details',$wl->product->slug) }}" class="button"> Add to Cart</a>
                                            @endif
                                            <!-- Change wishlist -->

                                            <!-- Remove from wishlist -->
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="6"></td>
                                    </tr>
                                </tfoot>

                            </table>

                            <input type="hidden" value="85fe311a9d" name="yith_wcwl_edit_wishlist" id="yith_wcwl_edit_wishlist"><input type="hidden" value="/electro/wishlist/" name="_wp_http_referer">

                        </form>

                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .col-full -->
</div>

@include('frontend.includes.brands')

@endsection

@section('scripts')

{{-- <script>
$('#unavailable').keyup(function(){
    var product_name = $('#product_name').val();

    if(product_name.length == 0)
    {
        $('#product_name_val').text('').css("color","green");
        $('#submit').prop('disabled', false);
        $('#submit').css('background-color', 'green');
    }
}); 
</script>   --}}

@endsection
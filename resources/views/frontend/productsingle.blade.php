@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 10px;">
                    @if(Session::has('flash_message_error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_error') !!} </strong>
                        </div>
                    @endif
                    @if(Session::has('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert"> X </button>
                            <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                    @endif
                </div>
            </div>
            <nav class="woocommerce-breadcrumb">
                <a href="{{route('index')}}">Home</a>
                <span class="delimiter"><i class="fa fa-angle-right"></i></span>
                <a href="{{ route('product.category.details',$productMainCategories->slug) }}">{{ $productMainCategories->name }}</a>
                <span class="delimiter"><i class="fa fa-angle-right"></i></span>
                <a href="{{ route('product.subcategory',$productDetails->category->slug) }}">{{ $productDetails->category->name }}</a>
                <span class="delimiter"><i class="fa fa-angle-right"></i>
                </span>{{ $productDetails->product_name }}
            </nav><!-- /.woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="product">
                        <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ $productDetails->product_name }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <img src="{{ asset('public/images/backend_images/product/medium/'.$productDetails->image) }}" alt="">
                                            </div>
                                            <div class="col-md-4">
                                                    <p>Price:</p>
                                                    <p><b> {{ rupee_format($productDetails->price) }} </b></p>
                                                    <p>Product Code:</p>
                                                    <p><b> {{ $productDetails->product_code }} </b></p>
                                                    <p>Availability </p>
                                                    <?php $total_stock = 0; ?>
                                                    @foreach($productDetails->attributes as $att)
                                                        <?php $total_stock = $att->stock + $total_stock; ?>
                                                    @endforeach
                                                    <?php
                                                    if($total_stock == 0) {
                                                    ?>
                                                    <h6 style="color:red;">Out of Stock</h6>
                                                    <?php } else {  ?>
                                                    <h6 style="color:green;">In Stock</h6>
                                                    <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($productDetails->images as $image)
                        <div class="modal fade" id="exampleModal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ $productDetails->product_name }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <img src="{{ asset('public/images/backend_images/product/medium/'.$image->image) }}" alt="">
                                            </div>
                                             <div class="col-md-4">
                                                 <p>Price:</p>
                                                 <p><b> {{ rupee_format($productDetails->price) }} </b></p>
                                                 <p>Product Code:</p>
                                                 <p><b> {{ $productDetails->product_code }} </b></p>
                                                 <p>Availability </p>
                                                 <?php $total_stock = 0; ?>
                                                 @foreach($productDetails->attributes as $att)
                                                     <?php $total_stock = $att->stock + $total_stock; ?>
                                                 @endforeach
                                                 <?php
                                                 if($total_stock == 0) {
                                                 ?>
                                                 <h6 style="color:red;">Out of Stock</h6>
                                                 <?php } else {  ?>
                                                 <h6 style="color:green;">In Stock</h6>
                                                 <?php } ?>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="single-product-wrapper">
                            <div class="product-images-wrapper">
                                @if($productDetails->onsale != null)
                                <span class="onsale">Sale!</span>
                                @endif
                                <div class="images electro-gallery ">
                                    <div class="thumbnails-single owl-carousel easyzoom easyzoom--adjacent easyzoom--with-thumbnails  ">
                                        <a href="{{ asset('public/images/backend_images/product/large/'.$productDetails->image) }}" data-toggle="modal" data-target="#exampleModal" class="zoom" title="" data-rel="prettyPhoto[product-gallery]"><span class="onsale">Sale!</span>
                                            <img style="width: 300px; height: 280px;" src="{{ asset('public/images/backend_images/product/large/'.$productDetails->image) }}" data-echo="{{ asset('public/images/backend_images/product/large/'.$productDetails->image) }}" class="wp-post-image" alt="">
                                        </a>
                                        @foreach($productDetails->images as $image)
                                        <ul class="thumbnails">
                                            <a href="{{ asset('public/images/backend_images/product/large/'.$image->image) }}" data-toggle="modal" data-target="#exampleModal{{$image->id}}"  class="zoom" title="" data-rel="prettyPhoto[product-gallery]">
                                                <img style="width: 300px; height: 280px;" src="{{ asset('public/images/backend_images/product/large/'.$image->image) }}" data-echo="{{ asset('public/images/backend_images/product/large/'.$image->image) }}" class="wp-post-image" alt="">
                                            </a>
                                        </ul>
                                        @endforeach

                                    </div><!-- .thumbnails-single -->
                                    <div class="thumbnails-all columns-5 owl-carousel">
                                        <a href="{{ asset('public/images/backend_images/product/small/'.$productDetails->image) }}" class="zoom" title="" data-rel="prettyPhoto[product-gallery]">
                                            <img style="width: 86px; height: 78px;" src="{{ asset('public/images/backend_images/product/small/'.$productDetails->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$productDetails->image) }}" class="wp-post-image" alt="">
                                        </a>
                                        @foreach($productDetails->images as $image)
                                            <a href="{{ asset('public/images/backend_images/product/small/'.$image->image) }}" class="zoom" title="" data-rel="prettyPhoto[product-gallery]">
                                                <img style="width: 86px; height: 78px;" src="{{ asset('public/images/backend_images/product/small/'.$image->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$image->image) }}" class="wp-post-image" alt="">
                                            </a>
                                        @endforeach
                                    </div><!-- .thumbnails-all -->
                                </div><!-- .electro-gallery -->
                            </div><!-- /.product-images-wrapper -->

                            <form action="{{route('cart.add')}}" id="addToCart" method="post">
                                @csrf
                                <div class="summary entry-summary">
                                <span class="loop-product-categories">
                                    <a href="" rel="tag">{{$productDetails->category->name}}</a>
                                </span><!-- /.loop-product-categories -->

                                <h1 itemprop="name" class="product_title entry-title">{{$productDetails->product_name}}</h1>
                                <?php $total_rating = 0; ?>
                                @foreach($productDetails->rating as $att)
                                    <?php $total_rating = $att->rating + $total_rating; ?>
                                @endforeach

                                @if($total_rating != 0)
                                <?php
                                    $avg_rate =  $total_rating/$productDetails->rating->count();
                                    $avg_rate_width = (($total_rating/$productDetails->rating->count())*100);
                                ?>

                                <div class="woocommerce-product-rating rating-bar">
                                    <div class="star-rating" title="Rated <?php echo number_format((float)$avg_rate, 1, '.', ''); ?> out of 5">
                                            <span style="width:{{ $avg_rate_width }}%">
                                        </span>
                                    </div>
                                    <a href="#reviews" class="woocommerce-review-link">(<span itemprop="reviewCount" class="count">{{ $productDetails->rating->count() }}</span> customer reviews)</a>
                                </div><!-- .woocommerce-product-rating -->
                                @endif


                                <div class="availability in-stock">Availablity: <span id="Availability">In stock</span></div><!-- .availability -->

                                <hr class="single-product-title-divider" />

                                <div itemprop="description">
                                    <p>{!! $productDetails->description !!}</p>
                                    <table class="variations">
                                        <tbody>
                                        <tr>
                                            <td class="label"><label>Select Size</label></td>
                                            <td class="value">
                                                <select id="selSize" class="" name="size" style="width: 150px; height: 35px;">
                                                    <option value="" disabled selected>Select Size</option>
                                                    @foreach($productDetails->attributes as $size)
                                                        <option value="{{$productDetails->id}}-{{$size->size}}" >{{ $size->size }}</option>
                                                    @endforeach
                                                </select>
                                                <p id="emptySize" class="error"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label"><label>Quantity</label></td>
                                            <td class="value">
                                                <input style="display: inline-block; width: 150px; height: 35px;" type="number" name="quantity" value="1" min="1" max="{{$size->quantity}}" title="Qty" class="input-text qty text"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if($productDetails->onsale != NULL)
                                                    <p class="price"><span class="electro-price"><ins><span class="amount" id="getPrice">Rs. <?php echo rupee_format($productDetails->price - ($productDetails->price * ($productDetails->onsale/100))) ?> </span></ins> <del><span class="amount">Rs. {{ rupee_format($productDetails->price) }}</span></del></span>
                                            @else
                                                <p class="price"><span class="electro-price"><ins><span class="amount" id="getPrice">Rs. {{ rupee_format($productDetails->price) }} </span></ins> </span>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!-- .description -->



                                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

                                        <input type="hidden" name="product_id" value="{{ $productDetails->id }}">
                                        <input type="hidden" name="product_name" value="{{ $productDetails->product_name }}">
                                        <input type="hidden" name="product_code" value="{{ $productDetails->product_code }}">
                                        <input type="hidden" name="product_color" value="{{ $productDetails->product_color }}">
                                        <input type="hidden" name="price" id="price" value="{{ rupee_format($productDetails->price) }}">
                                        <button type="submit" id="cartButton" class="single_add_to_cart_button button">Add to cart</button>
                                       
                                </div><!-- /itemprop -->
                            </div><!-- .summary -->

                        </form>

                            <div class="action-buttons">
                                    <input type="hidden" name="product_id" id="product_id" value="{{ $productDetails->id }}">
                                    <button  name="your_name" id="wishlistButton">&nbsp;Wishlist&nbsp;&nbsp;</button>

                                <br><br>
                                <h2>Share Product </h2>
                                <div class="addthis_inline_share_toolbox_ec5c"></div>
                            </div><!-- .action-buttons -->
                        </div><!-- /.single-product-wrapper -->
                   

                        <div class="woocommerce-tabs wc-tabs-wrapper">
                            <ul class="nav nav-tabs electro-nav-tabs tabs wc-tabs" role="tablist">

                                <li class="nav-item description_tab">
                                    <a href="#tab-description" class="active" data-toggle="tab">Description</a>
                                </li>

                                <li class="nav-item specification_tab">
                                    <a href="#tab-specification" data-toggle="tab">Specification</a>
                                </li>

                                <li class="nav-item reviews_tab">
                                    <a href="#tab-reviews" data-toggle="tab">Reviews</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active in panel entry-content wc-tab" id="tab-description">
                                    <div class="electro-description">
                                        {!! $productDetails->description !!}
                                    </div><!-- /.electro-description -->

                                    <div class="product_meta">
                                        {{-- <span class="sku_wrapper">SKU: <span class="sku" itemprop="sku">FW511948218</span></span> --}}

                                        <span class="posted_in">SKU:
                                            <a href="javascript: return false;">{{ $productDetails->product_code }}</a>
                                        </span>

                                        <span class="posted_in">Category:
                                            <a href="{{ route('product.subcategory',$productDetails->category->slug) }}" rel="tag">{{ $productDetails->category->name }}</a>
                                        </span>

                                        <?php
                                        $tags = explode(',',$productDetails->meta_keywords);
                                        ?>

                                        <span class="tagged_as">Product Code:
                                            <a>{{ $productDetails->product_code}}</a>
                                        </span>

                                        <span class="tagged_as">Tags:
                                            @foreach($tags as $t)
                                                <a rel="tag">{{ $t }}</a>,
                                            @endforeach
                                        </span>

                                    </div><!-- /.product_meta -->
                                </div>

                                <div class="tab-pane panel entry-content wc-tab" id="tab-specification">
                                    <h3>Product Specifications</h3>
                                    <table class="table">
                                        <tbody>
                                            @foreach( $productDetails->specifications as $sp)
                                            <tr>
                                                <td>{{ $sp->title }}</td>
                                                <td>{{ $sp->description }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.panel -->

                                <div class="tab-pane panel entry-content wc-tab" id="tab-reviews">
                                    <div id="reviews" class="electro-advanced-reviews">
                                        <div class="advanced-review row">
                                            <div class="col-xs-12 col-md-6">
                                                <h2 class="based-title">Based on {{ $productDetails->rating->count() }}  reviews</h2>

                                                <?php $total_rating = 0; ?>
                                                @foreach($productDetails->rating as $att)
                                                    <?php $total_rating = $att->rating + $total_rating; ?>
                                                @endforeach

                                                @if($total_rating == 0)

                                                <div class="avg-rating">
                                                    <span class="avg-rating-number"><?php echo "0"; ?></span> overall
                                                </div>

                                                @endif

                                                @if($total_rating != 0)
                                                <?php
                                                    $avg_rate =  $total_rating/$productDetails->rating->count();
                                                ?>

                                                <div class="avg-rating">
                                                    <span class="avg-rating-number"><?php echo number_format((float)$avg_rate, 1, '.', ''); ?></span> overall
                                                </div>

                                                <div class="rating-histogram">
                                                        <?php $one_rating = 0; ?>
                                                        <?php $two_rating = 0; ?>
                                                        <?php $three_rating = 0; ?>
                                                        <?php $four_rating = 0; ?>
                                                        <?php $five_rating = 0; ?>
                                                        @foreach($productDetails->rating as $att)
                                                        @if($att->rating == 1)
                                                            <?php $one_rating++ ?>
                                                        @endif
                                                        @if($att->rating == 2)
                                                            <?php $two_rating++ ?>
                                                        @endif
                                                        @if($att->rating == 3)
                                                            <?php $three_rating++ ?>
                                                        @endif
                                                        @if($att->rating == 4)
                                                            <?php $four_rating++ ?>
                                                        @endif
                                                        @if($att->rating == 5)
                                                            <?php $five_rating++ ?>
                                                        @endif
                                                        @endforeach
                                                        <?php
                                                        $one_rating_width = (($one_rating/$productDetails->rating->count())*100);
                                                        $two_rating_width = (($two_rating/$productDetails->rating->count())*100);
                                                        $three_rating_width = (($three_rating/$productDetails->rating->count())*100);
                                                        $four_rating_width = (($four_rating/$productDetails->rating->count())*100);
                                                        $five_rating_width = (($five_rating/$productDetails->rating->count())*100);
                                                        ?>

                                                    <div class="rating-bar">
                                                        <div class="star-rating" title="Rated 5 out of 5">
                                                            <span style="width:100%"></span>
                                                        </div>
                                                        <div class="rating-percentage-bar">
                                                            <span style="width:{{$five_rating_width}}%" class="rating-percentage">

                                                            </span>
                                                        </div>
                                                        <div class="rating-count">{{ $five_rating }}</div>
                                                    </div><!-- .rating-bar -->

                                                    <div class="rating-bar">
                                                        <div class="star-rating" title="Rated 4 out of 5">
                                                            <span style="width:80%"></span>
                                                        </div>
                                                        <div class="rating-percentage-bar">
                                                            <span style="width:{{$four_rating_width}}%" class="rating-percentage"></span>
                                                        </div>
                                                        <div class="rating-count">{{ $four_rating }}</div>
                                                    </div><!-- .rating-bar -->

                                                    <div class="rating-bar">
                                                        <div class="star-rating" title="Rated 3 out of 5">
                                                            <span style="width:60%"></span>
                                                        </div>
                                                        <div class="rating-percentage-bar">
                                                            <span style="width:{{$three_rating_width}}%" class="rating-percentage"></span>
                                                        </div>
                                                        <div class="rating-count zero">{{ $three_rating }}</div>
                                                    </div><!-- .rating-bar -->

                                                    <div class="rating-bar">
                                                        <div class="star-rating" title="Rated 2 out of 5">
                                                            <span style="width:40%"></span>
                                                        </div>
                                                        <div class="rating-percentage-bar">
                                                            <span style="width:{{$two_rating_width}}%" class="rating-percentage"></span>
                                                        </div>
                                                        <div class="rating-count zero">{{ $two_rating }}</div>
                                                    </div><!-- .rating-bar -->

                                                    <div class="rating-bar">
                                                        <div class="star-rating" title="Rated 1 out of 5">
                                                            <span style="width:20%"></span>
                                                        </div>
                                                        <div class="rating-percentage-bar">
                                                            <span style="width:{{$one_rating_width}}%" class="rating-percentage"></span>
                                                        </div>
                                                        <div class="rating-count zero">{{ $one_rating }}</div>
                                                    </div><!-- .rating-bar -->
                                                </div>
                                                @endif




                                                <p class="comment-form-rating">
                                                    <label>Your Rating</label>
                                                </p>

                                                <p class="stars">
                                                    <span>
                                                    <form id="rating" action="{{ route('rating.store') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="rate">
                                                            <input type="hidden" name="product_id" value="{{ $productDetails->id }}">
                                                            <input type="radio" class="rating" id="star5" name="rate" value="5" style="position: fixed;" />
                                                            <label for="star5" class="rating" title="5 stars">5 stars</label>
                                                            <input type="radio" class="rating" id="star4" name="rate" value="4" style="position: fixed;"/>
                                                            <label for="star4" class="rating" title="4 stars">4 stars</label>
                                                            <input type="radio" class="rating" id="star3" name="rate" value="3" style="position: fixed;"/>
                                                            <label for="star3" class="rating" title="3 stars">3 stars</label>
                                                            <input type="radio" class="rating" id="star2" name="rate" value="2" style="position: fixed;"/>
                                                            <label for="star2" class="rating" title="2 stars">2 stars</label>
                                                            <input type="radio" class="rating" id="star1" name="rate" value="1" style="position: fixed;"/>
                                                            <label for="star1" class="rating" title="1 stars">1 star</label>
                                                        </div>
                                                        <button type="submit" class="btn btn-bold btn-sm btn-pure btn-success float-right hover rating">Rate Now !</button>
                                                    </form>

                                                    </span>
                                                </p>


                                            </div><!-- /.col -->

                                            <div class="col-xs-12 col-md-6">
                                                <div id="review_form_wrapper">
                                                    <div id="review_form">
                                                        <div id="respond" class="comment-respond">

                                                            <p class="comment-form-comment">
                                                                <label for="comment">Your Review</label>
                                                                <div id="disqus_thread"></div>
                                                            </p>

                                                            <input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="c7106f1f46" />
                                                            <script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
                                                        </div><!-- #respond -->
                                                    </div>
                                                </div>

                                            </div><!-- /.col -->
                                        </div><!-- /.row -->

                                        <script>

                                                /**
                                                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                                /*
                                                var disqus_config = function () {
                                                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                                };
                                                */
                                                (function() { // DON'T EDIT BELOW THIS LINE
                                                var d = document, s = d.createElement('script');
                                                s.src = 'https://http-localhost-zealsale.disqus.com/embed.js';
                                                s.setAttribute('data-timestamp', +new Date());
                                                (d.head || d.body).appendChild(s);
                                                })();
                                                </script>
                                                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>




                                        <div class="clear"></div>
                                    </div><!-- /.electro-advanced-reviews -->
                                </div><!-- /.panel -->
                            </div>
                        </div><!-- /.woocommerce-tabs -->

                        <div class="related products">
                            <h2>Related Products</h2>
                            <ul class="products columns-5">

                                @foreach($products as $pp)
                                @if($productDetails->category_id == $pp->category_id)
                                @if($pp->id != $productDetails->id)
                                <li class="product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory',$pp->category->slug) }}" rel="tag">{{ $pp->category->name }}</a></span>
                                            <a href="{{ route('product.single.details',$pp->slug) }}">
                                                <h3>{{ $pp->product_name }}</h3>
                                                <div class="product-thumbnail">
                                                    <img data-echo="{{ asset('public/images/backend_images/product/small/'.$pp->image) }}" src="{{ asset('public/images/backend_images/product/small/'.$pp->image) }}" alt="">
                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <br>
                                                <span class="price">
                                                        @if($pp->onsale == NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. {{ rupee_format($pp->price) }}</span></ins>
                                                            {{-- <del><span class="amount">&#036;2,299.00</span></del> --}}
                                                        </span>
                                                        @endif
                                                        @if($pp->onsale != NULL)
                                                        <span class="electro-price">
                                                            <ins><span class="amount">Rs. <?php echo $pp->price - ($pp->price * ($pp->onsale/100)) ?></span></ins>
                                                            <del><span class="amount">Rs. {{ rupee_format($pp->price) }}</span></del>
                                                        </span>
                                                        @endif
                                                </span>
                                                <a rel="nofollow" href="{{ route('product.single.details',$pp->slug) }}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->


                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <form id="wishlist" action="{{ route('wishlist.store') }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{ $pp->id }}">
                                                        <button type="submit" name="your_name" value="your_value" class="add_to_wishlist btn-wishlist hoverhover">Wishlist</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                @endif
                                @endif
                                @endforeach

                            </ul><!-- /.products -->
                        </div><!-- /.related -->
                    </div><!-- /.product -->

                </main><!-- /.site-main -->
            </div><!-- /.content-area -->
        </div><!-- /.container -->
    </div><!-- /.site-content -->

    @include('frontend.includes.brands')


@endsection

@section('scripts')
<script>
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function(e) {
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });

    // Setup toggles example
    var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

    $('.toggle').on('click', function() {
        var $this = $(this);

        if ($this.data("active") === true) {
            $this.text("Switch on").data("active", false);
            api2.teardown();
        } else {
            $this.text("Switch off").data("active", true);
            api2._init();
        }
    });
</script>

<script id="dsq-count-scr" src="//http-localhost-zealsale.disqus.com/count.js" async></script>
    <script>
        jQuery( document ).ready(function( $ ) {
            $(document).ready(function () {
                $("#selSize").change(function () {
                    var idSize = $(this).val();
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'post',
                        url:'get-product-price',
                        data:{idSize:idSize},
                        success:function(resp) {
                            var arr = resp.split('#');
                            $("#getPrice").html(rupee_format("Rs. "+ arr[0]));
                            $("#price").val(arr[0]);

                            $("#emptySize").text('').css('color', 'red');
                            $('#cartButton').prop('disabled', false);

                            //Send the updated price based on size of the product
                            if (arr[1] == 0){
                                $("#cartButton").prop('disabled', true);
                                $("#Availability").text(" Out of Stock").css('color', 'red');
                                $("#emptySize").text('The product is out of stock').css('color', 'red');
                            }else {
                                $("#cartButton").prop('disabled', false);
                                $("#Availability").text(" In Stock").css('color', 'green');
                                $("#emptySize").text('').css('color', 'red');
                            }
                        },error:function (resp) {

                        }
                    });
                });
            });
        });
    </script>

    
    <script>
        jQuery( document ).ready(function( $ ) {

            $("#cartButton").click(function () {
            var size = $("#selSize").val();
            if (size === null){
                $("#emptySize").text('Select a size to continue').css('color', 'red');
                $('#cartButton').prop('disabled', true);
                return false;
            }
        });
        });

    </script>

<script>
    $( document ).ready(function( e ) {
        $("#wishlistButton").click(function () {
            e.preventDefault();
                //alert($(this).val());
            var product_id=($('#product_id').val());


           $.ajax({
               headers:{
                   'X-CSRF-TOKEN' : $ ('meta[name="csrf-token"]').attr('content')
               },
               type:'post',
               url:'wishlist/store',
               data:{product_id:product_id},
               success:function(resp){
                   //alert("success")
                   toastr.success('Product has been added to Wishlist');

               },error:function(resp){
                   alert("error");

               }
           });
        });
    });
</script>

    <script>
    $('label.rating').click(function(e){
        e.preventDefault();
    });

    $('input.rating').click(function(e){
        e.preventDefault();
    });
    </script>


@endsection


@section('styles')

<style>

a:hover{
    cursor:pointer;
}
button:hover{
    background: #c59b08;
}

</style>

<style>
.rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}

</style>

@endsection
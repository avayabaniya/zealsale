@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

<div id="content" class="site-content" tabindex="-1">
    <div class="container">

        <nav class="woocommerce-breadcrumb" >
            <a href="{{route('index')}}">Home</a>
            <span class="delimiter"><i class="fa fa-angle-right"></i></span>
            {{ $mainCategory->name }}
        </nav>

        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                <section>
                    <header>
                        <h2 class="h1">{{ $mainCategory->name }}</h2>
                    </header>

                    <div class="woocommerce columns-6">
                        <ul class="product-loop-categories">

                            @foreach($categories as $categories_data)
                            @if($mainCategory->id == $categories_data->parent_id )
                            <li class="product-category product">
                                <a href="{{ route('product.subcategory',$categories_data->slug) }}">
                                    <img src="{{ asset('public/images/backend_images/category/small/'.$categories_data->featured_image) }}" class="img-responsive" alt="">
                                    <h3>{{ $categories_data->name }}<mark class="count">(2)</mark></h3>
                                </a>
                            </li><!-- /.item -->
                            @endif
                            @endforeach

                        </ul>
                    </div>
                </section>

                <section class="section-products-carousel" >
                    <header>

                        <h2 class="h1">People buying in this category</h2>

                        <div class="owl-nav">
                            <a href="#products-carousel-prev" data-target="#product-category-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                            <a href="#products-carousel-next" data-target="#product-category-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a>
                        </div>

                    </header>

                    <div id="product-category-carousel">
                        <div class="woocommerce columns-6">

                            <div class="products owl-carousel products-carousel columns-6">

                                @foreach($categories as $categories_data)
                                @if($mainCategory->id == $categories_data->parent_id)
                                @foreach($products as $product_data )
                                @if($product_data->category_id == $categories_data->id)
                                <div class="product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="{{ route('product.subcategory', $categories_data->slug) }}" rel="tag">{{ $categories_data->name }}</a></span>
                                            <a href="{{ route('product.single.details', $product_data->slug) }}">
                                                <h3>{{$product_data->product_name}}</h3>
                                                <div class="product-thumbnail">
                                                    <img src="{{ asset('public/images/backend_images/product/small/'.$categories_data->image) }}" data-echo="{{ asset('public/images/backend_images/product/small/'.$product_data->image) }}" class="img-responsive" alt="">
                                                </div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                <br>
                                                @if($product_data->onsale == NULL)
                                                <span class="price">
                                                    <span class="electro-price">
                                                        <ins><span class="amount"> </span></ins>
                                                        <span class="amount"> Rs. {{ rupee_format($product_data->price) }}</span>
                                                    </span>
                                                </span>
                                                @else 
                                                
                                                <span class="price">
                                                    <span class="electro-price">
                                                        <ins><span class="amount"> Rs. <?php echo $product_data->price - ($product_data->price * ($product_data->onsale/100)) ?></span></ins>
                                                        <del><span class="amount">Rs. {{ rupee_format($product_data->price) }}</span></del>
                                                        <span class="amount"> </span>
                                                    </span>
                                                </span>
                                                    
                                                @endif
                                                <a rel="nofollow" href="{{route('product.single.details',$product_data->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                    <div class="action-buttons">
                                                            <input type="hidden" name="product_id" class="product_id" value="{{ $product_data->id }}">
                                                            <button name="your_name" class="wishlistButton" >Wishlist</button>
                                                    </div>
                                                </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </div><!-- /.products -->
                                @endif
                                @endforeach
                                @endif
                                @endforeach

                            </div>

                        </div>

                    </div>
                </section><!-- /.section-products-carousel -->
                <div class="tab-content">

                    <div id="grid" class="tab-pane active" role="tabpanel">
                        <ul class="products columns-3"></ul>
                    </div>

                    <div id="grid-extended" class="tab-pane " role="tabpanel">
                        <ul class="products columns-3"></ul>
                    </div>

                    <div id="list-view" class="tab-pane " role="tabpanel">
                        <ul class="products columns-3"></ul>
                    </div>

                    <div id="list-view-small" class="tab-pane " role="tabpanel">
                        <ul class="products columns-3"></ul>
                    </div>

                </div>

            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .col-full -->
</div><!-- #content -->

@include('frontend.includes.brands')


@endsection

@section('scripts')
    <script>
        jQuery( document ).ready(function( $ ) {
            $('.wishlistButton').click(function () {
                //alert($(this).val());
                var product_id=($('.product_id').val());


                $.ajax({
                    headers:{
                        'X-CSRF-TOKEN' : $ ('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url:'product/single/wishlist/store',
                    data:{product_id:product_id},
                    success:function(resp){
                        //alert("success")
                        toastr.success('Product has been added to Wishlist');

                    },error:function(resp){
                        alert("error");

                    }
                });
            });
        });
    </script>
@endsection

@section('styles')

<style>

a:hover{
    cursor:pointer;
}

</style>





@endsection
@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')


<div id="content" class="site-content" tabindex="-1">
    <div class="container">

        <nav class="woocommerce-breadcrumb" >
            <a href="{{route('index')}}">Home</a>
            <span class="delimiter"><i class="fa fa-angle-right"></i></span>
            Contact Us
        </nav><!-- .woocommerce-breadcrumb -->

        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                <article class="hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Contact Us</h1>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="wpb_wrapper outer-bottom-xs">
                                    <h2 class="contact-page-title">Leave us a Message</h2>
                                    <p>Can't find the answer you are looking for? Contact us and we will assist you.</p>
                                </div>

                                <div role="form" class="wpcf7">
                                    <div class="screen-reader-response"></div>
                                    <form id="contactus_form" action="{{route('contact.us.mail')}}" method="post" class="wpcf7-form">
                                        @csrf
                                        <div class="form-group row">
                                            <div class="col-xs-12 col-md-6">
                                                <label>First name*</label><br />
                                                <span class="wpcf7-form-control-wrap first-name">
                                                    <input type="text" name="first_name" value="" size="40" class="wpcf7-form-control input-text" aria-required="true" aria-invalid="false" />
                                                </span>
                                            </div>

                                            <div class="col-xs-12 col-md-6">
                                                <label>Last name*</label><br />
                                                <span class="wpcf7-form-control-wrap last-name">
                                                    <input type="text" name="last_name" value="" size="40" class="wpcf7-form-control input-text" aria-required="true" aria-invalid="false" />
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label><br />
                                            <span class="wpcf7-form-control-wrap subject"><input type="text" name="email" value="" size="40" class="wpcf7-form-control input-text" aria-invalid="false" /></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Subject</label><br />
                                            <span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control input-text" aria-invalid="false" /></span>
                                        </div>

                                        <div class="form-group">
                                            <label>Your Message</label><br />
                                            <span class="wpcf7-form-control-wrap your-message"><textarea name="your_message" cols="40" rows="10" class="wpcf7-form-control input-text wpcf7-textarea" aria-invalid="false"></textarea></span>
                                        </div>

                                        <div class="form-group clearfix">
                                            <p><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" /></p>
                                        </div>
                                    </form>
                                </div>
                            </div><!-- .col -->

                            <div class="store-info store-info-v2 col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="inner-left-xs outer-bottom-xs">

                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.083534304999!2d85.34786050144187!3d27.683812951716682!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198cb72ca34f%3A0xb9105c2f4f2e4fe9!2sOnlineZeal!5e0!3m2!1sen!2snp!4v1543999761257" width="600" height="288" style="border:0" allowfullscreen></iframe>
                                        </div>

                                        <div class="inner-left-xs">
                                            <div class="wpb_wrapper">
                                                <h2 class="contact-page-title">Our Address</h2>
                                                <p>{{ $sites_data->address }}<br />
                                                    Support {{ $sites_data->phone }}<br />
                                                    Email: <a href="mailto:{{ $sites_data->email}}">{{ $sites_data->email}}</a>
                                                </p>
                                                <h3>Support Time</h3>
                                                <p>{{ $sites_data->support_time }}</p>
                                                <h3>Careers</h3>
                                                <p>If you&#8217;re interested in employment opportunities at {{ $sites_data->organization_name }}, please email us: <a href="mailto:{{ $sites_data->email}}">{{ $sites_data->email}}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .col-full -->
</div><!-- #content -->

@include('frontend.includes.brands')

@endsection
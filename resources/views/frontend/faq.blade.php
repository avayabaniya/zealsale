@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')


<div id="content" class="site-content" tabindex="-1">
    <div class="container">
        <nav class="woocommerce-breadcrumb"><a href="{{route('index')}}">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>FAQ</nav>

        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <article class="page type-page status-publish hentry" >

                    <header class="entry-header">
                        <h1 itemprop="name" class="entry-title">Frequently Asked Questions</h1>
                    </header><!-- .entry-header -->

                    <div itemprop="mainContentOfPage" class="entry-content">

                        <div id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach($faqs as $faq_data)
                            <div class="vc_toggle panel panel-default">
                                <div class="panel-heading" role="tab" id="heading{{$loop->index+1}}">
                                    <div class="vc_toggle_title">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" style="color: #434343;" data-parent="#accordion" href="#collapse{{$loop->index+1}}" aria-expanded="true" aria-controls="collapse{{$loop->index+1}}">{{$faq_data->question}}</a>
                                        </h4>
                                        <i class="vc_toggle_icon"></i>
                                    </div>
                                </div>
                                <div id="collapse{{$loop->index+1}}" class="vc_toggle_content panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{$loop->index+1}}">
                                    <p>{!! $faq_data->answer !!}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div><!-- .entry-content -->
                </article>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .container -->
</div><!-- #content -->

@include('frontend.includes.brands')


@endsection
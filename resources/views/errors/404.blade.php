@extends('layouts.frontendLayout.design')

@include('frontend.includes.subheader')

@include('frontend.includes.subpagenavbar')

@section('content')

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <section class="section">
                        <div class="box" style="margin:20px;margin-top:60px;">
                            <div class="card">
                                <div class="text-center">
                                    <br>
                                    <p class="error-title text-danger" style="font-size:60px;text-align:center">404 ERROR</p>
                                    <br>
                                    <p class="text-uppercase error-subtitle" style="font-size:40px;text-align:center">PAGE NOT FOUND !</p>
                                    <br>
                                    <p class="text-muted m-t-30 m-b-30" style="font-size:40px;text-align:center">ARE YOU LOOKING FOR SOMETHING ELSE  </p>
                                    <br>
                                    <p style="text-align:center">
                                        <button class="btn btn-warning" style="text-align:center;background-color:gold"><a href="{{ route('contact.us') }}" style="color:black">Contact Us</a></button>
                                    </p>
                                    <h3 style="text-align:center;">or</h3>  
                                    <p style="text-align:center">
                                        <button class="btn btn-warning btn-lg" style="text-align:center;background-color:gold"><a href="{{ route('index') }}" style="color:black" >Shop Now</a></button>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End of .container -->
                    </section>
                    <!-- End of .search-results -->
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .container -->
    </div><!-- #content -->
    
@endsection

@section('styles')

<style>

a:hover{
    cursor:pointer;
}

</style>

@endsection
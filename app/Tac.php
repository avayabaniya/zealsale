<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tac extends Model
{
    protected $fillable = [
        'id',
        'title',
        'slug',
        'description',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'id',
        'product_id',
        'customer_id',
        'session_id',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}

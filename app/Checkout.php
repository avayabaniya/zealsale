<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable = [
        'id',
        'phone',
        'delivery_address',
        'order_notes',
        'payment_type',
        'cart_id',
        'customer_id',
    ];

    public function order(){
        return $this->hasMany('App\Order');
    }
}

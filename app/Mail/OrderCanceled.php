<?php

namespace App\Mail;

use App\Customer;
use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCanceled extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $order;

    /**
     * Create a new message instance.
     *
     * @param Customer $customer
     * @param Order $order
     */
    public function __construct(Customer $customer, Order $order)
    {
        $this->customer = $customer;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.order_canceled');
    }
}

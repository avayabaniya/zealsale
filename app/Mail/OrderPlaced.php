<?php

namespace App\Mail;

use App\Customer;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PhpParser\Node\Expr\Array_;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $orders;

    /**
     * Create a new message instance.
     *
     *@param Customer $customer
     *@param Array $orders
     */
    public function __construct(Customer $customer, Array $orders)
    {
        $this->customer = $customer;
        $this->orders = $orders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.order_placed');
    }
}

<?php

namespace App;

use App\Notifications\CustomerResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Customer extends Authenticatable 
{
    use Notifiable;

    protected $guard = 'customer';

    protected $fillable = [
        'id',
        'full_name',
        'slug',
        'email',
        'password',
        'address',
        'provider_id', 
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer(){
        return $this->hasMany('App\Order');
    }
    
    public function wishlist(){
        return $this->hasMany('App\Wishlist');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPasswordNotification($token));
    }

    public function coupon(){

        return $this->hasOne('App\Coupon');
    }

    public function couponuser(){

        return $this->belongsTo('App\CouponUser');
    }
    

    
}

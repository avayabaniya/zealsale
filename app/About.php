<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        'banner_image','banner_image_title','banner_image_text',
        'about_first_image','about_first_title','about_first_text',
        'about_second_image','about_second_title','about_second_text',
        'about_third_image','about_third_title','about_third_text',
    ];
}

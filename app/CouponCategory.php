<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCategory extends Model
{

    protected $dates = ['deleted_at'];
    protected $table = 'coupon_categories';
    protected $fillable = ['name', 'description', 'slug', 'featured_image'];

    protected function coupon(){

        return $this->hasMany('App\Coupon');
    }


}

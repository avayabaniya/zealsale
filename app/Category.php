<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Product;
use App\Advertisement;

class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'categories';
    protected $fillable = ['name','parent_id', 'description', 'slug', 'status', 'image'];

    public function categories() {
        return $this->hasMany('App\Category','parent_id');
    }
    
    public function product()
    {
        return $this->hasMany('App\Product');
    }

    protected static function boot(){
        parent::boot();

        static::deleting(function($category){
            foreach($category->categories as $cat){
                $cat->delete();
                foreach($cat->product as $catss){
                    $catss->delete();
                }
        
            }
        });
            
     }
    
    

    public function advertisement() {
        return $this->hasMany('App\Advertisement');
    }
}

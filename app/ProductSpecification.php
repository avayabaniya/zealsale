<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSpecification extends Model
{
    
    protected $fillable = [
        'id',
        'product_id',
        'title',
        'description'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}

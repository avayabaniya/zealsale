<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Slider;
use View;
use App\SiteSetting;
use App\Product;
use App\Category;
use App\Order;
use App\ProductRating;
use App\Brand;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer(['frontend.includes.slider'], function($view){
            $view->with('sliders', Slider::latest()->get());
          });
          
        View::composer(['*'], function($view){
            $view->with('sites_data', SiteSetting::whereId(1)->first());
          });
          
        View::composer(['*'], function($view){
            $view->with('categories_data', Category::whereParentId(0)->inRandomOrder()->take(20)->get());
          }); 

        View::composer(['*'], function($view){
            $view->with('onsale_product', Product::with('category', 'attributes')->where('onsale', '!=', null)->inRandomOrder()->whereStatus(1)->take(3)->get());
          }); 
          
        View::composer(['*'], function($view){
            $view->with('featuredProducts', Product::with('category', 'attributes')->whereOnsale(NULL)->inRandomOrder()->whereStatus(1)->take(3)->get());
          }); 
        
        View::composer(['*'], function($view){
          $view->with('categories_search', Category::where('parent_id','!=',0)->get());
        });
        
        View::composer(['*'], function($view){
          $view->with('top_rated', ProductRating::where('rating',5)->groupBy('product_id')->havingRaw('COUNT(*) > 1')->inRandomOrder()->take(3)->get());
        }); 
        
        View::composer(['*'], function($view){
          $view->with('brand_slider', Brand::inRandomOrder()->get());
        }); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

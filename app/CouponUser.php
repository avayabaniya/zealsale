<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponUser extends Model
{
    
    protected $fillable= ['customer_id','coupon_id','count'];

   public function customer(){
       return $this->belongsTo('App\Customer');
   }

   public function coupon(){
    
        return $this->belongTo('App\Coupon','coupon_id');
   } 
}

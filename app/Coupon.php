<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'coupon_code','amount_type','amount','expiry_date','status' ,'couponcategory_id','product_id','customer_id'
    ];

    public function category(){

        return $this->belongsTo('App\CouponCategory');

    }

    public function coupon(){

        return $this->hasMany('App\Product');
    }

    
    public function customer(){

        return $this->belongsTo('App\Customer');

    }

    public function coupons(){

        return $this->belongsTo('App\CouponUser');
    }

    public function couponcategory(){

        return $this->belongsTo('App\CouponCategory');
    }

}


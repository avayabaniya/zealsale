<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    protected $fillable = [
        'id',
        'organization_name',
        'email',
        'logo',
        'support_time',
        'phone',
        'address',
        'about',
        'facebook_uri',
        'instagram_uri',
        'linkedin_uri',
        'youtube_uri',
        'twitter_uri',
        'viber_uri',
        'whatsapp_uri',
    ];
}

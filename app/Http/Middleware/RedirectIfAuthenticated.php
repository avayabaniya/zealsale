<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     return redirect('/admin/dashboard');
        // }else{
        //     return redirect()->action('UsersController@login')->with('flash_message_error','Please Login to continue');
        // }

        // return $next($request);

        switch ($guard) {
            case 'customer':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('index');
                }
                break;

            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/admin/dashboard');
                }
                break;
        }

        return $next($request);
    }
}

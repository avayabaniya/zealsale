<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CouponCategory;
use Image;
use Illuminate\Support\Facades\Input;
use Session;
use File;

class CouponCategoryController extends Controller
{
    
    public function index()
    {
        $couponcategories = CouponCategory::orderBy('id', 'desc')->get();
        return view('backend.couponcategory.index')->with(compact('couponcategories'));
    }

    
    public function create(Request $request)
    {
        
        if ($request->isMethod('post')){
            $data = $request->all();

        //dd($request);
       
        if($request->hasFile('featured_image')){
            $file = Input::file('featured_image');
            if($file->isValid()) {
                //die('hfhdhg');

                $extension = $file->getClientOriginalExtension();
			$filename = rand(111,99999).'.'.$extension;
            

            $large_image_path = 'public/images/backend_images/couponcategory/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/couponcategory/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/couponcategory/small/'.$filename;

            //Resize Images
            Image::make($file)->save($large_image_path);
            Image::make($file)->resize(600,600)->save($medium_image_path);
            Image::make($file)->resize(360,280)->save($small_image_path);

             } else{
                 $filename = $data('current_image');
             }
            
		}
		
		$couponcategory = CouponCategory::create([
			'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name),
            'meta_title' => $request->meta_title,
			'meta_content' => $request->meta_content,
      'meta_description' => str_limit($request->meta_description, 100),
            'meta_keywords' => $request->meta_keywords, 
            'featured_image' => $filename,
           
		]);
		Session::flash('success', 'Coupon Category Was Inserted Successfully');
        return redirect()->route('couponcategory.index');
        }

        return view('backend.couponcategory.create');
    }         

    
    public function edit(Request $request, $id)
    {
        $couponcategory = CouponCategory::where('id',$id)->first();
        //dd($couponcategory);

        if ($request->isMethod('post')){
            $data = $request->all();

        //dd($request);
       
        if($request->hasFile('featured_image')){
            $file = Input::file('featured_image');
            if($file->isValid()) {
                //die('hfhdhg');

                $extension = $file->getClientOriginalExtension();
			$filename = rand(111,99999).'.'.$extension;
            

            $large_image_path = 'public/images/backend_images/couponcategory/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/couponcategory/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/couponcategory/small/'.$filename;

            //Resize Images
            Image::make($file)->save($large_image_path);
            Image::make($file)->resize(600,600)->save($medium_image_path);
            Image::make($file)->resize(360,280)->save($small_image_path);

             } else{
                 $filename = $data('current_image');
             }
            
		}
		
		
			$couponcategory->name = $request->name;
            $couponcategory->description = $request->description;
            $couponcategory->slug = str_slug($request->name);
            $couponcategory->meta_title = $request->meta_title;
            $couponcategory->meta_content = $request->meta_content;
            $couponcategory->meta_description = $request->meta_description;
            $couponcategory->meta_keywords = $request->meta_keywords;	 
            $couponcategory->featured_image = $filename;
           
		
		Session::flash('success', 'Coupon Category Updated Successfully');
        return redirect()->route('couponcategory.index')->with(compact('couponcategory'));
        }

        return view('backend.couponcategory.edit')->with(compact('couponcategory'));
    }         

    

   
    public function delete($id=null)
    {
        $couponcategory = CouponCategory::where('id', $id)->first();

        //dd($category);
        $couponcategory->delete();
        return redirect( route('couponcategory.index'))->with('flash_message_success', 'Coupon Category Successfully Deleted');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Order;
use App\ProductRating;

class UsersController extends Controller
{
    public function login(Request $request)
    {
        if($request->isMethod('post')) {
            $data = $request->input();

            if($data['email'] ==  NULL)
            {
                return redirect()->back()->with('flash_message_error','Email is required');
            }
            if($data['password'] ==  NULL)
            {
                return redirect()->back()->with('flash_message_error','Password is required');
            }

            if(Auth::attempt(['email'=>$data['email'], 'password' => $data['password'], 'admin' => 1]))
            {
                return redirect()->route('admin.dashboard');
            }
            else
            {
                return redirect()->back()->with('flash_message_error','These credentials do not match our records');
            }
        }
        return view('admin.admin_login')->with('flash_message_error','These credentials do not match our records');
    }

    public function logout(){
        Auth::guard()->logout();
        return redirect(route('admin.login'))->with('flash_message_success','Logged Out Successfully');
    }

    public function settings(Request $request)
    {
        if ($request->isMethod('post')){

            $this->validate($request,
                    ['email' => 'required|email',
                    'oldPassword' => 'required',
                    'newPassword' => 'required',
                    'confirmPassword' => 'required']);

            $data = $request->all();

            $check_password = User::where(['email' => Auth::user()->email])->first();

            $current_password = $data['oldPassword'];
            if(Hash::check($current_password,$check_password->password) && $data['email'] == $check_password->email)
            {
                $password = bcrypt($data['newPassword']);
                User::where('email',Auth::user()->email)->update(['password'=>$password]);
                return redirect()->back()->with('flash_message_success','Password updated Successfully!');
            }
            else {
                return redirect()->back()->with('flash_message_error','Incorrect Current Password!');
            }
        }

        return view('admin.settings');
    }

    public function chkUserPassword(Request $request){

        $data = $request->all();

        $current_password = $data['current_pwd'];
        $user_id = Auth::User()->id;
        $check_password = User::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function chkUserEmail(Request $request){

        $data = $request->all();

        $current_email = $data['current_email'];
        $user_id = Auth::User()->id;
        $check_email = User::where('id', $user_id)->first();
        if ($current_email === $check_email->email){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function dashboard()
    {
        $recent_ratings = ProductRating::latest()->take(5)->get();
        $order_received = Order::all();
        $total_revenue  = Order::whereOrderStatus('completed')->get();
        $total_sales = Order::whereOrderStatus('completed')->get();
        $total_cancelled = Order::whereOrderStatus('cancelled')->get();
        $order_list = Order::all();
        return view('admin.dashboard',compact('total_revenue','total_sales','total_cancelled','order_list','order_received','recent_ratings'));
    }

}

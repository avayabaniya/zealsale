<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tac;

class TACController extends Controller
{
    public function index ()
    {
        $terms = Tac::all();
        return view ('frontend.terms_and_conditions')->with('terms',$terms);
    }
}

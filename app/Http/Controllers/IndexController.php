<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductRating;
use App\Advertisement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index ()
    {
        $advertisements = Advertisement::inRandomOrder()->take(1)->get();
        $specialOffer = Product::with('category', 'attributes')->where('onsale', '!=', null)->inRandomOrder()->take(1)->whereStatus(1)->get();
        $products = Product::with('category')->whereOnsale(null)->inRandomOrder()->take(6)->whereStatus(1)->get();
        $productsFeatured = Product::with('category')->where('featured', 1)->inRandomOrder()->take(6)->whereStatus(1)->get();
        $productOnSale = Product::with('category', 'attributes')->where('onsale', '!=', null)->inRandomOrder()->take(6)->whereStatus(1)->get();
        $topRated = ProductRating::where('rating',5)->groupBy('product_id')->havingRaw('COUNT(*) > 1')->inRandomOrder()->take(6)->get();
        $latestProducts = Product::with('category')->latest()->take(20)->whereStatus(1)->get();
        $bestSellerProducts = Product::with('category')->inRandomOrder()->whereStatus(1)->get();
        $mainCategories = Category::where('parent_id', 0)->orderBy('featured', 'desc')->get();
        if(!empty($mainCategories[0])){
        $firstSubMain = Category::where('parent_id', $mainCategories[0]->id)->get()->toArray();
        $subcat = [];
        foreach ($firstSubMain as $key => $value)
        {
            array_push($subcat, $value['id']);
        }


        $firstCat = Product::with('category')->whereIn('category_id', $subcat)->inRandomOrder()->take(2)->whereStatus(1)->get();

        $midCat = Product::with('category')->whereIn('category_id', $subcat)->inRandomOrder()->take(1)->whereStatus(1)->get();
        $lastCat = Product::with('category')->whereIn('category_id', $subcat)->inRandomOrder()->take(2)->whereStatus(1)->get();
        }
        else{
            $firstCat = Product::all();
            $midCat = Product::all();
            $lastCat = Product::all();
        }

        $subMainCategories = Category::where('parent_id', 0)->inRandomOrder()->get();
        return view ('frontend.index')->with(compact('firstCat','productsFeatured','midCat','lastCat','specialOffer','products','productOnSale', 'latestProducts', 'bestSellerProducts', 'mainCategories', 'subMainCategories','topRated','advertisements'));

    }

    
    public function search(Request $request){
        if($request->method('get'))
        {
            $name = Input::get('search');
            return view('forntend.search');
        }
    }
}

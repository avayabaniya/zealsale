<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Customer;
use App\User;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $socialuser = Socialite::driver($provider)->user();

        $user = Customer::where('provider_id',$socialuser->getId())->first();

        //dd($socialuser);
        if(!$user){

            $user = Customer::create([

                'email' => $socialuser->getEmail(),
                'full_name' => $socialuser->getName(),
                'provider_id' => $socialuser->getId(),
    
            ]);
       
        }     

        Auth::guard('customer')->login($user, true);

        Session::put('full_name', $user->full_name);
        

        return redirect()->route('customer.dashboard');

        // $user->token;
    }

    
}


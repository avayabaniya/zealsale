<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyAccountController extends Controller
{
    public function myaccount()
    {
        return view ('frontend.account.my_account');
    }
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use Auth;
use Session;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::latest()->get();
        return view ('backend.faq.index')->with('faqs',$faqs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('backend.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'question' => 'required|unique:faqs|max:150|min:10',
            'answer' => 'required|max:1000|min:20',
        ]);

        $faq = Faq::create([
            'question' => ucwords(strtolower($request->question)),
            'slug' => str_slug($request->question,'-'),
            'answer' => $request->answer,
        ]);

        Session::flash('flash_message_success', 'New FAQ Created Successfully');
        return redirect()->route('faq.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $faq = Faq::where('slug',$slug)->first();
        return view ('backend.faq.edit')->with('faq',$faq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'question' => 'required|max:150|min:10',
            'answer' => 'required|max:1000|min:20',
        ]);

        $faq = Faq::findOrFail($id);
        
        $faq->question = ucwords(strtolower($request->question));
        $faq->slug = str_slug($request->question, '-');
        $faq->answer = $request->answer;
        $faq->save();

        Session::flash('flash_message_success', 'FAQ Updated Successfully ');
        return redirect()->route('faq.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        return redirect()->back()->with('flash_message_warning', 'FAQ Deleted ! ');
    }
}

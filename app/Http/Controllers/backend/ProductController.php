<?php

namespace App\Http\Controllers\backend;

use App\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\ProductAttribute;
use App\ProductSpecification;
use App\ProductRating;
use App\Customer;
use Auth;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Facades\Input;
use Image;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return view ('backend.product.index')->with('products',$products)->with('categories',$categories);
    }

    public function softdelete()
    {
        $products = Product::onlyTrashed()->paginate(12);
        return view ('backend.product.soft_delete')->with('products',$products);
    }

    public function restore($id)
    {
        $product = Product::withTrashed()->where('id',$id)->first();
        $product->restore();
        Session::flash('flash_message_success', 'Trashed Product Restored Successfully');
        return redirect()->route('product.index');
    }

    public function forcedelete($id)
    {        
        $product = Product::withTrashed()->where('id',$id)->first();
        $product->forceDelete();
        return redirect()->back()->with('flash_message_error', 'Product Trashed permanently');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainCategories = Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option value='' selected disabled>-- Select Category --</option>";

        foreach ($mainCategories as $cat)
        {
            $categories_dropdown .= "<option value='".$cat->id."'disabled>"."<strong>".strtoupper($cat->name)."</strong>"."</option>";
            $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
            foreach ($sub_categories as $sub_cat) {
                $categories_dropdown .= "<option value='".$sub_cat->id."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";
            }
        }

        $categories = Category::all();
        return view ('backend.product.create',compact('categories', 'categories_dropdown'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_name' => 'required|unique:products|max:100|min:6',
            'category_id' => 'required',
            'product_code' => 'required|max:30|min:4',
            'product_color' => 'required|max:30|min:3',
            'product_price' => 'required|max:30',
            // 'file' => 'required',
            'description' => 'required|max:5000|min:10',
        ]);
        
        // dd($request);

        $image_tmp = Input::file('image');
        
        if ($image_tmp->isValid()) {
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
            $large_image_path = 'public/images/backend_images/product/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/product/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/product/small/'.$filename;

            //Resize Images
            Image::make($image_tmp)->save($large_image_path);
            Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
            Image::make($image_tmp)->resize(250,232)->save($small_image_path);
        }

        if (empty($request->status)){
            $status = 0 ;
        } else {
            $status = 1 ;
        }

        $product = Product::create([
            'product_name' => ucwords(strtolower($request->product_name)),
            'slug' => str_slug($request->product_name,'-'),
            'category_id' => $request->category_id,
            'product_code' => $request->product_code,
            'product_color' => $request->product_color,
            'price' => $request->product_price,
            'image' => $filename,
            'description' => $request->description,
            'status' => $status,
            'meta_title' => $request->meta_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'meta_content' => $request->meta_content,
        ]);

        $productAttribute = ProductAttribute::create([
           'product_id' => $product->id,
            'sku' => $product->product_code,
            'size' => 'Medium',
            'price' => $product->price,
            'stock' => 1,
        ]);

        Session::flash('flash_message_success', 'New Product Created Successfully');
        return redirect()->route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where(['slug'=>$slug])->first();
        return view ('backend.product.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {

        $categories = Category::all();
        $product = Product::where(['slug'=>$slug])->first();

        $mainCategories = Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option value='' selected disabled>-- Select Category -- </option>";

        foreach ($mainCategories as $cat)
        {
            //Check for main categories
            if ($cat->id == $product->category_id)
            {
                $selected ="selected";
            }else {
                $selected="";
            }
            $categories_dropdown .= "<option disabled value='".$cat->id."' ".$selected.">".strtoupper($cat->name)."</option>";
            $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
            foreach ($sub_categories as $sub_cat)
            {
                //check for sub categories
                if ($sub_cat->id == $product->category_id)
                {
                    $selected = "selected";
                }else {
                    $selected = "";
                }
                $categories_dropdown .= "<option value='".$sub_cat->id."' ".$selected.">&nbsp;--&nbsp;".$sub_cat->name."</option>";
            }
        }



        return view ('backend.product.edit')->with('product',$product)->with('categories',$categories)->with(compact('categories_dropdown'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        
        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');

            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/product/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/product/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/product/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(250,232)->save($small_image_path);

                //Store image name in products table
                $product->image = $filename;
                $unlink = $request->current_image;
            }else{
                $product->image = $request->current_image;
            }
        }
        
        if (empty($request->status)){
            $status = 0;
        }else{
            $status = 1;
        }

        $product->product_name = ucwords(strtolower($request->product_name));
        $product->slug = str_slug($request->product_name, '-');
        $product->category_id = $request->category_id;
        $product->product_code = $request->product_code;
        $product->product_color = $request->product_color;
        $product->status = $status;
        $product->price = $request->product_price;
        $product->description = $request->description;
        $product->meta_title = $request->meta_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->meta_description = $request->meta_description;
        $product->meta_content = $request->meta_content;
        $product->save();

        Session::flash('flash_message_success', 'Product Updated Successfully ');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('product.softdelete')->with('flash_message_warning', 'Product Trashed ');
    }

    public function attribute(Request $request, $id)
    {
        $product = Product::with('attributes')->where(['id'=>$id])->first();
        $categories = Category::all();
        $attributes = ProductAttribute::all();


        if($request->isMethod('post'))
        {
            $data = $request->all();
            //$data brings back the 3 arrays inside another array making it an associative array
            //echo "<pre>";print_r($data);die;

            foreach ($data['sku'] as $key => $val) {
                //$key == '0','1'... $val = actual values
                if (!empty($val)){
                    //Prevent duplicate SKU
                    $attrCountSKU = ProductAttribute::where('sku',$val)->count();
                    if ($attrCountSKU > 0){
                        return redirect()->back()->with('flash_message_error', 'SKU already exists! Please add another SKU.');
                    }

                    //echo "<pre>"; print_r($id);die;

                    //Prevent duplicate size
                    $attrCountSize = ProductAttribute::where(['product_id' => $id])->where( ['size'=>$data['size'][$key]])->count();
                    // echo "<pre>"; print_r($attrCountSize);die;
                    if ($attrCountSize > 0){
                        return redirect()->back()->with('flash_message_error', '"'.$data['size'][$key].'" size already exists for this product! Please add another Size.');
                    }

                    $attribute = new ProductAttribute();
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->size = $data['size'][$key]; //array whose first index is 'size' and 2nd index is the value of $key
                    $attribute->price = $data['price'][$key]; //array whose first index is 'price' and 2nd index is the value of $key
                    $attribute->stock = $data['stock'][$key];
                    $attribute->save();

                }
                //In 1 loop the value of $key is same
                //So all the value having the same $key are store in one single object of ProductAttribute() model with product_id
                // echo "<pre>";print_r($attribute);die;
            }
            /* $productDetails = json_decode(json_encode($productDetails));
             echo "<pre>";print_r($productDetails);die;*/

            if (empty($attribute)) {
                return redirect()->back()->with('flash_message_error', 'SKU not entered');
            }

            return redirect()->back()->with('flash_message_success','Product Attribues has been added successfully!', compact('product'));
        }
        return view ('backend.product.attribute',compact('product','categories','attributes'));

    }

    public  function  editAttributes(Request $request, $id = null){
        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            foreach ($data['idAttr'] as $key => $attr) {
                ProductAttribute::where(['id'=>$data['idAttr'][$key]])->update([
                    'price'=>$data['price'][$key],
                    'stock'=>$data['stock'][$key]
                ]);
            }
            return redirect()->back()->with('flash_message_success', 'Products Attributes has been updates successfully!');

        }
    }

    public function deleteAttribute($id = null) {
        ProductAttribute::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_success', 'Attribute has been deleted successfully');
    }


    
    public function specification(Request $request, $id)
    {
        $product = Product::with('specifications')->where(['id'=>$id])->first();
        $categories = Category::all();
        $specifications = ProductSpecification::all();


        if($request->isMethod('post'))
        {
            $data = $request->all();
            //$data brings back the 3 arrays inside another array making it an associative array
            //echo "<pre>";print_r($data);die;

            foreach ($data['title'] as $key => $val) {
                //$key == '0','1'... $val = actual values
                if (!empty($val)){
                    //Prevent duplicate SKU
                    $attrCountTitle = ProductSpecification::where('title',$val)->where('product_id', $product->id)->count();
                    if ($attrCountTitle > 0){
                        return redirect()->back()->with('flash_message_error', 'Specification already exists! Please add another Specification.');
                    }

                    //echo "<pre>"; print_r($id);die;

                    //Prevent duplicate size
                    $attrCountDescription = ProductSpecification::where(['product_id' => $id])->where( ['description'=>$data['description'][$key]])->count();
                    // echo "<pre>"; print_r($attrCountSize);die;
                    if ($attrCountDescription > 0){
                        return redirect()->back()->with('flash_message_error', '"'.$data['description'][$key].'" Description already exists for this product! Please add another Description.');
                    }

                    $specification = new ProductSpecification();
                    $specification->product_id = $id;
                    $specification->title = $val;
                    $specification->description = $data['description'][$key]; //array whose first index is 'size' and 2nd index is the value of $key
                    $specification->save();

                }
                //In 1 loop the value of $key is same
                //So all the value having the same $key are store in one single object of Productspe$specification() model with product_id
                // echo "<pre>";print_r($specification);die;
            }
            /* $productDetails = json_decode(json_encode($productDetails));
             echo "<pre>";print_r($productDetails);die;*/

            if (empty($specification)) {
                return redirect()->back()->with('flash_message_error', 'Specification not entered');
            }

            return redirect()->back()->with('flash_message_success','Product Specifications has been added successfully!', compact('product'));
        }


        return view ('backend.product.specification',compact('product','categories','specifications'));


    }

    public  function  editSpecifications(Request $request, $id = null){
        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            foreach ($data['idAttr'] as $key => $attr) {
                ProductSpecification::where(['id'=>$data['idAttr'][$key]])->update([
                    'title'=>$data['title'][$key],
                    'description'=>$data['description'][$key]
                ]);
            }
            return redirect()->back()->with('flash_message_success', 'Products Specifications has been updates successfully!');

        }
    }

    public function deleteSpecification($id = null) 
    {
        ProductSpecification::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_success', 'Products Specification has been deleted successfully');
    }

    public function image(Request $request, $id)
    {
        $product = Product::with('attributes', 'images')->where(['id'=>$id])->first();
        $categories = Category::all();
        $attributes = ProductAttribute::all();


        if($request->isMethod('post'))
        {
            if ($request->hasFile('image')){
                $files = $request->file('image');
                foreach ($files as $file){

                    /*echo "<pre>"; print_r($data); die;*/
                    //Upload Image after resizing
                    $image = new ProductImage;
                    $extension = $file->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;

                    $large_image_path = 'public/images/backend_images/product/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/product/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/product/small/'.$filename;

                    //Resize Images
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(600,600)->save($medium_image_path);
                    Image::make($file)->resize(250,232)->save($small_image_path);
                    $image->image = $filename;
                    $image->product_id = $product['id'];
                    $image->save();

                }

            }
            return redirect()->back()->with('flash_message_success','Product Images has been added successfully', compact('image'));
        }


        return view ('backend.product.image',compact('product','categories','attributes'));


    }

    public function deleteImage($id = null)
    {
        $unlink = ProductImage::where(['id' => $id])->first();

        if (!empty($unlink->image))
        {
            $large_image_path = 'public/images/backend_images/product/large/';
            $medium_image_path = 'public/images/backend_images/product/medium/';
            $small_image_path = 'public/images/backend_images/product/small/';

            //Delete Large Image if  exists in folder
            if (file_exists($large_image_path.$unlink->image)) {
                unlink($large_image_path.$unlink->image);
            }

            //Delete Medium Image if  exists in folder
            if (file_exists($medium_image_path.$unlink->image)) {
                unlink($medium_image_path.$unlink->image);
            }

            //Delete Small Image if  exists in folder
            if (file_exists($small_image_path.$unlink->image)) {
                unlink($small_image_path.$unlink->image);
            }
        }

        ProductImage::where(['id' => $id])->delete();


        return redirect()->back()->with('flash_message_success', 'Image has been deleted successfully');
    }

    public function getProductPrice(Request $request)
    {
        //return "aaaa";

        $data = $request->all();

         $proArr = explode("-", $data['idSize']);

         $proAttr = ProductAttribute::where(['product_id' => $proArr[0], 'size' => $proArr[1]])->first();
         echo $proAttr->price;
         echo "#";
         echo $proAttr->stock;
    }

    public function onsale(Request $request, $id)
    {


        //$saleProduct = Product::where('id',$id)->get();

        $arr = explode('-', $request->discount);

        DB::table('products')
            ->where('id', $id)
            ->update(['onsale' => $arr[0]]);

        return redirect()->back()->with('flash_message_success', 'Discount Successfully applied');


    }

    public function removeOnsale($id){
        DB::table('products')
            ->where('id', $id)
            ->update(['onsale' => null]);

        return redirect()->back()->with('flash_message_success', 'On sale offer successfully removed');
    }

    // Rating store and update
    public function ratingStore(Request $request)
    {
        $rating = New ProductRating();

        $data = $request->all();

        //user email session
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
            return redirect()->route('my.account')->with('flash_message_error', 'Please Login First');
        }else{
            $data['user_email'] = $userEmail;
        }

        $product = Product::where('id', $data['product_id'])->first();
        $customerId = Customer::where('email',$userEmail)->first();

        $ratingCustomerCount = ProductRating::where('customer_id',$customerId->id)->where('product_id',$product->id)->count();

        if ($ratingCustomerCount > 0){
            $updateRating = ProductRating::where('customer_id',$customerId->id)->where('product_id',$product->id)->first();
            $updateRating->product_id = $product->id;
            if(Auth::guard('customer')->check()){
                $updateRating->customer_id = $customerId->id;
            }

            $updateRating->rating = $data['rate'];
            $updateRating->save();

            Session::flash('success', 'Rating updated successfully');
            return redirect()->back();
        }

        $rating->product_id = $product->id;
        if(Auth::guard('customer')->check()){
        $rating->customer_id = $customerId->id;
        }

        $rating->rating = $data['rate'];
        $rating->save();

        return redirect()->back();

    }

    public function featuredProduct (Request $request)
    {
        $product = Product::where('id', $request->product_id)->first();
        $product->featured = $request->value;
        $product->save();

        return "featured status changed";
    }
}

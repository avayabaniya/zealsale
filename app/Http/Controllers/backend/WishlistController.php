<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Customer;
use App\Wishlist;
use Auth;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $wishlist = New Wishlist();
        $data = $request->all();

        //   Checking for duplicate products
            //   $countProduct = DB::table('wishlists')->where([
            //    'product_id' => $data['product_id'],
            //    'customer_id' => $data['customer_id'],
            //    'session_id' => $session_id
            //  ])->count();
 
        //    if ($countProduct > 0) {
        //         return redirect()->back()->with('error', 'Product already exists in Wishlist!');
        //       }
        //   else
        //      {
        //         return redirect()->with('success', 'Product has been successfully added to wishlist');
        //      }
   

        //user email session
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
        }else{
            $data['user_email'] = $userEmail;
        }

        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }

        $product = Product::where('id', $data['product_id'])->first();
        $customerId = Customer::where('email',$userEmail)->first();


        $wishlist->product_id = $product->id;
        if(Auth::guard('customer')->check()){
        $wishlist->customer_id = $customerId->id;
        }
        $wishlist->session_id = $session_id;

        $wishlist->save();

            Session::flash('success', 'Product has been added to Wishlist ');
            return redirect()->back();

    }

    public function save(Request $request)
    {
        $wishlist = New Wishlist();
        $data = $request->all();


        //user email session
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
            return redirect(route('customer.login'))->with('flash_message_error', 'Please register to checkout');
        }else{
            $data['user_email'] = $userEmail;
        }

        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }

        $product = Product::where('id', $data['product_id'])->first();
        $customerId = Customer::where('email',$userEmail)->first();


        $wishlist->product_id = $product->id;
        if(Auth::guard('customer')->check()){
        $wishlist->customer_id = $customerId->id;
        }
        $wishlist->session_id = $session_id;

        $wishlist->save();

            Session::flash('success', 'Product has been added to Wishlist ');
            return redirect()->back();


    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlist = Wishlist::findOrFail($id);
        $wishlist->delete();
        return redirect()->back()->with('flash_message_error', 'Wishlist Removed !!');
    }
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Image;
use File;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view ('backend.slider.index')->with('sliders',$sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|unique:sliders|max:50|min:6',
            'price' => 'required|numeric',
            'image' => 'required',
        ]);

        $image_tmp = Input::file('image');
        
        if ($image_tmp->isValid()) {
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
            $large_image_path = 'public/images/backend_images/slider/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/slider/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/slider/small/'.$filename;

            //Resize Images
            Image::make($image_tmp)->resize(1920,466)->save($large_image_path);
            Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
            Image::make($image_tmp)->resize(300,300)->save($small_image_path);
        }

        $slider = Slider::create([
            'title' => ucwords(strtolower($request->title)),
            'slug' => str_slug($request->title,'-'),
            'price' => $request->price,
            'image' => $filename,
        ]);

        Session::flash('flash_message_success', 'New Slider Created Successfully');
        return redirect()->route('slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'new_title' => 'required|max:50|min:6',
            'new_price' => 'required|numeric',
        ]);
        $slider = Slider::findOrFail($id);
        
        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/slider/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/slider/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/slider/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->resize(1920,466)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                //Store image name in Slider table
                $slider->image = $filename;
                $unlink = $request->current_image;
            }else{
                $slider->image = $request->current_image;

            }
        }

        $slider->title = ucwords(strtolower($request->new_title));
        $slider->slug = str_slug($request->new_title, '-');
        $slider->price = $request->new_price;
        $slider->save();

        Session::flash('flash_message_success', 'Slider Updated Successfully ');
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $image_path = $slider->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }   
        $slider->delete();
        return redirect()->back()->with('flash_message_error', 'Slider Deleted');
    }
}

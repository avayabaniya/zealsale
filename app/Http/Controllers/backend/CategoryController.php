<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Image;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        //$parentCategory = Category::where('parent_id', '!0');

        /*$categories =json_decode(json_encode($categories));die;*/
        //return view('admin.categories.view_categories')->with(compact('categories'));
        return view('backend.category.index')->with(compact('categories'));

    }

    public function create(Request $request)
    {
        

        if ($request->isMethod('post')){
            $data = $request->all();


            if (empty($data['description'])){
                $data['description'] = '';
            }

            $category = new Category;
            $category->name = ucwords(strtolower($data ['category_name']));
            $category->parent_id =$data['parent_id'];
            $category->description = ucwords(strtolower($data['description']));
            $category->slug = str_slug($request->category_name,'-' );
            $category->meta_title = ucwords(strtolower($data['meta_title']));
            $category->meta_description = ucwords(strtolower($data['meta_description']));
            $category->meta_content = ucwords(strtolower($data['meta_content']));
            $category->meta_keywords = $data['meta_keywords'];

            if ($request->hasFile('featured_image')){
                $image_tmp = Input::file('featured_image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/images/backend_images/category/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/category/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/category/small/'.$filename;

                    //Resize Images
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(250,232)->save($small_image_path);

                    //Store image name in products table
                    $category->featured_image = $filename;
                }else{
                    $category->featured_image = '';

                }
            }


            $category->save();

            //redirect to view category after adding with messages
            return redirect(route('category.index'))->with('flash_message_success', 'Category Successfully Added');

        }

        $levels = Category::where('parent_id', 0)->get();

        return view('backend.category.create')->with(compact('levels'));


    }

    public function edit(Request $request, $id = null)
    {
        $category = Category::where('id', $id)->first();
        if ($request->isMethod('post'))
        {
            $data = $request->all();
            //dd($data);


            if (empty($data['description'])){
                $data['description'] = '';
            }

            $category->name = ucwords(strtolower($data ['category_name']));
            $category->parent_id =$data['parent_id'];
            $category->description = ucwords(strtolower($data['description']));
            $category->slug = str_slug($request->category_name,'-' );
            $category->meta_title = ucwords(strtolower($data['meta_title']));
            $category->meta_description = ucwords(strtolower($data['meta_description']));
            $category->meta_content = ucwords(strtolower($data['meta_content']));
            $category->meta_keywords = $data['meta_keywords'];


            if ($request->hasFile('featured_image')){
                $image_tmp = Input::file('featured_image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/images/backend_images/category/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/category/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/category/small/'.$filename;

                    //Resize Images
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(250,232)->save($small_image_path);

                    //Store image name in products table
                    $category->featured_image = $filename;
                    $unlink = $data['current_image'];
                }else{
                    $category->featured_image = $data['current_image'];

                }
            }


            $category->save();

            if (!empty($unlink))
            {
                $large_image_path = 'public/images/backend_images/category/large/';
                $medium_image_path = 'public/images/backend_images/category/medium/';
                $small_image_path = 'public/images/backend_images/category/small/';

                //Delete Large Image if  exists in folder
                if (file_exists($large_image_path.$unlink)) {
                    unlink($large_image_path.$unlink);
                }

                //Delete Medium Image if  exists in folder
                if (file_exists($medium_image_path.$unlink)) {
                    unlink($medium_image_path.$unlink);
                }

                //Delete Small Image if  exists in folder
                if (file_exists($small_image_path.$unlink)) {
                    unlink($small_image_path.$unlink);
                }
            }


            //redirect to view category after adding with messages
            return redirect(route('category.index'))->with('flash_message_success', 'Category Successfully Updated');
        }
        $levels = Category::where('parent_id', 0)->get();
        //dd($levels);
        return view('backend.category.edit')->with(compact('category', 'levels'));

    }

    public function delete($id)
    {
        $category = Category::find($id);
        // dd($category);
        $category->delete();

        return redirect()->back()->with('flash_message_success','Categories Deleted !!!');


        // return redirect(route('category.index'))->with('flash_message_success', 'Category Successfully Deleted');

    }

    public function featuredCategory (Request $request)
    {
        $product = Category::where('id', $request->category_id)->first();
        $product->featured = $request->value;
        $product->save();

        return "featured status changed";
    }

}

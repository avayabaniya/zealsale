<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;
use Image;
use File;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::where(['id'=>$id])->first();
        return view ('backend.about.edit')->with('about',$about);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'banner_image_title' => 'required|min:6|max:50',
            'banner_image_text' => 'required|min:10|max:1000',
            'about_first_title' => 'required|min:6|max:50',
            'about_first_text' => 'required|min:10|max:1000',
            'about_second_title' => 'required|min:6|max:50',
            'about_second_text' => 'required|min:10|max:1000',
            'about_third_title' => 'required|min:6|max:50',
            'about_third_text' => 'required|min:10|max:1000',
        ]);

        $about = About::findOrFail($id);
        
        if ($request->hasFile('banner_image')){
            $image_tmp = Input::file('banner_image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/about/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/about/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/about/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(200,60)->save($small_image_path);

                //Store image name in Brand table
                $about->banner_image = $filename;
                $unlink = $request->banner_current_image;
            }else{
                $about->banner_image = $request->banner_current_image;

            }
        }
        
        if ($request->hasFile('about_first_image')){
            $image_tmp = Input::file('about_first_image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/about/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/about/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/about/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(200,60)->save($small_image_path);

                //Store image name in Brand table
                $about->about_first_image = $filename;
                $unlink = $request->first_current_image;
            }else{
                $about->about_first_image = $request->first_current_image;

            }
        }
        
        
        if ($request->hasFile('about_second_image')){
            $image_tmp = Input::file('about_second_image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/about/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/about/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/about/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(200,60)->save($small_image_path);

                //Store image name in Brand table
                $about->about_second_image = $filename;
                $unlink = $request->second_current_image;
            }else{
                $about->about_second_image = $request->second_current_image;

            }
        }
        
        if ($request->hasFile('about_third_image')){
            $image_tmp = Input::file('about_third_image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/about/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/about/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/about/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(200,60)->save($small_image_path);

                //Store image name in Brand table
                $about->about_third_image = $filename;
                $unlink = $request->third_current_image;
            }else{
                $about->about_third_image = $request->third_current_image;

            }
        }
        

        $about->banner_image_title = $request->banner_image_title;
        $about->banner_image_text = $request->banner_image_text;

        $about->about_first_title = $request->about_first_title;
        $about->about_first_text = $request->about_first_text;

        $about->about_second_title = $request->about_second_title;
        $about->about_second_text = $request->about_second_text;

        $about->about_third_title = $request->about_third_title;
        $about->about_third_text = $request->about_third_text;

        $about->save();

        Session::flash('flash_message_success', 'About Us Updated Successfully ');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

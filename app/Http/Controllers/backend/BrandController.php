<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Brand;
use Session;
use Auth;
use File;
use Image;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::latest()->get();
        return view ('backend.brand.index')->with('brands',$brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:brands|max:50|min:2',
            'image' => 'required',
        ]);

        $image_tmp = Input::file('image');
        
        if ($image_tmp->isValid()) {
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
            $large_image_path = 'public/images/backend_images/brand/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/brand/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/brand/small/'.$filename;

            //Resize Images
            Image::make($image_tmp)->save($large_image_path);
            Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
            Image::make($image_tmp)->resize(200,60)->save($small_image_path);
        }

        $brand = Brand::create([
            'name' => ucwords(strtolower($request->name)),
            'slug' => str_slug($request->name,'-'),
            'logo' => $filename,
        ]);

        Session::flash('flash_message_success', 'New Brand Created Successfully');
        return redirect()->route('brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'new_name' => 'required|max:50|min:2',
        ]);

        $brand = Brand::findOrFail($id);
        
        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/brand/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/brand/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/brand/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(200,60)->save($small_image_path);

                //Store image name in Brand table
                $brand->logo = $filename;
                $unlink = $request->current_image;
            }else{
                $brand->logo = $request->current_image;

            }
        }

        $brand->name = ucwords(strtolower($request->new_name));
        $brand->slug = str_slug($request->new_name, '-');
        $brand->save();

        Session::flash('flash_message_success', 'Brand Updated Successfully ');
        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $image_path = $brand->logo;
        if(File::exists($image_path)){
            File::delete($image_path);
        }   
        $brand->delete();
        return redirect()->back()->with('flash_message_error', 'Brand Deleted !');
    }
}

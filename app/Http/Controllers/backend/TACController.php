<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tac;
use Auth;
use Session;

class TACController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tacs = Tac::latest()->get();
        return view ('backend.tac.index')->with('tacs',$tacs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('backend.tac.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|unique:tacs|max:50|min:6',
            'description' => 'required|max:5000|min:20',
        ]);

        $tac = Tac::create([
            'title' => ucwords(strtolower($request->title)),
            'slug' => str_slug($request->title,'-'),
            'description' => $request->description,
        ]);

        Session::flash('flash_message_success', 'New Terms & Conditions Created Successfully');
        return redirect()->route('tac.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $tac = Tac::where('slug',$slug)->first();
        return view ('backend.tac.edit')->with('tac',$tac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tac = Tac::findOrFail($id);
        
        $tac->title = ucwords(strtolower($request->title));
        $tac->slug = str_slug($request->title, '-');
        $tac->description = $request->description;
        $tac->save();

        Session::flash('flash_message_success', 'Terms & Conditions Updated Successfully ');
        return redirect()->route('tac.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tac = Tac::find($id);
        $tac->delete();
        return redirect()->back()->with('flash_message_warning', 'Terms & Conditions Deleted ! ');
    }
}

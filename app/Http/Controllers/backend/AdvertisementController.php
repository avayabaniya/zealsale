<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\Category;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Image;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisements = Advertisement::all();
        $categories = Category::whereParentId(0)->get();
        $advertisement_count = $advertisements->count();
        return view ('backend.advertisement.index',compact('advertisements','categories','advertisement_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required',
            'category_id' => 'required',
        ]);

        $image_tmp = Input::file('image');

        if ($image_tmp->isValid()) {
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(111,99999).'.'.$extension;
            $extra_large_image_path = 'public/images/backend_images/setting/'.$filename;
            $large_image_path = 'public/images/backend_images/setting/large/'.$filename;
            $medium_image_path = 'public/images/backend_images/setting/medium/'.$filename;
            $small_image_path = 'public/images/backend_images/setting/small/'.$filename;

            //Resize Images
            Image::make($image_tmp)->save($large_image_path);
            Image::make($image_tmp)->resize(1170,205)->save($large_image_path);
            Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
            Image::make($image_tmp)->resize(270,428)->save($small_image_path);
        }

        $advertisement = Advertisement::create([

            'category_id' => $request->category_id,
            'image' => $filename,
        ]);

        Session::flash('flash_message_success', 'New Advertisement Created Successfully');
        return redirect()->route('advertisement.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisements = Advertisement::where(['id'=>$id])->first();
        return view ('backend.advertisement.edit',compact('advertisements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertisement = Advertisement::findOrFail($id);

        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/setting/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/setting/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/setting/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(1170,205)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(270,428)->save($small_image_path);

                //Store image name in products table
                $advertisement->image = $filename;
                $unlink = $request->current_image;
            }else{
                $advertisement->image = $request->current_image;

            }
        }

        $advertisement->save();

        Session::flash('flash_message_success', 'Advertisement Updated Successfully ');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisement = Advertisement::find($id);
        $advertisement->delete();
        return redirect()->back()->with('flash_message_error', 'Advertisement Deleted !!');
    }
}

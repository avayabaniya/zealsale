<?php

namespace App\Http\Controllers\backend;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Coupon;
use App\CouponCategory;
use App\Customer;
use App\CouponUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function index(){
        $coupons = Coupon::all();
        $couponcategories = CouponCategory::all();
        return view('backend.coupon.index',compact('coupons','couponcategories'));
    }

    public function store(Request $request)
    {
            $data = $request->all();

            if (empty($data['status'])){
                $data['status'] = 0;
            }else{
                $data['status'] = 1 ;
            }

            $coupon = new Coupon;
            $coupon->coupon_code = $data['coupon_code'];
            // $coupon->category_id = $data['category_id'];
            $coupon->amount = $data['amount'];
            $coupon->amount_type = $data['amount_type'];
            $coupon->expiry_date = $data['expiry_date'];
            $coupon->status = $data['status'];
            $coupon->save();
            return redirect()->back()->with('couponcategories',CouponCategory::all())->with('flash_message_success', 'Coupon has been added Successfully');
    }

    public function update(Request $request, $id)
    {
        $coupon = Coupon::findOrFail($id);
        $data = $request->all();

        // dd($request);

        if (empty($data['new_status'])){
          $data['new_status'] = 0;
        }else{
            $data['new_status'] = 1 ;
        }

        $coupon->coupon_code = $data['coupon_code'];
        $coupon->category_id = $data['category_id'];
        $coupon->amount = $data['amount'];
        $coupon->amount_type = $data['amount_type'];
        $coupon->expiry_date = $data['expiry_date'];
        $coupon->status = $data['new_status'];
        $coupon->save();
        return redirect()->back()->with('flash_message_success', 'Coupon has been Updated Successfully');
    }

    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        return redirect()->back()->with('flash_message_error', 'Coupon Deleted !!');
    }

    public function editusercoupon($id){

        $coupons = Coupon::where('id',$id)->first();
        return view('backend.coupon.edit')->with(compact('coupons'));
    }

    public function applyCoupon(Request $request)
    {

        $discount = [];


        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $data = $request->all();   
        

         $couponCount = Coupon::where('coupon_code', $data['coupon_code'])->count();
        if ($couponCount == 0){
            return redirect()->back()->with('flash_message_error', 'This Coupon does not exits!');
        }else {
            //Will perform other checks like active/inactive, Expiry date..

            //Get Coupon Details
            $couponDetails = Coupon::where('coupon_code', $data['coupon_code'])->first();

            //If coupon is Inactive
            if ($couponDetails->status == 0) {
                return redirect()->back()->with('flash_message_error', 'This coupon is not active!');
            }

            //If coupon is Expired
            $time = strtotime($couponDetails->expiry_date);

            $expiry_date = date('Y-m-d',$time);
            //dd($expiry_date);

            //$expiry_date = $couponDetails->expiry_date;
            $current_date = date('Y-m-d');

            if ($expiry_date < $current_date) {
                return redirect()->back()->with('flash_message_error', 'This coupon is expired');
            }

            //Coupon is valid
            Session::put('CartId', $data['cart_id']);
            $item = DB::table('carts')->where(['id' => $data['cart_id']])->first();
            $total_amount = ($item->price * $item->quantity);

            if ($couponDetails->amount_type == "Fixed") {
                $couponAmount = $couponDetails->amount;
            }else{
                $couponAmount = $total_amount * ($couponDetails->amount/100);
            }

        
            
            //set current discount in cart table
            DB::table('carts')->where('id', $data['cart_id'])->update(['discount'=> $couponAmount]);
            $carts = DB::table('carts')->where('user_email' , Session::get('Email'))->get();
            $total_discount = 0;
            foreach ($carts as $cart){
                $total_discount = $total_discount + $cart->discount;
            }
            // echo "<pre>"; print_r($cartInfo); die;
            
            

            Session::put('CouponAmount', $couponAmount);
            Session::put('CouponCode', $data['coupon_code']);
            Session::put('TotalDiscount', $total_discount);

            return redirect()->back()->with('flash_message_success', 'Coupon successfully updated. You will recieve your discount');

        }


    }

    public function applyusercoupon(Request $request)
    {


        $data = $request->all();

        // dd($data);
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
            return redirect()->route('my.account')->with('flash_message_error', 'Please Login First');
        }else{
            $data['user_email'] = $userEmail;
        }

        $cid = Auth::guard('customer')->id();
        //  dd($cid);
        $couponuser = CouponUser::where('coupon_id',1)->where('customer_id',$cid)->where('count',1)->count();
        // dd($couponuser);

        
        if($couponuser == 0)
       
        {
             $discount = [];


            Session::forget('CouponAmount');
            Session::forget('CouponCode');
    
            $data = $request->all();   
            
    
             $couponCount = Coupon::where('coupon_code', $data['coupon_code'])->count();
            if ($couponCount == 0){
                return redirect()->back()->with('flash_message_error', 'This Coupon does not exits!');
            }else {
                //Will perform other checks like active/inactive, Expiry date..
    
                //Get Coupon Details
                $couponDetails = Coupon::where('coupon_code', $data['coupon_code'])->first();
    
                //If coupon is Inactive
                if ($couponDetails->status == 0) {
                    return redirect()->back()->with('flash_message_error', 'This coupon is not active!');
                }
    
                //If coupon is Expired
                $time = strtotime($couponDetails->expiry_date);
    
                $expiry_date = date('Y-m-d',$time);
                //dd($expiry_date);
    
                //$expiry_date = $couponDetails->expiry_date;
                $current_date = date('Y-m-d');
    
                if ($expiry_date < $current_date) {
                    return redirect()->back()->with('flash_message_error', 'This coupon is expired');
                }
    
                //Coupon is valid
                Session::put('CartId', $data['cart_id']);
                $item = DB::table('carts')->where(['id' => $data['cart_id']])->first();
                $total_amount = ($item->price * $item->quantity);
    
                if ($couponDetails->amount_type == "Fixed") {
                    $couponAmount = $couponDetails->amount;
                }else{
                    $couponAmount = $total_amount * ($couponDetails->amount/100);
                }
    
            
                
                //set current discount in cart table
                DB::table('carts')->where('id', $data['cart_id'])->update(['discount'=> $couponAmount]);
                $carts = DB::table('carts')->where('user_email' , Session::get('Email'))->get();
                $total_discount = 0;
                foreach ($carts as $cart){
                    $total_discount = $total_discount + $cart->discount;
                }
                // echo "<pre>"; print_r($cartInfo); die;
  
    
                Session::put('CouponAmount', $couponAmount);
                Session::put('CouponCode', $data['coupon_code']);
                Session::put('TotalDiscount', $total_discount);

                $users = CouponUser::where('coupon_id',1)->where('customer_id',$cid)->first();
                // dd($users);
                $userId = $users->id;
                //  dd($userId);

                $countusers = CouponUser::findOrFail($userId);

                $countusers->count = 1;
                $countusers->save();

    
                return redirect()->back()->with('flash_message_success', 'Coupon successfully updated. You will recieve your discount');
                
            }

        } 
        else{

            return redirect()->back()->with('flash_message_error','You have already used this coupon!!!');
        }
    }
       
     public function productcoupon(Request $request)
     {
           $data = $request->all();

        // dd($data);
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
            return redirect()->route('my.account')->with('flash_message_error', 'Please Login First');
           }
          else
          {
            $data['user_email'] = $userEmail;
          }

      }
       

}


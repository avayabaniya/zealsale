<?php

namespace App\Http\Controllers\backend;

use App\Checkout;
use App\Customer;
use App\Mail\CustomerCancelOrder;
use App\Mail\OrderCanceled;
use App\Mail\OrderComplete;
use App\Mail\OrderOnWay;
use App\Mail\OrderPending;
use App\Mail\OrderPlaced;
use App\Order;
use App\Product;
use App\ProductAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function confirmOrder(Request $request){

        //echo "test";die;

        $user_session_email = Session::get('Email');
        //echo $user_session_email; die;

        $cartNum = DB::table('carts')->where('user_email', $user_session_email)->count();

        //echo $cartNum; die;
        $cartItems = DB::table('carts')->where('user_email', $user_session_email)->get();
        $userDetails = Customer::where('email', $user_session_email)->first();
        $checkout = Session::get('checkout_id');

        $current_checkout = Checkout::latest()->first();

        $orders = array();
        foreach ($cartItems as $cartItem){

            $discount = $cartItem->discount;
            if (empty($cartItem->discount)){
                $discount = 0;
            }

            $order = new Order();
            $order->order_code = rand(999,9999999);
            $order->product_id = $cartItem->product_id;
            $order->customer_id = $userDetails->id;
            $order->checkout_id = $current_checkout->id;
            $order->order_type = 'Cash On Delivery';
            $order->size = $cartItem->size;
            $order->quantity = $cartItem->quantity;
            $order->order_price = ($cartItem->price *  $cartItem->quantity)  - $discount;
            $order->user_email = $user_session_email;
            $order->order_status = 'pending';
            
            $order->save();

            array_push($orders, $order);

            //Deduct stock order is confirmed
            $stock = ProductAttribute::where('product_id', $order->product_id)
                ->where('size', $order->size)
                ->first();

            $stock->stock = $stock->stock - $order->quantity;
            $stock->save();

            DB::table('carts')->where('user_email', $user_session_email)->delete();
        }

        $customer = Auth::guard('customer')->user();
        set_time_limit(60);
        Mail::to($customer)->send(new OrderPlaced($customer, $orders));

        return redirect(route('cart'))->with('flash_message_success','Your order has been placed');
    }

    public function index()
    {
        $pendingOrderCount = Order::where('order_status', 'pending')->count();
        $orders = Order::with('product', 'customer', 'checkout') ->orderBy('id', 'desc')->get();

        // dd($orders);

        return view('backend.order.index')->with(compact('pendingOrderCount', 'orders'));
    }

    public function editOrder(Request $request){

        $data = $request->all();
        //dd($data);
        $id = $data['order_id'];
        $status = $data['status'];


        if ($data['old_status'] === 'cancelled')
        {
            $notCancelledOrder = Order::where('id',$id)->first();
            $stock = ProductAttribute::where('product_id', $notCancelledOrder->product_id)
                ->where('size', $notCancelledOrder->size)
                ->first();
            if ($stock->stock - $notCancelledOrder->quantity > 0) {
                $stock->stock = $stock->stock - $notCancelledOrder->quantity;
            }else{
                return redirect()->back()->with('flash_message_error','Product already out of stock status cannot be changed');
            }
            $stock->save();
        }

        Order::where('id', $id)->update([
            'order_status' => $status
        ]);
        $order = Order::where('id',$id)->first();

        if ($data['status'] == 'cancelled'){
            //Add stock if order is cancelled
            $cancelledOrder = Order::where('id',$id)->first();

            $stock = ProductAttribute::where('product_id', $cancelledOrder->product_id)
                ->where('size', $cancelledOrder->size)
                ->first();
            $stock->stock = $stock->stock + $cancelledOrder->quantity;
            $stock->save();
        }

        $customer = Customer::where('id',$order->customer_id)->first();
        set_time_limit(60);

        if ($data['status'] === 'cancelled')
        {
            Mail::to($customer)->send(new OrderCanceled($customer, $order));
        }
        elseif ($data['status'] == 'pending')
        {
            Mail::to($customer)->send(new OrderPending($customer, $order));
        }
        elseif ($data['status'] == 'on-way')
        {
            Mail::to($customer)->send(new OrderOnWay($customer, $order));

        }
        elseif ($data['status'] == 'completed')
        {
            Mail::to($customer)->send(new OrderComplete($customer, $order));
        }


        return redirect(route('order.index'))->with('flash_message_success', 'Status Updated');
    }

    public function customerCancelOrder($id)
    {
        $order = Order::where('id',$id)->first();
        //dd($order);
        if ($order->order_status === 'pending'){
            $order->order_status = 'cancelled';
            $order->save();

            //Add stock if order is cancelled
            $cancelledOrder = Order::where('id',$id)->first();

            $stock = ProductAttribute::where('product_id', $cancelledOrder->product_id)
                ->where('size', $cancelledOrder->size)
                ->first();
            $stock->stock = $stock->stock + $cancelledOrder->quantity;
            $stock->save();

            $customer = Auth::guard('customer')->user();

            Mail::to('zealsalee@gmail.com')->send(new CustomerCancelOrder($customer, $order));
            Mail::to($customer)->send(new OrderCanceled($customer, $order));


            return redirect()->back()->with('flash_message_success', 'Your order had been successfully cancelled.');
        }
        return redirect()->back()->with('flash_message_error', 'The order cannot be cancelled please contact our office');



    }
}

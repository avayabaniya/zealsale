<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SiteSetting;
use Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Image;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sitesettings = SiteSetting::where(['id'=>$id])->first();
        return view ('backend.setting.edit')->with('sitesettings',$sitesettings);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'organization_name' => 'required|max:100|min:6',
            'email' => 'required|email|max:60|min:6',
            'support_time' => 'required|max:30|min:4',
            'address' => 'required|max:100|min:10',
            'phone' => 'required|max:100|min:8',
        ]);

        $sitesetting = SiteSetting::findOrFail($id);
        
        if ($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                //echo "test";die;
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(111,99999).'.'.$extension;
                $large_image_path = 'public/images/backend_images/settings/large/'.$filename;
                $medium_image_path = 'public/images/backend_images/settings/medium/'.$filename;
                $small_image_path = 'public/images/backend_images/settings/small/'.$filename;

                //Resize Images
                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                Image::make($image_tmp)->resize(250,232)->save($small_image_path);

                //Store image name in products table
                $sitesetting->logo = $filename;
                $unlink = $request->current_image;
            }else{
                $sitesetting->logo = $request->current_image;

            }
        }

        $sitesetting->organization_name = ucwords(strtolower($request->organization_name));
        $sitesetting->email = $request->email;
        $sitesetting->support_time = $request->support_time;
        $sitesetting->phone = $request->phone;
        $sitesetting->address = $request->address;
        $sitesetting->about = $request->about;
        $sitesetting->facebook_uri = $request->facebook;
        $sitesetting->instagram_uri = $request->instagram;
        $sitesetting->linkedin_uri = $request->linkedin;
        $sitesetting->twitter_uri = $request->twitter;
        $sitesetting->youtube_uri = $request->youtube;
        $sitesetting->viber_uri = $request->viber;
        $sitesetting->whatsapp_uri = $request->whatsapp;
        $sitesetting->save();

        Session::flash('flash_message_success', 'Site Settings Updated Successfully ');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

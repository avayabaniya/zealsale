<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FAQController extends Controller
{
    public function index ()
    {
        $faqs = Faq::all();
        return view ('frontend.faq')->with('faqs',$faqs);
    }
}

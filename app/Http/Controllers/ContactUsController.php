<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index ()
    {
        return view ('frontend.contact');
    }

    public function sendContactUsMail(Request $request)
    {
        //dd($request->all());
        if ($request->isMethod('post')){
            Mail::to('zealsalee@gmail.com')->send(new ContactUs($request->all()));
        }
        return view('frontend.contact')->with('flash_message_success', 'We have recieved your email we will contact you shortly');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Wishlist;
use App\Product;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class WishlistController extends Controller
{
    public function index (Request $request)
    {
        if (Auth::guard('customer')->check())
        {
            $customer_id = Auth::guard('customer')->user()->id;
            $wishlists = Wishlist::where('customer_id', $customer_id)->get();

        }else{
            $session_id = Session::get('session_id');
            $wishlists = Wishlist::where('session_id', $session_id)->get();

        }

        return view ('frontend.wishlist',compact('wishlists'));
    }
}

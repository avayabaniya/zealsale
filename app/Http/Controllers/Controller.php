<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Customer;
use App\Wishlist;
use App\Category;
use App\Order;
use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function mainCategories(){
        $mainCategories = Category::where(['parent_id' => 0 ])->get();
        return $mainCategories;
    }

    public static function categories(){
        $categories = Category::all();
        return $categories;
    }

    public static function featuredProducts(){
        $featuredProducts = Product::latest()->get();
        return $featuredProducts;
    }

    public static function cartCount(){
        //$cartCount = DB::table('carts')->where('user_email', Session::get('Email'))->count();
        //$session_id = Session::get('session_id');

        $user_email = Session::get('Email');
        $session_id = Session::get('session_id');

        if (empty($user_email)){
            $cartCount = DB::table('carts')->where('session_id', session_id())->count();
        }else{
            $cartCount = DB::table('carts')->where('user_email', $user_email)->count();
        }

        if ($cartCount == 0){
            $cartCount = null;
            return $cartCount;
        }
        return $cartCount;
    }

    public static function navCart()
    {
        $user_email = Session::get('Email');
        $session_id = Session::get('session_id');

        if (empty($user_email)){
            $navCart = Cart::where('session_id',$session_id)->get();
        }else{
            $navCart = Cart::where('user_email', $user_email)->get();
        }

        return $navCart;
    }

    public static function totalCartAmount(){
        $user_email = Session::get('Email');
        $session_id = Session::get('session_id');

        if (empty($user_email)){
            $totalPrice = 0;
            $totalDiscount = 0;
            $cartProducts = Cart::with('product')->where('session_id', $session_id)->get();
            foreach ($cartProducts as $price){
                $totalPrice = ($totalPrice +  ($price->quantity * $price->price));
                $totalDiscount = $totalDiscount + $price->discount;
            }
        }else{
            $totalPrice = 0;
            $totalDiscount = 0;
            $cartProducts = Cart::with('product')->where('user_email', $user_email)->get();
            foreach ($cartProducts as $price){
                $totalPrice = ($totalPrice +  ($price->quantity * $price->price));
                $totalDiscount = $totalDiscount + $price->discount;
            }
        }

        return $totalPrice - $totalDiscount;
    }

    public static function pendingOrders(){

        $pendingOrders = Order::where('order_status', 'pending')->count();
        return $pendingOrders;
    }

    public static function wishlistCount(){


        $email = Session::get('Email');
        $customer_id = Customer::where('email', $email)->first();
        //dd($customer_id->id);
        $session_id = Session::get('session_id');

        if (empty($customer_id)){
            $wishlistCount = DB::table('wishlists')->where('session_id', session_id())->count();
        }else{
            $wishlistCount = DB::table('wishlists')->where('customer_id', $customer_id->id)->count();
           // dd($wishlistCount);
        }

        if ($wishlistCount == 0){
            $wishlistCount = null;
            return $wishlistCount;
        }
        return $wishlistCount;
    }


}

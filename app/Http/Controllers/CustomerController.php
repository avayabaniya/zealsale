<?php

namespace App\Http\Controllers;

use App\Mail\CustomerRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Order;
use App\CouponUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    public function _construct()
    {
        $this->middleware('guest:customer');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login (Request $request)
    {
        if($request->isMethod('post')) {
            $data = $request->input();
            $session_id = Session::get('session_id');

            
            
            if(Auth::guard('customer')->attempt(['email'=>$data['email'], 'password' => $data['password']]))
            {
                Session::put('Email', $data['email']);
                //checks if a new product is added to cart before login
                if (!empty($session_id)){
                    return redirect(route('cart.update'));
                }

                return redirect()->route('customer.dashboard');
            }
            else
            {
                return redirect()->back()->with('flash_message_error','These credentials do not match our records');
            }
        }
        return view('frontend.account.my_account');
    }
    
    public function dashboard()
    {
        $customer = Auth::guard('customer')->user();
        $orders = Order::where('customer_id', $customer->id)->latest()->get();
        //dd($orders);
        return view ('frontend.account.dashboard', compact('orders', 'customer'));
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('customer')->logout();
        return redirect()->route('index')->with('flash_message_success','Logged Out Successfully');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'full_name' => 'required|max:50|min:06',
            'email' => 'required|unique:customers|max:50|min:6',
            'password' => 'required|nullable|regex:/(?=.*?[a-z])(?=.*?[0-9]).{6,}/',
        ],[
            'password.regex' => 'You Need to have atleaset 1 Number and letters',
        ]);
        
        $data = $request->all();
        // dd($data);

        $customer = new Customer;
        $customer->full_name =  ucwords(strtolower($data['full_name']));
        $customer->slug = str_slug($data['full_name'],'-').str_random(10);
        $customer->email = $data['email'];
        $customer->password = Hash::make($data['password']);
        $customer->save();

        
        $couponuser = new CouponUser;
        $couponuser->customer_id = $customer->id;
        $couponuser->coupon_id = 1;
        $couponuser->count= 0;
        $couponuser->save();

        
         $data = $request->input();
         $session_id = Session::get('session_id');

        if (Auth::guard('customer')->attempt(['email'=>$data['email'], 'password'=>$data['password']])){
            Session::put('Email', $data['email']);

            // $customer = Auth::guard('customer')->user();
            // Mail::to($customer)->send(new CustomerRegister($customer));
       
          
            //checks if a new product is added to cart before login
            if (!empty($session_id)){
                return redirect(route('cart.update'));
            }

            return redirect(route('cart'))->with('flash_message_success' , "Your Account has been created successfully");
        }


        Session::flash('flash_message_success', 'Your Account Has Been Created Successfully ');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('post')){

            if (!empty($request->oldPassword)){

                $this->validate($request,
                    ['newPassword' => 'required',
                        'confirmPassword' => 'required']);

                $data = $request->all();

                $check_password = Customer::where(['email' => Auth::guard('customer')->user()->email])->first();

                $current_password = $data['oldPassword'];
                if(Hash::check($current_password,$check_password->password))
                {
                    $password = bcrypt($data['newPassword']);
                    Customer::where('email',Auth::guard('customer')->user()->email)->update(['password'=>$password]);
                    return redirect()->back()->with('flash_message_success','Password updated Successfully!');
                }
                else {
                    return redirect()->back()->with('flash_message_error','Incorrect Current Password!');
                }
            }

            $customerDetails = Customer::where('id',$id)->first();
            $customerDetails->full_name = $request->full_name;
            $customerDetails->email = $request->email;
            $customerDetails->phone = $request->phone;
            $customerDetails->address = $request->address;
            $customerDetails->save();
        }
        //return redirect(route('index'))->with('flash_message_success','Personal details updated successfullt');
        return redirect()->back()->with('flash_message_success','Personal details updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function chkUserPassword(Request $request){

        $data = $request->all();

        $current_password = $data['current_pwd'];
        $user_id = Auth::guard('customer')->User()->id;
        $check_password = Customer::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }
}

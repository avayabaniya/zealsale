<?php

namespace App\Http\Controllers;

use Auth;
use App\Cart;
use App\Coupon;
use App\Product;
use App\Helper\MoneyFormat;
use App\ProductAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index ()
    {
        $session_id = Session::get('session_id');
        $user_email = Session::get('Email');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }

        if (empty($user_email)) {
            $cartProducts = Cart::with('product')->where('session_id', $session_id)->get();
        }else {
            $cartProducts = Cart::with('product')->where('user_email', $user_email)->get();
        }

        
//
        $totalPrice = 0;
        $totalDiscount = 0;
        foreach ($cartProducts as $price){
            $totalPrice = $totalPrice +  ($price->quantity * $price->price);
            $totalDiscount = $totalDiscount + ($price->discount);
        }

        $totalPrice = $totalPrice - $totalDiscount;

        return view ('frontend.cart')->with(compact('cartProducts','totalPrice'));
    }

    public function addToCart(Request $request)
    {
        //Session::forget('CouponAmount');
        //Session::forget('CouponCode');

        $data = $request->all();
        $customer = Auth::guard('customer')->user();

        //user email session
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
        }else{
            $data['user_email'] = $userEmail;
        }

        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }

        $sizeArr = explode("-", $data['size']);

        //When no size is selected
        if (empty($sizeArr[0])){
            return redirect()->back()->with('flash_message_error', 'Please select your size');
        }

        //check stock quantity
        $checkStock = ProductAttribute::where('product_id', $data['product_id'])
            ->where('size', $sizeArr[1])
            ->first();

        if ($checkStock->stock < $data['quantity']){
            return redirect()->back()->with('flash_message_error', 'Required product quantity not available in stock');
        }

        //Checking for duplicate products
        $countProduct = DB::table('carts')->where([
            'product_id' => $data['product_id'],
            'product_color' => $data['product_color'],
            'size' => $sizeArr[1],
            'session_id' => $session_id
        ])->count();

        if ($countProduct > 0) {
            return redirect()->back()->with('flash_message_error', 'Product already exists in Cart!');
        } else {

            $getSKU = ProductAttribute::select('sku')->where(['product_id'=>$data['product_id'], 'size'=>$sizeArr[1]])->first();

            //Product code if no size is selected
            if (empty($getSKU)) {
                $pcode = $data['product_code'];
            } else {
                $pcode = $getSKU->sku;
            }

            //get onsale value if a product is on sale
            $onSale = Product::where('id', $data['product_id'])->first();
            //dd($onSale->onsale);
            if (empty($onSale->onsale)){
                $onSaleDiscount = 0;
            }else{
                $onSaleDiscount = ($onSale->onsale/100) *  $data['price'];
                $data['price'] = $data['price'] - $onSaleDiscount;
            }

            DB::table('carts')->insert([
                'product_id' => $data['product_id'],
                'product_name'=>$data['product_name'],
                'product_code' => $pcode,
                'product_color' => $data['product_color'],
                'price' => $data['price'],
                'size' => $sizeArr[1],
                'quantity' => $data['quantity'],
                'user_email' => $data['user_email'],
                'session_id' => $session_id
            ]);

        }

        //Deduct 1 stock when added to cart
       /* $stock = ProductAttribute::where('product_id', $data['product_id'])
            ->where('size', $sizeArr[1])
            ->first();

        $stock->stock = $stock->stock - $data['quantity'];
        $stock->save();*/

        return redirect(route('cart'))->with('flash_message_success', 'Product has been added in Cart!');
    }

    public function updateCartQuantity( $id = null, $quantity = null)
    {
        //Get detail from cart table when the id matches
        $getCartDetails = DB::table('carts')->where('id', $id)->first();
        //Get details from productAttributes table when the product_code matches the product_code of the item in cart table
        $getAttributeStock = ProductAttribute::where('sku', $getCartDetails->product_code)->first();
        //Get quantity from cart table then add 1 or -1 to it
        $updated_quantity = $getCartDetails->quantity + $quantity;

        //return $updated_quantity;

        if ($quantity > 0){
            if ($getAttributeStock->stock >= $updated_quantity ){
                DB::table('carts')->where('id', $id)->increment('quantity', $quantity);

                //Deduct or add  stock when cart quantity updated
                //$getAttributeStock->stock = $getAttributeStock->stock - ($quantity);
                //$getAttributeStock->save();

                $changeCart = Cart::where('id',$id)->first();

                //total cart price after quantity change
                $totalPrice = 0;
                if (empty(Session::get('Email'))){
                    $cartProducts = Cart::with('product')->where('session_id', $changeCart->session_id)->get();
                }else{
                    $cartProducts = Cart::with('product')->where('user_email', $changeCart->user_email)->get();
                }
                foreach ($cartProducts as $price){
                    $totalPrice = $totalPrice +  ($price->quantity * $price->price);
                }


                //When quantity is changed calculate new discount amount and update discount field in cart table
                $check_discount_cart = Cart::where('id',$id)->first();
                if (!empty($check_discount_cart->discount)) {
                    $coupon_code = Session::get('CouponCode');
                    $couponDetails = Coupon::where('coupon_code', $coupon_code)->first();

                    Session::put('CartId', $id);
                    $item = DB::table('carts')->where(['id' => $id])->first();
                    $total_amount = ($item->price * $item->quantity);

                    if ($couponDetails->amount_type == "Fixed") {
                        $couponAmount = $couponDetails->amount;
                    } else {
                        $couponAmount = $total_amount * ($couponDetails->amount / 100);
                    }

                    DB::table('carts')->where('id', $id)->update(['discount' => $couponAmount]);

                    if (empty(Session::get('Email'))) {
                        $carts = DB::table('carts')->where('session_id', Session::get('session_id'))->get();
                    }else {
                        $carts = DB::table('carts')->where('user_email', Session::get('Email'))->get();
                    }
                    $total_discount = 0;
                    foreach ($carts as $cart) {
                        $total_discount = $total_discount + $cart->discount;
                    }


                    Session::put('CouponAmount', $couponAmount);
                    Session::put('CouponCode', $coupon_code);
                    Session::put('TotalDiscount', $total_discount);
                    //end calculation
                }

                $changeCart = Cart::where('id',$id)->first();

                if (empty($total_discount)){
                    $total_discount = 0;
                }
                $totalPrice = $totalPrice - $total_discount;

                return [$changeCart,$totalPrice];
            }else{
                //0 = quantity not available to be checked in ajax part
                return 0;

            }
        }elseif ($quantity < 0)
        {
            if ($updated_quantity != 0){
                DB::table('carts')->where('id', $id)->increment('quantity', $quantity);

                //Deduct or add  stock when cart quantity updated
                //$getAttributeStock->stock = $getAttributeStock->stock - ($quantity);
                //$getAttributeStock->save();

                $changeCart = Cart::where('id',$id)->first();

                //total cart price after quantity change
                $totalPrice = 0;
                if (empty(Session::get('Email'))){
                    $cartProducts = Cart::with('product')->where('session_id', $changeCart->session_id)->get();
                }else{
                    $cartProducts = Cart::with('product')->where('user_email', $changeCart->user_email)->get();
                }
                foreach ($cartProducts as $price){
                    $totalPrice = $totalPrice +  ($price->quantity * $price->price);
                }

                //When quantity is changed calculate new discount amount and update discount field in cart table
                $check_discount_cart = Cart::where('id',$id)->first();
                if (!empty($check_discount_cart->discount)) {
                    $coupon_code = Session::get('CouponCode');
                    $couponDetails = Coupon::where('coupon_code', $coupon_code)->first();

                    Session::put('CartId', $id);
                    $item = DB::table('carts')->where(['id' => $id])->first();
                    $total_amount = ($item->price * $item->quantity);

                    if ($couponDetails->amount_type == "Fixed") {
                        $couponAmount = $couponDetails->amount;
                    } else {
                        $couponAmount = $total_amount * ($couponDetails->amount / 100);
                    }

                    //$totalPrice = $totalPrice - $couponAmount;

                    DB::table('carts')->where('id', $id)->update(['discount' => $couponAmount]);


                    if (empty(Session::get('Email'))) {
                        $carts = DB::table('carts')->where('session_id', Session::get('session_id'))->get();
                    }else {
                        $carts = DB::table('carts')->where('user_email', Session::get('Email'))->get();
                    }
                    $total_discount = 0;
                    foreach ($carts as $cart) {
                        $total_discount = $total_discount + $cart->discount;
                    }

                    Session::put('CouponAmount', $couponAmount);
                    Session::put('CouponCode', $coupon_code);
                    Session::put('TotalDiscount', $total_discount);
                    //end calculation
                }

                $changeCart = Cart::where('id',$id)->first();

                if (empty($total_discount)){
                    $total_discount = 0;
                }
                $totalPrice = $totalPrice - $total_discount;

                return [$changeCart, $totalPrice];
            }else{
                //0 = quantity not available to be xhecked in ajax part
                return 0;
            }
        }
    }

    public function updateCart(){

        $session_user_email = Session::get('Email');
        $session_id = Session::get('session_id');

        $newCartDetails = DB::table('carts')->where('session_id', $session_id)->get();

        foreach ($newCartDetails as $cart){

            //Checking fot duplicate products
            $countProduct = DB::table('carts')
                ->where(['product_id' => $cart->product_id])
                ->where(['product_color' => $cart->product_color])
                ->where(['size' => $cart->size])
                ->where(['user_email' => $session_user_email])
                ->count();

            if ($countProduct > 0 ) {
                return redirect(route('cart'))->with('flash_message_error', 'The product you selected already exists in your cart');
            }

            DB::table('carts')->where('id', $cart->id)->update(['user_email' => $session_user_email]);
        }

        return redirect(route('cart'));
    }

    public  function deleteCartProduct($id = null){

        //Session::forget('CouponAmount');
        //Session::forget('CouponCode');

        //Adding to stock when product from cart is deleted
        $cartInfo = DB::table('carts')->where('id', $id)->first();
        $stock = ProductAttribute::where('product_id', $cartInfo->product_id)
            ->where('size', $cartInfo->size)
            ->first();
        $stock->stock = $stock->stock + $cartInfo->quantity;
        $stock->save();

        DB::table('carts')->where('id', $id)->delete();



        return redirect(route('cart'))->with('flash_message_success', 'Product has been deleted from Cart!');
    }

}

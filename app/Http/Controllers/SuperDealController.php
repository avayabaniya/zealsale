<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Advertisement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuperDealController extends Controller
{
    public function index ()
    {
        $advertisements = Advertisement::inRandomOrder()->take(1)->get();
        $featuredProduct = Product::with('category')->whereOnsale(NULL)->inRandomOrder()->take(12)->whereStatus(1)->get();
        $onsaleProduct = Product::with('category')->where('onsale','!=', null)->inRandomOrder()->take(12)->whereStatus(1)->get();
        $mainCategories = Category::whereParentId(0)->inRandomOrder()->take(1)->get();
        $nextMainCategories = Category::whereParentId(0)->inRandomOrder()->take(1)->get();
        $subCategories = Category::where('parent_id','!=', 0)->inRandomOrder()->get();
        $products = Product::with('category')->inRandomOrder()->whereStatus(1)->get();
        $bestSellers = Product::with('category')->take(6)->inRandomOrder()->whereStatus(1)->get();
        $bestSellerSingle = Product::with('category')->take(1)->inRandomOrder()->whereStatus(1)->get();
        $cat = Category::whereParentId(0)->inRandomOrder()->take(6)->get();
        $subcat = Category::where('parent_id' ,'!=' , 0)->inRandomOrder()->get();
        $productsFeatured = Product::with('category')->where('featured', 1)->inRandomOrder()->take(6)->whereStatus(1)->get();

        return view ('frontend.super_deals',compact('featuredProduct','productsFeatured','onsaleProduct','nextMainCategories','mainCategories','subCategories','products','bestSellers','bestSellerSingle','cat','subcat','advertisements'));
    }
}

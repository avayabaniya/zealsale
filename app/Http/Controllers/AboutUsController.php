<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;

class AboutUsController extends Controller
{
    public function index ()
    {
        $about = About::where('id',1)->first();
        return view ('frontend.about',compact('about'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Checkout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkout()
    {
        //prevent customer from going tocheckout page

        $customer = Auth::guard('customer')->user();

        $user_email = Session::get('Email');
        if (empty($user_email)){
            return redirect(route('customer.login'))->with('flash_message_error', 'Please register to checkout');
            //return view('backend.account.my_account')->with('flash_message_error', 'Please register to checkout');
        }else{
            $cartCount = Cart::where('user_email', $user_email)->count();
            if ($cartCount == 0){
                return redirect(route('cart'))->with('flash_message_error', 'Your cart is empty');
            }
        }

        $cartProducts = Cart::with('product')->where('user_email', $user_email)->get();

        $totalPrice = 0;
        foreach ($cartProducts as $price){
            $totalPrice = $totalPrice +  ($price->quantity * $price->price);
        }

        return view ('frontend.checkout')->with(compact('cartProducts', 'totalPrice','customer'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,
            ['phone' => 'required',
                'delivery_address' => 'required',
                'terms' => 'required']);

        $customer = Customer::where('email', Session::get('Email'))->first();

        $checkout = Checkout::create([
            'phone' =>  $request->phone,
            'delivery_address' => $request->delivery_address,
            'order_notes' => $request->order_notes,
            'payment_type' => $request->payment_method,
            'customer_id' => $customer->id
        ]);


        /*Session::put($current_checkout->id, 'checkout_id');
        dd($current_checkout);*/

        //return redirect()->back();
        return redirect(route('order.confirm'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Advertisement;

class ProductCategoryController extends Controller
{
    public function index ($slug)
    {
        $mainCategory = Category::where('slug',$slug)->first();
        $categories = Category::all();
        $products = Product::whereStatus(1)->get();
        return view ('frontend.product_category',compact('categories','mainCategory','products'));
    }
    
    public function subcategory ($slug)
    {
        $advertisements = Advertisement::inRandomOrder()->take(1)->get();
        $categoryDetails = Category::where('slug', $slug)->first();
        $subcategories = Category::where('slug',$slug)->first();
        $products = Product::whereStatus(1)->get();
        return view ('frontend.product_subcategory',compact('subcategories','products','categoryDetails','advertisements'));
    }
}

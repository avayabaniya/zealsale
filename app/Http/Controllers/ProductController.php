<?php

namespace App\Http\Controllers;

use App\Customer;
use Auth;
use PhpParser\Error;
use Psy\Exception\ErrorException;
use Session;
use App\Category;
use App\Product;
use App\ProductAttribute;
use App\ProductRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ProductController extends Controller
{
    public function productsingle($slug = null)
    {
        try{
            $products = Product::with('category')->inRandomOrder()->latest()->take(5)->whereStatus(1)->get();
            $productDetails = Product::with('category','attributes', 'specifications', 'images','rating')->where('slug', $slug)->first();
            $productMainCategories = Category::where('id', $productDetails->category->parent_id )->first();

            return view ('frontend.productsingle')->with(compact('productDetails', 'productMainCategories','products','ratings','productRating'));

        }catch (Exception $e){
            abort(404);
        }

    }
}

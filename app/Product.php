<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;

class Product extends Model
{
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'category_id',
        'product_name',
        'slug',
        'product_code',
        'product_color',
        'description',
        'price',
        'image',
        'status',
        'meta_title',
        'meta_description',
        'meta_content',
        'meta_keywords',
    ];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function attributes() {
        return $this->hasMany('App\ProductAttribute','product_id');
    }
    
    public function specifications() {
        return $this->hasMany('App\ProductSpecification','product_id')->orderBy('title', 'asc');
    }
    public function rating ()
    {
        return $this->hasMany('App\ProductRating','product_id');
    }

    public function images() {
        return $this->hasMany('App\ProductImage','product_id');
    }

    public function cart() {
        return $this->hasMany('App\Cart');
    }

    public function order() {
        return $this->hasMany('App\Order');
   
    }

    public function product(){

        return $this->belongsTo('App\Coupon');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Advertisement extends Model
{
    protected $fillable = [
        'id','image','category_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
}

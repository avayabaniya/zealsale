<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        'id',
        'avatar',
        'address',
        'phone',
        'user_id',
        'about',
    ];
}
